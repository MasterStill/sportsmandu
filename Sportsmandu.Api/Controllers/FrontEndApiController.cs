﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Elasticsearch.Net;
using HtmlAgilityPack;
using Mapster;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Caching.Memory;
using Nest;
using Newtonsoft.Json;
using Sportsmandu.Model.Club.ViewModels;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Entities.Ground;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Models;
using Sportsmandu.Models.Association;
using Sportsmandu.Models.Generic;
using Sportsmandu.Models.League;
using Sportsmandu.Models.Stats;
using Sportsmandu.Models.Website;
using static Sportsmandu.Models.Telegram.QuestionAnswer.FootballQuestions;
namespace Sportsmandu.Api.Controllers
{
    [Route("{culture}/api")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private static ElasticClient _elasticClient;
        private static string _baseUrl = "https://localhost:44373/";
        //private const string ConnectionString = "http://localhost:9200/";
        private const string ConnectionString = "https://4b482530a13b47e79872db503e31aefb.asia-northeast1.gcp.cloud.es.io:9243";
        //private static readonly HttpClient client = new HttpClient();        
        private static System.Uri[] nodes = new Uri[]
                {
                new Uri(ConnectionString
                //new Uri("http://myserver2:9200"),
                //new Uri("http://myserver3:9200")
                )};

        static StaticConnectionPool pool = new StaticConnectionPool(nodes);
        static ConnectionSettings settings = new ConnectionSettings(pool);            
        public ProductController()
        {
            SetUpElasticClient();
            //settings.DisableDirectStreaming(true);
            //settings.BasicAuthentication("elastic", "DjngpWIIRWzrMczGPU1w0LVL");
            //_elasticClient = new ElasticClient(settings);            
        }
        static void SetUpElasticClient()
        {
            try
            {
            if(_elasticClient == null)
            {
                    string devUrl = "http://localhost:9200";
                    var server = new Uri(devUrl);//DevMachine ? devUrl : liveUrl);
                    var conn = new ConnectionSettings(
                         server
                    );                    
                    _elasticClient = new ElasticClient(conn);
                    
                //    settings.DisableDirectStreaming(true);
                //settings.BasicAuthentication("elastic", "XMgsuhniaoOWzWMksUwadsbE");
                //_elasticClient = new ElasticClient(settings);
                }
            }
                catch(Exception){
                 
                }
        }
        [HttpGet]
        [Route("SiteSectionLanguageTitle")]
        public static List<SiteSectionLanguageTitleForEs> SiteSectionLanguageTitle(string culture,string Section,IMemoryCache cache)
        {
            List<SiteSectionLanguageTitleForEs> siteSectionLanguageTitle = new List<SiteSectionLanguageTitleForEs>();
            if (!cache.TryGetValue(Section + culture, out siteSectionLanguageTitle))
            {
                //HtmlWeb web = new HtmlWeb();
                //HtmlDocument document = web.Load($"{_baseUrl}GetAllSiteSectionLanguageTitleToSaveInEs");
                //var msg = document.Text;
                //if(msg.Length > 0)
                //{
                //siteSectionLanguageTitle = JsonConvert.DeserializeObject<List<SiteSectionLanguageTitleForEs>>(msg);                
                SetUpElasticClient();
                if (_elasticClient == null) return siteSectionLanguageTitle;
                var searchResponse = _elasticClient.Search<SiteSectionLanguageTitleForEs>(s => s
                   .Index(enumElasticIndexes.sportsmandusitesectionlanguagetitle.ToString())
                   .Take(1000)
                   //TODO Yield Wrong Results
                   //.Query(q =>
                   //   q.MatchPhrase(m => m.Field(x => x.SiteSectionName).Query(Section)) &&
                   //    q.MatchPhrase(m => m.Field(x => x.LanguageTitleGlobalizations.Select(y => y.CultureCode)).Query(culture))
                   //   )
                   );
                var returnUser = searchResponse.Documents;
                if (returnUser.Count > 0)
                {
                    siteSectionLanguageTitle = returnUser.ToList();
                    cache.Set(Section + culture, siteSectionLanguageTitle, new MemoryCacheEntryOptions()
                        .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
                }
            //}
            }            
            return siteSectionLanguageTitle == null ? new List<SiteSectionLanguageTitleForEs>() : siteSectionLanguageTitle;
        }

        [HttpGet]
        [Route("GetAllSiteSectionLanguageTitle")]
        public static IEnumerable<SiteSectionLanguageTitleForEs> GetAllSiteSectionLanguageTitle()
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<SiteSectionLanguageTitleForEs>(s => s
               .Take(1000)
               .Index(enumElasticIndexes.sportsmandusitesectionlanguagetitle.ToString())
               //.Type(enumElasticIndexes.sportsmandusitesectionlanguagetitle.ToString())
               //.Query(q =>
               //   q.MatchPhrase(m => m.Field(x => x.SiteSectionName).Query(Section)) &&
               //    q.MatchPhrase(m => m.Field(x => x.LanguageTitleGlobalizations.Select(y => y.CultureCode)).Query(culture))
               //   )
               );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.ToList();
            return null;
        }

        [HttpGet]
        [Route("Players")]
        public static IEnumerable<PlayerViewModel> GetPlayersForHomePage(string culture,IMemoryCache cache)
        {

            
            List<PlayerViewModel> Players = new List<PlayerViewModel>();
            if (!cache.TryGetValue("players" + culture, out Players))
            {
                string filterPlayer = "Paras";//culture.ToLower() == "en" ? TopPlayersNameEN : TopPlayersNameNp;
                SetUpElasticClient();
                var searchResponse = _elasticClient.Search<PlayerViewModel>(s => s
                   .Index(enumElasticIndexes.sportsmanduplayers.ToString())
                   //.Type(enumElasticIndexes.sportsmanduplayers.ToString())
                   .Query(q =>
                       q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(culture)) &&
                       q.Match(m => m.Field(x => x.Name).Query(filterPlayer))
                    )
                   .Take(500)
               );
                var returnUser = searchResponse.Documents;
                if (returnUser.Count > 0)
                {
                    Players = returnUser.ToList().OrderBy(r => Guid.NewGuid()).Where(x => x.Image != null).Take(20).ToList();
                    cache.Set("players" + culture, Players, new MemoryCacheEntryOptions()
                       .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
                }
            }                      
            return Players;
        }

        public static List<PlayerViewModel> GetCurrentRunningSeason(string culture, IMemoryCache cache,int count)
        {
            List<PlayerViewModel> Players = new List<PlayerViewModel>();
            var allMatches = AllSeasonMatch(culture, cache)
                .Where(x => x.Status == enumFixtureStatus.Not_Started.ToString())
                .OrderBy(x => x.Date)
                .Take(10);
                //.FirstOrDefault();
                foreach (var teams in allMatches)
                {
                    foreach (var players in teams.Teams.Where((x=>x.Players.Count > 0)))
                    {
                        Players.AddRange(players.Players.Adapt<List<PlayerViewModel>>());
                    }
                }
                return Players.OrderBy(r => Guid.NewGuid()).Take(count).ToList();
        }
        public static IEnumerable<AssociationVMFORES> GetAssociations(string culture)
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<AssociationVMFORES>(s => s
               .Index(enumElasticIndexes.sportsmanduassociation.ToString())
               //.Type(enumElasticIndexes.sportsmanduassociation.ToString())
               .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(culture))
                   //&&
                   //q.Match(m => m.Field(x => x.Name).Query(filterPlayer))
                )
               .Take(500)
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.ToList().OrderBy(r => Guid.NewGuid()).Where(x => x.Image != null).Take(4).ToList();
            return null;
        }

        [HttpGet]
        [Route("GetAllVideosFORES")]
        public static List<VideoVMForEs> GetVideos(string Culture,IMemoryCache cache)
        {
            List<VideoVMForEs> videos = new List<VideoVMForEs>();
            if (!cache.TryGetValue("videos" + Culture, out videos))
            {
                SetUpElasticClient();
            var searchResponse = _elasticClient.Search<VideoVMForEs>(s => s
               .Index(enumElasticIndexes.sportsmanduvideos.ToString())
               .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(Culture)
                  )
               )
               .Take(500)
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                videos  = returnUser.ToList().OrderBy(r => Guid.NewGuid()).Where(x => x.Image != null).Take(20).ToList();
                cache.Set("videos" + Culture, videos, new MemoryCacheEntryOptions()
                           .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
            }
            return videos;
        }

        public static VideoVMForEs GetVideo(string Culture, IMemoryCache cache,string Id)
        {
            VideoVMForEs video = new VideoVMForEs();
            if (!cache.TryGetValue("videos" + Culture, out video))
            {
                SetUpElasticClient();
                var searchResponse = _elasticClient.Search<VideoVMForEs>(s => s
                    .Index(enumElasticIndexes.sportsmanduvideos.ToString())
                    .Query(q =>
                        q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(Culture)
                        )
                        &&
                        q.MatchPhrase(m => m.Field(x => x.Guid).Query(Id)
                    ))
                    .Take(500)
                );
                var returnUser = searchResponse.Documents;
                if (returnUser.Count > 0)
                    video = returnUser.SingleOrDefault();
                cache.Set("videos" + Culture, video, new MemoryCacheEntryOptions()
                    .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
            }
            return video;
        }

        [HttpGet]
        [Route("BlogPosts")]
        public static IEnumerable<BlogPostVMFORES> GetBlogPosts(string culture)
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<BlogPostVMFORES>(s => s
               .Index(enumElasticIndexes.sportsmandublogposts.ToString())
               //.Type(enumElasticIndexes.sportsmandublogposts.ToString())
               .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(culture)
                  )
               )
               .Take(500)
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.ToList().OrderBy(r => Guid.NewGuid()).Take(20);
            return null;
        }


        [HttpGet]
        [Route("GetGrounds")]
        public static List<GroundVMFORES> GetGrounds(string Culture,IMemoryCache cache)
        {
            List<GroundVMFORES> grounds = new List<GroundVMFORES>();
            if (!cache.TryGetValue("grounds" + Culture, out grounds))
            {
                SetUpElasticClient();
            var searchResponse = _elasticClient.Search<GroundVMFORES>(s => s
               .Index(enumElasticIndexes.sportsmandugrounds.ToString())               
               .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(Culture)
                  )
               )
               .Take(500)
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                grounds= returnUser.OrderBy(r => Guid.NewGuid()).Take(20).ToList();
                cache.Set("grounds" + Culture, grounds, new MemoryCacheEntryOptions()
                       .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
            }
            return grounds;
        }
        [HttpGet]
        [Route("GetVenues")]
        public static IEnumerable<GroundVMFORES> GetVenues(string culture)
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<GroundVMFORES>(s => s
               .Index(enumElasticIndexes.sportsmandugrounds.ToString())
               //.Type(enumElasticIndexes.sportsmandugrounds.ToString())
               .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(culture)) &&
                   q.MatchPhrase(x => x.Field(y=>y.Type == enumVenueType.Venue)
                  )
               )
               .Take(500)
           );
            var returnUser = searchResponse.Documents.Where(x=>x.Type == enumVenueType.Venue).ToList();
            if (returnUser.Count > 0)
                return returnUser.ToList().OrderBy(r => Guid.NewGuid()).Take(20);
            return new List<GroundVMFORES>();
        }

        [HttpGet]
        [Route("BlogPost/{BlogId}")]
        public static BlogPostVMFORES GetBlogPostDetails(string culture, string BlogId)
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<BlogPostVMFORES>(s => s
               .Index(enumElasticIndexes.sportsmandublogposts.ToString())
               //.Type(enumElasticIndexes.sportsmandublogposts.ToString())
              .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(culture)) &&
                   q.MatchPhrase(m => m.Field(x => x.Guid).Query(BlogId))
                )
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.SingleOrDefault();
            return null;
        }

        [HttpGet]
        [Route("Season/{SeasonId}")]
        public static SeasonVMFORES GetSeasonDetails(string culture, string SeasonId)
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<SeasonVMFORES>(s => s
               .Index(enumElasticIndexes.sportsmanduseasons.ToString())
               //.Type(enumElasticIndexes.sportsmanduseasons.ToString())
              .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(culture)) &&
                   q.MatchPhrase(m => m.Field(x => x.Guid).Query(SeasonId))
                )
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.SingleOrDefault();
            return null;
        }

        //[HttpGet]
        //[Route("Season/{SeasonId}")]
        public static List<SeasonStandingVMFORES> GetSeasonStandings(string culture, string SeasonId)
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<SeasonStandingVMFORES>(s => s
               .Index(enumElasticIndexes.sportsmanduseasonstandings.ToString())
              //.Type(enumElasticIndexes.sportsmanduseasons.ToString())
              .Query(q =>
                   //q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(culture)) 
                   //&&
                   q.MatchPhrase(m => m.Field(x => x.Season.Guid).Query(SeasonId))
                )
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.ToList();
            return new List<SeasonStandingVMFORES>();
        }

        //[HttpGet]
        //[Route("Season/{SeasonId}")]
        public static List<SeasonStatsVMFORES> GetSeasonStats(string culture, string SeasonGuid)
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<SeasonStatsVMFORES>(s => s
               .Index(enumElasticIndexes.sportsmanduseasonstats.ToString())
              //.Type(enumElasticIndexes.sportsmanduseasons.ToString())
              .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(culture)) 
                   &&
                   q.Match(m => m.Field(x => x.SeasonGuid).Query(SeasonGuid))
                )
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.ToList();
            return new List<SeasonStatsVMFORES>();
        }

        [HttpGet]
        [Route("Players/{PlayerId}")]
        public static PlayerViewModelEs GetPlayerDetails(string culture,string PlayerId)
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<PlayerViewModelEs>(s => s
               .Index(enumElasticIndexes.sportsmanduplayers.ToString())
               //.Type(enumElasticIndexes.sportsmanduplayers.ToString())
              .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(culture)) &&
                   q.MatchPhrase(m => m.Field(x => x.Guid).Query(PlayerId))
                )
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.SingleOrDefault(); 
            return null;
        }

        [HttpGet] 
        [Route("Ground/{GroundId}")]
        public static GroundVMFORES GetGroundDetails(string culture, string GroundId)
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<GroundVMFORES>(s => s
               .Index(enumElasticIndexes.sportsmandugrounds.ToString())
               //.Type(enumElasticIndexes.sportsmandugrounds.ToString())
              .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(culture)) &&
                   q.MatchPhrase(m => m.Field(x => x.Guid).Query(GroundId))
                )
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.SingleOrDefault();
            return null;
        }
       
        [HttpGet]
        [Route("Association/{AssociationId}")]
        public static AssociationVMFORES GetAssociationDetails(string culture, string AssociationId)
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<AssociationVMFORES>(s => s
               .Index(enumElasticIndexes.sportsmanduassociation.ToString())
               //.Type(enumElasticIndexes.sportsmanduassociation.ToString())
              .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(culture)) &&
                   q.MatchPhrase(m => m.Field(x => x.Guid).Query(AssociationId))
                )
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.SingleOrDefault();
            return new AssociationVMFORES();
        }

        [HttpGet]
        [Route("Club/{ClubId}")]
        //public ClubViewModelFORES GetClubDetail(string culture, string clubId) // IF VUEJS ENABLED
        public static ClubViewModelFORES GetClubDetail(string culture, string guid)
        {
            Debug.WriteLine("Fetching Details For " + guid);
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<ClubViewModelFORES>(s => s
               .Index(enumElasticIndexes.sportsmanduclubs.ToString())
               //.Type(enumElasticIndexes.sportsmanduclubs.ToString())
              .Query(q =>
                    q.Match(m => m.Field(x => x.Guid).Query(guid))
                )
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.SingleOrDefault();
            return null;
        }


        [HttpPost]
        [Route("Club/{searchTerm}")]
        public static List<ClubPlayerViewModel> SearchClub(SearchViewModel SVM)
        {
            var searchResponse = _elasticClient.Search<ClubPlayerViewModel>(s => s
               .Index(enumElasticIndexes.sportsmanduclubs.ToString())
               //.Type(enumElasticIndexes.sportsmanduclubs.ToString())
               .Take(5)
               .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.Name).Query(SVM.SearchTerm))                
               )
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.ToList();
            return new List<ClubPlayerViewModel>();
        }

        public static SearchIndexViewModel Search(SearchViewModel SVM)
        {
            SearchIndexViewModel SIVM = new SearchIndexViewModel
            {
                Players = SearchPlayer(SVM),
                Clubs = SearchClub(SVM),                
            };
            return SIVM;
        }

        [HttpGet]
        //[Route("{playername}")]
        public static List<PlayerViewModel> SearchPlayer(SearchViewModel SVM)
        {
            var searchResponse = _elasticClient.Search<PlayerViewModel>(s => s
               .Index(enumElasticIndexes.sportsmanduplayers.ToString())
               //.Type(enumElasticIndexes.sportsmanduplayers.ToString())
               .Take(5)
               .Query(q =>
                   q.MultiMatch(x =>
                       x.Query(SVM.SearchTerm))));
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.ToList();
            return new List<PlayerViewModel>();
        }

        [HttpGet]
        [Route("{Category}/{CategoryName}")]
        public ActionResult<List<PlayerViewModel>> GetProductByBrand(string Category, string CategoryName)
        {
            var searchResponse = _elasticClient.Search<PlayerViewModel>(s => s
               .Index(enumElasticIndexes.sportsmanduplayers.ToString())
               //.Type(enumElasticIndexes.sportsmanduplayers.ToString())
               .Take(10)
               .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CategoryName).Query(Category)) &&
                   q.MatchPhrase(m => m.Field(x => x.CategoryName).Query(CategoryName))
                ));
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.ToList();
            return new List<PlayerViewModel>();
        }

        [HttpGet]
        [Route("AuditReport/{UserName}")]
        public static dynamic GetAuditReport(string UserName)
        {
            _elasticClient = new ElasticClient(settings);
            var json = @"
            {
                ""_source"": {
                    ""includes"": [
                        ""environment.userName"",
                        ""entityFrameworkEvent.entries"",
                        ""entityFrameworkEvent.entries.action"",
                        ""entityFrameworkEvent.entries.table"",
                        ""entityFrameworkEvent.entries.columnValues""
                    ]
                }
            }";
            var searchResponse = _elasticClient.LowLevel.Search<SearchResponse<dynamic>>(enumElasticIndexes.defaultauditcontext.ToString(), json);
            return searchResponse.Documents;
        }
        public class ElasticView
        {
            public ElasticEnvironment Environment { get; set; }
        }
        public class ElasticEnvironment
        {
            public string UserName { get; set; }
        }
        [HttpGet]
        [Route("AllCategories")]
        public static IEnumerable<CategoryViewModel> AllCategories(string Culture)
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<CategoryViewModel>(s => s
               .Index(enumElasticIndexes.sportsmanducategories.ToString())
               //.Type(enumElasticIndexes.sportsmanducategories.ToString())
               .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(Culture)
                  )
               )
               .Take(5000)
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.Take(50).ToList();
            return null;
        }

        [HttpGet]
        [Route("AllSeasonMatch")] //For Telegram
        public static List<SeasonMatchViewModel> AllSeasonMatch(string Culture,IMemoryCache cache)
        {
            if(cache == null)
            {
                SetUpElasticClient();
                var searchResponse = _elasticClient.Search<SeasonMatchViewModel>(s => s
                   .Index(enumElasticIndexes.sportsmanduseasonmatch.ToString())
                   .Query(q =>
                       q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(Culture)
                      )
                   )
                   .Take(5000)
               );
                var returnUser = searchResponse.Documents;
                if (returnUser.Count > 0)
                    return  returnUser.Where(x => x.Teams != null).OrderByDescending(x => x.Date).Take(500).ToList();
                else
                {
                    return new List<SeasonMatchViewModel>();
                }
            }
            List<SeasonMatchViewModel> seasonMatchViewModels = new List<SeasonMatchViewModel>();
            if (!cache.TryGetValue("seasonmatch" + Culture, out seasonMatchViewModels))
            {
                SetUpElasticClient();
            var searchResponse = _elasticClient.Search<SeasonMatchViewModel>(s => s
               .Index(enumElasticIndexes.sportsmanduseasonmatch.ToString())
               .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(Culture)
                  )
               )
               .Take(5000)
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                seasonMatchViewModels =  returnUser.Take(500).OrderByDescending(x=>x.Date).ToList();
                cache.Set("seasonmatch" + Culture, seasonMatchViewModels, new MemoryCacheEntryOptions()
                           .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
            }
            return seasonMatchViewModels;
        }

        [HttpGet]
        [Route("SeasonMatchForTeam")] //For Telegram
        public static IEnumerable<SeasonMatchViewModel> SeasonMatchForTeam(string Culture,String teamName)
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<SeasonMatchViewModel>(s => s
               .Index(enumElasticIndexes.sportsmanduseasonmatch.ToString())
               //.Type(enumElasticIndexes.sportsmanduseasonmatch.ToString())
               .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.Teams.SingleOrDefault().Name).Query(teamName)
                  )
               )
               .Take(5000)
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.Take(5000).OrderByDescending(x => x.Date).ToList();
            return null;
        }

        [HttpGet]
        [Route("SeasonMatchForTeam")] //For Telegram
        public static IEnumerable<SeasonMatchViewModel> SeasonMatchForSeason(string Culture, String seasonGuid)
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<SeasonMatchViewModel>(s => s
               .Index(enumElasticIndexes.sportsmanduseasonmatch.ToString())
               //.Type(enumElasticIndexes.sportsmanduseasonmatch.ToString())
               .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.Season.Guid).Query(seasonGuid)
                  )
               )
               .Take(5000)
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.Take(5000).OrderByDescending(x => x.Date).ToList();
            return null;
        }

        [HttpGet]
        [Route("AllSeasonMatch")] //For Telegram
        public static SeasonMatchViewModel SeasonMatch(string Culture, string MatchGuid)
        {
            SetUpElasticClient();
            var searchResponse = _elasticClient.Search<SeasonMatchViewModel>(s => s
               .Index(enumElasticIndexes.sportsmanduseasonmatch.ToString())
               //.Type(enumElasticIndexes.sportsmanduseasonmatch.ToString())
               .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(Culture)) &&
                   q.MatchPhrase(m => m.Field(x => x.Id).Query(MatchGuid))
               ).Take(5000)
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                return returnUser.SingleOrDefault();
            return null;
        }

        //[HttpGet]
        //[Route("GetPredictions")]
        public static List<PredictionVMFORES> GetPredictions(string Culture, string Email,IMemoryCache cache)
        {
            List<PredictionVMFORES> predictions = new List<PredictionVMFORES>();

            if (!cache.TryGetValue("predictions" + Email + Culture, out predictions))
            {
                SetUpElasticClient();
            var searchResponse = _elasticClient.Search<PredictionVMFORES>(s => s
               .Index(enumElasticIndexes.sportsmanduprediction.ToString())
               //.Type(enumElasticIndexes.sportsmanduprediction.ToString())
               .Query(q =>
                   q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(Culture)) &&
                   q.MatchPhrase(m => m.Field(x => x.UserEmail).Query(Email))
               ).Take(5000)
           );
            var returnUser = searchResponse.Documents;
            if (returnUser.Count > 0)
                predictions =  returnUser.OrderBy(x=>x.FixtureDate).Take(100).ToList();
                cache.Set("predictions" + Email + Culture, predictions, new MemoryCacheEntryOptions()
                           .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
            }
            return predictions;
        }
        public static List<PredictionVMFORES> GetPredictionsFixture(string Culture, string fixtureGuid, IMemoryCache cache)
        {
            if(fixtureGuid.Contains("_En") || fixtureGuid.Contains("_Np"))
            {
                fixtureGuid = fixtureGuid.Replace("_En", "").Replace("_Np","");
            }
            List<PredictionVMFORES> predictions = new List<PredictionVMFORES>();

            if (!cache.TryGetValue("predictions" + fixtureGuid + Culture, out predictions))
            {
                SetUpElasticClient();
                var searchResponse = _elasticClient.Search<PredictionVMFORES>(s => s
                   .Index(enumElasticIndexes.sportsmanduprediction.ToString())
                   //.Type(enumElasticIndexes.sportsmanduprediction.ToString())
                   .Query(q =>
                       q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(Culture)) &&
                       q.MatchPhrase(m => m.Field(x => x.FixtureGuid).Query(fixtureGuid))
                   ).Take(5000)
               );
                var returnUser = searchResponse.Documents;
                if (returnUser.Count > 0)
                    predictions = returnUser.OrderBy(x => x.FixtureDate).Take(500).ToList();
                cache.Set("predictions" + fixtureGuid + Culture, predictions, new MemoryCacheEntryOptions()
                           .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
            }
            return predictions;
        }

        [HttpGet]
        [Route("AllBlogPost")] //For Telegram
        public static List<BlogPostVMFORES> AllBlogPost(string Culture,IMemoryCache cache)
        {
            List<BlogPostVMFORES> blogs = new List<BlogPostVMFORES>();
            if (!cache.TryGetValue("blogs" + Culture, out blogs))
            {
                    SetUpElasticClient();
                var searchResponse = _elasticClient.Search<BlogPostVMFORES>(s => s
                   .Index(enumElasticIndexes.sportsmandublogposts.ToString())
                   .Query(q =>
                       q.MatchPhrase(m => m.Field(x => x.CultureCode).Query(Culture)
                      )
                   )
                   .Take(5000)
               );
                var returnUser = searchResponse.Documents;
                if (returnUser.Count > 0)
                    blogs =  returnUser.Take(5000).OrderByDescending(x=>x.PublishDate).ToList();
                cache.Set("blogs" + Culture, blogs, new MemoryCacheEntryOptions()
                       .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
            }
            return blogs;
        }

        [HttpGet]
        [Route("SiteKoHeaderSettings")]
        public TypingTextt TypingTextt(string Culture)
        {
            return new TypingTextt(Culture);
        }

        [HttpGet]
        [Route("TelegramCheckAuth/{UserName}")]
        public static GenericResult TelegramCheckAuth(string UserName)
        {
            return new GenericResult()
            {
                Message = "Not Authorized",
                Succeded = false
            };
        }
        public static void FootballGoalQuestionEffects(System.Dynamic.ExpandoObject dynamicObject)
        {
            var kk = JsonConvert.SerializeObject(dynamicObject);
            Goal goal = JsonConvert.DeserializeAnonymousType<Goal>(kk,new Goal());
            //Elastic Search ko kaam haru yetai garnee ! 
            //And post to Main APi To Relay For All Channels !            
        }
        public static void FootballSubstituteQuestionEffects(System.Dynamic.ExpandoObject dynamicObject)
        {
            var kk = JsonConvert.SerializeObject(dynamicObject);
            Substitute goal = JsonConvert.DeserializeAnonymousType<Substitute>(kk, new Substitute());
        }
    }
}