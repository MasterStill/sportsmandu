﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using Sportsmandu.Data;
using Sportsmandu.Hubs;
using Sportsmandu.Model.Generic;
using Sportsmandu.Models;
using Sportsmandu.Models.Generic;
using Sportsmandu.Models.League;
using Sportsmandu.Models.Stats;
using Sportsmandu.Models.Telegram.QuestionAnswer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using static Sportsmandu.Models.Telegram.QuestionAnswer.FootballQuestions;

namespace Sportsmandu.Controllers
{
    [AllowAnonymous]
    [Route("api/QuestionEffects")]
    [Produces("application/json")]
    public class QuestionEffectsController : Controller
    {
        public AppDbContext _context;        
        private IHubContext<SportsmanduHub> _connectionManager;
        Sportsmandu.Services.IEmailSender _emailSender;
        static HttpClient client = new HttpClient();
        public QuestionEffectsController(AppDbContext _context,  Sportsmandu.Services.IEmailSender _emailSender, IHubContext<SportsmanduHub> _connectionManager)
        {
            this._connectionManager = _connectionManager;
            this._emailSender = _emailSender;
            this._context = _context;            
        }
        [HttpPost("FootballGoalQuestionEffects")]
        public GenericResult FootballGoalQuestionEffects([FromBody] Goal footballQuestion)
        {
            if (!CorrectGUID(footballQuestion.ParamGUID)) return Global.AccessDenied();
            List<ClassFieldTypeForView> ClassFieldTypeForView = new List<ClassFieldTypeForView>();
            var kk = JsonConvert.SerializeObject(footballQuestion);
            Goal goal = JsonConvert.DeserializeAnonymousType<Goal>(kk, new Goal());            
            var SeasonMatchTeamStat = _context.SeasonFixtureTeams
                        .Where(x => x.SeasonFixtureGuid == goal.Question_Which_SeasonFixture.Guid)
                        .Where(x => x.ClubId == footballQuestion.Question_Which_Team_Scored_A_Goal.Id)
                        .SingleOrDefault();
            if(SeasonMatchTeamStat.StatsString == null){
                SeasonMatchStatsFootball SeasonMatchStatsFootball = new SeasonMatchStatsFootball();
                SeasonMatchStatsFootball.Goals = 1;
                Type myType = SeasonMatchStatsFootball.GetType();
                foreach (var property in myType.GetRuntimeProperties())
                {
                    ClassFieldTypeForView.Add(new ClassFieldTypeForView
                    {
                        Name = property.Name,
                        Type = property.PropertyType.Name
                    });
                }
                ClassFieldTypeForView.Where(x => x.Name == enumGameEventTypeFootball.Goal.ToString() + "s").SingleOrDefault().Value = 1.ToString(); // Stupid fucking milauna lai TODO AT END
                SeasonMatchTeamStat.StatsString = JsonConvert.SerializeObject(ClassFieldTypeForView);
            }
            else
            {
                List<ClassFieldTypeForView> Stats = JsonConvert.DeserializeObject<List<ClassFieldTypeForView>>(SeasonMatchTeamStat.StatsString);
                int currentgoal = Int32.Parse(Stats.Where(x => x.Name == enumGameEventTypeFootball.Goal.ToString() + "s").SingleOrDefault().Value);
                var neededField = Stats.Where(x => x.Name == enumGameEventTypeFootball.Goal.ToString() + "s").SingleOrDefault().Value = (currentgoal + 1).ToString();
                SeasonMatchTeamStat.StatsString = JsonConvert.SerializeObject(Stats);
            }
            _context.Entry(SeasonMatchTeamStat).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();

            List<SeasonMatchViewModel> data = new List<SeasonMatchViewModel>();
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync($"http://localhost:5000/api/GetAllSeasonasdasdasdMatchForEsSingle/{footballQuestion.Question_Which_SeasonFixture.Guid}");
            var msg = stringTask.Result;
            data = JsonConvert.DeserializeObject<List<SeasonMatchViewModel>>(msg);
            UniversalDataContainer universalDataContainer = new UniversalDataContainer
            {
                Command = enumCommand.SeasonFixture.ToString(),
                Param = enumParam.UPDATE.ToString(),
                Data = data
            };
            _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
            return Global.Error(null,"Not Implemented");
        }
        [HttpPost("CricketSettingsQuestionEffects")]
        public GenericResult CricketSettingsQuestionEffects([FromBody] CricketQuestions.Settings CricketSettings)
        {
            return Global.Error(null,"Not Implemented");
        }
        [HttpPost("CricketNo_BallQuestionEffects")]
        public GenericResult CricketNo_BallQuestionEffects([FromBody] CricketQuestions.No_Ball No_ball)
        {
            return Global.Error(null, "Not Implemented");
        }
        private bool CorrectGUID(string GUID)
        {
            //TODO CHECK IF GUID IS VALID
            return true;
        }
    }
}