﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using Audit.EntityFramework;
using AutoMapper;
using HashidsNet;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Sportsmandu.Data;
using Sportsmandu.Hubs;
using Sportsmandu.Model.Club.ViewModels;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Entities.Ground;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Model.Generic;
using Sportsmandu.Models;
using Sportsmandu.Models.Association;
using Sportsmandu.Models.Gallery;
using Sportsmandu.Models.Generic;
using Sportsmandu.Models.League;
using Sportsmandu.Models.Messaging.Notification;
using Sportsmandu.Models.Stats;
using Sportsmandu.Services.Abstract;
using static Sportsmandu.Api.Areas.Admin.Controllers.ClubsController;
using static Sportsmandu.Api.Areas.Admin.Controllers.RolesController;
namespace Sportsmandu.Controllers
{
    [AllowAnonymous]
    [Route("api/")]
    [Produces("application/json")]
    public class ApiController : Controller
    {
        static HttpClient _client = new HttpClient();
        public AppDbContext _context;
        public AppDbContext _context1;
        public static AppDbContext _context2;
        private IMemoryCache cache;
        private IMapper _mapper { get; set; }
        Sportsmandu.Services.IEmailSender _emailSender;
        private IHubContext<SportsmanduHub> _connectionManager;
        private IHostingEnvironment _hostingEnviroment;
        private IFileAndDirectoryService _fileService;
        public ApiController(IMemoryCache cache, IFileAndDirectoryService _fileService, AppDbContext _context, AppDbContext _context1, AppDbContext _context2, IMapper _mapper, Sportsmandu.Services.IEmailSender _emailSender, IHubContext<SportsmanduHub> _connectionManager, IHostingEnvironment _hostingEnviroment)
        {
            this._hostingEnviroment = _hostingEnviroment;
            this._connectionManager = _connectionManager;
            this._emailSender = _emailSender;
            this._context = _context;
            this._context1 = _context1;
            this._mapper = _mapper;
            this._fileService = _fileService;
            this.cache = cache;
        }
        private class PlayerFromSourceVM
        {
            public string Player { get; set; }
            public string CategorySport { get; set; }
        }
        public class GalleryCreateVM
        {
            public string Name { get; set; }
            public enumClaimTypeSegments Segment { get; set; }
            public string SegmentId { get; set; }
            public string Guid { get; set; }
        }

        [HttpGet("GetDbIdFromXId/{segment}/{XID}")]
        public int GETDBID(enumClaimTypeSegments segment,int XID)
        {
            if (segment.ToString() == enumClaimTypeSegments.Clubs.ToString())
            {
                var result = _context.Clubs.Where(x => x.XId == XID).SingleOrDefault();
                if(result != null)
                {
                    return result.Id;
                }
            }
            if (segment.ToString() == enumClaimTypeSegments.Players.ToString())
            {
                var result = _context.Players.Where(x => x.XId == XID).SingleOrDefault();
                if (result != null)
                {
                    return result.Id;
                }
            }
            return 0;
        }

        [HttpGet("DeleteGallery/{id}")]
        public GenericResult DeleteGallery(int Id)
        {
            return Global.Error(null, "Not implemented");
        }
        [HttpGet("DeleteGalleryPhotos/{id}")]
        public GenericResult DeleteGalleryPhotos(int Id)
        {
            return Global.Error(null, "Not implemented");
        }
        [HttpPost("SaveGallery")]
        public GenericResult SaveGallery([FromBody] GalleryCreateVM gvm)
        {
            if (!Authorization.Authorized("Gallery", this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Global.AccessDenied());
            if (gvm.Guid == null)
            {
                Gallery g = new Gallery
                {
                    CreatedDate = DateTime.Now,
                    CreatedById = GetUserId(),
                    Guid = Guid.NewGuid().ToString(),
                    CultureId = 1,
                    DelFlag = false,
                    Verified = true,
                    Segment = gvm.Segment,
                    Name = gvm.Name,
                    SegmentId = gvm.SegmentId,
                    Multilingualid = 0
                };
                try
                {
                    _context.Entry(g).State = EntityState.Added;
                    _context.SaveChanges();
                    return Global.Success("Gallery Added", g.Id.ToString());
                }
                catch (Exception ex)
                {
                    return Global.Error(ex);
                    throw;
                }
            }
            else
            {
                return Global.Error(null, "Edit Not Implemented");
            }
        }
        [HttpGet("Gallery/{Segment}/{SegmentId}")] // For Admin Panel Gallery
        public dynamic GetAllGallery(enumClaimTypeSegments segment, string segmentId)
        {
            var result = _context.Gallery
                .Where(x => x.Segment == segment)
                .Where(x => x.SegmentId == segmentId)
                .Include(x => x.Culture)
                .Where(x => x.CultureId == 1)
                .Where(x => x.DelFlag == false)
                .OrderBy(x => x.Name)
                .Select(x => new { x.Id, x.Thumbnail, x.Name })
                .ToList();
            return result;
        }
        [HttpGet("GetCountryId/{Name}")]
        public int GetAllGallery(string Name)
        {
            var result = _context.Countries
                .Where(x => x.Name == Name)
                .SingleOrDefault();
            if (result == null)
            {
                Country C = new Country
                {
                    Name = Name,
                    CultureId =  1,
                    CreatedById =  1,
                    CreatedDate = DateTime.Now,
                    Guid = Global.UniqueGuid("Countries", _context)
                };
                _context.Entry(C).State = EntityState.Added;
                _context.SaveChanges();
                _context.Entry(C).State = EntityState.Modified;
                C.multilingualid = C.Id;
                _context.SaveChanges();
                return C.Id;
            }
            else
            {
                return result.Id;
            }
        }
        [HttpGet("GetCategoryId/{Name}")]
        public int GetCategoryId(string Name)
        {
            if (Name == "Soccer")
            {
                Name = "Football";
            }
            var result = _context.Categories
                .Where(x => x.Name == Name)
                .SingleOrDefault();
            return result.Id;
        }
        [HttpGet("Gallery/{GalleryId}")] // For Admin Panel Gallery
        public List<GalleryPhoto> GetAllGalleryImages(int GalleryId)
        {
            List<GalleryPhoto> result = _context.GalleryPhoto.Where(x => x.GalleryId == GalleryId).ToList();
            return result;
        }

        [HttpPost("SaveGalleryImages/{Segment}/{SegmentId}/{AlbumId}")]
        public GenericResult SaveGalleryImages(string Segment, string SegmentId, int AlbumId, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Global.AccessDenied());
            var _result = _fileService.CreateFiles("Sportsmandu", "Gallery", formCollection, _hostingEnviroment, 1, Segment + "_" + SegmentId + "_" + AlbumId.ToString());
            GalleryPhoto GalleryPhoto = new GalleryPhoto
            {
                Verified = false,
                Image = _result.ArrayData.FirstOrDefault(),
                Thumbnail = _result.ArrayData.FirstOrDefault(),
                CultureId = 1,
                Guid = Guid.NewGuid().ToString(),
                Multilingualid = 0,
                GalleryId = AlbumId
            };
            _context.Entry(GalleryPhoto).State = EntityState.Added;
            _context.SaveChanges();
            return Global.Success("Done");
        }

        [HttpGet("players/{CategoryId}")] // For Admin Panel
        public dynamic GetAllPlayers(int CategoryId)
        {
            var result = _context.Players.Include(x => x.Culture).Where(x => x.CultureId == 1)//.Take(5)
                .Where(x => x.Delflag == false)
                .Where(x => x.Category.Id == CategoryId)
                    .OrderBy(x => x.Name)
                    .Select(x => new { x.Id, x.Image, x.Name })
                    .ToList();
            return result;
            //List<ApiPlayerViewModel> PVM = _mapper.Map<List<ApiPlayerViewModel>>(result);
            //return PVM;   
        }

        [HttpGet("Players/ClubPlayers/{ClubId}/{CategoryId}")] // For Admin Panel
        public dynamic GetClubPlayers(int ClubId, int CategoryId)
        {
            var result = _context.ClubPlayers
                            .Where(x => x.ClubId == ClubId)
                            .Where(x => x.Player.Category.Id == CategoryId)
                            .Include(x => x.Club)
                            .ThenInclude(y => y.Players)
                            .ThenInclude(x => x.Player)
                            .ThenInclude(x => x.Category)
                            .ToList();
            List<ClubPlayerViewModelApi> PVM = _mapper.Map<List<ClubPlayerViewModelApi>>(result);
            return PVM;
            //return PVM;
        }
        public class EmployeeSaveVM
        {
            public enumClaimTypeSegments Segment { get; set; }
            public int? EmployeeId { get; set; }
            public int SegmentId { get; set; }
            public int DesignationId { get; set; }
            public string Name { get; set; }
            public DateTime? JoinedDate { get; set; }
            public DateTime? ResignedDate { get; set; }
        }

        [HttpPost("SaveEmployee")]
        public GenericResult SaveEmployee([FromBody] EmployeeSaveVM e)
        {
            if (e == null) return Global.Error(null, "Error ! Contact Administrator !");
            if (!Authorization.Authorized(e.Segment.ToString(), "Employee", e.SegmentId, User.Claims)) return (Global.AccessDenied());
            if (e.Name == null) return Global.Error(null, "Error ! Please Provide Name!");
            string dbName = e.Segment.ToString().Remove(e.Segment.ToString().Length - 1);
            int EmployeeID = 0;
            if (e.EmployeeId == null)
            {
                Employee employee = new Employee
                {
                    Name = e.Name,
                };
                _context.Entry(employee).State = EntityState.Added;
                _context.SaveChanges();
                EmployeeID = employee.Id;
            }
            else
            {
                EmployeeID = e.EmployeeId.Value;
            }
            using (var context = _context)
            {
                try
                {
                    var commandText = $"insert into {dbName}Employee (designationid,{dbName}id,employeeid) values('{(int)e.DesignationId}','{e.SegmentId}','{EmployeeID}')";
                    //#NEW
                    //context.Database.ExecuteSqlCommand(commandText);
                    context.SaveChanges();
                    return Global.Success("Employee Mapped !");
                }
                catch (Exception ex)
                {
                    return Global.Error(ex);
                }
            }
        }

        [HttpGet("AllEmployeeDesignation")]
        public dynamic AllEmployeeDesignation()
        {
            var result = _context.Designation.Where(x => x.Delflag == false).Where(x => x.Verified == true).Select(x => new { x.Id, x.Name }).ToList();
            return result;
        }
        [HttpGet("Employee/{segment}/{ClubId}")] // For Admin Panel
        public dynamic GetClubEmployees(int ClubId, enumClaimTypeSegments Segment)
        {
            var result = _context.ClubEmployee
                            .Where(x => x.ClubId == ClubId)
                            .Include(x => x.Employee)
                            .Include(x => x.Designation)
                            .ToList();
            List<EmployeeViewModelApi> PVM = result.Adapt<List<EmployeeViewModelApi>>();
            return PVM;
        }

        [HttpGet("DeleteEmployee/{Segment}/{SegmentId}/{EmployeeId}")]
        public GenericResult DeleteEmployee(enumClaimTypeSegments Segment, int SegmentId, int EmployeeId)
        {
            //if (e == null) return Global.Error(null, "Error ! Contact Administrator !");
            if (!Authorization.Authorized(Segment.ToString(), "Delete", SegmentId, User.Claims)) return (Global.AccessDenied());
            string dbName = Segment.ToString().Remove(Segment.ToString().Length - 1);
            using (var context = _context)
            {
                try
                {
                    var commandText = $"delete from {dbName}Employee where employeeid = {EmployeeId} ";
                    //#NEW
                    //context.Database.ExecuteSqlCommand(commandText);
                    context.SaveChanges();
                    return Global.Success("Employee Mapped !");
                }
                catch (Exception ex)
                {
                    return Global.Error(ex);
                }
            }
            return Global.Error(null, "Error");
        }
        [HttpGet("Employee/AllEmployee/{searchTerm}")] // For Admin Panel
        public dynamic Employees(string SearchTerm)
        {
            var result = _context.Employee.Where(x => x.Name.Contains(SearchTerm)).ToList();
            List<EmployeeViewModelApi> PVM = result.Adapt<List<EmployeeViewModelApi>>();
            return PVM;
        }

        [HttpGet("SeasonClubPlayers/{ClubId}/{SeasonId}")] // Clubs/Edit ko lagi ho yo
        public List<SeasonClubPlayersViewModel> SeasonClubPlayers(int ClubId, int SeasonId)
        {
            var result = _context.SeasonClubPlayers
                    .Where(x => x.ClubId == ClubId)
                    .Where(x => x.SeasonId == SeasonId)
                    .Include(x => x.Player)
                    .ThenInclude(x => x.Category)
                    .ToList()
                    .Select(x => x.Player)
                    //.SingleOrDefault()
                    ;
            var neededStuff = result.Adapt<List<SeasonClubPlayersViewModel>>();
            return neededStuff;
        }

        [HttpGet("ClubCategories/{ClubId}")] // Clubs/Edit ko lagi ho yo
        public dynamic LoadClubCategories(int ClubId)
        {
            var result = _context.ClubCategory
                    .Where(x => x.ClubId == ClubId)
                    .Select(x => x.Category)
                    .Select(x => new { x.Id, x.Name, x.Image });
            return result;
        }

        [HttpPost("AddCategoryToClub")]
        public GenericResult AddPlayersToSeasonClub([FromBody]ClubCategory ClubCategory)
        {
            if (ClubCategory == null)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "CCVM is null ! Please Contact Administrator"
                };
            }
            ClubCategory ccvm = ClubCategory.Adapt<ClubCategory>();
            if (!Authorization.Authorized(enumClaimTypeSegments.Clubs.ToString(), "Edit", ccvm.ClubId, User.Claims)) return (Global.AccessDenied());
            if (ModelState.IsValid)
            {
                ccvm.Club = null;
                ccvm.Category = null;
                ccvm.Club = null;
                try
                {
                    var resss = checkIfClubCategoryExists(ccvm);
                    Debug.WriteLine("This is ress");
                    Debug.WriteLine(ccvm);
                    if (resss == null)
                    {
                        _context.Entry(ccvm).State = EntityState.Added;
                        _context.ClubCategory.Add(ccvm);
                        _context.SaveChanges();
                    }
                    else
                    {
                        _context.Entry(ccvm).State = EntityState.Modified;
                        //TODO Bholi ko day ma kei add garna parne huna sakcha here like when was the category started and shit 
                        _context.Update(resss);
                        _context.SaveChanges();
                    }
                    return new GenericResult()
                    {
                        Succeded = true,
                        Message = "Sports Assigned to Club...!"
                    };
                }
                catch (Exception ex)
                {
                    return new GenericResult()
                    {
                        Succeded = false,
                        Message = ex.Message
                    };
                }
            }
            return new GenericResult()
            {
                Succeded = false,
                Message = "Modelstate Not Valid"
            };
        }

        [HttpPost("AddPhotosForVerification")]
        public GenericResult AddPhotosForVerification([FromForm]PhotoVerifications PhotoVerifications)
        {
            if (PhotoVerifications == null)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "PhotoVerification is null ! Please Contact Administrator"
                };
            }
            try
            {
                _context.Entry(PhotoVerifications).State = EntityState.Added;
                _context.PhotoVerifications.Add(PhotoVerifications);
                _context.SaveChanges();
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Photos Added To Verifications !"
                };
            }
            catch (Exception ex)
            {
                return Global.Error(ex);
            }
        }

        [HttpPost("AddPlayersToSeasonClub")]
        public GenericResult AddPlayersToSeasonClub([FromBody]SeasonClubplayersCreateViewModel SeasonClubplayersCreateViewModel)
        {
            if (SeasonClubplayersCreateViewModel == null)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "CCVM is null ! Please Contact Administrator"
                };
            }

            SeasonClubPlayers ccvm = SeasonClubplayersCreateViewModel.Adapt<SeasonClubPlayers>();
            if (!Authorization.Authorized(enumClaimTypeSegments.Clubs.ToString(), "Edit", ccvm.ClubId, User.Claims)) return (Global.AccessDenied());
            if (ModelState.IsValid)
            {
                ccvm.Club = null;
                ccvm.Player = null;
                ccvm.Season = null;
                try
                {
                    var resss = checkIfSeasonClubPlayerExists(ccvm);
                    Debug.WriteLine("This is ress");
                    Debug.WriteLine(ccvm);
                    if (resss == null)
                    {
                        _context.Entry(ccvm).State = EntityState.Added;
                        _context.SeasonClubPlayers.Add(ccvm);
                        _context.SaveChanges();
                    }
                    else
                    {
                        _context.Entry(ccvm).State = EntityState.Modified;
                        //TODO Bholi ko day ma kei add garna parne huna sakcha here like Roles of that player in that season
                        _context.Update(resss);
                        _context.SaveChanges();
                    }
                    return new GenericResult()
                    {
                        Succeded = true,
                        Message = "Player Assigned to Season From Club...!"
                    };
                }
                catch (Exception ex)
                {
                    return new GenericResult()
                    {
                        Succeded = false,
                        Message = ex.Message
                    };
                }
            }
            return new GenericResult()
            {
                Succeded = false,
                Message = "Modelstate Not Valid"
            };
        }

        [HttpGet("getallpartners")] // For Admin Panel
        public dynamic GetAllPartners()
        {
            var result = _context.Partners.Include(x => x.Culture).Where(x => x.CultureId == 1)
                    .Select(x => new { x.Id, x.Image, x.Name })
                    .ToList();            
            return result;            
        }

        [HttpGet("getallpartnerstitle")] // For Admin Panel
        public dynamic GetAllPartnerstitle()
        {
            var result = _context.PartnerTitles.Include(x => x.Culture).Where(x => x.CultureId == 1)//.Take(5)
                    .Select(x => new { x.Id, x.Name })
                    .ToList();
            return result;
        }
        [HttpGet("playersTEST/{Culture}")]
        public List<ApiPlayerViewModel> GetAllPlplayersTEST(string Culture)
        {
            var _result = _context.Players
                  .Include(x => x.Culture)
                  .Include(x => x.Category)
                  .Include(x => x.Club)                  
                  .Take(1000000000)
                  .Where(x => x.Delflag == false).ToList();

            var categories = _context.Categories.ToList();
            foreach (var product in _result)
            {
                if (product.multilingualid != product.Id)
                {
                    var neededCategory = categories
                                .Where(x => x.CultureId == product.CultureId)
                                .Where(x => x.multilingualid == product.Category.Id)
                                .FirstOrDefault();
                    if (neededCategory != null)
                        product.Category = neededCategory;

                    if (product.Image == null)
                    {
                        var image = _result.Where(x => x.Id == product.multilingualid).SingleOrDefault();
                        product.Image = image.Image;
                    }
                }
            }
            var objToReturn = _mapper.Map<List<ApiPlayerViewModel>>(_result.Where(x => x.Culture.Code == Culture).Take(50));
            return objToReturn;
        }

        [HttpGet("clubs/{seasonId}")]
        public List<ApiClubViewModel> GetAllclubs(int seasonId)
        {
            List<Club> Clubs = new List<Club>();
            enumLeagueType leagueType = _context.Seasons.Include(x => x.League).Where(x => x.Id == seasonId).SingleOrDefault().League.LeagueType;
            if (leagueType == enumLeagueType.International)
            {
                Clubs = _context.Clubs.Where(x => x.CultureId == 1).Where(x => x.Delflag == false).Where(x => x.Name.Contains("national team")).OrderBy(x => x.Name).ToList();
                foreach (var items in Clubs)
                {
                    items.Name.Replace("National Team", "");
                }
            }
            else
            {
                Clubs = _context.Clubs.Where(x => x.CultureId == 1).Where(x => x.Delflag == false).OrderBy(x => x.Name).ToList();
            }
            List<ApiClubViewModel> PVM = _mapper.Map<List<ApiClubViewModel>>(Clubs);
            return PVM;
        }

        [HttpGet("players/{ClubId}/{CategoryId}")]
        public List<ClubPlayersViewModel> GetPlayersFromClub(int ClubId, int CategoryId)
        {
            var result = _context.ClubPlayers
                .Where(x => x.ClubId == ClubId)
                .OrderBy(x => x.Player.Name)
                .Include(x => x.Player)
                        .ThenInclude(x => x.Category)
                        .Include(x => x.Player)
                        .ThenInclude(x => x.Culture)
                        .Include(x => x.Club)
                        .ToList();
            if (result.Count() > 100)
            {
                result = result.Where(x => x.Player.Category.Id == CategoryId).ToList();
            }
            List<ClubPlayersViewModel> PVM = _mapper.Map<List<ClubPlayersViewModel>>(result);
            return PVM;
        }
        [HttpGet("seasons/{seasonId}")]
        public List<SeasonClubsViewModel> GetClubFromSeason(int seasonId)
        {
            var result = _context.SeasonClubs
                .Where(x => x.SeasonId == seasonId)
                .Include(x => x.Club)
                .Include(x => x.Season)
                        .ToList();
            List<SeasonClubsViewModel> PVM = _mapper.Map<List<SeasonClubsViewModel>>(result);
            return PVM;
        }

        //Season MATCHFIXTURES ko lagi banayeko ho yo ! ##ADMINAPI
        [HttpGet("seasons/GetSeasonMatch/{seasonId}/{categoryId}")]
        public List<SeasonMatchViewModel> GetMatchFixturesFromSeason(int seasonId, int CategoryId)
        {
            var result = _context.SeasonFixture
                .Where(x => x.Season.Id == seasonId)
                .Include(x => x.Category)
                .Include(x => x.Ground)
                .Include(x => x.Season)
                .Include(x => x.Teams)
                .ThenInclude(x => x.Club)
                .OrderByDescending(x => x.Date)                
                .ToList();
            List<SeasonMatchViewModel> objToReturn = result.Adapt<List<SeasonMatchViewModel>>();
            foreach (var seasonMatch in objToReturn)
            {
                foreach (var team in seasonMatch.Teams)
                {
                    var neededTeam = result.Where(x => x.Guid == seasonMatch.Guid)
                            .Select(x => x.Teams)
                            .SingleOrDefault()
                            .Where(x => x.ClubId == team.Id)
                            .SingleOrDefault();                    
                    team.Status = neededTeam.Status.ToString();
                    if (neededTeam.StatsString == null)
                    {
                        try
                        {
                            team.Stats = GetPlayerGameStats("SeasonMatch", seasonMatch.Category.Name);
                        }
                        catch (Exception ex)
                        {
                            Global.Error(ex);
                        }
                    }
                    else
                    {
                        var stats = result.Where(x => x.Guid == seasonMatch.Guid)
                            .Select(x => x.Teams)
                            .SingleOrDefault()
                            .Where(x => x.ClubId == team.Id)
                            .SingleOrDefault()
                            .StatsString;
                        team.Stats = JsonConvert.DeserializeObject<List<ClassFieldTypeForView>>(stats);
                    }
                }
            }            
            return objToReturn;
        }
        [HttpGet("seasons/GetSeasonPartner/{seasonId}")]
        public List<Sportsmandu.Models.League.SeasonPartnerViewModel> GetSeasonPartnerForSeason(int seasonId)
        {
            var result = _context.SeasonPartners
                .Where(x => x.SeasonId == seasonId)
                .Include(x => x.Partner)
                .Include(x => x.Season)
                .Include(x => x.PartnerTitle)
                .ToList();
            List<Sportsmandu.Models.League.SeasonPartnerViewModel> objToReturn = result.Adapt<List<Sportsmandu.Models.League.SeasonPartnerViewModel>>();
            return objToReturn;
        }
        [HttpGet("GetCategoriesFromSeason/{seasonId}")]
        public List<SeasonCategoriesViewModel> GetCategoriesFromSeason(int seasonId)
        {
            var result = _context.SeasonCategories
                .Where(x => x.SeasonId == seasonId)
                .Include(x => x.Category)
                .ThenInclude(x => x.SubCategories)
                .Include(x => x.Season)
                        .ToList();
            List<SeasonCategoriesViewModel> PVM = result.Adapt<List<SeasonCategoriesViewModel>>();
            return PVM;
        }
        [HttpGet("GetGroundsFromSeason/{seasonId}")]
        public List<SeasonGroundViewModel> GetGroundsFromSeason(int seasonId)
        {
            var _result = _context.SeasonGround
                .Where(x => x.SeasonId == seasonId)
                .Include(x => x.Ground)                
                .ToList();
            List<SeasonGroundViewModel> objToReturn = new List<SeasonGroundViewModel>();
            foreach (var items in _result)
            {
                objToReturn.Add(new SeasonGroundViewModel
                {
                    Ground = new NameIdImageViewModel
                    {
                        Image = items.Ground.Image,
                        Guid = items.Ground.Guid,
                        Name = items.Ground.Name,
                        Thumbnail = items.Ground.Thumbnail,
                        URL = items.Ground.URL,
                        Id = items.Ground.Id
                    }
                    ,
                    SeasonId = items.SeasonId
                });
            }
            return objToReturn;
        }

        private SeasonFixture checkIfSeasonMatchExists(SeasonFixture SeasonMatch)
        {
            //_context.Entry(SeasonMatch).State = EntityState.Modified;
            var result = _context.SeasonFixture
                .Where(x => x.SeasonId == SeasonMatch.SeasonId)
                .Where(x => x.Guid == SeasonMatch.Guid)
                .SingleOrDefault();
            if (result != null)
            {
                return result;
            }
            return null;
        }
        private SeasonClubs checkIfSeasonClubExists(SeasonClubs SeasonClub)
        {
            _context.Entry(SeasonClub).State = EntityState.Modified;
            var result = _context.SeasonClubs.Where(x => x.ClubId == SeasonClub.ClubId).Where(x => x.SeasonId == SeasonClub.SeasonId).SingleOrDefault();
            if (result != null)
            {
                return result;
            }
            return null;
        }
        private SeasonPartners checkIfSeasonPartnerExists(SeasonPartners SeasonClub)
        {
            _context.Entry(SeasonClub).State = EntityState.Modified;
            var result = _context.SeasonPartners.Where(x => x.PartnerId == SeasonClub.PartnerId).Where(x => x.SeasonId == SeasonClub.SeasonId).SingleOrDefault();
            if (result != null)
            {
                return result;
            }
            return null;
        }
        private SeasonGround checkIfSeasonGroundExists(SeasonGround SeasonGround)
        {
            _context.Entry(SeasonGround).State = EntityState.Modified;
            var result = _context.SeasonGround.Where(x => x.SeasonId == SeasonGround.SeasonId).Where(x => x.GroundId == SeasonGround.GroundId).SingleOrDefault();
            if (result != null)
            {
                return result;
            }
            return null;
        }
        private SeasonCategories checkIfSeasonCategoryExists(SeasonCategories SeasonCategories)
        {
            _context.Entry(SeasonCategories).State = EntityState.Modified;
            var result = _context.SeasonCategories.Where(x => x.CategoryId == SeasonCategories.CategoryId).Where(x => x.SeasonId == SeasonCategories.SeasonId).SingleOrDefault();
            if (result != null)
            {
                return result;
            }
            return null;
        }
        private ClubPlayers checkIfClubPlayerExists(ClubPlayers ClubPlayer)
        {
            _context.Entry(ClubPlayer).State = EntityState.Modified;
            var result = _context.ClubPlayers.Where(x => x.PlayerId == ClubPlayer.PlayerId).Where(x => x.ClubId == ClubPlayer.ClubId).SingleOrDefault();
            if (result != null)
            {
                return result;
            }
            return null;
        }
        private SeasonClubPlayers checkIfSeasonClubPlayerExists(SeasonClubPlayers SeasonClubPlayers)
        {
            _context.Entry(SeasonClubPlayers).State = EntityState.Modified;
            var result = _context.SeasonClubPlayers
                .Where(x => x.PlayerId == SeasonClubPlayers.PlayerId)
                .Where(x => x.SeasonId == SeasonClubPlayers.SeasonId)
                .Where(x => x.ClubId == SeasonClubPlayers.ClubId)
                .SingleOrDefault();
            if (result != null)
            {
                return result;
            }
            return null;
        }
        private SeasonClubPlayers checkIfClubCategoryExists(ClubCategory ClubCategory)
        {
            _context.Entry(ClubCategory).State = EntityState.Modified;
            var result = _context.SeasonClubPlayers
                .Where(x => x.PlayerId == ClubCategory.ClubId)
                .Where(x => x.SeasonId == ClubCategory.CategoryId)
                .Where(x => x.ClubId == ClubCategory.ClubId)
                .SingleOrDefault();
            if (result != null)
            {
                return result;
            }
            return null;
        }
        [HttpPost("AddPlayersToClub")]
        public GenericResult AddPlayersToClub([FromBody]ClubplayersCreateViewModel ClubplayersCreateViewModel)
        {
            if (ClubplayersCreateViewModel == null)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "CCVM is null ! Please Contact Administrator"
                };
            }
            if (ClubplayersCreateViewModel.ShirtNumber == "") ClubplayersCreateViewModel.ShirtNumber = "0";
            ClubPlayers ccvm = ClubplayersCreateViewModel.Adapt<ClubPlayers>();
            if (!Authorization.Authorized(enumClaimTypeSegments.Clubs.ToString(), "Edit", ccvm.ClubId, User.Claims)) return (Global.AccessDenied());
            if (ModelState.IsValid)
            {
                ccvm.Club = null;
                ccvm.Player = null;
                try
                {
                    var resss = checkIfClubPlayerExists(ccvm);
                    Debug.WriteLine("This is ress");
                    Debug.WriteLine(ccvm);
                    if (resss == null)
                    {
                        _context.Entry(ccvm).State = EntityState.Added;
                        _context.ClubPlayers.Add(ccvm);
                        _context.SaveChanges();
                    }
                    else
                    {
                        _context.Entry(ccvm).State = EntityState.Modified;
                        resss.ShirtNumber = ccvm.ShirtNumber;
                        resss.ResignedDate = ccvm.ResignedDate.Value;
                        resss.ResignedDate = ccvm.JoinedDate.Value;
                        _context.Update(resss);
                        _context.SaveChanges();
                    }
                    return new GenericResult()
                    {
                        Succeded = true,
                        Message = "Player Assigned to Club...!"
                    };
                }
                catch (Exception ex)
                {
                    return new GenericResult()
                    {
                        Succeded = false,
                        Message = ex.Message
                    };
                }
            }
            return new GenericResult()
            {
                Succeded = false,
                Message = "Modelstate Not Valid"
            };
        }        
        [HttpGet("GetSeasonFixtureTeamPlayers/{ClubId}/{SeasonFixtureGuid}")]
        public List<SeasonFixtureTeamPlayers> GetSeasonFixtureTeamPlayers(int ClubId, string SeasonFixtureGuid)
        {
            return _context.SeasonFixtureTeamPlayers
                .Include(x => x.SeasonFixture)
                .Where(x => x.ClubId == ClubId)
                .Where(x => x.SeasonFixtureGuid == SeasonFixtureGuid).ToList();
        }
        [HttpPost("AddPlayersToSeasonFixture")] // SeasonMatch
        public GenericResult AddPlayersToSeasonFixture([FromBody]SeasonFixtureTeamPlayers ccvm)
        {
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), "Edit", ccvm.ClubId, User.Claims)) return (Global.AccessDenied());
            try
            {
                _context.Entry(ccvm).State = EntityState.Added;
                _context.SeasonFixtureTeamPlayers.Add(ccvm);
                _context.SaveChanges();
                return Global.Success("Player Added To Season Match");
            }
            catch (Exception ex)
            {
                return Global.Error(ex);
            }
        }
        public class SeasonFixtureTeamsStatusChangeViewModel
        {
            public int SeasonId { get; set; }
            public string Guid { get; set; } // SeasonfixtureId
            public int ClubId { get; set; }
            public enumTeamFixtureStatus Status { get; set; }
        }

        private void DoNeedFulForPredictions(SeasonFixtureTeamsStatusChangeViewModel ccvm)
        {
            var PredictionsForMatch = _context.Predict.Include(x => x.User).Where(x => x.FixtureGuid == ccvm.Guid).ToList();
            if (ccvm.Status == enumTeamFixtureStatus.Won || ccvm.Status == enumTeamFixtureStatus.Lost)
                foreach (var Predictions in PredictionsForMatch.Where(x => x.TeamId != null).Where(x => x.TeamId == ccvm.ClubId))
                {
                    if (Predictions.Prediction == enumFixturePredict.Win)
                    {
                        if (ccvm.Status == enumTeamFixtureStatus.Won)
                        {
                            Predictions.PredictionSucceded = true;
                            Predictions.Result = PredictionResult.Correct;
                        }
                        else
                        {
                            Predictions.PredictionSucceded = false;
                            Predictions.Result = PredictionResult.Prediction_Failed;
                        }
                    }
                    else if (ccvm.Status == enumTeamFixtureStatus.Lost)
                    {
                        if (ccvm.Status == enumTeamFixtureStatus.Lost)
                        {
                            Predictions.PredictionSucceded = true;
                            Predictions.Result = PredictionResult.Correct;
                        }
                        else
                        {
                            Predictions.PredictionSucceded = false;
                            Predictions.Result = PredictionResult.Prediction_Failed;
                        }
                    }
                    else if (ccvm.Status == enumTeamFixtureStatus.Draw)
                    {
                        Predictions.PredictionSucceded = false;
                        Predictions.Result = PredictionResult.Prediction_Failed;
                    }
                    ProcessPredictions(Predictions.User.Email, Predictions.FixtureGuid);
                    _context.Entry(Predictions).State = EntityState.Modified;
                }



            foreach (var Predictions in PredictionsForMatch.Where(x => x.TeamId == null))
            {
                if (Predictions.Prediction == enumFixturePredict.Draw)
                {
                    if (ccvm.Status == enumTeamFixtureStatus.Draw)
                    {
                        Predictions.PredictionSucceded = true;
                        Predictions.Result = PredictionResult.Correct;
                    }
                    else
                    {
                        Predictions.PredictionSucceded = false;
                        Predictions.Result = PredictionResult.Prediction_Failed;
                    }
                    ProcessPredictions(Predictions.User.Email, Predictions.FixtureGuid, SupportedCultures.En, false);
                    _context.Entry(Predictions).State = EntityState.Modified;
                }
            }
            _context.SaveChanges();
        }
        
        [HttpPost("TeamFixtureStatusChange")] // SeasonMatch
        public GenericResult TeamFixtureStatusChange([FromBody]SeasonFixtureTeamsStatusChangeViewModel ccvm)
        {
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), "Edit", ccvm.ClubId, User.Claims)) return (Global.AccessDenied());
            try
            {
                var dbResult = _context.SeasonFixtureTeams
                    .Where(x => x.ClubId == ccvm.ClubId)
                    .Where(x => x.SeasonFixtureGuid == ccvm.Guid)
                    .SingleOrDefault();
                dbResult.Status = ccvm.Status;
                _context.Entry(dbResult).State = EntityState.Modified;
                _context.SaveChanges();

                //Process All Predictions
                DoNeedFulForPredictions(ccvm);
                foreach (var items in GetAllSeasonMatchForEsSingle(dbResult.SeasonFixture.SeasonId, dbResult.SeasonFixtureGuid))
                {
                    UniversalDataContainer universalDataContainer = new UniversalDataContainer
                    {
                        Command = enumCommand.SeasonFixture.ToString(),
                        Param = enumParam.UPDATE.ToString(),
                        Result = items// TODO IMPORTANT NEED To UPDATE ONLY ONE MATCH TOO !
                    };
                    _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
                }
                return Global.Success("Team Status Changes");
            }
            catch (Exception ex)
            {
                return Global.Error(ex);
            }
            //##TODO TO DO Live Processing Part 
        }
        
        [HttpPost("AddMatchToSeason")] // SeasonMatch //AddFixtureToSeason( //SaveSeasonFixture
        public GenericResult AddMatchToSeason([FromBody]SeasonMatchCreateViewModel ccvm)
        {
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), ccvm.SeasonId, User.Claims)) return (Global.AccessDenied());
            SeasonFixture smNew = ccvm.Adapt<SeasonFixture>();
            if (ModelState.IsValid)
            {
                try
                {
                    var resss = checkIfSeasonMatchExists(smNew);
                    if (resss == null)
                    {
                        smNew.Guid = Guid.NewGuid().ToString();
                        _context.Entry(smNew).State = EntityState.Added;
                        _context.SeasonFixture.Add(smNew);
                        _context.SaveChanges();
                        foreach (var teams in ccvm.Teams)
                        {

                            try
                            {
                                SeasonFixtureTeams seasonFixtureTeams = new SeasonFixtureTeams
                                {
                                    ClubId = teams.Id,
                                    SeasonFixtureGuid = smNew.Guid
                                };
                                _context.Entry<SeasonFixtureTeams>(seasonFixtureTeams).State = EntityState.Added;
                                _context.SeasonFixtureTeams.Add(seasonFixtureTeams);
                                _context.SaveChanges();
                            }
                            catch (Exception ex)
                            {
                                Global.Error(ex);
                            }
                        }
                    }
                    else
                    {                        
                        _context.Entry(resss).CurrentValues.SetValues(smNew);
                        _context.SaveChanges();
                        //TODO ##TODO Branstrom if this is necessary !
                        //if(resss.Status != enumFixtureStatus.Completed && ccvm.Status != enumFixtureStatus.Completed.ToString())
                        //{
                        //    foreach(var items in resss.Teams)
                        //    {
                        //        if(items.Status == enumTeamFixtureStatus.)
                        //    }
                        //    //Process All Predictions
                        //}

                        foreach (var teams in ccvm.Teams)
                        {
                            try
                            {
                                if (_context.SeasonFixtureTeams.Where(x => x.SeasonFixtureGuid == resss.Guid).Where(x => x.ClubId == teams.Id).SingleOrDefault() == null)
                                {
                                    SeasonFixtureTeams seasonFixtureTeams = new SeasonFixtureTeams
                                    {
                                        ClubId = teams.Id,
                                        SeasonFixtureGuid = resss.Guid,
                                        Club = null,
                                    };
                                    _context.Entry<SeasonFixtureTeams>(seasonFixtureTeams).State = EntityState.Added;
                                    seasonFixtureTeams.Club = null;
                                    seasonFixtureTeams.SeasonFixture = null;
                                    _context.SeasonFixtureTeams.Add(seasonFixtureTeams);
                                    _context.SaveChanges();
                                }
                            }
                            catch (Exception ex)
                            {
                                Global.Error(ex);
                            }
                        }
                    }
                    var match = GetAllSeasonMatchForEsSingle(0, smNew.Guid); // TODO IMPORTANT NEED To UPDATE ONLY ONE MATCH TOO !
                    foreach (var items in match)
                    {
                        UniversalDataContainer universalDataContainer = new UniversalDataContainer
                        {
                            Command = enumCommand.SeasonFixture.ToString(),
                            Param = enumParam.UPDATE.ToString(),
                            Result = items
                        };
                        //_connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
                        _connectionManager.Clients.All.SendAsync("onUniversalGet", universalDataContainer);
                    }

                    return new GenericResult()
                    {
                        Succeded = true,
                        Message = "Fixtures Details Saved to Season...!"
                    };
                }
                catch (Exception ex)
                {
                    return new GenericResult()
                    {
                        Succeded = false,
                        Message = ex.Message
                    };
                }
            }
            return new GenericResult()
            {
                Succeded = false,
                Message = "Modelstate Not Valid"
            };
        }

        
        [HttpPost("AddClubToSeason")]
        public GenericResult AddClubToSeason([FromBody]SeasonClubs ccvm)
        {
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), ccvm.SeasonId, User.Claims)) return (Global.AccessDenied());
            if (ModelState.IsValid)
            {
                try
                {
                    var resss = checkIfSeasonClubExists(ccvm);
                    Debug.WriteLine("This is ress");
                    Debug.WriteLine(ccvm);
                    if (resss == null)
                    {
                        _context.SeasonClubs.Add(ccvm);
                        _context.SaveChanges();
                    }
                    else
                    {
                        _context.Update(resss);
                        _context.SaveChanges();
                    }
                    return new GenericResult()
                    {
                        Succeded = true,
                        Message = "Club Assigned to Season...!"
                    };
                }
                catch (Exception ex)
                {
                    return new GenericResult()
                    {
                        Succeded = false,
                        Message = ex.Message
                    };
                }
            }
            return new GenericResult()
            {
                Succeded = false,
                Message = "Modelstate Not Valid"
            };
        }

        
        [HttpPost("AddPartnerToSeason")]
        public GenericResult AddPartnerToSeason([FromBody]SeasonPartners ccvm)
        {
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), ccvm.SeasonId, User.Claims)) return (Global.AccessDenied());
            ccvm.Season = null;
            ccvm.Partner = null;
            ccvm.PartnerTitle = null;
            if (ModelState.IsValid)
            {
                try
                {
                    var resss = checkIfSeasonPartnerExists(ccvm);
                    Debug.WriteLine("This is ress");
                    Debug.WriteLine(ccvm);
                    if (resss == null)
                    {
                        _context.SeasonPartners.Add(ccvm);
                        _context.SaveChanges();
                    }
                    else
                    {
                        _context.Update(resss);
                        _context.SaveChanges();
                    }
                    return new GenericResult()
                    {
                        Succeded = true,
                        Message = "Partner Assigned to Season...!"
                    };
                }
                catch (Exception ex)
                {
                    return new GenericResult()
                    {
                        Succeded = false,
                        Message = ex.Message
                    };
                }
            }
            return new GenericResult()
            {
                Succeded = false,
                Message = "Modelstate Not Valid"
            };
        }
        
        [HttpPost("DeleteGroundFromSeason")]
        public GenericResult DeleteGroundFromSeason([FromBody]SeasonGround ccvm)
        {
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), ccvm.SeasonId, User.Claims)) return (Global.AccessDenied());
            return new GenericResult()
            {
                Succeded = false,
                Message = "Delete Ground Not Implemented"
            };
        }
        [HttpPost("DeleteCategoryFromClub")]
        public GenericResult DeleteCategoryFromClub([FromBody]ClubCategory ccvm)
        {
            if (!Authorization.Authorized(enumClaimTypeSegments.Clubs.ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), ccvm.ClubId, User.Claims)) return (Global.AccessDenied());
            try
            {
                ccvm.Category = null;
                ccvm.Club = null;
                _context.Entry(ccvm).State = EntityState.Deleted;
                _context.ClubCategory.Remove(ccvm);
                _context.SaveChanges();
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Removed!"
                };
            }
            catch (Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = ex.Message
                };
            }
        }
        [HttpPost("DeleteSeasonClubPlayer")]
        public GenericResult DeleteSeasonClubPlayer([FromBody]SeasonClubPlayers ccvm)
        {
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), "club", ccvm.SeasonId, User.Claims)) return (Global.AccessDenied());
            try
            {
                ccvm.Club = null;
                ccvm.Season = null;
                ccvm.Player = null;
                _context.Entry(ccvm).State = EntityState.Deleted;
                _context.SeasonClubPlayers.Remove(ccvm);
                _context.SaveChanges();
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Removed!"
                };
            }
            catch (Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = ex.Message
                };
            }            
        }

        [HttpPost("DeletClubPlayer")]
        public GenericResult DeletClubPlayer([FromBody]ClubPlayers ccvm)
        {
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), "club", ccvm.ClubId, User.Claims)) return (Global.AccessDenied());
            try
            {
                var toDelete = _context.ClubPlayers.Where(x => x.ClubId == ccvm.ClubId).Where(x => x.PlayerId == ccvm.PlayerId).SingleOrDefault();
                _context.Entry(toDelete).State = EntityState.Deleted;
                _context.ClubPlayers.Remove(toDelete);
                _context.SaveChanges();
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Removed!"
                };
            }
            catch (Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = ex.Message
                };
            }            
        }

        public class SeasonFixtureDeleteViewModel
        {
            public string Guid { get; set; }
            public int SeasonId { get; set; }
        }
        [HttpPost("DeleteSeasonFixture")]
        public GenericResult DeleteSeasonFixture([FromBody]SeasonFixtureDeleteViewModel ccvm)
        {
            if (ccvm == null)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "CCVM is null ! Please Contact Administrator"
                };
            }
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), "club", ccvm.SeasonId, User.Claims)) return (Global.AccessDenied());
            try
            {
                var result = _context.SeasonFixture.Where(x => x.SeasonId == ccvm.SeasonId).Where(x => x.Guid == ccvm.Guid).SingleOrDefault();
                _context.Entry(result).State = EntityState.Deleted;
                _context.SeasonFixture.Remove(result);
                _context.SaveChanges();
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Fixture Removed!"
                };
            }
            catch (Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = ex.Message
                };
            }
        }

        [HttpPost("DeleteSeasonFixturePlayer")]
        public GenericResult DeleteSeasonFixturePlayer([FromBody]SeasonFixtureTeamPlayers ccvm)
        {
            if (ccvm == null)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "CCVM is null ! Please Contact Administrator"
                };
            }
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), "club", ccvm.ClubId, User.Claims)) return (Global.AccessDenied());
            try
            {
                var result = _context.SeasonFixtureTeamPlayers
                        .Where(x => x.ClubId == ccvm.ClubId)
                        .Where(x => x.SeasonFixtureGuid == ccvm.SeasonFixtureGuid)
                        .Where(x => x.PlayerId == ccvm.PlayerId)
                        .SingleOrDefault();
                _context.Entry(result).State = EntityState.Deleted;
                _context.SeasonFixtureTeamPlayers.Remove(result);
                _context.SaveChanges();
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Player Removed From Fixture Removed!"
                };
            }
            catch (Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = ex.Message
                };
            }
        }

        [HttpPost("DeleteTeamFromSeasonMatchFixture")]
        public GenericResult DeleteTeamFromSeasonMatchFixture([FromBody]SeasonFixtureTeams ccvm)
        {
            if (ccvm == null)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "CCVM is null ! Please Contact Administrator"
                };
            }
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), "SeasonFixtureTeams", 0, User.Claims)) return (Global.AccessDenied());
            try
            {
                ccvm.Club = null;
                ccvm.SeasonFixture = null;
                _context.Entry(ccvm).State = EntityState.Deleted;
                _context.SeasonFixtureTeams.Remove(ccvm);
                _context.SaveChanges();
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Removed!"
                };
            }
            catch (Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = ex.Message
                };
            }
        }
        [HttpPost("DeletePartnerFromSeason")]
        public GenericResult DeletePartnerFromSeason([FromBody]SeasonPartners ccvm)
        {
            if (ccvm == null)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "CCVM is null ! Please Contact Administrator"
                };
            }
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), "SeasonFixtureTeams", 0, User.Claims)) return (Global.AccessDenied());
            try
            {
                var result = _context.SeasonPartners.Where(x => x.SeasonId == ccvm.SeasonId).Where(x => x.PartnerId == ccvm.PartnerId).SingleOrDefault();
                _context.Entry(result).State = EntityState.Deleted;
                _context.SeasonPartners.Remove(result);
                _context.SaveChanges();
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Partner Removed From Season!"
                };
            }
            catch (Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = ex.Message
                };
            }
        }
        
        [HttpPost("AddGroundToSeason")]
        public GenericResult AddGroundToSeason([FromBody]SeasonGround ccvm)
        {
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), ccvm.SeasonId, User.Claims)) return (Global.AccessDenied());
            ccvm.Ground = null;
            ccvm.Season = null;
            if (ModelState.IsValid)
            {
                try
                {
                    var resss = checkIfSeasonGroundExists(ccvm);
                    Debug.WriteLine("This is ress");
                    Debug.WriteLine(ccvm);
                    if (resss == null)
                    {
                        _context.SeasonGround.Add(ccvm);
                        _context.SaveChanges();
                    }
                    else
                    {
                        _context.Update(resss);
                        _context.SaveChanges();
                    }
                    return new GenericResult()
                    {
                        Succeded = true,
                        Message = "Ground Assigned to Season...!"
                    };
                }
                catch (Exception ex)
                {
                    return new GenericResult()
                    {
                        Succeded = false,
                        Message = ex.Message
                    };
                }
            }
            return new GenericResult()
            {
                Succeded = false,
                Message = "Modelstate Not Valid"
            };
        }

        
        [HttpPost("AddCategoryToSeason")]
        public GenericResult AddCategoryToSeason([FromBody]SeasonCategories ccvm)
        {
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), ccvm.SeasonId, User.Claims)) return (Global.AccessDenied());
            if (ModelState.IsValid)
            {
                try
                {
                    var resss = checkIfSeasonCategoryExists(ccvm);
                    Debug.WriteLine("This is ress");
                    Debug.WriteLine(ccvm);
                    if (resss == null)
                    {
                        _context.SeasonCategories.Add(ccvm);
                        _context.SaveChanges();
                    }
                    else
                    {
                        _context.Update(resss);
                        _context.SaveChanges();
                    }
                    return new GenericResult()
                    {
                        Succeded = true,
                        Message = "Game Assigned to Season...!"
                    };
                }
                catch (Exception ex)
                {
                    return new GenericResult()
                    {
                        Succeded = false,
                        Message = ex.Message
                    };
                }
            }
            return new GenericResult()
            {
                Succeded = false,
                Message = "Modelstate Not Valid"
            };
        }

        [HttpGet("GetAllCategories")]
        public List<CategoryViewModel> GetAllCategories()
        {
            var _result = _context.Categories.Include(x => x.Culture).Where(x => x.CultureId == 1).ToList();
            foreach (var categories in _result.Where(x => x.multilingualid != x.Id))
            {
                if (categories.Image == null)
                {
                    var image = _result.Where(x => x.Id == categories.multilingualid).SingleOrDefault();
                    categories.Image = image.Image;
                }
            }
            var objToReturn = _mapper.Map<List<CategoryViewModel>>(_result);
            return objToReturn;
        }
        [HttpGet("GetAllGrounds/{CountryId}")]
        public List<GroundViewModel> GetAllGrounds(int countryId)
        {
            var _result = _context.Grounds
                .Include(x => x.Culture)
                .Include(x => x.Country)                
                .Where(x => x.CultureId == 1)
                .OrderBy(x => x.Name)
                .ToList();
            foreach (var categories in _result.Where(x => x.multilingualid != x.Id))
            {
                if (categories.Image == null)
                {
                    var image = _result.Where(x => x.Id == categories.multilingualid).SingleOrDefault();
                    categories.Image = image.Image;
                }
            }
            List<GroundViewModel> objToReturn = _result.Adapt<List<GroundViewModel>>();
            return objToReturn;
        }
        [HttpGet("GetAllAssociationFORES")]
        public List<AssociationVMFORES> GetAllAssociationFORES()
        {
            var _result = _context.Association
                .Include(x => x.Culture)
                .Include(x => x.Country)                
                .ToList();
            foreach (var association in _result.Where(x => x.multilingualid != x.Id))
            {
                if (association.Image == null)
                {
                    var image = _result.Where(x => x.Id == association.multilingualid).SingleOrDefault();
                    association.Image = image.Image;
                }
                if (association.Thumbnail == null)
                {
                    var image = _result.Where(x => x.Id == association.multilingualid).SingleOrDefault();
                    association.Thumbnail = image.Thumbnail;
                }
                if (association.CoverImage == null)
                {
                    var image = _result.Where(x => x.Id == association.multilingualid).SingleOrDefault();
                    association.CoverImage = image.CoverImage;
                }
            }
            List<AssociationVMFORES> objToReturn = _result.Adapt<List<AssociationVMFORES>>();
            foreach (var items in objToReturn)
            {
                items.Gallery = GetMyGallery(enumClaimTypeSegments.Association, items.Id);
            }
            return objToReturn;
        }
        [HttpGet("GetAllAssociationFORES/{id}")]
        public AssociationVMFORES GetAllAssociationFORES(int id)
        {
            var _result = _context.Association
                .Include(x => x.Culture)
                .Include(x => x.Country)
                .Where(x => x.CultureId == 1)
                .Where(x => x.Id == id)
                .SingleOrDefault();
            if (_result.multilingualid != _result.Id)
            {
                if (_result.Image == null)
                {
                    var image = _context.Association.Where(x => x.Id == _result.multilingualid).SingleOrDefault();
                    _result.Image = image.Image;
                }
            }
            AssociationVMFORES objToReturn = _result.Adapt<AssociationVMFORES>();
            objToReturn.Gallery = GetMyGallery(enumClaimTypeSegments.Association, id.ToString());
            return objToReturn;
        }
        [HttpGet("GetAllSiteSectionLanguageTitleToSaveInEs")]
        public List<SiteSectionLanguageTitleForEs> GetAllSiteSectionLanguageTitleToSaveInEs()
        {
            var aaaa = _context.SiteSectionLanguageTitles.FirstOrDefault(); ;
            var Query = _context.SiteSectionLanguageTitles
                    .Include(x => x.SiteSection)
                    .Include(x => x.LanguageTitle)
                    .ThenInclude(x => x.Globalizations)
                    .ThenInclude(x => x.Culture)
                    .Select(x => new SiteSectionLanguageTitle
                    {                        
                        SiteSection = new SiteSection
                        {
                            Id = x.SiteSectionId,
                            Name = x.SiteSection.Name
                        },
                        LanguageTitle = new LanguageTitle
                        { 
                            Globalizations = x.LanguageTitle.Globalizations,
                            Name = x.LanguageTitle.Name,
                            Id = x.LanguageTitle.Id,                            
                        },
                        LanguageTitleId = x.LanguageTitleId,
                        SiteSectionId  = x.SiteSectionId
                    })
                    .Take(5000);
            var kk = Query.ToListAsync().Result;
            var aaaaaaaa = _context.SiteSections.Include(x => x.LanguageTitles).Take(5).ToList();
            //List<SiteSectionLanguageTitle> _result = new List<SiteSectionLanguageTitle>();//Query.ToList();            
            Console.WriteLine(Query.ToQueryString());
            //var aa = kk.Adapt<List<SiteSectionLanguageTitle>>();
            var objToReturn = _mapper.Map<List<SiteSectionLanguageTitleForEs>>(kk.AsEnumerable().Reverse());
            return objToReturn;
        }

        [HttpGet("GetAllSeasonMatchForEs")]
        public List<SeasonViewModel> GetAllSeasonMatchForEs()
        {
            var objTosend = _context.Seasons
                    .ToList()
                    .OrderByDescending(x => x.CreatedDate);
            return objTosend.Adapt<List<SeasonViewModel>>();
        }
        [HttpGet("GetAllSeasonasdasdasdMatchForEsSingle/{MatchFixtureGuid}")]
        public List<SeasonMatchViewModel> GetAllSeasonasdasdasdMatchForEsSingle(string MatchFixtureGuid)
        {
            return GetAllSeasonMatchForEsSingle(0, MatchFixtureGuid);
        }

        [HttpGet("GetAllSeasonMatchForEs/{SeasonId}")]
        public List<SeasonMatchViewModel> GetAllSeasonMatchForEsSingle(int SeasonId, string MatchFixtureGuid = null)
        {
            List<SeasonMatchViewModel> SFTVMP = new List<SeasonMatchViewModel>();
            List<SeasonMatchViewModel> SFTVMPNP = new List<SeasonMatchViewModel>();
            List<SeasonFixture> objTosend = new List<SeasonFixture>();
            var aa = _context.SeasonFixture
            .Include(x => x.Category)
            .ThenInclude(x => x.SubCategories)
            .Include(x => x.Ground)
            .Include(x => x.Season)
            .Include(x => x.Teams)
            .ThenInclude(x => x.Club)
            .ThenInclude(x => x.Country)
            .ThenInclude(x => x.Culture);
            if (MatchFixtureGuid != null)
            {
                objTosend = aa.Where(x => x.Guid == MatchFixtureGuid).OrderByDescending(x => x.Date).ToList();
            }
            else
            {
                objTosend = aa.Where(x => x.SeasonId == SeasonId).OrderByDescending(x => x.Date).ToList();
            }
            var TranslatedGrounds = _context.Grounds.Include(x => x.Country).Include(x => x.Culture).Where(x => x.CultureId == 2).Take(500).ToList();
            var TranslatedSeasons = _context.Seasons.Include(x => x.Culture).Where(x => x.CultureId == 2).Take(500).ToList();
            var TranslatedClubs = _context.Clubs.Include(x => x.Country).Include(x => x.Culture).Where(x => x.CultureId == 2).ToList();
            var TranslatedCategories = _context.Categories.Include(x => x.Culture).Where(x => x.CultureId == 2).ToList();
            var TranslatedPlayers = _context.Players.Include(x => x.Country).Include(x => x.Category).Include(x => x.Culture).Where(x => x.CultureId == 2).Take(5000).ToList();
            foreach (var SeasonFixture in objTosend)
            {
                SFTVMP.Add(new SeasonMatchViewModel
                {
                    Category = new NameIdImageViewModel
                    {
                        Name = SeasonFixture.Category.Name,
                        Thumbnail = SeasonFixture.Category.Image,
                        Image = SeasonFixture.Category.Image,
                        Id = SeasonFixture.Category.Id
                    },
                    Season = new NameIdImageViewModel
                    {
                        URL = SeasonFixture.Season.URL,
                        Image = SeasonFixture.Season.Image,
                        Thumbnail = SeasonFixture.Season.Thumbnail,
                        CoverImage = SeasonFixture.Season.CoverImage,
                        Name = SeasonFixture.Season.Name,
                        Guid = SeasonFixture.Season.Guid,
                        Id = SeasonFixture.Season.Id
                    },
                    SeasonId = SeasonFixture.SeasonId,
                    Ground = new NameIdImageViewModel
                    {
                        Name = SeasonFixture.Ground.Name,
                        Thumbnail = SeasonFixture.Ground.Thumbnail,
                        Image = SeasonFixture.Ground.Image,
                        URL = SeasonFixture.Ground.URL,
                        Id = SeasonFixture.Ground.Id,
                        Guid = SeasonFixture.Ground.Guid
                    },
                    Teams = SeasonFixture.Teams.Adapt<List<SeasonFixtureTeamsViewModel>>(),
                    Date = SeasonFixture.Date,
                    CultureCode = "En",
                    Id = SeasonFixture.Guid + "_En",
                    Guid = SeasonFixture.Guid,
                    CurrentRound = SeasonFixture.CurrentRound.ToString(),
                    MatchType = SeasonFixture.MatchType.ToString(),
                    Status = SeasonFixture.Status.ToString()
                });
            }
            foreach (var fixtures in SFTVMP)
            {
                foreach (var Teams in fixtures.Teams)
                {
                    List<SeasonFixtureTeamsViewModelPlayers> playersToAdd = new List<SeasonFixtureTeamsViewModelPlayers>();
                    var fixtuersPlayers = _context.SeasonFixtureTeamPlayers
                        .Where(x => x.ClubId == Teams.Id)
                        .Where(x => x.SeasonFixtureGuid == fixtures.Guid)
                        .Select(x => x.Player).Adapt<List<SeasonFixtureTeamsViewModelPlayers>>()
                        .ToList();
                    if (fixtuersPlayers.Count == 0)
                    {
                        playersToAdd.AddRange(_context.SeasonClubPlayers                                
                                .Where(x => x.ClubId == Teams.Id)
                                .Select(x => x.Player)
                                .Where(x => x.CategoryId == fixtures.Category.Id)
                                .Adapt<List<SeasonFixtureTeamsViewModelPlayers>>());
                    }
                    else
                    {
                        playersToAdd.AddRange(fixtuersPlayers);
                    }
                    Teams.Players = playersToAdd;
                    Teams.URL = objTosend.Where(x => x.Guid == fixtures.Guid).Select(x => x.Teams).SingleOrDefault().Where(x => x.ClubId == Teams.Id).SingleOrDefault().Club.URL;
                    if (Teams.Thumbnail == null)
                    {
                        Teams.Thumbnail = Teams.Image;
                    }
                    if (Teams.CoverImage == null) Teams.CoverImage = Teams.Image;
                }
            }

            foreach (var SeasonFixture in SFTVMP.Where(x => x.Id.Contains("_En")))
            {
                var seasonThahasTranslatedData = TranslatedSeasons.Where(x => x.multilingualid == SeasonFixture.SeasonId).SingleOrDefault();
                if (seasonThahasTranslatedData.Thumbnail == null)
                {
                    seasonThahasTranslatedData.Thumbnail = SeasonFixture.Season.Thumbnail;
                }
                if (seasonThahasTranslatedData.CoverImage == null)
                {
                    seasonThahasTranslatedData.CoverImage = SeasonFixture.Season.Thumbnail;
                }
                var categoryThatTranslatedHasData = TranslatedCategories.Where(x => x.multilingualid == SeasonFixture.Category.Id).SingleOrDefault();
                List<SeasonFixtureTeamsViewModel> multiTeam = new List<SeasonFixtureTeamsViewModel>();
                foreach (var Team in SeasonFixture.Teams)
                {
                    List<SeasonFixtureTeamsViewModelPlayers> playersToAdd = new List<SeasonFixtureTeamsViewModelPlayers>();
                    foreach (var player in Team.Players)
                    {
                        Debug.WriteLine("Procesing For Player ID : " + player.Id);
                        try
                        {
                            var PlayerThathasTranslatedData = TranslatedPlayers.Where(x => x.multilingualid == player.Id)
                                .Where(x => x.Delflag == false)
                                .SingleOrDefault();
                            playersToAdd.Add(new SeasonFixtureTeamsViewModelPlayers
                            {
                                Image = player.Image,
                                Id = player.Id,
                                Name = PlayerThathasTranslatedData.Name,
                                URL = PlayerThathasTranslatedData.URL,
                                PlayerRole = player.PlayerRole
                            });
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("This breakpoint shoud never be hit !");
                        }


                    }
                    var TeamsThathasTranslatedData = TranslatedClubs.Where(x => x.multilingualid == Team.Id).SingleOrDefault();
                    multiTeam.Add(new SeasonFixtureTeamsViewModel
                    {
                        URL = TeamsThathasTranslatedData.URL,
                        Image = Team.Image,
                        Name = TeamsThathasTranslatedData.Name,
                        Thumbnail = Team.Thumbnail,
                        CoverImage = Team.CoverImage,
                        Id = Team.Id,                        
                        Stats = Team.Stats,
                        Status = Team.Status.ToString(),
                        Players = playersToAdd
                    });




                }
                var GroundThatHasTranslatedData = TranslatedGrounds.Where(x => x.multilingualid == SeasonFixture.Ground.Id)
                    .Where(x => x.CultureId == 2)
                    .SingleOrDefault();

                SFTVMPNP.Add(new SeasonMatchViewModel
                {
                    Category = new NameIdImageViewModel
                    {
                        Name = categoryThatTranslatedHasData.Name,
                        Thumbnail = SeasonFixture.Category.Image,
                        Image = SeasonFixture.Category.Image,
                    },
                    Season = new NameIdImageViewModel
                    {
                        URL = seasonThahasTranslatedData.URL,
                        Image = seasonThahasTranslatedData.Image,
                        Thumbnail = seasonThahasTranslatedData.Thumbnail,
                        Name = seasonThahasTranslatedData.Name,
                        Guid = seasonThahasTranslatedData.Guid,
                        Id = seasonThahasTranslatedData.Id
                    },
                    SeasonId = SeasonFixture.SeasonId,
                    Ground = new NameIdImageViewModel
                    {
                        Name = GroundThatHasTranslatedData.Name,
                        Thumbnail = SeasonFixture.Ground.Thumbnail,
                        Image = SeasonFixture.Ground.Image,
                        URL = GroundThatHasTranslatedData.URL,
                        Id = GroundThatHasTranslatedData.Id,
                        Guid = GroundThatHasTranslatedData.Guid
                    },
                    Teams = multiTeam,
                    Date = SeasonFixture.Date,
                    CultureCode = "Np",
                    Id = SeasonFixture.Guid + "_Np",
                    Guid = SeasonFixture.Guid,
                    Status = SeasonFixture.Status.ToString(),
                }
                );
            }
            SFTVMP.AddRange(SFTVMPNP);
            foreach (var items in SFTVMP)
            {
                items.Gallery = GetMyGallery(enumClaimTypeSegments.Fixture, items.Id);
            }
            
            return SFTVMP;
        }

        [HttpGet("GetAllSeasonForEs")]
        public List<SeasonViewModel> GetAllSeasonForEss()
        {
            var objTosend = _context.Seasons
                .OrderByDescending(x => x.CreatedDate)
                .ToList();
            return objTosend.Adapt<List<SeasonViewModel>>();
        }
        [HttpGet("GetAllSeasonForEs/{SeasonId}")]
        public SeasonVMFORES GetAllSeasonForEs(int SeasonId)
        {
            var _result = _context.Seasons
                .Where(x => x.Id == SeasonId)
                .Include(x => x.Culture)
                .Include(x => x.League)
                .ThenInclude(x => x.Category)
                .Include(x => x.League)
                .ThenInclude(x => x.Country)
                .Include(x => x.League)
                .ThenInclude(x => x.Culture)
                .Include(x => x.Partners)
                .ThenInclude(x => x.Partner)
                .Include(x => x.Partners)
                .ThenInclude(x => x.PartnerTitle)
                .Include(x => x.Clubs)
                .ThenInclude(x => x.Club)
                .ThenInclude(x => x.Country)
                .SingleOrDefault();
            var allPartners = _context.Partners.ToList();
            var allLeagues = _context.Leagues.ToList();
            var allClubs = _context.Clubs.ToList();
            //foreach (var _result in _result)
            {
                if (_result.Id != _result.multilingualid)
                {
                    ICollection<SeasonPartners> SeasonPartners = new List<SeasonPartners>();
                    ICollection<SeasonClubs> SeasonClubs = new List<SeasonClubs>();
                    var translatedLeague = allLeagues.Where(x => x.CultureId == _result.CultureId)
                                               .Where(x => x.multilingualid == _result.LeagueId).SingleOrDefault();
                    if (translatedLeague != null)
                    {
                        if (translatedLeague.Image == null)
                        {
                            var image = allLeagues
                                            .Where(x => x.Id == _result.LeagueId).SingleOrDefault().Image;
                            if (image == null) image = allLeagues
                                             .Where(x => x.Id == _result.LeagueId).SingleOrDefault().Image;
                            translatedLeague.Image = image;
                        }
                        _result.League = translatedLeague;
                    }

                    Season seasonThatHasData = _context.Seasons
                        .Include(x => x.Clubs)
                        .ThenInclude(x => x.Club)
                        .ThenInclude(x => x.Country)
                        .Include(x => x.Partners)
                        .ThenInclude(x => x.Partner)
                        //.ThenInclude(x => x.Country)
                        .Where(x => x.Id == _result.multilingualid).SingleOrDefault();
                    foreach (var Partner in seasonThatHasData.Partners)
                    {
                        var translatedPartner = allPartners.Where(x => x.CultureId == _result.CultureId)
                                               .Where(x => x.multilingualid == Partner.PartnerId).SingleOrDefault();
                        if (translatedPartner != null)
                        {
                            if (translatedPartner.Image == null)
                            {
                                var image = allPartners
                                                .Where(x => x.Id == translatedPartner.multilingualid).SingleOrDefault();
                                translatedPartner.Image = image.Image;
                            }
                            SeasonPartners.Add(new SeasonPartners
                            {
                                Partner = translatedPartner,
                                PartnerTitle = Partner.PartnerTitle
                                //TODO TRANSLATIONS
                            });
                        }
                    }

                    foreach (var Club in seasonThatHasData.Clubs)
                    {
                        var translatedClub = allClubs.Where(x => x.CultureId == _result.CultureId)
                                               .Where(x => x.multilingualid == Club.ClubId);

                        //var translatedClub = allClubs.Where(x => x.CultureId == Season.CultureId)
                        //                       .Where(x => x.multilingualid == Club.ClubId).SingleOrDefault();
                        //TODO MATHI KO COMMENTED STATEMENT IS RIGHT BUT FOR NOW ITS OK ! 
                        if (translatedClub != null)
                        {
                            if (translatedClub.FirstOrDefault().Image == null)
                            {
                                var image = allClubs
                                                .Where(x => x.Id == translatedClub.FirstOrDefault().multilingualid).SingleOrDefault();
                                translatedClub.FirstOrDefault().Image = image.Image;
                            }
                            SeasonClubs.Add(new SeasonClubs
                            {
                                Club = translatedClub.FirstOrDefault()
                            });
                        }
                    }
                    _result.Clubs = SeasonClubs;
                    _result.Partners = SeasonPartners;
                }
            }
            //https://github.com/aspnet/EntityFrameworkCore/issues/3474            
            //var mappedResult = _mapper.Map<List<SeasonVMFORES>>(_result);
            SeasonVMFORES SVMES = new SeasonVMFORES();
            //foreach(var _result in _result)
            //{
            SVMES = (new SeasonVMFORES
            {
                Clubs = _result.Clubs.Adapt<List<ClubVMForSeasonVMFORES>>(),
                CultureCode = _result.Culture.Code,
                Description = _result.Description,
                Guid = _result.Guid,
                Id = _result.Id.ToString(),
                EndDate = _result.EndDate,
                League = _result.League.Adapt<LeagueVMforSeasonVMFORES>(),
                Partners = _result.Partners.Adapt<List<PartnerVMforSeasonVMFORES>>(),
                Image = _result.Image,
                CoverImage = _result.CoverImage,
                Thumbnail = _result.Image,
                URL = _result.URL,
                Name = _result.Name,
                StartedDate = _result.StartedDate,
                Gallery = GetMyGallery(enumClaimTypeSegments.Seasons, _result.Id.ToString()),
                TournamentType = _result.TournamentType.ToString(),
                Stats = GetSeasonStats(_result.Id,_result.Guid),
                Standings = GetSeasonStandings(_result.multilingualid)
            });
            return SVMES;
        }

        private List<SeasonStatsVMFORES> GetSeasonStats(int SeasonId,string SeasonGuid)
        {
            //if (SeasonId <= 0) throw new ArgumentOutOfRangeException(nameof(SeasonId));
            var season = _context.SeasonStats.Where(x => x.SeasonId == SeasonId).SingleOrDefault();
            if (season == null) return new List<SeasonStatsVMFORES>();
            List<SeasonStats> S = new List<SeasonStats>();
            S = JsonConvert.DeserializeObject<List<SeasonStats>>(season.SeasonStatsData);
            List<SeasonStatsVMFORES> SSVM = new List<SeasonStatsVMFORES>();
            foreach (var items in S)
            {
                SeasonStatsVMFORES ssvm = new SeasonStatsVMFORES();
                ssvm.Stats_Title = items.Stats_Title.ToString();
                ssvm.SeasonId = SeasonId;
                ssvm.SeasonGuid = SeasonGuid;
                ssvm.CultureCode = "En";
                foreach (var player in items.Players)
                {
                    Player neededPlayer = _context.Players.Include(x => x.Category).Include(x => x.Culture).Include(x => x.Country)
                        .Where(x => x.Id == player.Id).SingleOrDefault();
                    Club NeededClub = _context.Clubs.Where(x => x.Id == player.ClubId).SingleOrDefault();
                    PlayersForSeasonStatsVMFORES playertoAdd = new PlayersForSeasonStatsVMFORES
                    {
                        Club = NeededClub.Name,
                        ClubImage = NeededClub.Thumbnail,
                        Data = player.TypeData,
                        Name = neededPlayer.Name,
                        Thumbnail = neededPlayer.Thumbnail,
                        URL = neededPlayer.URL
                    };
                    //TODO HERE DO IT PROPER WAY
                    ssvm.Players.Add(playertoAdd);
                }
                SSVM.Add(ssvm);
            }
            return SSVM;
        }

        [HttpGet("GetAllCategoriesFORES")]
        public List<CategoryViewModelFORES> GetAllCategoriesFORES()
        {
            var _result = _context.Categories.Include(x => x.Culture).ToList();
            foreach (var categories in _result.Where(x => x.multilingualid != x.Id))
            {
                if (categories.Image == null)
                {
                    var image = _result.Where(x => x.Id == categories.multilingualid).SingleOrDefault();
                    categories.Image = image.Image;
                }
            }            
            var objToReturn = _result.Adapt<List<CategoryViewModelFORES>>();
            return objToReturn;
        }

        [HttpGet("GetAllVideosFORES")]
        public List<Video> GetAllVideosFORES()
        {
            var result = _context.Videos.Where(x => x.Delflag == false).Where(x => x.Verified == true).Include(x => x.Culture).ToList();
            return result;
        }
        public Video GetAllVideosFORES(int id)
        {
            var result = _context.Videos
                .Where(x => x.Delflag == false)
                .Where(x=>x.Id == id)
                .Include(x => x.Culture)
                .SingleOrDefault();
            return result;
        }

        [HttpGet("GetAllPlayersForES")]
        public List<PlayerViewModelEs> GetAllPlayersForES()
        {
            List<Player> _allPlayers = _context.Players
                 .Where(x => x.Delflag == false)
                 .OrderByDescending(x => x.CreatedDate)
                 .ToList();
            var objToReturn = _allPlayers.Adapt<List<PlayerViewModelEs>>();
            foreach (var items in _allPlayers)
            {
                UniversalDataContainer universalDataContainer = new UniversalDataContainer
                {
                    Command = enumCommand.Players.ToString(),
                    Param = enumParam.UPDATE.ToString(),
                    Result = GetAllPlayersForES(items.Id)
                };
                _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
            }
            return objToReturn;
        }
        [HttpGet("GetAllPlayersForESMulTiSave/{PageNo}")]
        public List<PlayerViewModelEs> GetAllPlayersForESMulTiSave(int PageNo)
        {
            List<PlayerViewModelEs> playerstoReturn = new List<PlayerViewModelEs>();
            List<Player> _allPlayers = _context.Players
                .Where(x => x.Delflag == false)
                .OrderByDescending(x => x.CreatedDate)
                .Skip(PageNo)
                .Take(25)
                .ToList();
            foreach (var player in _allPlayers)
            {
                playerstoReturn.Add(GetAllPlayersForES(player.Id));
            }
            return playerstoReturn;
        }
        
        [HttpGet("GetSeasonStanding/{SeasonId}")]
        public List<SeasonStandingVMFORES> GetSeasonStandings(int SeasonId)
        {
            List<SeasonStanding> standings = new List<SeasonStanding>();
            foreach (var fixture in _context.SeasonFixture.Include(x => x.Season).Include(x => x.Teams).ThenInclude(X => X.Club).Where(x => x.SeasonId == SeasonId).Where(x => x.Status == enumFixtureStatus.Completed))
            {
                foreach (var team in fixture.Teams)
                {
                    if (standings.Where(x => x.Club.Id == team.ClubId).SingleOrDefault() == null)
                    {
                        SeasonStanding s =
                            new SeasonStanding
                            {
                                Played = 1,
                                Club = team.Club,
                                Season = fixture.Season
                            };

                        if (team.Status == enumTeamFixtureStatus.Draw)
                        {
                            s.Draw = 1;
                        }
                        if (team.Status == enumTeamFixtureStatus.Won)
                        {
                            s.Won = 1;
                        }
                        if (team.Status == enumTeamFixtureStatus.Lost)
                        {
                            s.Lost = 1;
                        }
                        standings.Add(s);
                    }
                    else
                    {
                        if (team.Status == enumTeamFixtureStatus.Draw)
                        {
                            standings.Where(x => x.Club.Id == team.ClubId).SingleOrDefault().Draw = standings.Where(x => x.Club.Id == team.ClubId).SingleOrDefault().Draw + 1;
                        }
                        if (team.Status == enumTeamFixtureStatus.Won)
                        {
                            standings.Where(x => x.Club.Id == team.ClubId).SingleOrDefault().Won = standings.Where(x => x.Club.Id == team.ClubId).SingleOrDefault().Won + 1;
                        }
                        if (team.Status == enumTeamFixtureStatus.Lost)
                        {
                            standings.Where(x => x.Club.Id == team.ClubId).SingleOrDefault().Lost = standings.Where(x => x.Club.Id == team.ClubId).SingleOrDefault().Lost + 1;
                        }
                        standings.Where(x => x.Club.Id == team.ClubId).SingleOrDefault().Played = standings.Where(x => x.Club.Id == team.ClubId).SingleOrDefault().Played + 1;
                    }
                }
            }
            return standings.OrderByDescending(x => x.Points).ToList().Adapt<List<SeasonStandingVMFORES>>();
        }
        [HttpGet("GetAllSeasonStanding")]
        public void GetSeasonStandings()
        {
            foreach (var season in _context.Seasons.ToList())
            {
                var standings = GetSeasonStandings(season.Id);
                if (standings.Count > 0)
                {
                    UniversalDataContainer universalDataContainer = new UniversalDataContainer
                    {
                        Command = enumCommand.SeasonStandings.ToString(),
                        Param = enumParam.ADD.ToString(),
                        Result = standings
                    };
                    _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
                }
            }
        }
        private PlayerViewModelEs GetAllPlayersForES(int PlayerId)
        {
            Player Player = _context.Players
                 .Include(x => x.Social)
                 .Include(x => x.Culture)
                 .Include(x => x.Category)
                 .Include(x => x.Country)
                 .Include(x => x.Club)
                 .ThenInclude(x => x.Club)
                 .Include(x => x.Season)
                 .ThenInclude(x => x.Season)
                 .ThenInclude(x => x.League)
                 .Where(x => x.Id == PlayerId)
                 .Where(x => x.Delflag == false).SingleOrDefault();          

            if (Player.multilingualid != Player.Id)
            {
                var neededPlayerFromDb = _context.Players.Where(x => x.Id == Player.multilingualid).SingleOrDefault();
                if (Player.Description == null) Player.Description = GetPlayerDescription(Player);
                List<ClubPlayers> CulbPlayers = new List<ClubPlayers>();
                List<SeasonClubPlayers> SeasonClubPlayers = new List<SeasonClubPlayers>();
                var neededCategory = getmeCategory(Player.CategoryId, Player.CultureId.Value);                                
                if (neededCategory != null)
                    Player.Category = neededCategory;
                if (Player.Image == null)
                {
                    var image = neededPlayerFromDb;
                    if (image != null)
                        Player.Image = image.Image;
                }
                if (Player.Thumbnail == null)
                {
                    var Thumbnail = neededPlayerFromDb;
                    if (Thumbnail != null)
                        Player.Thumbnail = Thumbnail.Thumbnail;
                }
                if (Player.CoverImage == null)
                {
                    var CoverImage = neededPlayerFromDb;
                    if (CoverImage != null)
                        Player.CoverImage = CoverImage.CoverImage;
                }
                var playerClubs = _context.ClubPlayers.Where(x => x.PlayerId == Player.multilingualid).ToList();
                foreach (var club in playerClubs)
                {
                    var neededClub = GetmeClub(club.ClubId, Player.CultureId.Value);
                    neededClub.Image = club.Club.Image;
                    neededClub.Thumbnail = club.Club.Thumbnail;
                    neededClub.CoverImage = club.Club.CoverImage;
                    //TODO ABOVE STATEMENT GIVES WRONG RESULT !!  
                    //THE BREAKPOINT SHOULD NOT BE HIT !
                    //var neededClub = allClubs.Where(x => x.multilingualid == club.ClubId).Where(x => x.CultureId == player.CultureId).SingleOrDefault();
                    CulbPlayers.Add(new ClubPlayers
                    {
                        //Player = translatedPlayer,                            
                        Club = neededClub,
                        ClubId = neededClub.Id,
                        JoinedDate = club.JoinedDate, // TODO
                        ResignedDate = club.ResignedDate, // TODO
                        ShirtNumber = club.ShirtNumber // TODO
                    });
                }
                Player.Club = CulbPlayers;
                var playerSeason = _context.SeasonClubPlayers.Where(x => x.PlayerId == neededPlayerFromDb.Id).ToList();
                foreach (var season in playerSeason)
                {
                    var neededSeason = getmeSeason(season.SeasonId,Player.CultureId.Value);                        
                    neededSeason.Image = season.Season.Image;
                    neededSeason.Thumbnail = season.Season.Thumbnail;
                    neededSeason.CoverImage = season.Season.CoverImage;
                    SeasonClubPlayers.Add(new SeasonClubPlayers
                    {
                        Season = neededSeason,
                        SeasonId = neededSeason.Id,
                        ClubId = season.ClubId,
                        Club = season.Club,
                        PlayerRole = season.PlayerRole
                    });
                }
                Player.Season = SeasonClubPlayers;
            }
            var objToReturn = _mapper.Map<PlayerViewModelEs>(Player);
            objToReturn.Gallery = GetMyGallery(enumClaimTypeSegments.Players, objToReturn.Id);
            return objToReturn;
        }

        private string GetPlayerDescription(Player Player)
        {
            return $"{Player.Name} is associated with {Player.Club.SingleOrDefault()}";
        }
        [HttpGet("GetAllClubsFORES")]
        public List<ClubViewModel> GetAllClubsFORES()
        {
            return _context.Clubs
                .Where(x => x.Delflag == false)                
                .ToList().Adapt<List<ClubViewModel>>();
            ;
        }

        [HttpGet("GetAllClubsFORES/{Id}")]
        public List<ClubViewModelFORES> GetAllClubsFORES(int Id)
        {
            var allSeasons = _context.Seasons.Include(x => x.League).ToList();
            var allCategories = _context.Categories.ToList();
            List<Club> _result = _context.Clubs
                        .Include(x => x.ClubSocial)
                        .Include(x => x.Culture)
                        .Include(x => x.Players)
                        .ThenInclude(x => x.Player)
                        .ThenInclude(x => x.Category)
                        .Include(x => x.Seasons)
                        .ThenInclude(x => x.Season)
                        .Include(x => x.Players)
                        .ThenInclude(x => x.Player)
                        .ThenInclude(x => x.Country)
                        .Include(x => x.Players)
                        .ThenInclude(x => x.Player)
                        .ThenInclude(x => x.Culture)
                        .Include(x => x.Players)
                        .ThenInclude(x => x.Player)
                        .ThenInclude(x => x.Category)
                        .Include(x=>x.Employee)
                        .ThenInclude(x=>x.Designation)
                        .Include(x => x.Employee)
                        .ThenInclude(x => x.Employee)                        
                        .Where(x => x.Id == Id)
                        .ToList();
            foreach (var club in _result)
            {
                if (club.Thumbnail == null)
                {
                    club.Thumbnail = club.Image;
                }
                if (club.CoverImage == null)
                {
                    club.CoverImage = club.Image;
                }
                if (club.Id != club.multilingualid)
                {
                    var allPlayers = _context.ClubPlayers
                            .Where(x => x.ClubId == club.multilingualid)
                            .Include(x => x.Player.ConvertedCulture).ToList()
                            .Select(x => x.Player.ConvertedCulture.Where(y => y.CultureId == club.CultureId).Select(y => y.Player))
                            .ToList()
                            ;
                    List<ClubPlayers> CulbPlayers = new List<ClubPlayers>();
                    List<ClubPlayers> CulbExPlayers = new List<ClubPlayers>();
                    List<SeasonClubs> ClubSeason = new List<SeasonClubs>();
                    List<Gallery> Gallery = new List<Gallery>();
                    var clubThatHasData = new Club();
                    clubThatHasData = _context.Clubs
                        .Include(x => x.Seasons)
                        .ThenInclude(x => x.Season)
                        .Where(x => x.Id == club.multilingualid)
                        .SingleOrDefault();
                    foreach (var season in clubThatHasData.Seasons)
                    {                        
                        var translatedSeason = _context.Seasons
                                                    .Include(x => x.League)
                                                    .Where(x => x.CultureId == club.CultureId)
                                                    .Where(x => x.multilingualid == season.SeasonId)
                                                    .SingleOrDefault();
                        if (translatedSeason != null)
                        {
                            if (translatedSeason.Image == null)
                            {
                                var image = allSeasons
                                                .Where(x => x.Id == season.SeasonId).SingleOrDefault().Image;
                                if (image == null) image = allSeasons
                                                 .Where(x => x.Id == season.SeasonId).SingleOrDefault().League.Image;
                                translatedSeason.Image = image;
                            }
                            if (translatedSeason.Thumbnail == null)
                            {
                                var image = allSeasons
                                                .Where(x => x.Id == season.SeasonId).SingleOrDefault().Thumbnail;
                                if (image == null) image = allSeasons
                                                 .Where(x => x.Id == season.SeasonId).SingleOrDefault().League.Thumbnail;
                                translatedSeason.Thumbnail = image;
                            }
                            if (translatedSeason.CoverImage == null)
                            {
                                var image = allSeasons
                                                .Where(x => x.Id == season.SeasonId).SingleOrDefault().CoverImage;
                                if (image == null) image = allSeasons
                                                 .Where(x => x.Id == season.SeasonId).SingleOrDefault().League.CoverImage;
                                translatedSeason.CoverImage = image;
                            }
                            ClubSeason.Add(new SeasonClubs
                            {
                                Season = translatedSeason
                            });
                        }
                    }
                    foreach (var player in clubThatHasData.Players)
                    {
                        
                        Console.WriteLine("1545 : " + player.PlayerId);
                        var translatedPlayer = _context.Players.Where(x => x.CultureId == club.CultureId)
                                                .Where(x => x.multilingualid == player.PlayerId)
                                                .Include(X => X.Culture)
                                                .Include(X => X.Country)
                                                .Include(X => X.Category)
                                                //TODO Should be singleOr Default.
                                                .FirstOrDefault();
                        var mainPlayer = _context.Players
                                                                        .Where(x => x.Id == player.PlayerId)
                                                                        .Include(X => X.Culture)
                                                                        .Include(X => X.Country)
                                                                        .Include(X => X.Category)
                                                                        //TODO Should be singleOr Default.
                                                                        .FirstOrDefault();
                        if (translatedPlayer != null)
                        {
                            var neededCategory = allCategories
                                    .Where(x => x.CultureId == club.CultureId)
                                    .Where(x => x.multilingualid == translatedPlayer.Category.Id)
                                    .FirstOrDefault();
                            if (neededCategory != null)
                                translatedPlayer.Category = neededCategory;
                            translatedPlayer.Image = mainPlayer.Image;
                            translatedPlayer.CoverImage = mainPlayer.CoverImage;
                            translatedPlayer.Thumbnail = mainPlayer.Thumbnail;
                            var clubPlayer = (new ClubPlayers
                            {
                                Player = translatedPlayer,
                                ClubId = clubThatHasData.Id,                                
                                JoinedDate = player.JoinedDate,
                                ResignedDate = player.ResignedDate,
                                PlayerId = translatedPlayer.Id,
                                ShirtNumber = player.ShirtNumber,
                            });
                            if (player.ResignedDate == null)
                            {
                                CulbPlayers.Add(clubPlayer);
                            }
                            else
                            {
                                CulbExPlayers.Add(clubPlayer);
                            }
                        }
                    };
                    club.Players = CulbPlayers;
                    club.ExPlayers = CulbExPlayers;
                    club.Seasons = ClubSeason;
                    club.Image = clubThatHasData.Image;
                    club.Thumbnail = clubThatHasData.Thumbnail;
                    club.CoverImage = clubThatHasData.CoverImage;
                    club.Latitude= clubThatHasData.Latitude;
                    club.Longitude= clubThatHasData.Longitude;                    
                }
                else
                {
                    club.ExPlayers = club.Players.Where(x => x.ResignedDate != null).ToList();
                    List<ClubPlayers> NewCulbPlayers = club.Players.Where(x => x.ResignedDate == null).ToList();
                    club.Players = NewCulbPlayers;
                }
            }
            var objToReturn = _result.Adapt<List<ClubViewModelFORES>>();
            foreach(var clubs in objToReturn)
            {
                clubs.Gallery = GetMyGallery(enumClaimTypeSegments.Clubs, clubs.Id.ToString());
            }
            return objToReturn;
        }

        [HttpGet("partnersFORES")]
        public List<PartnerVMFORES> partnersFORES()
        {
            var result = _context.Partners
                .Include(x => x.Culture)
                .Include(x => x.Seasons)
                .ThenInclude(x => x.Season)
                .Where(x => x.Delflag == false).ToList();
            foreach (var partner in result)
            {
                if (partner.multilingualid != partner.Id)
                {
                    var realPartner = result.Where(x => x.Id == partner.multilingualid).SingleOrDefault();
                    if (partner.Image == null)
                    {
                        partner.Image = realPartner.Image;
                    }
                    if (partner.Thumbnail == null)
                    {
                        partner.Thumbnail = realPartner.Thumbnail;
                    }
                    if (partner.CoverImage == null)
                    {
                        partner.CoverImage = realPartner.CoverImage;
                    }
                }
            }
            List<PartnerVMFORES> PVM = result.Adapt<List<PartnerVMFORES>>();
            return PVM;
        }
        [HttpGet("partnersFORES/{id}")]
        public PartnerVMFORES partnersFORES(int id)
        {
            var partner = _context.Partners
                .Where(x => x.Id == id)
                .Where(x => x.Delflag == false)
                .Include(x => x.Culture)
                .Include(x => x.Seasons)
                .ThenInclude(x => x.Season)
                .SingleOrDefault();
            //foreach (var partner in result)
            {
                if (partner.multilingualid != partner.Id)
                {
                    var realPartner = _context.Partners.Where(x => x.Id == partner.multilingualid).SingleOrDefault();
                    if (partner.Image == null)
                    {
                        partner.Image = realPartner.Image;
                    }
                    if (partner.Thumbnail == null)
                    {
                        partner.Thumbnail = realPartner.Thumbnail;
                    }
                    if (partner.CoverImage == null)
                    {
                        partner.CoverImage = realPartner.CoverImage;
                    }
                }
            }
            PartnerVMFORES PVM = partner.Adapt<PartnerVMFORES>();
            return PVM;
        }

        [HttpGet("GetAllGroundsFORES")]
        public void GetAllGroundsFORES()
        {
            var grounds = _context.Grounds
                .Include(x => x.Country)
                .Include(x => x.Culture)
                .OrderBy(x => x.CreatedDate)
                .Where(x => x.Delflag == false).ToList();
            foreach (var ground in grounds)
            {
                GetAllGroundsFORES(ground.Id);
            }
        }
        private List<GalleryVMFORES> GetMyGallery(enumClaimTypeSegments segment,string segmentId)
        {
            List<Gallery> gallerys = new List<Gallery>();
            gallerys = _context.Gallery
                .Include(x => x.Photos)
                .Where(x => x.Segment == segment)
                .Where(x => x.SegmentId == segmentId.ToString())
                .ToList();
            if(gallerys.Count() == 0)
            {
                segment = enumClaimTypeSegments.Associations;
                gallerys = _context.Gallery
                .Include(x => x.Photos)
                .Where(x => x.Segment == segment)
                .Where(x => x.SegmentId == segmentId.ToString())
                .ToList();
            }
            List<GalleryVMFORES> neededGallery = gallerys.Adapt<List<GalleryVMFORES>>();
            foreach (var gallery in neededGallery)
            {
                if (gallery.Thumbnail == null)
                {
                    gallery.Thumbnail = gallery.Photos.FirstOrDefault().Thumbnail;
                }
            }
            return neededGallery;
        }
        private GroundVMFORES GetAllGroundsFORES(int GroundId)
        {
            var ground = _context.Grounds
                .Include(x => x.Country)
                .Include(x => x.Culture)
                .Where(x => x.Delflag == false)
                .Where(x => x.Id == GroundId)
                .SingleOrDefault();            
            if (ground.multilingualid != ground.Id)
            {
                var neededGround = _context.Grounds.Where(x => x.Id == ground.multilingualid).SingleOrDefault();
                if (ground.Image == null)
                {
                    ground.Image = neededGround.Image;
                }
                if (ground.Thumbnail == null)
                {
                    ground.Thumbnail = neededGround.Thumbnail;
                }
                if (ground.CoverImage == null)
                {
                    ground.CoverImage = neededGround.CoverImage;
                    if (ground.CoverImage == null && ground.Thumbnail != null)
                    {
                        //ground.CoverImage = Global.FrontEndUseChangeBackground(ground.Thumbnail, SportsmanduImage.enumImageSegment.club_coverImage);
                    }
                }
                ground.Latitude = neededGround.Latitude;
                ground.Longitude = neededGround.Longitude;
            }
            GroundVMFORES PVM = ground.Adapt<GroundVMFORES>();
            PVM.Gallery = GetMyGallery(enumClaimTypeSegments.Grounds, GroundId.ToString());
            UniversalDataContainer universalDataContainer = new UniversalDataContainer
            {
                Command = enumCommand.Grounds.ToString(),
                Param = enumParam.UPDATE.ToString(),
                Result = PVM
            };
            _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
            return PVM;
        }

        [HttpGet("GetAllAssociationsFORES")]
        public List<AssociationVMFORES> GetAllAssociationsFORES()
        {
            var result = _context.Association
                .Include(x => x.Country)
                .Include(x => x.Culture)                
                .Where(x => x.Delflag == false).ToList();
            foreach (var association in result)
            {
                if (association.multilingualid != association.Id)
                {
                    if (association.Image == null)
                    {
                        var image = result.Where(x => x.Id == association.multilingualid).SingleOrDefault();
                        association.Image = image.Image;
                        association.Latitude = image.Latitude;
                        association.Longitude = image.Longitude;
                    }
                }
            }
            List<AssociationVMFORES> PVM = result.Adapt<List<AssociationVMFORES>>();
            foreach(var items in PVM)
            {
                items.Gallery = GetMyGallery(enumClaimTypeSegments.Association, items.Id.ToString());
            }
            return PVM;
        }
        [HttpGet("GetAllBlogPostFORES")]
        public UniversalDataContainer GetAllBlogPostFORES()
        {
            foreach (var items in _context.BlogPosts.ToList().OrderByDescending(x => x.CreatedDate).ToList())
            {
                UniversalDataContainer universalDataContainer = new UniversalDataContainer
                {
                    Command = enumCommand.BlogPost.ToString(),
                    Param = enumParam.UPDATE.ToString(),
                    Result = GetAllBlogPostFORES(items.Id)
                };
                _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
            }
            return new UniversalDataContainer();
        }
        public BlogPostVMFORES GetAllBlogPostFORES(int id)
        {
            BlogPost blogPost = _context.BlogPosts
                .Include(x => x.Culture)
                .Where(x => x.Delflag == false)
                .Where(x => x.Id == id)
                .SingleOrDefault();

            if (blogPost.multilingualid != blogPost.Id)
            {
                var dbBlogPost = _context.BlogPosts.Where(x => x.Id == blogPost.multilingualid).SingleOrDefault();
                if (blogPost.Image == null)
                {
                    blogPost.Image = dbBlogPost.Image;
                }
                if (blogPost.Thumbnail == null)
                {
                    blogPost.Thumbnail = dbBlogPost.Thumbnail;
                }
                if (blogPost.CoverImage == null)
                {
                    if (dbBlogPost == null)
                    {
                        blogPost.CoverImage = blogPost.Image;
                    }
                    else
                    {
                        blogPost.CoverImage = dbBlogPost.CoverImage;
                    }
                }
            }
            BlogPostVMFORES PVM = blogPost.Adapt<BlogPostVMFORES>();
            return PVM;
        }

        [HttpGet("LeagueVMFORES")]
        public List<LeagueVMFORES> leagueVMFORES()
        {
            var result = _context.Leagues
                .Include(x => x.Culture)
                .Include(x => x.Country)                
                .Where(x => x.Delflag == false).ToList();
            foreach (var Leagues in result)
            {
                if (Leagues.multilingualid != Leagues.Id)
                {
                    if (Leagues.Image == null)
                    {
                        var image = result.Where(x => x.Id == Leagues.multilingualid).SingleOrDefault();
                        Leagues.Image = image.Image;
                    }
                    if (Leagues.Thumbnail == null)
                    {
                        var image = result.Where(x => x.Id == Leagues.multilingualid).SingleOrDefault();
                        Leagues.Thumbnail = image.Thumbnail;
                    }
                    if (Leagues.CoverImage == null)
                    {
                        var image = result.Where(x => x.Id == Leagues.multilingualid).SingleOrDefault();
                        Leagues.CoverImage = image.CoverImage;
                    }
                    Leagues.Country = result.Where(x => x.Id == Leagues.multilingualid).SingleOrDefault().Country;
                }
            }
            List<LeagueVMFORES> PVM = result.Adapt<List<LeagueVMFORES>>();
            foreach(var Leagues in PVM)
            {
                Leagues.Gallery = GetMyGallery(enumClaimTypeSegments.Leagues, Leagues.Id);
            }
            return PVM;
        }

        [HttpGet("GetCategoryRoles/{id}")]
        public List<Global.EnumForView> GetCategoryRoles(string id)
        {
            string fullQualifiedName = $"Sportsmandu.Models.enums.enumGameRoles{id}, Sportsmandu.Models, Version = 1.0.0.0, Culture = neutral, PublicKeyToken = null";
            Global.GetEnumList(fullQualifiedName);
            return Global.GetEnumList(fullQualifiedName);
        }

        [AllowAnonymous]
        [HttpPost("SaveNewsLetter")]
        public GenericResult SaveStats([FromBody]Newsletter NewsLetter)
        {
            try
            {
                _context.Entry(NewsLetter).State = EntityState.Added;
                _context.SaveChanges();
                return Global.Success();
            }
            catch (Exception ex)
            {
                return Global.Error(ex);
            }
        }


        [AllowAnonymous]
        [HttpPost("SaveContactForm")]
        public GenericResult SaveContactForm([FromBody]ContactForm ContactForm)
        {
            try
            {
                _context.Entry(ContactForm).State = EntityState.Added;
                _context.SaveChanges();
                _emailSender.SendEmailAsync("kiran@sportsmandu.com","New Message", ContactForm.Message);
                return Global.Success();
            }
            catch (Exception ex)
            {
                return Global.Error(ex);
            }
        }

        [Authorize]
        [HttpPost("SaveStats/{segment}/{Id}")]
        public GenericResult SaveStats([FromBody]List<ClassFieldTypeForView> data, string segment, string id)
        {
            if (!Authorization.Authorized("Stats", "AddEdit", 0, base.User.Claims)) return (Global.AccessDenied());
            ClassFieldTypeForViewFORES cft = new ClassFieldTypeForViewFORES();
            if (segment == "Player")
            {
                var player = _context.Players.Where(x => x.Id == Int32.Parse(id)).SingleOrDefault();
                cft.Guid = player.Guid;
                cft.Id = player.Id;
                cft.Stats = data.Adapt<List<NameAndValueForES>>();
                player.StatsString = JsonConvert.SerializeObject(cft);
                _context.Entry(player).State = EntityState.Modified;
                _context.SaveChanges();
                return Global.Failed("Stats Saved!!");
            }
            else if (segment == "SeasonMatch")
            {
                var seasonMatch = _context.SeasonFixture.Where(x => x.Guid == id).SingleOrDefault();

                cft.Stats = data.Adapt<List<NameAndValueForES>>();
                seasonMatch.StatsString = JsonConvert.SerializeObject(cft);
                _context.Entry(seasonMatch).State = EntityState.Modified;
                _context.SaveChanges();
            }
            return Global.Failed("Not Implemented !!");
        }
        [Authorize]
        [HttpPost("SaveSeasonMatchStats")]
        public GenericResult SaveSeasonMatchStats([FromBody]SeasonMatchViewModel data) 
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Global.AuthorizationFailureMessage());
            var teams = data.Teams;
            foreach (var team in teams)
            {
                SeasonFixtureTeams teamToEdit = _context.SeasonFixtureTeams
                    .Where(x => x.SeasonFixtureGuid == data.Guid)
                    .Where(x => x.ClubId == team.Id)
                    .SingleOrDefault();
                var statsString = team.Stats;
                teamToEdit.StatsString = JsonConvert.SerializeObject(statsString);
                _context.Entry(teamToEdit).State = EntityState.Modified;
                try
                {
                    _context.SaveChanges();

                }
                catch (Exception ex)
                {
                    return Global.Error(ex);
                }
            }
            return new GenericResult
            {
                Succeded = true,
                Message = "Stats Saved"
            };
        }

        [HttpPost("SaveSeasonGroup")]
        public GenericResult SaveSeasonGroup([FromBody] SeasonCategoryClubGroup SCCG)
        {
            try
            {
                _context.Entry(SCCG).State = EntityState.Added;
                _context.SaveChanges();
                return Global.Success("Club Assigned Group");
            }
            catch (Exception ex)
            {
                return Global.Error(ex);
            }
        }

        [HttpGet("GetSeasonCategoryClubGroup/{SeasonId}")]
        public List<SeasonCategoryClubGroup> SeasonCategoryClubGroup(int SeasonId)
        {
            var neededSeason = _context.SeasonCategoryClubGroups.Where(x => x.SeasonId == SeasonId).ToList();
            return neededSeason;
        }

        [HttpPost("SaveGameScore")]
        public GenericResult SaveGameScore([FromBody]GameScoreCreateVM gscvm)
        {
            var user = _context.Users.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            var game = _context.Games.Where(x => x.Guid == gscvm.Guid).SingleOrDefault();
            var hashids = new Hashids(gscvm.Guid);
            int HighScore = hashids.Decode(gscvm.HighScore).SingleOrDefault();
            int Level = hashids.Decode(gscvm.Level).SingleOrDefault();
            int Points = hashids.Decode(gscvm.Points).SingleOrDefault();
            var userGameScore = _context.ApplicationUserGames.Where(x => x.GameId == game.Id)
                .Where(x => x.UserId == user.Id).SingleOrDefault();
            if (userGameScore == null)
            {
                ApplicationUserGames g = new ApplicationUserGames
                {
                    UserId = GetUserId(),
                    GameId = game.Id,
                    HighScore = HighScore,
                    Level = Level,
                    Points = Points,
                };
                _context.Entry(g).State = EntityState.Added;
                _context.SaveChanges();
                return Global.Success("New High Score Added");
            }
            else
            {
                if (userGameScore.Points < Points || userGameScore.Level < Level || userGameScore.HighScore < HighScore)
                {
                    userGameScore.HighScore = HighScore;
                    userGameScore.Level= Level;
                    userGameScore.Points = Points;
                }

                _context.Entry(userGameScore).State = EntityState.Modified;
                _context.SaveChanges();
                return Global.Success("New High Score Added");
            }
            return Global.Error(null, "Thanks for tweaking that out !");
        }

        [HttpGet("GetPlayerGameStatsFields/{Segment}/{id}")] 
        public List<ClassFieldTypeForView> GetPlayerGameStats(string Segment, string id)
        {
            Segment = Segment.Replace(" ", "_");
            List<ClassFieldTypeForView> ClassFieldTypeForView = new List<ClassFieldTypeForView>();
            string fullQualifiedName = $"Sportsmandu.Models.Stats.{Segment}Stats{id}, Sportsmandu.Models, Version = 1.0.0.0, Culture = neutral, PublicKeyToken = null";
            Type myType = Type.GetType(fullQualifiedName);
            foreach (var property in myType.GetRuntimeProperties())
            {
                ClassFieldTypeForView.Add(new ClassFieldTypeForView
                {
                    Name = property.Name,
                    Type = property.PropertyType.Name
                });
            }
            return ClassFieldTypeForView;
        }

        [HttpGet("GetAllCategoryRoles/{id}")]
        public List<Global.EnumForView> GetAllCategoryRoles(string id)
        {
            id = id.Replace(" ", "_");
            string fullQualifiedName = $"Sportsmandu.Models.enums.enumPlayerRole, Sportsmandu.Models, Version = 1.0.0.0, Culture = neutral, PublicKeyToken = null";
            List<Global.EnumForView> allCategoryRoles = Global.GetEnumList(fullQualifiedName);
            List<Global.EnumForView> neededCategoryRoles = GetCategoryRoles(id);
            List<Global.EnumForView> finalList = new List<Global.EnumForView>();
            foreach (var items in allCategoryRoles)
            {
                bool found = false;
                foreach (var roles in neededCategoryRoles)
                {
                    if (roles.Name == items.Name)
                    {
                        finalList.Add(items);
                        found = true;
                    }
                }
            }
            return finalList;
        }

        
        [HttpGet("Delete/{Segment}/{Id}")]
        public GenericResult GenericDelete(string Segment, int id)
        {
            if (!Authorization.Authorized("Segment", "Delete", id, User.Claims)) return (Global.AccessDenied());
            EntityFrameworkEvent e = new EntityFrameworkEvent();
            using (var context = _context)
            {
                var commandText = $"update {Segment} set delflag = 'true' where id={id}";
                //#NEWcontext.Database.ExecuteSqlCommand(commandText);
                context.SaveChanges();
                return Global.Success("Verified");
            }
        }
        public class SocialCreateViewModel
        {
            public enumSocial social { get; set; }
            public string link { get; set; }
            public int Id { get; set; }
            public enumClaimTypeSegments Type { get; set; }
        }

        [HttpPost("AddSocial")] //SaveSocial
        public GenericResult AddSocial([FromBody]SocialCreateViewModel Social)
        {
            if (Social == null) return Global.Error(null, "Error ! Contact Administrator !");
            if (!Authorization.Authorized(Social.Type.ToString(), "Social", Social.Id, User.Claims)) return (Global.AccessDenied());
            string dbName = Social.Type.ToString().Remove(Social.Type.ToString().Length - 1);
            EntityFrameworkEvent e = new EntityFrameworkEvent();
            using (var context = _context)
            {
                try
                {
                    var commandText = $"insert into {dbName}Socials (social,link,{dbName}id) values('{(int)Social.social}','{Social.link}','{Social.Id}')";
                    //#NEW
                    //context.Database.ExecuteSqlCommand(commandText);
                    context.SaveChanges();
                    return Global.Success("Social Saved");
                }
                catch (Exception ex)
                {
                    return Global.Error(ex);
                }
            }
        }

        

        //[Authorize("Super_Admin")] // TODO let other people invite too !
        [HttpGet("InviteUser")]
        public IActionResult setUserToRole()
        {
            RoleViewModel r = new RoleViewModel
            {
                RoleName = "Editor",
                Segment = enumClaimTypeSegments.Players,
                SegmentValue = "All",
                UserEmail = "masterstill@sportsmandu.com"
            };

            //if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Global.AuthorizationFailureRoute());
            ApplicationUserRole role = new ApplicationUserRole();
            role.Segment = r.Segment;
            role.RoleId = _context.Roles.SingleOrDefault(x => x.Name == r.RoleName).Id;
            role.UserId = GetUserId(r.UserEmail, r.Segment, r.SegmentValue, role.RoleId);
            if (role.UserId == 0)
            { // User Not signed up so send invitation email
                var invitedbyUser = _context.Users.SingleOrDefault(x => x.Email == GetUserName());
                var kk = Global.EmailTemplate(Sportsmandu.Models.enums.enumEmailTemplate.InvitationForRoleNewUser);
                kk.Subject = kk.Subject.Replace("{fullname}", (invitedbyUser.FullName != null) ? invitedbyUser.FullName : invitedbyUser.Email);
                kk.Message = kk.Message.Replace("{fullname}", invitedbyUser.FullName);
                kk.Message = kk.Message.Replace("{emailaddress}", invitedbyUser.Email);
                kk.Message = kk.Message.Replace("{rolename}", r.RoleName);
                //kk.Message = kk.Message.Replace("{websitename}", _context.Clubs.SingleOrDefault(x => x.Id == r.se).Name);
                kk.Message = kk.Message.Replace("{invitedemail}", r.UserEmail);
                _emailSender.SendEmailAsync(r.UserEmail, kk.Subject,kk.Message);
            }
            try
            {
                var invitedbyUser = _context.Users.SingleOrDefault(x => x.Email == GetUserName());
                var kk = Global.EmailTemplate(Sportsmandu.Models.enums.enumEmailTemplate.InvitationForRoleNewUser);
                kk.Subject = kk.Subject.Replace("{fullname}", (invitedbyUser.FullName != null) ? invitedbyUser.FullName : invitedbyUser.Email);
                kk.Message = kk.Message.Replace("{fullname}", invitedbyUser.FullName);
                kk.Message = kk.Message.Replace("{emailaddress}", invitedbyUser.Email);
                kk.Message = kk.Message.Replace("{rolename}", r.RoleName);
                //kk.Message = kk.Message.Replace("{websitename}", _context.Clubs.SingleOrDefault(x => x.Id == r.se).Name);
                kk.Message = kk.Message.Replace("{invitedemail}", r.UserEmail);
                _emailSender.SendEmailAsync(r.UserEmail, kk.Subject,
                       kk.Message);
                _context.UserRoles.Add(role);
                _context.SaveChanges();

                try
                {
                    var RoleGivenToUser = _context.Users.Where(x => x.Id == role.UserId).SingleOrDefault();
                    //_connectionManager.GetHubContext<Custo\merSupportHub>().Clients.Group(RoleGivenToUser.SignalrGuid.ToString()).onUpdateMe(SignalrUpdate.RolesControllersetUserToRole.ToString());
                }
                catch (Exception ex)
                {
                    Global.Error(ex);
                }
            }
            catch
            {

            }
            return Ok();
        }
        private int GetUserId(string Email, enumClaimTypeSegments Segment, string SegmentValue, int RoleId) //for InviteUser
        {
            var user = _context.Users.SingleOrDefault((x => x.UserName == Email));
            if (user == null)
            {
                return UserNotInDb(Email, Segment, SegmentValue, RoleId);
            }
            else
            {
                return user.Id;
            }

        }

        private int UserNotInDb(string Email, enumClaimTypeSegments Segment, string SegmentValue, int RoleId) // For InviteUser
        {
            var user = new EmailInvitation
            {
                Email = Email,
                Segment = Segment,
                SegmentValue = SegmentValue,
                InvitedDate = DateTime.Now,
                RoleId = RoleId,
                AddedById = GetUserId()
            };
            try
            {
                _context.Add(user);
                _context.SaveChanges();
                //Invite the user for the Role
            }
            catch { }
            return 0;// Need to workaround this;
        }
        private string GetUserName()
        {
            string UserName = User.Identity.Name;
            if (UserName == null)
            {
                UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            }
            return UserName;
        }
        private int GetUserId()
        {
            string UserName = User.Identity.Name;
            //if (UserName == null)
            //{
            //    UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            //}
            int UserID = _context.Users.SingleOrDefault(x => x.UserName == UserName).Id;
            return UserID;
        }
        public class GenericReport
        {
            public int Club { get; set; }
            public int Player { get; set; }
            public int Association { get; set; }
            public int League { get; set; }
            public int Season { get; set; }
            public int Ground { get; set; }
            public int Partners { get; set; }
            public int Fixtures { get; set; }
            public int Sport { get; set; }
        }
        public class ClubPlayerReport
        {
            public string ClubName { get; set; }
            public int PlayerCount { get; set; }
            //public int SeasonCount { get; set; }
        }


        [HttpGet("GenericReport")]
        public GenericReport ViewGenericReport()
        {
            GenericReport g = new GenericReport
            {
                Association = _context.Association.Where(x => x.Delflag == false).Where(x => x.CultureId == 1).Count(),
                Club = _context.Clubs.Where(x => x.Delflag == false).Where(x => x.CultureId == 1).Count(),
                Ground = _context.Grounds.Where(x => x.Delflag == false).Where(x => x.CultureId == 1).Count(),
                League = _context.Leagues.Where(x => x.Delflag == false).Where(x => x.CultureId == 1).Count(),
                Season = _context.Seasons.Where(x => x.Delflag == false).Where(x => x.CultureId == 1).Count(),
                Partners = _context.Partners.Where(x => x.Delflag == false).Where(x => x.CultureId == 1).Count(),
                Player = _context.Players.Where(x => x.Delflag == false).Where(x => x.CultureId == 1).Count(),
                Fixtures = _context.SeasonFixture.Count(),
                Sport = _context.Categories.Where(x => x.CultureId == 1).Count()
            };
            return g;
        }
        [HttpGet("AuditReport")]
        public void AuditReport()
        {
            EntityFrameworkEvent e = new EntityFrameworkEvent();

        }

        [HttpGet("Verify/{segment}/{Id}")]
        public GenericResult Verify(enumClaimTypeSegments Segment, int Id)
        {
            if (!Authorization.Authorized("Segment", "Verify", Id, User.Claims)) return (Global.AccessDenied());
            EntityFrameworkEvent e = new EntityFrameworkEvent();
            if (Segment == enumClaimTypeSegments.Players)
            {
                UniversalDataContainer universalDataContainer = new UniversalDataContainer
                {
                    Command = enumCommand.Players.ToString(),
                    Param = enumParam.UPDATE.ToString(),
                    Result = GetAllPlayersForES(Id)
                };
                _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
            }
            else if (Segment == enumClaimTypeSegments.Grounds)
            {
                UniversalDataContainer universalDataContainer = new UniversalDataContainer
                {
                    Command = enumCommand.Grounds.ToString(),
                    Param = enumParam.UPDATE.ToString(),
                    Result = GetAllGroundsFORES(Id)
                };
                _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
            }
            else if (Segment == enumClaimTypeSegments.Clubs)
            {
                UniversalDataContainer universalDataContainer = new UniversalDataContainer
                {
                    Command = enumCommand.Clubs.ToString(),
                    Param = enumParam.UPDATE.ToString(),
                    Result = GetAllClubsFORES(Id)
                };
                _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
            }
            else if (Segment == enumClaimTypeSegments.Seasons)
            {
                UniversalDataContainer universalDataContainer = new UniversalDataContainer
                {
                    Command = enumCommand.Seasons.ToString(),
                    Param = enumParam.UPDATE.ToString(),
                    Result = GetAllSeasonForEs(Id)
                };
                _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
            }
            else if (Segment == enumClaimTypeSegments.Association || Segment == enumClaimTypeSegments.Associations)
            {
                UniversalDataContainer universalDataContainer = new UniversalDataContainer
                {
                    Command = enumCommand.Association.ToString(),
                    Param = enumParam.UPDATE.ToString(),
                    Result = GetAllAssociationFORES(Id)
                };
                _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
            }
            else if (Segment == enumClaimTypeSegments.Partners)
            {
                UniversalDataContainer universalDataContainer = new UniversalDataContainer
                {
                    Command = enumCommand.Partners.ToString(),
                    Param = enumParam.UPDATE.ToString(),
                    Result = partnersFORES(Id)
                };
                _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
            }
            else if (Segment == enumClaimTypeSegments.BlogPosts)
            {
                UniversalDataContainer universalDataContainer = new UniversalDataContainer
                {
                    Command = enumCommand.BlogPost.ToString(),
                    Param = enumParam.UPDATE.ToString(),
                    Result = GetAllBlogPostFORES(Id)
                };
                _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
            }
            else if (Segment == enumClaimTypeSegments.Videos)
            {
                UniversalDataContainer universalDataContainer = new UniversalDataContainer
                {
                    Command = enumCommand.Videos.ToString(),
                    Param = enumParam.ADD.ToString(),
                    Result = GetAllVideosFORES(Id)
                };
                _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
            }
            //else if (Segment == enumClaimTypeSegments.Leagues)
            //{
            //    UniversalDataContainer universalDataContainer = new UniversalDataContainer
            //    {
            //        Command = enumCommand.Leagues.ToString(),
            //        Param = enumParam.UPDATE.ToString(),
            //        Result = leagues(Id)
            //    };
            //    _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
            //}
            using (var context = _context1)
            {
                var commandText = $"update {Segment.ToString()} set verified = 'true' where id={Id}";
                //#NEW_context1.Database.ExecuteSqlCommand(commandText);
                context.SaveChanges();
            }
            return Global.Success("Verified");
        }
        [Authorize]
        [HttpGet("Subscribe/{segment}/{Id}")]
        public GenericResult Subscribe(string Segment, string Id)
        {
            try
            {
                Console.WriteLine("Subscription Received : " + Segment + " :: " + Id);                
                EntityFrameworkEvent e = new EntityFrameworkEvent();
                int intreadDbId = 0;
                using (var context = _context)
                {
                    var commandText = $"insert into {Segment}subscriptions (\"{Segment}Guid\",\"UserEmail\") values ('{Id}','{User.Identity.Name}')";
                    //#NEWcontext.Database.ExecuteSqlCommand(commandText);
                    context.SaveChanges();
                    return Global.Success("Added To Subscription List");
                }
            }
            catch (Exception Exception)
            {
                return Global.Error(Exception);
            }
        }

        [HttpGet("RemoveSubscription/{segment}/{Id}")]
        public GenericResult RemoveSubscription(string Segment, string Id)
        {
            var user = _context.Users.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            EntityFrameworkEvent e = new EntityFrameworkEvent();
            using (var context = _context)
            {
                var commandText = $"insert into {Segment}subscriptions ('{Segment}id','UserId') ('{Id}','{user.Id}')";
                //#NEWcontext.Database.ExecuteSqlCommand(commandText);
                context.SaveChanges();
                return Global.Success("Removed From Subscription");
            }
        }

        //[Authorize("Super_Admin")] // TODO let other people invite too !
        [HttpGet("ClubPlayerReport")]
        public List<ClubPlayerReport> ClubPlayerInternalReport()
        {
            List<ClubPlayerReport> report = new List<ClubPlayerReport>();
            var result = _context.ClubPlayers
                    .Include(x => x.Club)
                    .ToList();
            foreach (var ClubPlayers in result.DistinctBy(x => x.Club.Name))
            {
                report.Add(new ClubPlayerReport
                {
                    ClubName = ClubPlayers.Club.Name,
                    PlayerCount = result.Where(x => x.ClubId == ClubPlayers.ClubId).Count()
                });
            }
            foreach (var Club in _context.Clubs.ToList().Where(x => x.CultureId == 1))
            {
                if (report.Where(x => x.ClubName == Club.Name).SingleOrDefault() == null)
                {
                    report.Add(new ClubPlayerReport
                    {
                        ClubName = Club.Name,
                        PlayerCount = 0
                    });
                }
            }
            return report.OrderBy(x => x.PlayerCount).ToList();
        }

        [HttpGet("FetchVideoInformation/{URL}")]
        public Sportsmandu.Models.Video FetchVideoInformation(string Url)
        {
            var url = $"https://www.youtube.com/watch?v={Url}";
            var id = YoutubeExplode.YoutubeClient.ParseVideoId(url); // "bnsUkE8i0tU"
            var client = new YoutubeExplode.YoutubeClient();
            //client.GetChannelAsync("UCFlPB2adH_uHaJfUCODTdrQ");
            YoutubeExplode.Models.Video video = client.GetVideoAsync(id).Result;
            Sportsmandu.Models.Video sportsmanduVido = new Sportsmandu.Models.Video
            {
                YouTubeId = id,
                Author = video.Author,
                CreatedById = 1,
                Delflag = false,
                Verified = true,
                Description = video.Description,
                Duration = video.Duration,
                Title = video.Title,
                Thumbnail = video.Thumbnails.HighResUrl,
                CoverImage = video.Thumbnails.HighResUrl,
                Image = video.Thumbnails.HighResUrl,
                UploadDate = video.UploadDate,
                YouTubeURL = url,
            };
            foreach (var tags in video.Keywords)
            {
                sportsmanduVido.Keywords = sportsmanduVido.Keywords + tags + ",";

            }
            //sportsmanduVido.Keywords = sportsmanduVido.Keywords
            if (sportsmanduVido.Keywords != null)
                sportsmanduVido.Keywords = sportsmanduVido.Keywords.Trim();
            return sportsmanduVido;
        }

        [Authorize]
        [HttpGet("RegisterDeviceId/{Platform}/{DeviceId}")]
        public GenericResult RegisterDeviceId(string platform, string deviceid)
        {
            try
            {
                _context.UserNotification.Add(
                new UserNotification
                {
                    DeviceId = deviceid,
                    UserId = GetUserId()
                });
                _context.SaveChanges();
                return Global.Success("Great Added To Internal Stuffs too !");
            }
            catch (Exception exception)
            {
                return Global.Error(exception);
            }
        }
        [HttpGet("ConnectedUsers")]
        public dynamic ConnectedUsers()
        {
            var kk = _connectionManager.Clients.Group("IAmConsole");
            return kk;
        }        
        [AllowAnonymous]
        [HttpGet("GetmyMultiPrediciton/{web}/{page}")]
        public List<PredictionVMFORES> ProcessPredictions(string UserEmail, string FixtureGuid, SupportedCultures culture = SupportedCultures.En, bool web = false,int page = 1)
        {
            int noOfRowsPerPage = 25;
            var predictions = _context.Predict
                    //.Where(x=>x.FixtureGuid == "5e2f857f-725f-40ad-9024-0bcd5dc61279")
                    .Include(x => x.User)
                    .Include(x => x.Team)
                    .ThenInclude(x => x.Country)
                    .Include(x => x.Fixture)
                    .ThenInclude(x => x.Season)
                    .Include(x => x.Fixture)
                    .ThenInclude(x => x.Teams)
                    .ThenInclude(x => x.Club)
                    .ThenInclude(x => x.Culture)
                    .Include(x => x.Fixture)
                    .ThenInclude(x => x.Teams)
                    .ThenInclude(x => x.Club)
                    .ThenInclude(x => x.Country)
                    .Include(x => x.Fixture)
                    .ThenInclude(x => x.Season)
                    .OrderByDescending(x => x.CreatedDate);
            List<Predict> newList = new List<Predict>();
            if (FixtureGuid != null)
            {
                newList = predictions
                             .Where(x => x.User.Email == UserEmail)
                             .Where(x => x.FixtureGuid == FixtureGuid).ToList();
            }
            else
            {
                newList = predictions
                    .Skip(page * noOfRowsPerPage)
                    .Take(noOfRowsPerPage)
                .ToList();
            }
            if (!web)
            {
                foreach (var prediction in newList)
                {
                    List<PredictionVMFORES> esPrediciton = SavePredictionInEs(prediction);
                    foreach (var items in esPrediciton)
                    {
                        UniversalDataContainer universalDataContainer = new UniversalDataContainer
                        {
                            Command = enumCommand.Prediction.ToString(),
                            Param = enumParam.UPDATE.ToString(),
                            Result = items.Adapt<PredictionVMFORES>()
                        };
                        _connectionManager.Clients.Group(UserEmail + items.CultureCode).SendAsync("onUniversalGet", universalDataContainer);
                        _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
                    }
                }
                return null;
            }
            else
            {
                List<PredictionVMFORES> predictionsTosend = new List<PredictionVMFORES>();
                foreach (var prediction in newList)
                {
                    predictionsTosend.AddRange(SavePredictionInEs(prediction));
                }
                return predictionsTosend;
            }
        }

        private Club GetmeClub(int? multiLingualId,int cultureId)
        {
            //cache;
            Club club = new Club();
            if (!cache.TryGetValue("club" + multiLingualId + cultureId, out club))
            {
                var translatedClub = _context.Clubs
                    .Include(x => x.Country)
                    .Include(x => x.Culture)
                    .Where(x => x.multilingualid == multiLingualId)
                    .Where(x => x.CultureId == cultureId)
                    .SingleOrDefault();
                club = translatedClub;
                cache.Set("club" + multiLingualId + cultureId,translatedClub, new MemoryCacheEntryOptions()
                       .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
            }
            return club;
        }
        private Category getmeCategory(int? multiLingualId, int cultureId)
        {
            //cache;
            Category club = new Category();
            if (!cache.TryGetValue("category" + multiLingualId + cultureId, out club))
            {
                var translatedClub = _context.Categories
                    .Include(x => x.Culture)
                    .Where(x => x.multilingualid == multiLingualId)
                    .Where(x => x.CultureId == cultureId)
                    .SingleOrDefault();
                club = translatedClub;
                cache.Set("category" + multiLingualId + cultureId, translatedClub, new MemoryCacheEntryOptions()
                       .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
            }
            return club;
        }
        private Season getmeSeason(int? multiLingualId, int cultureId)
        {
            //cache;
            Season club = new Season();
            if (!cache.TryGetValue("season" + multiLingualId + cultureId, out club))
            {
                var translatedClub = _context.Seasons
                    .Include(x => x.Culture)
                    .Where(x => x.multilingualid == multiLingualId)
                    .Where(x => x.CultureId == cultureId)
                    .SingleOrDefault();
                club = translatedClub;
                cache.Set("season" + multiLingualId + cultureId, translatedClub, new MemoryCacheEntryOptions()
                       .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
            }
            return club;
        }
        private NameIdImageViewModel getPredictionSeason(int seasonId, int cultureId)
        {           
            //cache;
            NameIdImageViewModel club = new NameIdImageViewModel();
            if (!cache.TryGetValue("season" + seasonId + cultureId, out club))
            {
                var translatedSeason = _context.Seasons
                   .Where(x => x.multilingualid == seasonId)
                   .Where(x => x.CultureId == 2)
                   .Include(x => x.Culture)
                   .SingleOrDefault()
                   .Adapt<NameIdImageViewModel>();
                club = translatedSeason;
                cache.Set("season" + seasonId + cultureId, translatedSeason, new MemoryCacheEntryOptions()
                       .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
            }
            return club;
        }
        private List<PredictionVMFORES> SavePredictionInEs(Predict prediction)
        {
            var translatedClub = GetmeClub(prediction.TeamId,2);// _context.Clubs.Where(x => x.multilingualid == prediction.TeamId).Where(x => x.CultureId == 2).SingleOrDefault();
            List<PredictionVMFORES> predictionVMToReturn = new List<PredictionVMFORES>();
            PredictionVMFORES predictionVM = new PredictionVMFORES
            {
                CultureCode = "Np",
                CreatedDate = prediction.CreatedDate,
                FixtureGuid = prediction.FixtureGuid,
                Prediction = prediction.Prediction.ToString(),
                FixtureDate = prediction.Fixture.Date,
                Result = prediction.Result.ToString(),
                Team = translatedClub?.Adapt<NameIdImageViewModel>(),
                TeamId = translatedClub?.Id,
                UserEmail = prediction.User.Email,
                UserId = prediction.User.Id,
                FullName = prediction.User.FullName,
                UserImage = prediction.User.Image == null ? "/images/no-profile-image.gif" : prediction.User.Image
            };
            List<NameIdImageViewModel> multiTeam = new List<NameIdImageViewModel>();
            foreach (var Team in prediction.Fixture.Teams)
            {
                var TeamsThathasTranslatedData = GetmeClub(Team.ClubId, 2);                    
                multiTeam.Add(new NameIdImageViewModel
                {
                    URL = TeamsThathasTranslatedData.URL,
                    Image = Team.Club.Image,
                    Name = TeamsThathasTranslatedData.Name,
                    Thumbnail = Team.Club.Thumbnail,
                    Id = Team.ClubId,
                });
            }
            predictionVM.FixtureSeason = getPredictionSeason(prediction.Fixture.Season.Id, 2);            
            predictionVM.Teams = multiTeam;

            predictionVMToReturn.Add(predictionVM);
            var kk = prediction.Adapt<PredictionVMFORES>();
            if (kk.CultureCode == null) kk.CultureCode = "En";
            kk.UserEmail = prediction.User.Email;
            kk.UserId = prediction.User.Id;
            kk.FullName = prediction.User.FullName;
            kk.UserImage = prediction.User.Image == null ? "/images/no-profile-image.gif" : prediction.User.Image;
            predictionVMToReturn.Add(kk);
            return predictionVMToReturn;
        }


        [Authorize]
        [HttpPost("SavePrediction")]
        public GenericResult SavePrediction([FromBody]Predict P)
        {
            try
            {
                if (User.Identity.Name == null) return Global.AuthorizationFailureMessage();
                var seasonFixture = _context.SeasonFixture.Where(x => x.Guid == P.FixtureGuid).SingleOrDefault();
                if (seasonFixture.Status != enumFixtureStatus.Not_Started)
                {
                    return Global.Failed("Cannot Bid On This Match Now");
                }
                var userId = _context.Users.Where(x => x.Email == User.Identity.Name).SingleOrDefault().Id;
                P.UserId = userId;
                P.CreatedDate = DateTime.Now;
                if (P.Prediction == enumFixturePredict.Draw)
                {
                    P.TeamId = null;
                    P.Team = null;
                }
                try
                {
                    _context.Entry(P).State = EntityState.Added;
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    _context.Entry(P).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                finally
                {
                    cache.Remove("predictions" + User.Identity.Name + "En");
                    cache.Remove("predictions" + User.Identity.Name + "Np");
                    ProcessPredictions(User.Identity.Name, P.FixtureGuid, SupportedCultures.Np);
                }
                return Global.Success("Predictions Registered");
            }
            catch (Exception ex)
            {
                return Global.Error(ex);
            }
        }

        [HttpPost("SaveFixtureStatsFromOutSource/{website}/{fixtureId}")] //GOOGLE
        public GenericResult SaveFixtureStatsFromOutSource([FromBody] dynamic e, int fixtureId,string Website)
        {

            return Global.Error(null, "Not Implemented");
        }

        [HttpPost("SaveStatsFromOutSource/{website}/{SeasonId}")] //GOOGLE
        public GenericResult SaveStatsFromOutSource([FromBody] dynamic e, int SeasonId,string Website)
        {
            if (e == null) return Global.Error(null, "Error ! Contact Administrator !");
            //if (!Authorization.Authorized(e.FirstOrDefault().Segment.ToString(), "Stats", 0, User.Claims)) return (Global.AccessDenied());
            //if (e.Name == null) return Global.Error(null, "Error ! Please Provide Name!");
            Season season = _context.Seasons.Where(x => x.Id == SeasonId).SingleOrDefault();
            List<SeasonStatsFromGoogle> stats = e.ToObject<List<SeasonStatsFromGoogle>>();
            List<SeasonStats> S = new List<SeasonStats>();
            foreach (var items in stats.Where(x=>x.Players.Count > 0))
            {
                List<StatPlayer> Players = new List<StatPlayer>();
                foreach(var Player in items.Players)
                {
                    Player ourDbPlayer = _context.Players.Where(x => x.Name == Player.Name)
                        //.Where(x => x..Name == Player.Country)
                        .SingleOrDefault();
                    if(ourDbPlayer!= null)
                    {
                        string TermToSeasonFor = Player.Country.Replace("National Team","");
                        Players.Add(new StatPlayer
                        {
                            Id = ourDbPlayer.Id,
                            TypeData = Player.TypeData,
                            ClubId = _context.Clubs
                                        .Where(x=>x.Name.Contains(TermToSeasonFor))
                                        .Where(x=>x.Delflag == false)
                                        .SingleOrDefault().Id
                        });
                    }
                    else
                    {
                        Debug.WriteLine("//TODO Create Player or do whatever we want !");
                    }
                }
                SeasonStats s = new SeasonStats
                    {
                        SeasonId = SeasonId,
                        Stats_Title = Enum.Parse<enumCricketStats>(items.Title.Replace(" ","_")),
                        Players = Players
                    };
                S.Add(s);
            }
            SeasonStatsDb toSave = new SeasonStatsDb
            {
                SeasonId = SeasonId,
                SeasonStatsData = JsonConvert.SerializeObject(S)//.Replace("\"","'")
            };
            try
            {
                var existingStats = _context.SeasonStats.Where(x => x.SeasonId == SeasonId).SingleOrDefault();
                if (existingStats == null)
                {
                    _context.Entry(toSave).State = EntityState.Added;
                    //_context.SeasonStats.Add(toSave);
                    _context.SaveChanges();
                    //return Global.Success("Saved To Database");
                }
                else
                {
                    _context.Entry(existingStats).State = EntityState.Modified;
                    existingStats.SeasonStatsData = toSave.SeasonStatsData;
                    _context.SeasonStats.Update(existingStats);
                    _context.SaveChanges();
                    //return Global.Success("Modified In Database");
                }
            }
            catch (Exception ex)
            {
                //return Global.Error(ex);
            }

            //now Saving for elasticsearch
            List<SeasonStatsVMFORES> SSVM = new List<SeasonStatsVMFORES>();
            foreach (var items in S)
            {
                SeasonStatsVMFORES ssvm = new SeasonStatsVMFORES();
                ssvm.Stats_Title = items.Stats_Title.ToString();
                ssvm.SeasonId = SeasonId;
                ssvm.SeasonGuid = season.Guid;
                ssvm.CultureCode = "En";
                foreach(var player in items.Players)
                {
                    Player neededPlayer = _context.Players.Include(x => x.Category).Include(x => x.Culture).Include(x => x.Country)
                                   .Where(x => x.Id == player.Id).SingleOrDefault();
                    Club NeededClub = _context.Clubs.Where(x => x.Id == player.ClubId).SingleOrDefault();
                    PlayersForSeasonStatsVMFORES playertoAdd = new PlayersForSeasonStatsVMFORES
                    {
                        Club = NeededClub.Name,
                        ClubImage = NeededClub.Thumbnail,
                        Data = player.TypeData,
                        Name = neededPlayer.Name,
                        Thumbnail = neededPlayer.Thumbnail,
                        URL = neededPlayer.URL
                    };
                    //TODO HERE DO IT PROPER WAY
                    ssvm.Players.Add(playertoAdd);
                }
                SSVM.Add(ssvm);
            }
            UniversalDataContainer universalDataContainer = new UniversalDataContainer
            {
                Command = enumCommand.SeasonStats.ToString(),
                Param = enumParam.ADD.ToString(),
                Result = SSVM
            };
            _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
            return Global.Success("Done");
        }

        [Authorize]
        [HttpGet("AdminMessage/{Message}")]
        public GenericResult SendMessageToPageAllUSers(string Message)
        {
            if (User.Identity.Name == "masterstill@gmail.com")
            {
                UniversalDataContainer universalDataContainer = new UniversalDataContainer
                {
                    Command = enumCommand.AdminMessage.ToString(),
                    Param = enumParam.ADD.ToString(),
                    Result = Message
                };
                _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);
                return new GenericResult();
            }
            else
            {
                return Global.AuthorizationFailureMessage();
            }
        }
        [HttpGet("Alexa/{WebsiteName}/{GlobalRank}/{CountryRank}")]
        [AllowAnonymous]
        public GenericResult AlexaCreate(string WebsiteName , long GlobalRank , long CountryRank)
        {
            Alexa alexa = new Alexa
            {
                CountryRank = CountryRank,
                GlobalRank = GlobalRank,
                WebsiteName = WebsiteName
            };
            _context.Entry(alexa).State = EntityState.Added;
            _context.SaveChanges();
            return Global.Success();
        }
    //[HttpPost("Key")]
    //[AllowAnonymous]
    //public IActionResult Get([FromForm] AutoItViewModel Website)
    //{
    //    try
    //    {
    //        UniversalDataContainer universalDataContainer = new UniversalDataContainer
    //        {
    //            Command = enumCommand.AdminMessage.ToString(),
    //            Param = enumParam.ADD.ToString(),
    //            Result = Website.Key + "!!!!@#!@#!@#@!#!@#" + Website.More
    //        };
    //        _connectionManager.Clients.Group("key").SendAsync("onUniversalGet", universalDataContainer);
    //    }
    //    catch
    //    {
    //        return BadRequest();
    //    }
    //    return Ok();
    //}
    public class AutoItViewModel
        {
            public string Key { get; set; }
            public string More { get; set; }
        }        
    }
}