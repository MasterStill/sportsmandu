﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Sportsmandu.Model.Entities.Globalization;
using System.Collections.Generic;
using System.Linq;

namespace Sportsmandu.Controllers
{
    public class ErrorsController : Controller
    {
        private IMemoryCache _cache;
        public ErrorsController(IMemoryCache _cache)
        {
            this._cache = _cache;
        }
        [Route("Error/{statusCode}")]
        public IActionResult HandleErrorCode(int statusCode)
        {
            var statusCodeData = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            switch (statusCode)
            {
                case 404:
                    ViewBag.ErrorMessage = "Sorry the page you requested could not be found";
                    ViewBag.RouteOfException = statusCodeData.OriginalPath;
                    ViewBag.ErrorCode = "404";
                    break;
                case 500:
                    ViewBag.ErrorMessage = "Sorry something went wrong on the server, We've Been Notified !";
                    ViewBag.RouteOfException = statusCodeData.OriginalPath;
                    ViewBag.ErrorCode = "500";
                    break;
            }
            SetSiteSectionLanguagetitle("En", "layout");
            return View("Error");
        }
        public void SetSiteSectionLanguagetitle(string Culture, string section)
        {
            try
            {
                List<SiteSectionLanguageTitleForEs> siteSectionLanguageTitle = Sportsmandu.Api.Controllers.ProductController.SiteSectionLanguageTitle(Culture, section, _cache).ToList();
                foreach (var items in siteSectionLanguageTitle)
                {
                    this.HttpContext.Items[items.LanguageTitleName] = items.LanguageTitleGlobalizations.Where(x => x.CultureCode == Culture).SingleOrDefault().Value;
                }
            }
            catch (System.Exception)
            {


            }
          }
    }
}