﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AutoMapper;
using HashidsNet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Model.Club.ViewModels;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Models;
using Sportsmandu.Models.Generic;
using Sportsmandu.Models.League;
using Sportsmandu.Services.Abstract;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using static Sportsmandu.Models.SportsmanduImage;
using Microsoft.Extensions.Caching.Memory;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Models.Association;
using HtmlAgilityPack;

namespace Sportsmandu.Controllers
{
    public class HomeController : Controller
    {
        AppDbContext _context;
        IMapper _mapper;
        private IMemoryCache _cache;
        private IBlogService _blogService;
        Sportsmandu.Services.IEmailSender _emailSender;        
        public HomeController(IMemoryCache _cache, AppDbContext _context, IMapper _mapper, IBlogService _blogService, Sportsmandu.Services.IEmailSender _emailSender)
        {
            this._cache = _cache;
            this._blogService = _blogService;
            this._context = _context;
            this._mapper = _mapper;
            this._emailSender = _emailSender;
        }
        public dynamic CreateStructuresForStats(int CategoryId = 1)
        {
            StatsDB stats = new StatsDB();
            stats.CategoryId = CategoryId;
            return stats;
        }
        public IActionResult Alexa(string culture,string id)
        {
            SetSiteSectionLanguagetitle(culture, "layout");
            SetSiteSectionLanguagetitle(culture, "alexa");
            var alexaRank = _context.Alexa.OrderBy(x => x.Date).Where(x=>x.WebsiteName == id).ToList();
            return View(alexaRank);
        }
        public IActionResult Key(string culture)
        {
            return View();
        }
        public GenericResult ProcessPredictions()
        {
            var PredictionsForMatch = _context.Predict.ToList();
            return new GenericResult();
            //if (ccvm.Status == enumTeamFixtureStatus.Won || ccvm.Status == enumTeamFixtureStatus.Lost)
            //    //Below is for Game Winning Tosses
            //    foreach (var Predictions in PredictionsForMatch.Where(x => x.TeamId != null).Where(x => x.TeamId == ccvm.ClubId))
            //    {
            //        if (Predictions.Prediction == enumFixturePredict.Win)
            //        {
            //            if (ccvm.Status == enumTeamFixtureStatus.Won)
            //            {
            //                Predictions.PredictionSucceded = true;
            //                Predictions.Result = PredictionResult.Correct;
            //            }
            //            else
            //            {
            //                Predictions.PredictionSucceded = false;
            //                Predictions.Result = PredictionResult.Prediction_Failed;
            //            }
            //        }
            //        else if (ccvm.Status == enumTeamFixtureStatus.Lost)
            //        {
            //            if (ccvm.Status == enumTeamFixtureStatus.Lost)
            //            {
            //                Predictions.PredictionSucceded = true;
            //                Predictions.Result = PredictionResult.Correct;
            //            }
            //            else
            //            {
            //                Predictions.PredictionSucceded = false;
            //                Predictions.Result = PredictionResult.Prediction_Failed;
            //            }
            //        }
            //        _context.Entry(Predictions).State = EntityState.Modified;
            //    }


            foreach (var Predictions in PredictionsForMatch.Where(x => x.TeamId == null))
            {
                if (Predictions.Prediction == enumFixturePredict.Draw)
                {                    
                        Predictions.PredictionSucceded = true;
                        Predictions.Result = PredictionResult.Correct;                                        
                        _context.Entry(Predictions).State = EntityState.Modified;
                }
            }
            _context.SaveChanges();
        }
        public IActionResult Predictions(string Culture)
        {
            SetSiteSectionLanguagetitle(Culture, "layout");
            SetSiteSectionLanguagetitle(Culture, "prediction");
            List<PredictionVMFORES> predictions = new List<PredictionVMFORES>();           
            var users = _context.Users
                .Include(x => x.Predictions)
                .Include(x=>x.Roles)                
                //.OrderByDescending(x=>x.Predictions.Count())
                .ToList();
            List<ApplicationUserVMFORES> topPredictionsToSend = users.Adapt<List<ApplicationUserVMFORES>>();
            ViewBag.TopPredictions = topPredictionsToSend.OrderByDescending(x=>x.Successed_Predictions_Count).Take(5).ToList();
            SetMatch(Culture, 8, "Matches", true);
                SetMyPredictions(Culture);
            return View();
        }
        private void SetMyPredictions(string Culture)
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.MyPredictions = Sportsmandu.Api.Controllers.ProductController.GetPredictions(Culture, User.Identity.Name,_cache).Adapt<List<PredictionVMFORES>>();
            }
        }
        //static IEnumerable<T> Randomize<T>(this IEnumerable<T> source)
        //{
        //    var array = source.ToArray();
        //    // randomize indexes (several approaches are possible)
        //    return array;
        //}
        //[Authorize]
        public IActionResult FBPredictions(string Culture)
        {
            SetSiteSectionLanguagetitle(Culture, "layout");
            SetSiteSectionLanguagetitle(Culture, "prediction");            
            List<PredictionVMFORES> predictions = new List<PredictionVMFORES>();
            var users = _context.Users
                .Include(x => x.Predictions)
                .Include(x=>x.Roles)                
                .ToList();
            if (User.Identity.IsAuthenticated)
            {                
                ViewBag.MyImage = users.Where(x => x.Email == User.Identity.Name).SingleOrDefault().Avatar;
            }
            List<ApplicationUserVMFORES> topPredictionsToSend = users.Adapt<List<ApplicationUserVMFORES>>();
            var topPredictions = topPredictionsToSend.OrderByDescending(x=>x.Successed_Predictions_Count).Take(20).ToList();
            foreach(var items in topPredictions)
            {
                if(items.Avatar == null)
                {
                    items.Avatar = "/images/users/no_profile.jpg";
                }
            }
            ViewBag.TopPredictions = topPredictions;
            SetMatch(Culture, 6, "Matches",true);
            SetMyPredictions(Culture);
            return View();
        }
       public IActionResult Construction(string Culture)
        {
            SetSiteSectionLanguagetitle(Culture, "construction_page");
            return View();
        }
        public void SetSiteSectionLanguagetitle(string Culture,string section)
        {
            //            
            List<SiteSectionLanguageTitleForEs> siteSectionLanguageTitle = Sportsmandu.Api.Controllers.ProductController.SiteSectionLanguageTitle(Culture, section, _cache).ToList();
            foreach (var items in siteSectionLanguageTitle)
            {
                var itsValue = items.LanguageTitleGlobalizations.Where(x => x.CultureCode == Culture).SingleOrDefault();
                this.HttpContext.Items[items.LanguageTitleName] = itsValue == null ? "ITS NULL" : itsValue.Value;
            }
        }
        public IActionResult Game(string Culture,string Id)
        {
            var gamesTosend = _context.Games.ToList();
            var gameTosend = gamesTosend.FirstOrDefault().Adapt<GameViewModel>();
            var highScores = _context.ApplicationUserGames
                .Include(x => x.User)
                .Where(x => x.GameId == gameTosend.Id);

            if(gameTosend.Type == enumGameType.High_Score)
            {
                highScores = highScores.OrderByDescending(x => x.HighScore)
                .Take(5);
            }else if (gameTosend.Type == enumGameType.Level)
            {
                highScores = highScores.OrderByDescending(x => x.Level)
                    .Take(5);
            }
            else if (gameTosend.Type == enumGameType.Points)
            {
                highScores = highScores.OrderByDescending(x => x.Points)
                    .Take(5);
            }
            gameTosend.HighScore = highScores.Adapt<List<ScoreViewModel>>();
            //string gameHtml = System.IO.File.ReadAllText("wwwroot/games/" + gameTosend.FolderUrl + "/" + "PlayMe.html");
            //ViewBag.GameHtml = gameHtml;
            return View(gameTosend);
        }

        [Authorize]
        public IActionResult PlayGame(string Culture, string Id)
        {
            var gamesTosend = _context.Games.ToList();
            var gameTosend = gamesTosend.FirstOrDefault().Adapt<GameViewModel>();
            var highScores = _context.ApplicationUserGames
                .Include(x => x.User)
                .OrderByDescending(x => x.HighScore)
                .Where(x => x.GameId == gameTosend.Id)
                .Take(5);
            gameTosend.HighScore = highScores.Adapt<List<ScoreViewModel>>();
            string gameHtml = System.IO.File.ReadAllText("wwwroot/games/" + gameTosend.FolderUrl + "/" + "PlayMe.html");
            ViewBag.GameHtml = gameHtml;

            var user = _context.Users.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
            var hashids = new Hashids(gameTosend.Guid);
            ViewBag.Salt = hashids;
            return View(gameTosend);
        }
        private List<SeasonMatchViewModel> SetMatch(string Culture,int Count, string ViewBagName,bool futureMatch = false)
        {
            List<SeasonMatchViewModel> matches = Sportsmandu.Api.Controllers.ProductController.AllSeasonMatch(Culture, _cache).OrderByDescending(x => x.Date).ToList(); // TODO Filter and all shit ;
            if (futureMatch)
            {
                ViewData[ViewBagName] = 
                    matches.Where(x => x.Status == enumFixtureStatus.Not_Started.ToString())
                    .Where(x=>x.Date > DateTime.Now)
                    .Where(x=>x.Season.Name != "Sportsmandu Cup")
                    .Where(x=>x.Category.Name != "Football")
                    .OrderBy(x=>x.Date)
                    .Where(x => x.Teams.Count() > 1).Take(Count).ToList(); 
            }
            else
            { 
                ViewData[ViewBagName] = matches
                    .Where(x => x.Status != enumFixtureStatus.Not_Started.ToString())
                    .Take(Count).ToList();
            }
            return matches;
        }
        public IActionResult Index(string Culture)
        {
            //Game g = new Game
            //{
            //    Name = "Duck Hunt",
            //    Guid = Global.UniqueGuid("Games", _context),
            //    CreatedById = 1,
            //    CultureId = 1,
            //    Verified = true
            //};
            //_context.Entry(g).State = EntityState.Added;
            //_context.SaveChanges();
            //g.multilingualid = g.Id;
            //_context.Entry(g).State = EntityState.Modified;
            //_context.SaveChanges();
                
            //var P = Sportsmandu.Api.Controllers.ProductController.GetCurrentRunningSeason(Culture, _cache,8);
            
            
            SetSiteSectionLanguagetitle(Culture, "layout");
            //SetSiteSectionLanguagetitle(Culture, "homepage");

            //SetPlayers(Culture);
            //SetMatch(Culture,7, "FutureMatches",true);
            //SetMatch(Culture, 7, "CompletedMatches",false);
            ////ViewBag.Grounds = Sportsmandu.Api.Controllers.ProductController.GetGrounds(Culture,_cache).Where(x=>x.Thumbnail != null).Where(x=>x.CountryName == "Nepal").Take(3).ToList();
            //var news = Sportsmandu.Api.Controllers.ProductController.AllBlogPost(Culture,_cache);
            //if (news != null) ViewBag.News = news.Take(3).ToList();
            ////ViewBag.Video = Sportsmandu.Api.Controllers.ProductController.GetVideos("En",_cache).ToList().OrderByDescending(x=>x.UploadDate).Take(4).ToList(); // TODO Filter and all shit ;
            //SetMyPredictions(Culture);


            return View();
        }

        public IActionResult Videos()
        {
            List<VideoVMForEs> Videos = Sportsmandu.Api.Controllers.ProductController.GetVideos("En",_cache).ToList().OrderByDescending(x=>x.UploadDate).Take(5000).ToList(); // TODO Filter and all shit ;
            return View(Videos);
        }
        public IActionResult Video(string culture,string id)
        {
            VideoVMForEs Video = Sportsmandu.Api.Controllers.ProductController.GetVideo("En", _cache,id);
            return View(Video);
        }
        //private List<PlayerViewModel> GetPlayersforViewBag(string Culture)
        //{
        //    return Sportsmandu.Api.Controllers.ProductController.GetPlayersForHomePage(Culture,_cache).ToList();
        //}
        public IActionResult Contact(string culture)
        {
            //SetSiteSectionLanguagetitle(culture, "contact");
            return Advertise(culture);
        }
        public IActionResult RegisterEvent(string culture)
        {
            //SetSiteSectionLanguagetitle(culture, "contact");
            return View();
            //return Advertise(culture);
        }
        public IActionResult Apply(string culture)
        {
            //SetSiteSectionLanguagetitle(culture, "contact");
            return Advertise(culture);
        }
        public IActionResult Advertise(string culture)
        {
            SetSiteSectionLanguagetitle(culture, "contact");
            return View("Advertise");
        }        
        public dynamic AuditReport(string culture, string Id)
        {
            var pves = Sportsmandu.Api.Controllers.ProductController.GetAuditReport(Id);
            return pves;
        }
        public IActionResult Player(string culture, string id)
        {
            SetSiteSectionLanguagetitle(culture, "player_single");           
            PlayerViewModelEs pves = Sportsmandu.Api.Controllers.ProductController.GetPlayerDetails(culture, id.ToString());
            return View(pves);
        }
        public IActionResult Ground(string culture, string id)
        {       
            Model.Entities.Ground.GroundVMFORES pves = Sportsmandu.Api.Controllers.ProductController.GetGroundDetails(culture, id.ToString());
            ViewBag.Matches = Sportsmandu.Api.Controllers.ProductController.AllSeasonMatch(culture,_cache).Where(x => x.Ground.Guid == id).ToList();
            return View(pves);
        }
        public IActionResult Venue(string culture, string id)
        {
            SetSiteSectionLanguagetitle(culture, "layout");
            SetSiteSectionLanguagetitle(culture, "venue");
            Model.Entities.Ground.GroundVMFORES pves = Sportsmandu.Api.Controllers.ProductController.GetGroundDetails(culture, id.ToString());
            return View(pves);
        }
        public IActionResult Season(string culture, string id)
        {
            SetSiteSectionLanguagetitle(culture, "layout");
            SetSiteSectionLanguagetitle(culture, "season");
            SeasonVMFORES pves = Sportsmandu.Api.Controllers.ProductController.GetSeasonDetails(culture, id.ToString());
            //pves.Standings = Sportsmandu.Api.Controllers.ProductController.GetSeasonStandings(culture, id.ToString());
            pves.Stats = Sportsmandu.Api.Controllers.ProductController.GetSeasonStats(culture, id.ToString());
            IEnumerable<SeasonMatchViewModel> Fixtures = Sportsmandu.Api.Controllers.ProductController
                .SeasonMatchForSeason(culture, id)
                .Where(x => x.Status == enumFixtureStatus.Not_Started.ToString())
                    .Where(x => x.Date > DateTime.Now)
                    .OrderBy(x => x.Date)
                    .Where(x => x.Teams.Count() > 1).Take(10).ToList();
            ViewBag.FutureMatches = Fixtures;
            ViewBag.CompletedMatches = Sportsmandu.Api.Controllers.ProductController
                                    .SeasonMatchForSeason(culture, id)
                                    .OrderByDescending(x => x.Date)
                                    .Where(x => x.Status != enumFixtureStatus.Not_Started.ToString())
                                    .Take(10).ToList();
            //ViewBag.SeasonGroups = _context.SeasonCategoryClubGroups
            //    .Include(x => x.Club)
            //    .ThenInclude(x => x.Country)
            //    .Include(x => x.Club)
            //    .ThenInclude(x => x.Culture)
            //    .Include(x => x.Category).Include(x => x.Season).ToList();
            return View(pves);
        }
        public IActionResult Association(string culture, string id)
        {
            SetSiteSectionLanguagetitle(culture, "layout");
            SetSiteSectionLanguagetitle(culture, "association");
            AssociationVMFORES pves = Sportsmandu.Api.Controllers.ProductController.GetAssociationDetails(culture, id.ToString());

            return View(pves);
        }
        public IActionResult Team(string culture, string Id)
        {
            SetSiteSectionLanguagetitle(culture, "layout");
            SetSiteSectionLanguagetitle(culture, "team");
            ClubViewModelFORES pves = Sportsmandu.Api.Controllers.ProductController.GetClubDetail(culture, Id.ToString());
            //SetMatch(culture, 7, "FutureMatches", true);
            //SetMatch(culture, 10, "CompletedMatches", false);
            IEnumerable<SeasonMatchViewModel> Fixtures = Sportsmandu.Api.Controllers.ProductController
                .SeasonMatchForTeam(culture, pves.Name)
                .Where(x => x.Status == enumFixtureStatus.Not_Started.ToString())
                    .Where(x => x.Date > DateTime.Now)
                    .OrderBy(x => x.Date)
                    .Where(x => x.Teams.Count() > 1).Take(10).ToList();
            ViewBag.FutureMatches = Fixtures;
            ViewBag.CompletedMatches = Sportsmandu.Api.Controllers.ProductController
                                    .SeasonMatchForTeam(culture, pves.Name)
                                    .OrderByDescending(x => x.Date)
                                    .Where(x => x.Status != enumFixtureStatus.Not_Started.ToString())
                                    .Take(10).ToList();

            return View(pves);
        }
        public IActionResult News(string culture)
        {
            SetSiteSectionLanguagetitle(culture, "layout");
            SetSiteSectionLanguagetitle(culture, "news");
            var News = Sportsmandu.Api.Controllers.ProductController.AllBlogPost(culture,_cache);
            if (News != null) News = News.Take(40).ToList();
            return View(News);
        }
        public IActionResult TestPage(string culture)
        {
            ViewBag.SeasonGroups = _context.SeasonCategoryClubGroups
                .Include(x=>x.Club)
                .ThenInclude(x=>x.Country)
                .Include(x => x.Club)
                .ThenInclude(x => x.Culture)
                .Include(x=>x.Category).Include(x=>x.Season).ToList();
            return View();
        }
        public IActionResult FixtureDetails(string culture,string id)
        {
            SetSiteSectionLanguagetitle(culture, "layout");
            SetSiteSectionLanguagetitle(culture, "homepage");            
            List<PredictionVMFORES> predictions = Sportsmandu.Api.Controllers.ProductController.GetPredictionsFixture(culture, id,_cache);
            var teamIds = predictions.Select(x => x.TeamId).Distinct();
            int totalPredictions = predictions.Count();
            var i = 0;
            foreach (var teamId in teamIds)
            {
                i = i + 1;
                int votedForThisTeam = 0;
                if (teamId == null)
                {//Draw
                    votedForThisTeam = predictions.Where(x => x.TeamId == null).Count();
                }
                else
                {
                    votedForThisTeam = predictions.Where(x => x.TeamId == teamId).Count();
                }
                decimal winningPercentageForThisTeam = ((decimal)votedForThisTeam / (decimal)totalPredictions) / (decimal)0.01;
                ViewData["team" + i.ToString()] = winningPercentageForThisTeam;
                string teamName = string.Empty;
                teamName = teamId == null ? "Draw" : predictions.Select(x => x.Team).Where(x => x.Id == teamId).FirstOrDefault().Name;
                ViewData["team" + i.ToString() + "Name"] = teamName;
            }
            ViewBag.Predictions = predictions;
            SeasonMatchViewModel match = Sportsmandu.Api.Controllers.ProductController.SeasonMatch(culture, id);
            return View(match);
        }
        //public IActionResult TestPageClub(string culture, string Id)
        //{
        //    ClubViewModelFORES pves = Sportsmandu.Api.Controllers.ProductController.GetClubDetail(culture, Id.ToString());
        //    IEnumerable<SeasonMatchViewModel> Fixtures = Sportsmandu.Api.Controllers.ProductController.SeasonMatchForTeam(culture, pves.Name).Take(10).ToList();
        //    ViewBag.Fixtures = Fixtures;
        //    return View(pves);
        //}
        public IActionResult NewsDetails(string culture, string Id)
        {
            SetSiteSectionLanguagetitle(culture, "layout");
            var News = Sportsmandu.Api.Controllers.ProductController.GetBlogPostDetails(culture,Id);
            ViewBag.RecentPost = Sportsmandu.Api.Controllers.ProductController.AllBlogPost(culture,_cache)
                .Where(x=>x.Guid != News.Guid)
                .Take(12)
                .ToList();
            SetPlayers(culture);
            return View(News);
        }

        private void SetPlayers(string Culture,int count = 6)
        {

            List<PlayerViewModel> players  = Sportsmandu.Api.Controllers.ProductController.GetCurrentRunningSeason(Culture, _cache, count);
            ViewBag.Players = players;
        }
        [HttpPost]
        public SearchIndexViewModel Search([FromBody] SearchViewModel svm) // id =  route TODO Searchterm ho yo
        {           
            var pves = Sportsmandu.Api.Controllers.ProductController.Search(svm);
            return pves;
        }
        public IActionResult Privacy(string Culture)
        {
            SetSiteSectionLanguagetitle(Culture, "layout");
            SetSiteSectionLanguagetitle(Culture, "privacy");
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public List<string> GetUrlForBot()
        {
            return Global.BOTURL(_context);
        }
        public void FillInGuid()
        {
            var AllSeasons = _context.Leagues.ToList();
            foreach (var Season in AllSeasons.Where(x => x.Guid == null))
            {
                var Guid = Global.UniqueGuid("Leagues", _context);
                Season.Guid = Guid;
                try
                {
                    _context.Entry(Season).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                catch (Exception) { }
            }
        }
        //public void DeletePredictions()
        //{
        //    foreach(var items in _context.Predict)
        //    {
        //        _context.Remove(items);
        //    }
        //    _context.SaveChanges();
        //}
        public void fillConvertedCulture()
        {
            var grounds = _context.Clubs.OrderByDescending(x=>x.CreatedDate).ToList();
            foreach(var Club in grounds)
            {
                ClubConvertedCulture pc = new ClubConvertedCulture
                {
                    CultureId = Club.CultureId.Value,
                    ClubId = Club.multilingualid,
                    Club = null,
                    Culture = null
                };
               
                try
                {
                    _context.Entry(pc).State = EntityState.Added;
                    _context.ClubConvertedCulture.Add(pc);
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {

                }
               
            }
        }

        public void setGamestoCompleted()
        {
            foreach(var items in _context.SeasonFixture.Where(x=>x.Date < DateTime.Now)){
                items.Status = enumFixtureStatus.Completed;
                _context.Entry(items).State = EntityState.Modified;
            }
            _context.SaveChanges();
        }
        public void ProcessImages()
        {
            foreach (var Items in _context.Clubs.ToList()) //.Where(x=>x.Image != null)){
            { 
                //if (!Items.Image.Contains("Testing"))
                {
                    try
                    {
                    //Items.Image=Items.Image.Replace("http://Sportsmandu.ngrok.io/", "");
                    if(Items.Guid == null)
                    {
                        var Guid = Global.UniqueGuid("Seasons", _context);
                            Items.Guid = Guid;
                    }
                    //    Items.CoverImage = Global.FrontEndUseChangeBackground(Items.Image,enumImageSegment.player_coverImage);
                    Items.Thumbnail = Global.FrontEndUseChangeBackground(Items.Image, enumImageSegment.player_profile);
                    _context.Entry(Items).State = EntityState.Modified;
                    }
                    catch (Exception) { }
                }
            }
                _context.SaveChanges();
        }
        private int? GetUserId(string Email)
        {
            var user = _context.Users.SingleOrDefault((x => x.UserName == Email));
            if (user == null)
            {
                return null;
            }
            else
            {
                return user.Id;
            }
        }
    }
}

//public void ProcessAllImages()
//{
//    bool reset = false;
//    int processForCulture = 1;
//    foreach (var Player in _context.Players.Where(x => x.CultureId == processForCulture))
//    {
//        if (Player.Image == null)
//        {
//            _context.Entry(Player).State = EntityState.Modified;
//            Player.Image = LoadImageSizes().Where(x => x.enumImageSegment == enumImageSegment.player_profile).SingleOrDefault().DefaultSavePath + Player.Name + ".jpg";
//        }
//        else
//        {
//            _context.Entry(Player).State = EntityState.Modified;
//            Player.Image = Player.Image.Replace("~/", "");
//            Player.CoverImage = Player.CoverImage.Replace("~/", "");
//            Player.Thumbnail = Player.Thumbnail.Replace("~/", "");
//        }
//    }
//    foreach (var Club in _context.Clubs.Where(x => x.CultureId == processForCulture))
//    {
//        if (Club.Image == null)
//        {
//            _context.Entry(Club).State = EntityState.Modified;
//            Club.Image = LoadImageSizes().Where(x => x.enumImageSegment == enumImageSegment.club_profile).SingleOrDefault().DefaultSavePath + Club.Name + ".jpg";
//        }
//        else
//        {
//            _context.Entry(Club).State = EntityState.Modified;
//            Club.Image = Club.Image.Replace("~/", "");
//            Club.CoverImage = Club.CoverImage.Replace("~/", "");
//            Club.Thumbnail = Club.Thumbnail.Replace("~/", "");
//        }
//    }
//    foreach (var Season in _context.Seasons.Where(x => x.CultureId == processForCulture))
//    {
//        if (Season.Image == null)
//        {
//            _context.Entry(Season).State = EntityState.Modified;
//            Season.Image = LoadImageSizes().Where(x => x.enumImageSegment == enumImageSegment.season_profile).SingleOrDefault().DefaultSavePath + Season.Name + ".jpg";
//        }
//        else
//        {
//            _context.Entry(Season).State = EntityState.Modified;
//            Season.Image = Season.Image.Replace("~/", "");
//            Season.CoverImage = Season.CoverImage.Replace("~/", "");
//            Season.Thumbnail = Season.Thumbnail.Replace("~/", "");
//        }
//    }
//    foreach (var League in _context.Leagues.Where(x => x.CultureId == processForCulture))
//    {
//        if (League.Image == null)
//        {
//            _context.Entry(League).State = EntityState.Modified;
//            League.Image = LoadImageSizes().Where(x => x.enumImageSegment == enumImageSegment.league_profile).SingleOrDefault().DefaultSavePath + League.Name + ".jpg";
//        }
//        else
//        {
//            _context.Entry(League).State = EntityState.Modified;
//            League.Image = League.Image.Replace("~/", "");
//            League.CoverImage = League.CoverImage.Replace("~/", "");
//            League.Thumbnail = League.Thumbnail.Replace("~/", "");
//        }
//    }
//    foreach (var Ground in _context.Grounds.Where(x => x.CultureId == processForCulture))
//    {
//        if (Ground.Image == null)
//        {
//            _context.Entry(Ground).State = EntityState.Modified;
//            Ground.Image = LoadImageSizes().Where(x => x.enumImageSegment == enumImageSegment.ground_profile).SingleOrDefault().DefaultSavePath + Ground.Name + ".jpg";
//        }
//        else
//        {
//            _context.Entry(Ground).State = EntityState.Modified;
//            Ground.Image = Ground.Image.Replace("~/", "");
//            Ground.CoverImage = Ground.CoverImage.Replace("~/", "");
//            Ground.Thumbnail = Ground.Thumbnail.Replace("~/", "");
//        }
//    }
//    _context.SaveChanges();
//    var PlayerImageSettings = LoadImageSizes().Where(x => x.enumImageSegment == enumImageSegment.player_profile).SingleOrDefault();
//    foreach (var Player in _context.Players.Where(x => x.CultureId == processForCulture))
//    {
//        if (reset) Player.Thumbnail = null;
//        if (Player.Thumbnail == null)
//        {
//            string imageLocation = string.Empty;
//            if (Player.Image != null)
//            {
//                imageLocation = Player.Image.Replace("http://Sportsmandu.ngrok.io", "wwwroot");
//            }
//            else
//            {
//                imageLocation = PlayerImageSettings.DefaultNoImagePath;
//            }
//            using (SixLabors.ImageSharp.Image<SixLabors.ImageSharp.PixelFormats.Rgba32> image = SixLabors.ImageSharp.Image.Load(imageLocation))
//            {
//                image.Mutate(x => x
//                     .Resize(PlayerImageSettings.Width, PlayerImageSettings.Width)
//                     );
//                //.Grayscale());
//                Player.Thumbnail = Global.GetCleanUrl(PlayerImageSettings.DefaultSavePath + Player.Name + ".jpg");
//                image.Save(Player.Thumbnail);
//                Player.Thumbnail = Player.Thumbnail.Replace("Wwwroot/", "");
//            }
//            var CoverImageSettings = LoadImageSizes().Where(x => x.enumImageSegment == enumImageSegment.player_coverImage).SingleOrDefault();
//            Image<Rgba32> img1 = Image.Load(imageLocation);
//            Player.CoverImage = GetCoverImage(img1, Global.GetCleanUrl(CoverImageSettings.DefaultSavePath + Player.Name + ".jpg"), CoverImageSettings);
//            _context.Entry(Player).State = EntityState.Modified;
//        }
//    }
//    _context.SaveChanges();

//    var ClubImageSettings = LoadImageSizes().Where(x => x.enumImageSegment == enumImageSegment.club_profile).SingleOrDefault();
//    foreach (var Club in _context.Clubs.Include(x => x.Players).ThenInclude(x => x.Player).Where(x => x.CultureId == processForCulture).ToList())
//    {
//        if (reset) Club.Thumbnail = null;
//        if (Club.Thumbnail == null)
//        {
//            string imageLocation = string.Empty;
//            if (Club.Image != null)
//            {
//                imageLocation = Club.Image.Replace("http://Sportsmandu.ngrok.io", "wwwroot");
//            }
//            else
//            {
//                imageLocation = ClubImageSettings.DefaultNoImagePath;
//            }
//            using (SixLabors.ImageSharp.Image<SixLabors.ImageSharp.PixelFormats.Rgba32> image = SixLabors.ImageSharp.Image.Load(imageLocation))
//            {
//                image.Mutate(x => x
//                     .Resize(ClubImageSettings.Width, ClubImageSettings.Width)
//                     );
//                //.Grayscale());
//                Club.Thumbnail = Global.GetCleanUrl(ClubImageSettings.DefaultSavePath + Club.Guid + ".jpg");
//                image.Save(Club.Thumbnail);
//                Club.Thumbnail = Club.Thumbnail.Replace("Wwwroot/", "");
//            }
//            //if(Club.CoverImage == null)
//            {
//                var CoverImageSettings = LoadImageSizes().Where(x => x.enumImageSegment == enumImageSegment.club_coverImage).SingleOrDefault();

//                List<Image<Rgba32>> clubPlayerImages = new List<Image<Rgba32>>();
//                foreach (var ClubPlayers in Club.Players.Where(x => x.Player.Image != null))
//                {
//                    Image<Rgba32> img1 = Image.Load(ClubPlayers.Player.Image.Replace("http://Sportsmandu.ngrok.io", "wwwroot"));
//                    clubPlayerImages.Add(img1);
//                }
//                if (clubPlayerImages.Count == 0)
//                {
//                    Image<Rgba32> img1 = Image.Load(imageLocation);
//                    Club.CoverImage = GetCoverImage(img1, Global.GetCleanUrl(CoverImageSettings.DefaultSavePath + Club.Name + ".jpg"), CoverImageSettings);
//                }
//                else
//                {
//                    Club.CoverImage = GetCoverImage(null, Global.GetCleanUrl(CoverImageSettings.DefaultSavePath + Club.Name + ".jpg"), CoverImageSettings, clubPlayerImages);
//                }
//            }
//            _context.Entry(Club).State = EntityState.Modified;
//            //_context.SaveChanges();
//        }
//    }
//    _context.SaveChanges();


//    var SeasonImageSettings = LoadImageSizes().Where(x => x.enumImageSegment == enumImageSegment.club_profile).SingleOrDefault();
//    foreach (var Season in _context.Seasons.Include(x => x.Clubs).ThenInclude(x => x.Club).Where(x => x.CultureId == processForCulture).ToList())
//    {
//        if (reset) Season.Thumbnail = null;
//        if (Season.Thumbnail == null)
//        {
//            string imageLocation = string.Empty;
//            if (Season.Image != null)
//            {
//                imageLocation = Season.Image.Replace("http://Sportsmandu.ngrok.io", "wwwroot");
//            }
//            else
//            {
//                imageLocation = SeasonImageSettings.DefaultNoImagePath;
//            }
//            using (SixLabors.ImageSharp.Image<SixLabors.ImageSharp.PixelFormats.Rgba32> image = SixLabors.ImageSharp.Image.Load(imageLocation))
//            {
//                image.Mutate(x => x
//                     .Resize(SeasonImageSettings.Width, SeasonImageSettings.Width)
//                     );
//                Season.Thumbnail = Global.GetCleanUrl(SeasonImageSettings.DefaultSavePath + Season.Name + ".jpg");
//                image.Save(Season.Thumbnail);
//                Season.Thumbnail = Season.Thumbnail.Replace("Wwwroot/", "");
//            }
//            //if(Club.CoverImage == null)
//            {
//                var SeasonCoverImageSettings = LoadImageSizes().Where(x => x.enumImageSegment == enumImageSegment.season_coverImage).SingleOrDefault();
//                List<Image<Rgba32>> SeasonClubImages = new List<Image<Rgba32>>();
//                foreach (var ClubSeason in Season.Clubs.Where(x => x.Club.Image != null))
//                {
//                    Image<Rgba32> img1 = Image.Load(ClubSeason.Club.Image.Replace("http://Sportsmandu.ngrok.io", "wwwroot"));
//                    SeasonClubImages.Add(img1);
//                }
//                if (SeasonClubImages.Count == 0)
//                {
//                    Image<Rgba32> img1 = Image.Load(imageLocation);
//                    Season.CoverImage = GetCoverImage(img1, Global.GetCleanUrl(SeasonCoverImageSettings.DefaultSavePath + Season.Name + ".jpg"), SeasonCoverImageSettings);
//                }
//                else
//                {
//                    Season.CoverImage = GetCoverImage(null, Global.GetCleanUrl(SeasonCoverImageSettings.DefaultSavePath + Season.Name + ".jpg"), SeasonCoverImageSettings, SeasonClubImages);
//                }
//            }
//            _context.Entry(Season).State = EntityState.Modified;
//        }
//    }
//    _context.SaveChanges();
//}