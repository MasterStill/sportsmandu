﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using AutoMapper;
using Mapster;
//using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Models;
namespace Sportsmandu.Controllers
{
    [Route("Api/Globalization/")]
    [Produces("application/json")]
    public class GlobalizationApiController : Controller
    {
        static HttpClient client = new HttpClient();
        public AppDbContext _context;
        private IMapper _mapper { get; set; }
        Sportsmandu.Services.IEmailSender _emailSender;
        public GlobalizationApiController(AppDbContext _context, IMapper _mapper, Sportsmandu.Services.IEmailSender _emailSender)
        {
            this._context = _context;
            this._mapper = _mapper;
        }

        [HttpPost("AddNewLanguageTitle")]
        public GenericResult AddNewLanguageTitle([FromBody] LanguageTitle LanguageTitle)
        {
            if (LanguageTitle == null)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "CCVM is null ! Please Contact Administrator"
                };
            }
            // LanguageTitle LanguageTitle = LanguageTitle.Adapt<LanguageTitle>();
            if (!Authorization.Authorized(enumClaimTypeSegments.Clubs.ToString(), "Edit", 0, User.Claims)) return (Global.AccessDenied());
            if (ModelState.IsValid)
            {
                try
                {
                    var resss = checkIfLanguageTitleExists(LanguageTitle);
                    Debug.WriteLine("This is ress");
                    Debug.WriteLine(LanguageTitle);
                    if (resss == null)
                    {
                        //_context.Entry(LanguageTitle).State = EntityState.Added;
                        //#NEW_context.Database.ExecuteSqlCommand($"insert into LanguageTitles (name) values({LanguageTitle.Name})");
                        //_context.LanguageTitles.Add(LanguageTitle);
                        _context.SaveChanges();
                    }
                    else
                    {
                        _context.Entry(LanguageTitle).State = EntityState.Modified;
                        //Bholi ko day ma kei add garna parne huna sakcha here like when was the category started and shit 
                        _context.Update(resss);
                        _context.SaveChanges();
                    }
                    return new GenericResult()
                    {
                        Succeded = true,
                        Message = "Site Section Added"
                    };
                }
                catch (Exception ex)
                {
                    return new GenericResult()
                    {
                        Succeded = false,
                        Message = ex.Message
                    };
                }
            }
            else
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "ModalState Invalid"
                };
            }
        }
        private LanguageTitle checkIfLanguageTitleExists(LanguageTitle LanguageTitle)
        {
            _context.Entry(LanguageTitle).State = EntityState.Modified;
            var result = _context.LanguageTitles.Where(x => x.Id == LanguageTitle.Id)
            .SingleOrDefault();
            if (result != null)
            {
                return result;
            }
            return null;
        }
        [HttpGet("GetAllSiteSections")]
        public List<SiteSection> GetAllSiteSection()
        {
            return _context.SiteSections.ToList();
        }
        [HttpGet("GetAllLanguageTitles")]
        public List<LanguageTitle> GetAllLanguageTitles()
        {
            return _context.LanguageTitles.ToList();
        }
        [HttpGet("GetAllSiteSectionLanguageTitle")] // For Globalization Admin Panel
        public List<SiteSectionLanguageTitle> GetAllSiteSectionLanguageTitle()
        {
            var result = _context.SiteSectionLanguageTitles
                 //.Include(x=>x.LanguageTitle)
                 //.ThenInclude(x => x.SiteSection)
                .ToList();
            //var result = Sportsmandu.Api.Controllers.ProductController.GetAllSiteSectionLanguageTitle().ToList();
            return result;
        }

        
        [HttpPost("AddNewSiteSection")]
        public GenericResult AddNewSiteSection([FromBody] SiteSection SiteSection)
        {
            if (SiteSection == null)
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "CCVM is null ! Please Contact Administrator"
                };
            // SiteSection SiteSection = SiteSection.Adapt<SiteSection>();
            if (!Authorization.Authorized(enumClaimTypeSegments.Clubs.ToString(), "Edit", 0, User.Claims)) return (Global.AccessDenied());
            if (ModelState.IsValid)
            {
                try
                {
                    var resss = checkIfSiteSectionExists(SiteSection);
                    Debug.WriteLine("This is ress");
                    Debug.WriteLine(SiteSection);
                    if (resss == null)
                    {
                        SiteSection.LanguageTitles = null;
                        //#NEW_context.Database.ExecuteSqlCommand($"insert into SiteSections (name) values({SiteSection.Name})");
                        _context.SaveChanges();
                        //_context.Entry(SiteSection).State = EntityState.Added;
                        //_context.SiteSections.Add(SiteSection);
                        //_context.SaveChanges();
                    }
                    else
                    {
                        _context.Entry(SiteSection).State = EntityState.Modified;
                        //Bholi ko day ma kei add garna parne huna sakcha here like when was the category started and shit 
                        _context.Update(resss);
                        _context.SaveChanges();
                    }
                    return new GenericResult()
                    {
                        Succeded = true,
                        Message = "Site Section Added"
                    };
                }
                catch (Exception ex)
                {
                    return new GenericResult()
                    {
                        Succeded = false,
                        Message = ex.Message
                    };
                }
            }
            else
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "ModalState Invalid"
                };
            }
        }
        [HttpPost("AddNewSiteSectionLanguageTitle")]
        public GenericResult AddNewSiteSectionLanguageTitle([FromBody] SiteSectionLanguageTitle SiteSection)
        {
            if (SiteSection == null)
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "CCVM is null ! Please Contact Administrator"
                };
            // SiteSection SiteSection = SiteSection.Adapt<SiteSection>();
            if (!Authorization.Authorized(enumClaimTypeSegments.Globalizations.ToString(), "Edit", 0, User.Claims)) return (Global.AccessDenied());
            if (ModelState.IsValid)
            {
                try
                {
                    var resss = checkIfSiteSectionLanguageTitleExists(SiteSection);
                    Debug.WriteLine("This is ress");
                    Debug.WriteLine(SiteSection);
                    if (resss == null)
                    {
                        SiteSection.LanguageTitle = null;
                        SiteSection.SiteSection = null;
                        _context.Entry(SiteSection).State = EntityState.Added;
                        _context.SiteSectionLanguageTitles.Add(SiteSection);
                        _context.SaveChanges();
                        //_context.Database.ExecuteSqlCommand($"insert into SiteSectionLanguageTitles (sitesectionid,languagetitleid) values({SiteSection.SiteSectionId},{SiteSection.LanguageTitleId})");
                        //_context.SaveChanges();
                    }
                    else
                    {
                        _context.Entry(SiteSection).State = EntityState.Modified;
                        //Bholi ko day ma kei add garna parne huna sakcha here like when was the category started and shit 
                        _context.Update(resss);
                        _context.SaveChanges();
                    }
                    return new GenericResult()
                    {
                        Succeded = true,
                        Message = "Site Section Added"
                    };
                }
                catch (Exception ex)
                {
                    return new GenericResult()
                    {
                        Succeded = false,
                        Message = ex.Message
                    };
                }
            }
            else
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "ModalState Invalid"
                };
            }
        }
        private SiteSection checkIfSiteSectionExists(SiteSection SiteSection)
        {
            _context.Entry(SiteSection).State = EntityState.Modified;
            var result = _context.SiteSections.AsNoTracking().Where(x => x.Id == SiteSection.Id)
            .SingleOrDefault();
            if (result != null)
            {
                return result;
            }
            return null;
        }
        private SiteSectionLanguageTitle checkIfSiteSectionLanguageTitleExists(SiteSectionLanguageTitle SiteSection)
        {
            _context.Entry(SiteSection).State = EntityState.Modified;
            var result = _context.SiteSectionLanguageTitles.AsNoTracking().Where(x => x.SiteSectionId == SiteSection.SiteSectionId)
                .Where(x=>x.LanguageTitleId == SiteSection.LanguageTitleId)
            .SingleOrDefault();
            if (result != null)
            {
                return result;
            }
            return null;
        }
        private string GetUserName()
        {
            string UserName = User.Identity.Name;
            //if (UserName == null)
            //{
            //    UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            //}
            return UserName;
        }
        private int GetUserId()
        {
            string UserName = User.Identity.Name;
            //if (UserName == null)
            //{
            //    UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            //}
            int UserID = _context.Users.SingleOrDefault(x => x.UserName == UserName).Id;
            return UserID;
        }
    }
}