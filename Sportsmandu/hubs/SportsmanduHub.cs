using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using Sportsmandu.Data;
using Sportsmandu.Models;
using Sportsmandu.Models.Generic;
using Mapster;
using Sportsmandu.Models.League;
using Microsoft.Extensions.Caching.Memory;
using System.Net;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Model.Entities.Player;
using System.Collections.Generic;
namespace Sportsmandu.Hubs
{
    public class SportsmanduHub : Hub
    {
        AppDbContext _context;
        private IMemoryCache _memoryCache;
        private IHubContext<SportsmanduHub> _connectionManager;
        private IHttpContextAccessor _accessor;
        public static List<SuperVisor> _superVisors = new List<SuperVisor>();
        public SportsmanduHub(IHttpContextAccessor accessor,AppDbContext _context, IHubContext<SportsmanduHub> _connectionManager, IMemoryCache MemoryCache)
        {
            this._context = _context;
            this._connectionManager = _connectionManager;
            this._memoryCache = MemoryCache;
            _accessor = accessor;
        }
        public void IAmConsole()
        {
            Groups.AddToGroupAsync(Context.ConnectionId, "console");
        }        
        public void IAmSupervisorForAutomation(MoreInfoAboutAutmationSupervisor MoreInfo)
        {
                //##TODO instead needa do using roles rather than generic ones.
                Groups.AddToGroupAsync(Context.ConnectionId, "AutomationSupervisor" + MoreInfo.Role.ToString());
                _superVisors.Add(new SuperVisor
                {
                    ConnectionId = Context.ConnectionId,
                    Role = MoreInfo.Role
                });
        }
        public void IAmScrapper()
        {
            Groups.AddToGroupAsync(Context.ConnectionId, "scrapper");
        }
        public void IAmTelegram()
        {
            Groups.AddToGroupAsync(Context.ConnectionId, "telegram");
        }
        public void IAmDiscord(string MoreInfo)
        {
            Groups.AddToGroupAsync(Context.ConnectionId, "discord");
        }
        public void IAmKey()
        {
            Groups.AddToGroupAsync(Context.ConnectionId, "key");
        }
        //private List<SeasonMatchEvent> _seasonEventMatch = new List<SeasonMatchEvent>(); 
        public void RegisterMe(string culture)
        {
            if (Context.User.Identity.IsAuthenticated)
            {
                Groups.AddToGroupAsync(Context.ConnectionId, Context.User.Identity.Name + culture);
            }
            else
            {
                Groups.AddToGroupAsync(Context.ConnectionId, "Guest");
            }
        }
        public override async Task OnDisconnectedAsync(Exception? ex)
        {
            var connectionId = Context.ConnectionId;
            if(_superVisors.Where(x=>x.ConnectionId == connectionId).Any()){
                _superVisors.Remove(_superVisors.Where(x => x.ConnectionId == connectionId).SingleOrDefault());
            }
            base.OnDisconnectedAsync(ex);
        }
        public override Task OnConnectedAsync()
        {

            //var seasonMatch = _context.SeasonFixture
            //        .Include(x => x.Season)
            //        .Include(x => x.Ground)
            //        .Include(x => x.Teams)
            //        //.Include(x=>x.TeamOneId)
            //        //.Include(x => x.TeamOne)
            //        //.ThenInclude(x=>x.Players)
            //        //.ThenInclude(x => x.Player)
            //        //.Include(x => x.TeamTwo)
            //        //.ThenInclude(x => x.Players)
            //        //.ThenInclude(x => x.Player)
            //        .Include(x => x.Category)
            //        .FirstOrDefault();

            //List<SeasonMatchPlayers> seasonMatchPlayers = _context.SeasonMatchPlayers
            //                        //.Where(x => x.SeasonFixtureGuid == seasonMatch.Guid)
            //                        .ToList();



            //var SeasonMatchEvent = _context.SeasonMatchEvent
            //        .Include(x => x.Player)
            //        .Include(x => x.SeasonMatch)
            //        .Include(x => x.Club).ToList();

            ////TypeAdapterConfig<SeasonMatch, SeasonMatchViewModelForJhole>
            ////    .NewConfig()
            ////    .Map(dest => dest.TeamOne.Players, src => src.TeamOne.Players);

            ////TypeAdapterConfig<SeasonMatch, SeasonMatchViewModelForJhole>
            ////    .NewConfig()
            ////    .Map(dest => dest.TeamOne,
            ////         src => new TeamViewModelForJHOLE
            ////         {
            ////             Players = src.TeamOne.Players.Select(x => x.Player).ToList(),
            ////         });
            //SeasonMatchViewModelForJhole seasonMatchToSend = seasonMatch.Adapt<SeasonMatchViewModelForJhole>();
            //List<Player> TeamOnePlayer = _context.ClubPlayers.Where(x => x.ClubId == seasonMatchToSend.TeamTwo.Id).Where(x => x.Player.CategoryId == seasonMatch.CategoryId).Select(x => x.Player).ToList().Take(11).ToList();
            //List<Player> TeamTwoPlayer = _context.ClubPlayers.Where(x => x.ClubId == seasonMatchToSend.TeamOne.Id).Where(x => x.Player.CategoryId == seasonMatch.CategoryId).Select(x => x.Player).ToList().Take(11).ToList();
            //seasonMatchToSend.TeamOne.Players = TeamOnePlayer.Adapt<List<PlayersViewModelForTeamViewModelForJHOLE>>();
            //int playerXdefault = -400;
            ////int playerYdefault = 550;
            //foreach(var player in seasonMatchToSend.TeamOne.Players)
            //{
            //    var existingFormation = seasonMatchPlayers.Where(x => x.PlayerId == player.Id).SingleOrDefault();
            //    if(existingFormation != null)
            //    {
            //        player.PlayerX = existingFormation.PlayerX;
            //        player.PlayerY = existingFormation.PlayerY;
            //        player.PlayerRole = existingFormation.PlayerRole;
            //        player.PlayersType = existingFormation.PlayersType;
            //    }
            //    else
            //    {
            //        playerXdefault = playerXdefault + 80;
            //        player.PlayerX = playerXdefault;
            //        player.PlayerY = 550;
            //        player.PlayerRole = "";
            //        player.PlayersType = enumSeasonMatchPlayersType.Substitute;
            //    }
            //}
            //playerXdefault = -400;
            //seasonMatchToSend.TeamTwo.Players = TeamTwoPlayer.Adapt<List<PlayersViewModelForTeamViewModelForJHOLE>>();
            //foreach (var player in seasonMatchToSend.TeamTwo.Players)
            //{
            //    var existingFormation = seasonMatchPlayers.Where(x => x.PlayerId == player.Id).SingleOrDefault();
            //    if (existingFormation != null)
            //    {
            //        player.PlayerX = existingFormation.PlayerX;
            //        player.PlayerY = existingFormation.PlayerY;
            //        player.PlayerRole = existingFormation.PlayerRole;
            //        player.PlayersType = existingFormation.PlayersType;
            //    }
            //    else
            //    {
            //        playerXdefault = playerXdefault + 80;
            //        player.PlayerX = playerXdefault;
            //        player.PlayerY = 550;
            //        player.PlayerRole = "";
            //        player.PlayersType = enumSeasonMatchPlayersType.Substitute;
            //    }
            //}
            ////IDNAMEIMAGEVMFORES
            //List<SeasonMatchEventViewModel> seasonMatchEventToSend = SeasonMatchEvent.Adapt<List<SeasonMatchEventViewModel>>();
            //UniversalDataContainer udc = new UniversalDataContainer
            //{
            //    Command = enumCommand.SeasonMatchEvent.ToString(),
            //    Param = enumParam.LIST.ToString(),
            //    Result = seasonMatchEventToSend
            //};
            //SendToCaller(udc);            
            //UniversalDataContainer udc1 = new UniversalDataContainer
            //{
            //    Command = enumCommandAutomationCommand.START_GAME.ToString(),
            //    //Param = enumParam.LIST.ToString(),
            //    //Result = seasonMatchToSend
            //};
            //SendToCaller(udc1);

            // Clients.Caller.SendAsync("onLoading", true);
            //IPHostEntry heserver = Dns.GetHostEntry(Dns.GetHostName());
            //var ip = heserver.AddressList[2].ToString();
            //var test = (this._accessor.HttpContext.Connection.RemoteIpAddress.ToString());
            //GetClintstuff(ip);
            return base.OnConnectedAsync();
        }
        public static string GetClintstuff(string ip)
        {
            IpInfo ipInfo = new IpInfo();
            try
            {
                string info = new WebClient().DownloadString($"ipinfo.io/{ip}?token=761eb4a516ab70");
                ipInfo = JsonConvert.DeserializeObject<IpInfo>(info);
                RegionInfo myRI1 = new RegionInfo(ipInfo.Country);
                ipInfo.Country = myRI1.EnglishName;
            }
            catch (Exception)
            {
                ipInfo.Country = null;
            }
            return ipInfo.Country;
        }
        public void UniversalGet(UniversalDataContainer UniversalDataContainer)
        {
            InvokeAWESOME(UniversalDataContainer.Command, UniversalDataContainer);            
        }
        public Task Clubs(UniversalDataContainer UniversalDataContainer)
        {
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                    var stringg = JsonConvert.SerializeObject(UniversalDataContainer.Data);
                    Club club = JsonConvert.DeserializeObject<Club>(stringg);
                    var inDb = _context.Clubs.Where(x => x.XId == club.XId).SingleOrDefault();
                    if (inDb == null)
                    {
                        club.Guid = Global.UniqueGuid("Clubs", _context);
                        _context.Entry(club).State = EntityState.Added;
                        _context.SaveChanges();
                        club.multilingualid = club.Id;
                        _context.Entry(club).State = EntityState.Modified;
                        _context.SaveChanges();
                        UniversalDataContainer.Data = club.Name + " Saved !";
                        UniversalDataContainer.Result = club.Id;
                        ClubConvertedCulture lcc = new ClubConvertedCulture
                        {
                            CultureId = club.CultureId.Value,
                            ClubId = club.multilingualid
                        };
                        _context.Entry(lcc).State = EntityState.Added;
                        _context.SaveChanges();
                    }
                    else
                    {
                        //LeagueConvertedCulture lcc = new LeagueConvertedCulture
                        //{
                        //    CultureId = club.CultureId.Value,
                        //    LeagueId = club.multilingualid
                        //};
                        //_context.Entry(lcc).State = EntityState.Added;
                        //_context.SaveChanges();
                        club.Guid = inDb.Guid;
                        club.Id = inDb.Id;
                        _context.Entry(club).State = EntityState.Modified;
                        _context.SaveChanges();
                        UniversalDataContainer.Data =inDb.Name + "Edited !";
                        UniversalDataContainer.Result = club.Id;
                    }
                    return SendToCaller(UniversalDataContainer);
                case enumParam.UPDATE:
                    UniversalDataContainer.Result = "asd";
                    return SendToCaller(UniversalDataContainer);
                case enumParam.LIST:
                    return SendToCaller(UniversalDataContainer, null);
                case enumParam.DELETE:
                    //ElasticSearchGlobal.DeleteGeneric
                    return SendToCaller(UniversalDataContainer);
                case enumParam.EXECUTE:
                    return SendToCaller(UniversalDataContainer);// _MatchEventRepository.Execute(UniversalDataContainer));
                default:
                    Debug.WriteLine("Execute");
                    return Clients.Caller.SendAsync("onUniversalGet", UniversalDataContainer);
            }
        }
        public Task ClubPlayers(UniversalDataContainer UniversalDataContainer)
        {
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                    var stringg = JsonConvert.SerializeObject(UniversalDataContainer.Data);
                    ClubPlayers clubPlayers = JsonConvert.DeserializeObject<ClubPlayers>(stringg);
                    var inDb = _context.ClubPlayers.Where(x => x.ClubId == clubPlayers.ClubId).Where(x=>x.PlayerId == clubPlayers.PlayerId).SingleOrDefault();
                    if (inDb == null)
                    {
                        _context.Entry(clubPlayers).State = EntityState.Added;
                        _context.SaveChanges();
                        UniversalDataContainer.Data = "Player Assigned To Club";
                    }
                    else
                    {
                        UniversalDataContainer.Data = "Player Modified";
                        inDb.PlayerRole = clubPlayers.PlayerRole;
                        _context.Entry(inDb).State = EntityState.Modified;
                        _context.SaveChanges();
                    }
                    return SendToCaller(UniversalDataContainer);
                case enumParam.UPDATE:
                    UniversalDataContainer.Result = "asd";
                    return SendToCaller(UniversalDataContainer);
                case enumParam.LIST:
                    return SendToCaller(UniversalDataContainer, null);
                case enumParam.DELETE:
                    //ElasticSearchGlobal.DeleteGeneric
                    return SendToCaller(UniversalDataContainer);
                case enumParam.EXECUTE:
                    return SendToCaller(UniversalDataContainer);// _MatchEventRepository.Execute(UniversalDataContainer));
                default:
                    Debug.WriteLine("Execute");
                    return Clients.Caller.SendAsync("onUniversalGet", UniversalDataContainer);
            }
        }
        public Task Players(UniversalDataContainer UniversalDataContainer)
        {
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                    var stringg = JsonConvert.SerializeObject(UniversalDataContainer.Data);
                    Player player = JsonConvert.DeserializeObject<Player>(stringg);
                    var inDb = _context.Players.Where(x => x.XId == player.XId).SingleOrDefault();
                    if (inDb == null)
                    {
                        player.Guid = Global.UniqueGuid("Players", _context);
                        _context.Entry(player).State = EntityState.Added;
                        _context.SaveChanges();
                        player.multilingualid = player.Id;
                        _context.Entry(player).State = EntityState.Modified;
                        _context.SaveChanges();
                        UniversalDataContainer.Data = player.Name + " Saved !";
                        UniversalDataContainer.Result = player.Id;
                        PlayerConvertedCulture lcc = new PlayerConvertedCulture
                        {
                            CultureId = player.CultureId.Value,
                            PlayerId = player.multilingualid
                        };
                        _context.Entry(lcc).State = EntityState.Added;
                        _context.SaveChanges();
                    }
                    else
                    {
                        player.Guid = inDb.Guid;
                        player.Id = inDb.Id;
                        _context.Entry(player).State = EntityState.Modified;
                        _context.SaveChanges();
                        UniversalDataContainer.Data = inDb.Name + "Edited !";
                        UniversalDataContainer.Result = player.Id;
                    }
                    return SendToCaller(UniversalDataContainer);
                case enumParam.UPDATE:
                    UniversalDataContainer.Result = "asd";
                    return SendToCaller(UniversalDataContainer);
                case enumParam.LIST:
                    return SendToCaller(UniversalDataContainer, null);
                case enumParam.DELETE:
                    //ElasticSearchGlobal.DeleteGeneric
                    return SendToCaller(UniversalDataContainer);
                case enumParam.EXECUTE:
                    return SendToCaller(UniversalDataContainer);// _MatchEventRepository.Execute(UniversalDataContainer));
                default:
                    Debug.WriteLine("Execute");
                    return Clients.Caller.SendAsync("onUniversalGet", UniversalDataContainer);
            }
        }
        public Task Leagues(UniversalDataContainer UniversalDataContainer)
        {
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                    var stringg = JsonConvert.SerializeObject(UniversalDataContainer.Data);
                    League league = JsonConvert.DeserializeObject<League>(stringg);
                    var inDb = _context.Leagues.Where(x => x.XId == league.XId).SingleOrDefault();
                    if (inDb == null)
                    {
                        league.Guid = Global.UniqueGuid("Leagues", _context);
                        _context.Entry(league).State = EntityState.Added;
                        _context.SaveChanges();
                        league.multilingualid = league.Id;
                        _context.Entry(league).State = EntityState.Modified;
                        _context.SaveChanges();
                        UniversalDataContainer.Data = "League Reached , Now Saving Part Left ";
                        LeagueConvertedCulture lcc = new LeagueConvertedCulture
                        {
                            CultureId = league.CultureId.Value,
                            LeagueId = league.multilingualid
                        };
                        _context.Entry(lcc).State = EntityState.Added;
                        _context.SaveChanges();
                    }
                    else
                    {
                        //LeagueConvertedCulture lcc = new LeagueConvertedCulture
                        //{
                        //    CultureId = league.CultureId.Value,
                        //    LeagueId = league.multilingualid
                        //};
                        //_context.Entry(lcc).State = EntityState.Added;
                        //_context.SaveChanges();
                        UniversalDataContainer.Data = "Edit Part Not Completed";
                    }
                    return SendToCaller(UniversalDataContainer);
                case enumParam.UPDATE:
                    UniversalDataContainer.Result = "asd";
                    return SendToCaller(UniversalDataContainer);
                case enumParam.LIST:
                    return SendToCaller(UniversalDataContainer, null);
                case enumParam.DELETE:
                    //ElasticSearchGlobal.DeleteGeneric
                    return SendToCaller(UniversalDataContainer);
                case enumParam.EXECUTE:
                    return SendToCaller(UniversalDataContainer);// _MatchEventRepository.Execute(UniversalDataContainer));
                default:
                    Debug.WriteLine("Execute");
                    return Clients.Caller.SendAsync("onUniversalGet", UniversalDataContainer);
            }
        }
        public Task SeasonMatchEvent(UniversalDataContainer UniversalDataContainer)
        {
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                    var stringg = JsonConvert.SerializeObject(UniversalDataContainer.Data);
                    SeasonMatchEvent b = JsonConvert.DeserializeObject<SeasonMatchEvent>(stringg);
                    b.Club = _context.Clubs.Find(b.ClubId);
                    b.Player = _context.Players.Find(b.PlayerId);
                    b.SeasonMatch = _context.SeasonFixture.Find(b.SeasonMatchGuid);
                    var objTosend = b.Adapt<SeasonMatchEventViewModel>();
                    return SendToCaller(UniversalDataContainer, objTosend);                    
                case enumParam.UPDATE:
                    UniversalDataContainer.Result = "asd";
                    return SendToCaller(UniversalDataContainer);
                case enumParam.LIST:
                    return SendToCaller(UniversalDataContainer, null);
                case enumParam.DELETE:
                    //ElasticSearchGlobal.DeleteGeneric
                    return SendToCaller(UniversalDataContainer);
                case enumParam.EXECUTE:
                    return SendToCaller(UniversalDataContainer);// _MatchEventRepository.Execute(UniversalDataContainer));
                default:
                    Debug.WriteLine("Execute");
                    return Clients.Caller.SendAsync("onUniversalGet", UniversalDataContainer);
            }            
        }
        public Task MatchEvent(UniversalDataContainer UniversalDataContainer)
        {
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                case enumParam.UPDATE:                    
                    UniversalDataContainer.Result = "asd";
                    return SendToCaller(UniversalDataContainer);
                case enumParam.LIST:
                    return SendToCaller(UniversalDataContainer, null);
                case enumParam.DELETE:
                    //ElasticSearchGlobal.DeleteProfitTrailLossTransaction(UniversalDataContainer.SubParam);
                    return SendToCaller(UniversalDataContainer);
                case enumParam.EXECUTE:
                    return SendToCaller(UniversalDataContainer);// _MatchEventRepository.Execute(UniversalDataContainer));
                default:
                    Debug.WriteLine("Execute");
                    return Clients.Caller.SendAsync("onUniversalGet", UniversalDataContainer);
            }
        }
        public void CREATE_ROOM(UniversalDataContainer UniversalDataContainer)
        {
            var supervisorToInstruct = _superVisors.Where(x=>x.Role == enumSuperVisorRole.Roome_Creation).Where(x => x.isBusy == false).FirstOrDefault();
            //var supervisorToInstruct = _superVisors.Where(x=>x.Role == enumSuperVisorRole.Roome_Creation).FirstOrDefault();
            //Check if we already have some room ?
            if(_superVisors.Where(x=>x.RoomId.Count > 0).Any())
            {
                UniversalDataContainer.Command = enumCommandAutomationCommandResponseBack.ROOM_CREATED.ToString();
                UniversalDataContainer.SubParam = _superVisors.Where(x => x.RoomId.Count > 0).SingleOrDefault().RoomId.FirstOrDefault().RoomId;
                Clients.Group("discord").SendAsync("onUniversalGet", UniversalDataContainer);
                //ROOM_CREATED(UniversalDataContainer);
            }
            // Clients.Group("automationsupervisor").SendAsync("onUniversalGet", UniversalDataContainer);            
            else if (supervisorToInstruct != null)
            {
                //UniversalDataContainer.Command = enumCommandAutomationCommandResponseBack.MESSAGE.ToString();
                _superVisors.Where(x=>x.ConnectionId == supervisorToInstruct.ConnectionId).FirstOrDefault().isBusy = true;
                Clients.Clients(supervisorToInstruct.ConnectionId).SendAsync("onUniversalGet", UniversalDataContainer);
            }
            else
            {
                UniversalDataContainer.Command = enumCommandAutomationCommandResponseBack.REQUEST_NOT_FULFILLED.ToString();
                UniversalDataContainer.Message = "No CREATE_ROOM Workers are available for this request, Since i need to develop and deploy in single machine , when i am working on this bot the bot itself becomes inactive..thanks for your cooperation";
                // Need to arrange this
                Clients.Group("discord").SendAsync("onUniversalGet", UniversalDataContainer);            
            }
        }
        public void Worker_Free(UniversalDataContainer universalDataContainer)
        {
            // TO CREATE AS MANY ROOM AS POSSIBLE
            _superVisors.Where(x => x.ConnectionId == Context.ConnectionId).SingleOrDefault().isBusy = false;
            _superVisors.Where(x => x.ConnectionId == Context.ConnectionId).SingleOrDefault().RoomId = new List<RoomDetails>();
            UniversalDataContainer UniversalDataContainer = new UniversalDataContainer
            {
                Command = enumCommandAutomationCommand.CREATE_ROOM.ToString(),
            };
            CREATE_ROOM(UniversalDataContainer);
        }
        public void ROOM_CREATED(UniversalDataContainer UniversalDataContainer)
        {
            _superVisors.Where(x => x.ConnectionId == Context.ConnectionId).SingleOrDefault().RoomId.Add(new RoomDetails
            {
                Password = "1",
                RoomId = UniversalDataContainer.SubParam
            });
            Clients.Group("discord").SendAsync("onUniversalGet", UniversalDataContainer);
        }
        public void USER_VERIFIED(UniversalDataContainer UniversalDataContainer)
        {
            Clients.Group("discord").SendAsync("onUniversalGet", UniversalDataContainer);
        }
        public void VERIFY_USER(UniversalDataContainer UniversalDataContainer)
        {
            var askToSuperVisor = _superVisors.Where(x => x.Role == enumSuperVisorRole.User_Verification).SingleOrDefault();
            if (askToSuperVisor == null)
            {
                UniversalDataContainer.Command = enumCommandAutomationCommandResponseBack.REQUEST_NOT_FULFILLED.ToString();
                UniversalDataContainer.Message = "No User_Verification Workers are available for this request, Since i need to develop and deploy in single machine , when i am working on this bot the bot itself becomes inactive..thanks for your cooperation";
                // Need to arrange this
                Clients.Group("discord").SendAsync("onUniversalGet", UniversalDataContainer);
            }
            else
            {
                Clients.Client(askToSuperVisor.ConnectionId).SendAsync("onUniversalGet", UniversalDataContainer);
            }
        }
        public Task SeasonFixture(UniversalDataContainer UniversalDataContainer)
        {            
            //TODO ##TODO yo function ma lera janu bhanda pani tala ko function dynamic huna paryo!
            // _MatchEventRepository.Execute(UniversalDataContainer));
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                case enumParam.UPDATE:
                    UniversalDataContainer.Result = "asd";
                    return SendToCaller(UniversalDataContainer);
                case enumParam.LIST:
                case enumParam.DELETE:
                    return SendToCaller(UniversalDataContainer);
                case enumParam.EXECUTE:
                    return SendToCaller(UniversalDataContainer);
                default:
                    return Clients.Caller.SendAsync("onUniversalGet", UniversalDataContainer);
            }
        }
        private void InvokeAWESOME(string methodName, UniversalDataContainer UniversalDataContainer)
        {
            try
            {
                Type myType = typeof(SportsmanduHub);
                var kk = myType.GetMethod(methodName);
                MethodInfo dynMethod = this.GetType().GetMethod(methodName,
                    BindingFlags.Default | BindingFlags.OptionalParamBinding | BindingFlags.Public | BindingFlags.Instance);
                dynMethod.Invoke(this, new UniversalDataContainer[] { UniversalDataContainer });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("SOME COMMAND WRONG !");
                Global.Error(ex);
            }
        }
        private Task SendToCaller(UniversalDataContainer UniversalDataContainer, dynamic Result = null)
        {
            if (Result != null) UniversalDataContainer.Result = Result;
            return Clients.Caller.SendAsync("onUniversalGet", UniversalDataContainer);
        }
    }
}
public class UseraModel
{
    [JsonProperty("g")]
    public string GroupName { get; set; }
    [JsonProperty("f")]
    public string FullName { get; set; }
    [JsonProperty("a")]
    public string Avatar { get; set; }
}
public class SpecialMessage
{
    public SpecialMessage()
    {
        this.Voice = true;
    }
    public bool Voice { get; set; }
    public string Message { get; set; }
}
public class Loading
{
    public Loading(bool Loading)
    {
        this.isLoading = Loading;
    }
    public bool isLoading { get; set; }
}