function CMain(oData){
    var _bUpdate;
    var _iCurResource = 0;
    var RESOURCE_TO_LOAD = 0;
    var _iState = STATE_LOADING;
    var _oData;
    
    var _oPreloader;
    var _oMenu;
    var _oHelp;
    var _oGame;

    this.initContainer = function(){
        s_oCanvas = document.getElementById("canvas");
        s_oStage = new createjs.Stage(s_oCanvas);
        s_oStage.preventSelection = false;
        createjs.Touch.enable(s_oStage);
		
	s_bMobile = jQuery.browser.mobile;
        if(s_bMobile === false){
            s_oStage.enableMouseOver(20);  
            $('body').on('contextmenu', '#canvas', function(e){ return false; });
        }
		
        s_iPrevTime = new Date().getTime();

	createjs.Ticker.addEventListener("tick", this._update);
        createjs.Ticker.framerate = 30;
        
        if(navigator.userAgent.match(/Windows Phone/i)){
                DISABLE_SOUND_MOBILE = true;
        }
        
        s_oSpriteLibrary  = new CSpriteLibrary();

        //ADD PRELOADER
        _oPreloader = new CPreloader();

    };
    
    this.preloaderReady = function(){
        if(DISABLE_SOUND_MOBILE === false || s_bMobile === false){
            this._initSounds();
        }
        
        this._loadImages();
        _bUpdate = true;
    };
    
    this.soundLoaded = function(){
         _iCurResource++;

         if(_iCurResource === RESOURCE_TO_LOAD){
             _oPreloader.unload();
            
            if (!isIOS()) {
                s_oSoundTrack = playSound("soundtrack", 1, true);
            }

            s_oMain.gotoMenu();
         }
    };

    
    this._initSounds = function(){
        var aSoundsInfo = new Array();
        aSoundsInfo.push({ path: baseUrl + 'games/duckhunt/sounds/',filename:'duck_hit',loop:false,volume:1, ingamename: 'duck_hit'});
        aSoundsInfo.push({ path: baseUrl + 'games/duckhunt/sounds/',filename:'click',loop:false,volume:1, ingamename: 'click'});
        aSoundsInfo.push({ path: baseUrl + 'games/duckhunt/sounds/',filename:'duck_intro',loop:false,volume:1, ingamename: 'duck_intro'});
        aSoundsInfo.push({ path: baseUrl + 'games/duckhunt/sounds/',filename:'game_over',loop:false,volume:1, ingamename: 'game_over'});
        aSoundsInfo.push({ path: baseUrl + 'games/duckhunt/sounds/',filename:'shot',loop:false,volume:1, ingamename: 'shot'});
        aSoundsInfo.push({ path: baseUrl + 'games/duckhunt/sounds/',filename:'no_bullets',loop:false,volume:1, ingamename: 'no_bullets'});
        aSoundsInfo.push({ path: baseUrl + 'games/duckhunt/sounds/',filename:'soundtrack',loop:true,volume:1, ingamename: 'soundtrack'});
        
        RESOURCE_TO_LOAD += aSoundsInfo.length;

        s_aSounds = new Array();
        for(var i=0; i<aSoundsInfo.length; i++){
            s_aSounds[aSoundsInfo[i].ingamename] = new Howl({ 
                                                            src: [aSoundsInfo[i].path+aSoundsInfo[i].filename+'.mp3', aSoundsInfo[i].path+aSoundsInfo[i].filename+'.ogg'],
                                                            autoplay: false,
                                                            preload: true,
                                                            loop: aSoundsInfo[i].loop, 
                                                            volume: aSoundsInfo[i].volume,
                                                            onload: s_oMain.soundLoaded
                                                        });
        }
        
    };  

    this._loadImages = function(){
        s_oSpriteLibrary.init( this._onImagesLoaded,this._onAllImagesLoaded, this );

        s_oSpriteLibrary.addSprite("but_play",baseUrl + "games/duckhunt/sprites/but_play.png");
        s_oSpriteLibrary.addSprite("but_exit",baseUrl + "games/duckhunt/sprites/but_exit.png");
        s_oSpriteLibrary.addSprite("bg_menu",baseUrl + "games/duckhunt/sprites/bg_menu.jpg");
        s_oSpriteLibrary.addSprite("msg_box",baseUrl + "games/duckhunt/sprites/msg_box.png");
        
        s_oSpriteLibrary.addSprite("joypad",baseUrl + "games/duckhunt/sprites/joypad.png");
        s_oSpriteLibrary.addSprite("shot_panel",baseUrl + "games/duckhunt/sprites/shot_panel.png");
        s_oSpriteLibrary.addSprite("hit_panel",baseUrl + "games/duckhunt/sprites/hit_panel.png");
        s_oSpriteLibrary.addSprite("bullet",baseUrl + "games/duckhunt/sprites/bullet.png");
        s_oSpriteLibrary.addSprite("tap_shot",baseUrl + "games/duckhunt/sprites/tap_shot.png");
        s_oSpriteLibrary.addSprite("hit_icon",baseUrl + "games/duckhunt/sprites/hit_icon.png");
        s_oSpriteLibrary.addSprite("audio_icon",baseUrl + "games/duckhunt/sprites/audio_icon.png");
        s_oSpriteLibrary.addSprite("bg_game",baseUrl + "games/duckhunt/sprites/bg_game.jpg");
        s_oSpriteLibrary.addSprite("grass",baseUrl + "games/duckhunt/sprites/grass.png");
        s_oSpriteLibrary.addSprite("scope",baseUrl + "games/duckhunt/sprites/gun_sight.png");
        s_oSpriteLibrary.addSprite("tap_shot",baseUrl + "games/duckhunt/sprites/tap_shot.png");
        s_oSpriteLibrary.addSprite("duck_1",baseUrl + "games/duckhunt/sprites/duck_1.png");
        s_oSpriteLibrary.addSprite("duck_2",baseUrl + "games/duckhunt/sprites/duck_2.png");
        s_oSpriteLibrary.addSprite("duck_3",baseUrl + "games/duckhunt/sprites/duck_3.png");
        s_oSpriteLibrary.addSprite("duck_4",baseUrl + "games/duckhunt/sprites/duck_4.png");

        s_oSpriteLibrary.addSprite("life",baseUrl + "games/duckhunt/sprites/life.png");
        
        s_oSpriteLibrary.addSprite("space_bar",baseUrl + "games/duckhunt/sprites/space_bar.png");
        s_oSpriteLibrary.addSprite("keys",baseUrl + "games/duckhunt/sprites/keys.png");
        s_oSpriteLibrary.addSprite("swift_hand",baseUrl + "games/duckhunt/sprites/swift_hand.png");
        s_oSpriteLibrary.addSprite("touch_hand",baseUrl + "games/duckhunt/sprites/touch_hand.png");
        s_oSpriteLibrary.addSprite("dividing_line",baseUrl + "games/duckhunt/sprites/dividing_line.png");
        
        s_oSpriteLibrary.addSprite("rock_sx_1",baseUrl + "games/duckhunt/sprites/rock_sx_1.png");
        s_oSpriteLibrary.addSprite("rock_sx_2",baseUrl + "games/duckhunt/sprites/rock_sx_2.png");
        s_oSpriteLibrary.addSprite("rock_dx_1",baseUrl + "games/duckhunt/sprites/rock_dx_1.png");
        s_oSpriteLibrary.addSprite("rock_dx_2",baseUrl + "games/duckhunt/sprites/rock_dx_2.png");
        
        s_oSpriteLibrary.addSprite("but_home",baseUrl + "games/duckhunt/sprites/but_home.png");
        s_oSpriteLibrary.addSprite("but_restart",baseUrl + "games/duckhunt/sprites/but_restart.png");
        s_oSpriteLibrary.addSprite("but_credits",baseUrl + "games/duckhunt/sprites/but_credits.png");
        s_oSpriteLibrary.addSprite("but_fullscreen",baseUrl + "games/duckhunt/sprites/but_fullscreen.png");
        s_oSpriteLibrary.addSprite("logo_ctl",baseUrl + "games/duckhunt/sprites/logo_ctl.png");
        
        RESOURCE_TO_LOAD += s_oSpriteLibrary.getNumSprites();
        s_oSpriteLibrary.loadSprites();
    };
    
    this._onImagesLoaded = function(){
        _iCurResource++;
        var iPerc = Math.floor(_iCurResource/RESOURCE_TO_LOAD *100);
        _oPreloader.refreshLoader(iPerc);
        
        if(_iCurResource === RESOURCE_TO_LOAD){
            _oPreloader.unload();
            
            if (!isIOS()) {
                s_oSoundTrack = playSound("soundtrack", 1, true);
            }
            
            this.gotoMenu();
        }
    };
    
    this._onAllImagesLoaded = function(){
        
    };
    
    this.gotoMenu = function(){
        _oMenu = new CMenu();
        _iState = STATE_MENU;
    };

    this.gotoGame = function(){
        _oGame = new CGame(_oData);   
							
        _iState = STATE_GAME;
    };
    
    this.gotoHelp = function(){
        _oHelp = new CHelp();
        _iState = STATE_HELP;
    };
	
    this.stopUpdate = function(){
        _bUpdate = false;
        createjs.Ticker.paused = true;
        $("#block_game").css("display","block");
        
        if(DISABLE_SOUND_MOBILE === false || s_bMobile === false){
            Howler.mute(true);
        }
        
    };

    this.startUpdate = function(){
        s_iPrevTime = new Date().getTime();
        _bUpdate = true;
        createjs.Ticker.paused = false;
        $("#block_game").css("display","none");
        
        if(DISABLE_SOUND_MOBILE === false || s_bMobile === false){
            if(s_bAudioActive){
                Howler.mute(false);
            }
        }
        
    };

    
    this._update = function(event){
		if(_bUpdate === false){
			return;
		}
                
        var iCurTime = new Date().getTime();
        s_iTimeElaps = iCurTime - s_iPrevTime;
        s_iCntTime += s_iTimeElaps;
        s_iCntFps++;
        s_iPrevTime = iCurTime;
        
        if ( s_iCntTime >= 1000 ){
            s_iCurFps = s_iCntFps;
            s_iCntTime-=1000;
            s_iCntFps = 0;
        }
                
        if(_iState === STATE_GAME){
            _oGame.update();
        }
        
        s_oStage.update(event);

    };
    
    s_oMain = this;
    
    _oData = oData;
    ENABLE_FULLSCREEN = oData.fullscreen;
    ENABLE_CHECK_ORIENTATION = oData.check_orientation;
    
    this.initContainer();
}
var s_bMobile;
var s_bAudioActive = true;
var s_iCntTime = 0;
var s_iTimeElaps = 0;
var s_iPrevTime = 0;
var s_iCntFps = 0;
var s_iCurFps = 0;

var s_oDrawLayer;
var s_oStage;
var s_oMain;
var s_oSpriteLibrary;
var s_oSoundTrack = null;
var s_oCanvas;
var s_bFullscreen = false;
var s_aSounds;