baseUrl = "https://sportsmandu.com/";
var API = baseUrl + 'Api/SaveGameScore';
SaveFinalScore = function (score) {
    //console.log("Saving Score : " , score);
    var hashids = new Hashids.default(GAME_ID);
    var dataToSend = {
        Salt: GAME_SALT,
        Guid: GAME_ID,
        Points: hashids.encode(score.points),
        Level: hashids.encode(score.level)
    };
    score.points = hashids.encode(score.points);
    //console.log(dataToSend);
    axios.post(API, dataToSend).then((res) => {
        //console.log(res);
    });
};
$(document).ready(function () {
    var oMain = new CMain({
        scope_accelleration: 2,    //SCOPE ACCELLERATION
        scope_friction: 0.85,      //SCOPE FRICTION
        max_scope_speed: 40,       //MAXIMUM SCOPE SPEED
        num_bullets: 5,            //NUMBER OF PLAYER BULLETS FOR EACH SHOT LEVEL
        hit_score: 100,           //POINTS GAINED WHEN DUCK IS HITTEN
        bonus_time: 4000,          //BONUS TIME IN MILLISECONDS
        lives: 5,                  //NUMBER OF PLAYER LIVES
        duck_increase_speed: 0.5, //INCREASE THIS VALUE TO SPEED UP DUCKS AFTER EACH LEVEL SHOT
        duck_occurence: [1,  //NUMBER OF DUCKS IN SHOT 1
            1,  //NUMBER OF DUCKS IN SHOT 2
            2,  //NUMBER OF DUCKS IN SHOT 3
            1,  //NUMBER OF DUCKS IN SHOT 4
            1,  //NUMBER OF DUCKS IN SHOT 5
            1,  //NUMBER OF DUCKS IN SHOT 6
            2,  //NUMBER OF DUCKS IN SHOT 7
            2,  //NUMBER OF DUCKS IN SHOT 8
            2,  //NUMBER OF DUCKS IN SHOT 9
            3   //NUMBER OF DUCKS IN SHOT 10
            //ADD NEW DUCK OCCURENCE HERE IF YOU NEED...
        ],
        fullscreen: true,        //SET THIS TO FALSE IF YOU DON'T WANT TO SHOW FULLSCREEN BUTTON
        check_orientation: true //SET TO FALSE IF YOU DON'T WANT TO SHOW ORIENTATION ALERT ON MOBILE DEVICES
    });

    $(oMain).on("start_session", function (evt) {
        if (getParamValue('ctl-arcade') === "true") {
            parent.__ctlArcadeStartSession();
        }
        //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("end_session", function (evt) {
        if (getParamValue('ctl-arcade') === "true") {
            parent.__ctlArcadeEndSession();
        }
        //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("save_score", function (evt, iScore) {
        if (getParamValue('ctl-arcade') === "true") {
            parent.__ctlArcadeSaveScore({ score: iScore });
        }
        console.log(evt, iScore);
        //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("start_level", function (evt, iLevel) {
        if (getParamValue('ctl-arcade') === "true") {
            parent.__ctlArcadeStartLevel({ level: iLevel });
        }
        //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("end_level", function (evt, iLevel) {
        if (getParamValue('ctl-arcade') === "true") {
            parent.__ctlArcadeEndLevel({ level: iLevel });
        }
        //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("restart_level", function (evt, iLevel) {
        if (getParamValue('ctl-arcade') === "true") {
            parent.__ctlArcadeRestartLevel({ level: iLevel });
        }
        //...ADD YOUR CODE HERE EVENTUALLY
    });


    $(oMain).on("show_interlevel_ad", function (evt) {
        if (getParamValue('ctl-arcade') === "true") {
            parent.__ctlArcadeShowInterlevelAD();
        };
        //alert("HJere");
        //...ADD YOUR CODE HERE EVENTUALLY
    });

    $(oMain).on("share_event", function (evt, iScore) {
        if (getParamValue('ctl-arcade') === "true") {
            parent.__ctlArcadeShareEvent({
                img: TEXT_SHARE_IMAGE,
                title: TEXT_SHARE_TITLE,
                msg: TEXT_SHARE_MSG1 + iScore + TEXT_SHARE_MSG2,
                msg_share: TEXT_SHARE_SHARE1 + iScore + TEXT_SHARE_SHARE1
            });
        }
        //...ADD YOUR CODE HERE EVENTUALLY
    });

    if (isIOS()) {
        setTimeout(function () { sizeHandler(); }, 200);
    } else {
        sizeHandler();
    }
});