﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Linq;
using Sportsmandu.Models;
using Sportsmandu.Models.enums;
using Sportsmandu.Models.Globalization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Security.Claims;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Entities.ViewModels;
using AutoMapper;
using static Sportsmandu.Models.SportsmanduImage;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;
using System.Net;
using Sportsmandu.Data;
using Microsoft.EntityFrameworkCore;

namespace Sportsmandu
{   
    public class Authorization
    {
        public static bool Authorized(string segment, string action, int id, IEnumerable<Claim> UserClaims)
        {            
            return UserClaims.Where(x => x.Value == id.ToString()).Where(x => x.Type == segment.ToString()).FirstOrDefault() != null ? true :
            UserClaims.Where(x => x.Value == "Super_Admin").Where(x => x.Type.ToLower().Contains("role")).FirstOrDefault() != null ? true : 
            false;
        }

        //internal static bool Authorized(object club, string v, object id, IEnumerable<Claim> claims)
        //{
        //    throw new NotImplementedException();
        //}
    }
    public class GlobalUser
    {
        public int UserIdFromJWT(ClaimsPrincipal ci)
        {
            var identity = ci.Identity as ClaimsIdentity;
            var claims = from c in identity.Claims
                         select new
                         {
                             subject = c.Subject.Name,
                             type = c.Type,
                             value = c.Value
                         };
            var UserId = claims.Where(x => x.type.Contains("nameidentifier")).Select(x => x.value).SingleOrDefault();
            return Int32.Parse(UserId);
        }
    }

    public static class Global
    {
        public static List<string>  BOTURL(AppDbContext _context)
        {
            List<string> URLS = new List<string>();
            //List<Player> _allPlayers = _context.Players
            //    .Include(x => x.Social)
            //    .Include(x => x.Culture)
            //    .Include(x => x.Category)
            //    .Include(x => x.Country)
            //    .Include(x => x.Club)
            //    .ThenInclude(x => x.Club)
            //    .Include(x => x.Season)
            //    .ThenInclude(x => x.Season)
            //    .ThenInclude(x => x.League)
            //    .Where(x => x.Delflag == false)
            //    .ToList();
            //foreach (var items in _allPlayers)
            //{
            //    URLS.Add(items.URL);
            //}

            List<Club> Clubs = _context.Clubs
                        .Include(x => x.ClubSocial)
                        .Include(x => x.Culture)
                        .Include(x => x.Players)
                        .ThenInclude(x => x.Player)
                        .ThenInclude(x => x.Category)
                        .Include(x => x.Seasons)
                        .ThenInclude(x => x.Season)
                        .Include(x => x.Players)
                        .ThenInclude(x => x.Player)
                        .ThenInclude(x => x.Country)
                        .Include(x => x.Players)
                        .ThenInclude(x => x.Player)
                        .ThenInclude(x => x.Culture)
                        .ToList();
            foreach(var items in Clubs)
            {
                URLS.Add(items.URL);
            }
            return URLS;
        }
        public class EnumForView
        {            
            public string Name { get; set; }
            public int Id { get; set; }
        }
        public static List<EnumForView> GetEnumList(string enumFullQualifiedName)
        {
            List<EnumForView> lst = new List<EnumForView>();
            Type eventType = Type.GetType(enumFullQualifiedName);
            int i=0;
            if (eventType != null)
                foreach (var Event in eventType.GetFields())
                {
                    if (Event.Name != "value__")
                    {
                        lst.Add(new EnumForView
                        {
                            Name = Event.Name,
                            Id = i

                        });
                        i = i + 1;
                    }
                }
            return lst;
        }
        public static string UniqueGuid(string dbTable,Data.AppDbContext _context)
        {
            RandomGenerator:
            const string AllowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string AllowedCharsNum = "0123456789";
            Random rng = new Random();
            string randomm = string.Empty;
            string randomm1 = string.Empty;
            string randommInt = string.Empty;
            string randommInt1 = string.Empty;
            foreach (var randomString in RandomStrings(AllowedChars, 4, 4, 1, rng))
            {
                randomm = randomString;
            }
            foreach (var randomString in RandomStrings(AllowedCharsNum, 4, 4, 1, rng))
            {
                randommInt = randomString;
            }
            foreach (var randomString in RandomStrings(AllowedCharsNum, 4, 4, 1, rng))
            {
                randommInt1 = randomString;
            }
            foreach (var randomString in RandomStrings(AllowedChars, 2, 2, 1, rng))
            {
                randomm1 = randomString;
            }
            //Generate a Unique Support Ticket
            string UniqueSupportTicketID = randomm + randommInt + randommInt1 + randomm1;
            //var kk = _context.Database.SqlQuery
            if(dbTable == "Players")
            {
                var result = _context.Players.Where(x => x.Guid == UniqueSupportTicketID).SingleOrDefault();
                if(result != null)
                {
                    goto RandomGenerator;
                }
                else
                {
                    return UniqueSupportTicketID;
                }
            }else if(dbTable == "BlogPosts")
            {
                var result = _context.BlogPosts.Where(x => x.Guid == UniqueSupportTicketID).SingleOrDefault();
                if (result != null)
                {
                    goto RandomGenerator;
                }
                else
                {
                    return UniqueSupportTicketID;
                }
            }
            else if (dbTable == "Grounds")
            {
                var result = _context.Grounds.Where(x => x.Guid == UniqueSupportTicketID).SingleOrDefault();
                if (result != null)
                {
                    goto RandomGenerator;
                }
                else
                {
                    return UniqueSupportTicketID;
                }
            }
            else if (dbTable == "Clubs")
            {
                var result = _context.Clubs.Where(x => x.Guid == UniqueSupportTicketID).SingleOrDefault();
                if (result != null)
                {
                    goto RandomGenerator;
                }
                else
                {
                    return UniqueSupportTicketID;
                }
            }
            else if (dbTable == "Leagues")
            {
                var result = _context.Leagues.Where(x => x.Guid == UniqueSupportTicketID).SingleOrDefault();
                if (result != null)
                {
                    goto RandomGenerator;
                }
                else
                {
                    return UniqueSupportTicketID;
                }
            }
            else if (dbTable == "Partners")
            {
                var result = _context.Partners.Where(x => x.Guid == UniqueSupportTicketID).SingleOrDefault();
                if (result != null)
                {
                    goto RandomGenerator;
                }
                else
                {
                    return UniqueSupportTicketID;
                }
            }
            else if (dbTable == "Seasons")
            {
                var result = _context.Seasons.Where(x => x.Guid == UniqueSupportTicketID).SingleOrDefault();
                if (result != null)
                {
                    goto RandomGenerator;
                }
                else
                {
                    return UniqueSupportTicketID;
                }
            }
            else if (dbTable == "SubCategories")
            {
                var result = _context.SubCategories.Where(x => x.Guid == UniqueSupportTicketID).SingleOrDefault();
                if (result != null)
                {
                    goto RandomGenerator;
                }
                else
                {
                    return UniqueSupportTicketID;
                }
            }
            else if (dbTable == "Countries")
            {
                var result = _context.Countries.Where(x => x.Guid == UniqueSupportTicketID).SingleOrDefault();
                if (result != null)
                {
                    goto RandomGenerator;
                }
                else
                {
                    return UniqueSupportTicketID;
                }
            }
            else if (dbTable == "Videos")
            {
                var result = _context.Videos.Where(x => x.Guid == UniqueSupportTicketID).SingleOrDefault();
                if (result != null)
                {
                    goto RandomGenerator;
                }
                else
                {
                    return UniqueSupportTicketID;
                }
            }
            else if (dbTable == "Games")
            {
                var result = _context.Games.Where(x => x.Guid == UniqueSupportTicketID).SingleOrDefault();
                if (result != null)
                {
                    goto RandomGenerator;
                }
                else
                {
                    return UniqueSupportTicketID;
                }
            }
            //using (var connection = _context.Database.GetDbConnection())
            //{
            //    connection.Open();
            //    using (var command = connection.CreateCommand())
            //    {
            //        command.CommandText = $"SELECT * FROM {dbTable} where Guid ='{UniqueSupportTicketID}'";
            //        var result = command.ExecuteScalar();
            //        if (result != null)
            //        {
            //            connection.Close();
            //            command.Dispose();
            //            goto RandomGenerator;
            //        }
            //        command.Dispose();
            //    }
            //    connection.Close();
            //}                           
            //return UniqueSupportTicketID;
            return null;
        }
        private static IEnumerable<string> RandomStrings(
            string allowedChars,
            int minLength,
            int maxLength,
            int count,
            Random rng)
        {
            char[] chars = new char[maxLength];
            int setLength = allowedChars.Length;

            while (count-- > 0)
            {
                int length = rng.Next(minLength, maxLength + 1);

                for (int i = 0; i < length; ++i)
                {
                    chars[i] = allowedChars[rng.Next(setLength)];
                }

                yield return new string(chars, 0, length);
            }
        }
        public static int CommissionPercentage = 2;
        public static int VatPercentage = 13;

        //public static SelectList ToSelectList(DataTable table, string valueField, string textField)
        //{
        //    List<SelectListItem> list = new List<SelectListItem>();

        //    foreach (DataRow row in table.Rows)
        //    {
        //        list.Add(new SelectListItem()
        //        {
        //            Text = row[textField].ToString(),
        //            Value = row[valueField].ToString()
        //        });
        //    }

        //    return new SelectList(list, "Value", "Text");
        //}
        public static string BaseUrl()
        {
            //   return "http://0ff57244.ngrok.io";
            //## Todo : Replaced by store subdomain
            if (System.IO.Directory.GetCurrentDirectory().ToLower().Contains("repos"))
            {
                //return "http://7909bbf3.ngrok.io";                            
                return "http://localhost:5000";
            }
            return "http://sportsmandu.com";
        }
        private static string GetFileName(IFormFile file)
        {
            return file.FileName;
        }
        public static List<ImageSize> GlobalImageSizes()
        {
            return new List<ImageSize>
            {
                 new ImageSize
                {
                    Height = 400,
                    Width = 470,
                    enumImageSegment = enumImageSegment.user_avatar     ,
                    DefaultNoImagePath = "wwwroot/images/no-profile-image.jpg",
                    DefaultSavePath = "wwwroot/useruploads/1/users/"
                },
                new ImageSize
                {
                    Height = 400,
                    Width = 470,
                    enumImageSegment = enumImageSegment.player_profile     ,
                    DefaultNoImagePath = "wwwroot/images/no-profile-image.jpg",
                    DefaultSavePath = "wwwroot/useruploads/1/players/thumbnail/"
                },
                new ImageSize
                {
                    Height = 380,
                    Width = 1920,
                    enumImageSegment = enumImageSegment.player_coverImage,
                    DefaultNoImagePath = "wwwroot/images/no-profile-image.jpg",
                    DefaultSavePath = "wwwroot/useruploads/1/players/CoverImage/"
                },
                new ImageSize
                {
                    Height = 400,
                    Width = 470,
                    enumImageSegment = enumImageSegment.club_profile     ,
                    DefaultNoImagePath = "wwwroot/images/no-profile-image.jpg",
                    DefaultSavePath = "wwwroot/useruploads/1/clubs/thumbnail/"
                }, new ImageSize
                {
                    Height = 380,
                    Width = 1920,
                    enumImageSegment = enumImageSegment.club_coverImage,
                    DefaultNoImagePath = "wwwroot/images/no-profile-image.jpg",
                    DefaultSavePath = "wwwroot/useruploads/1/clubs/CoverImage/"
                },
                new ImageSize
                {
                    Height = 400,
                    Width = 470,
                    enumImageSegment = enumImageSegment.season_profile     ,
                    DefaultNoImagePath = "wwwroot/images/no-profile-image.jpg",
                    DefaultSavePath = "wwwroot/useruploads/1/Seasons/thumbnail/"
                }, new ImageSize
                {
                    Height = 380,
                    Width = 1920,
                    enumImageSegment = enumImageSegment.season_coverImage,
                    DefaultNoImagePath = "wwwroot/images/no-profile-image.jpg",
                    DefaultSavePath = "wwwroot/useruploads/1/Seasons/CoverImage/"
                },
                new ImageSize
                {
                    Height = 400,
                    Width = 470,
                    enumImageSegment = enumImageSegment.ground_profile     ,
                    DefaultNoImagePath = "wwwroot/images/no-profile-image.jpg",
                    DefaultSavePath = "wwwroot/useruploads/1/grounds/thumbnail/"
                }, new ImageSize
                {
                    Height = 380,
                    Width = 1920,
                    enumImageSegment = enumImageSegment.ground_coverImage,
                    DefaultNoImagePath = "wwwroot/images/no-profile-image.jpg",
                    DefaultSavePath = "wwwroot/useruploads/1/grounds/CoverImage/"
                },
                new ImageSize
                {
                    Height = 400,
                    Width = 470,
                    enumImageSegment = enumImageSegment.league_profile     ,
                    DefaultNoImagePath = "wwwroot/images/no-profile-image.jpg",
                    DefaultSavePath = "wwwroot/useruploads/1/leagues/thumbnail/"
                }, new ImageSize
                {
                    Height = 380,
                    Width = 1920,
                    enumImageSegment = enumImageSegment.league_coverImage,
                    DefaultNoImagePath = "wwwroot/images/no-profile-image.jpg",
                    DefaultSavePath = "wwwroot/useruploads/1/leagues/CoverImage/"
                },
            };
        }
        public static string SaveAndGetImage(IFormFile file, string path, IHostingEnvironment _hostingEnvironment, string fileNameToSave)
        {
            List<ImageSize> LoadImageSizes = GlobalImageSizes();
            var uploads = Path.Combine(_hostingEnvironment.WebRootPath, path);
            Directory.CreateDirectory(uploads);
            string NewfileName = GetFileName(file);
            int lastPart = GetFileName(file).LastIndexOf(".");
            string FirstPart = NewfileName.Substring(0, lastPart);
            int intRemoveFirstPartTrailing = FirstPart.LastIndexOf("-");
            if (intRemoveFirstPartTrailing > 0) FirstPart = FirstPart.Substring(0, intRemoveFirstPartTrailing);
            string LastPart = NewfileName.Substring(lastPart + 1);
            if (fileNameToSave.Length > 0)
            { // Predefined Filename should be saved.
                NewfileName = string.Format("{0}.{1}", fileNameToSave, LastPart);
                goto CheckPoint;
            }
            if (System.IO.File.Exists(uploads + "/" + NewfileName))
            {
                int counter = 0;
                do
                {
                    counter += 1;
                    NewfileName = string.Format("{0}-{1}.{2}", FirstPart, counter.ToString(), LastPart);
                } while (System.IO.File.Exists(uploads + "/" + NewfileName));
            }
        CheckPoint:            
            using (var fileStream = new FileStream(Path.Combine(uploads, NewfileName), FileMode.Create))
            {
                file.CopyTo(fileStream);
            }
            string imageLocation = $"{path}/{ NewfileName}";
            if (path.ToLower().Contains("coverimage"))
            {
                var CoverImageSettings = LoadImageSizes.Where(x => x.enumImageSegment == enumImageSegment.player_coverImage).SingleOrDefault();
                Image<Rgba32> img1 = Image.Load("wwwroot/" + imageLocation);
                string coverImage = GetCoverImage(img1, GetCleanUrl("wwwroot/" +path + "/" + NewfileName),CoverImageSettings,null,true);
                return coverImage;
            }else if (path.ToLower().Contains("thumbnail"))
            {
                var CoverImageSettings = LoadImageSizes.Where(x => x.enumImageSegment == enumImageSegment.player_profile).SingleOrDefault();
                Image<Rgba32> img1 = Image.Load("wwwroot/" + imageLocation);
                string thumbNail = GetCoverImage(img1, GetCleanUrl("wwwroot/" + path + "/" + NewfileName), CoverImageSettings, null, true);
            }
            return ($"{path}/{NewfileName}");
        }
        public static int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
        public static string GetFbImage(string Url,string FileName)
        {
            WebClient webClient = new WebClient();
            string file = "images/users/images/" + FileName + ".jpg";
            webClient.DownloadFile(Url,"wwwroot/" + file);
            return file;
        }
        public static string FrontEndUseChangeBackground(string backgroundImage,enumImageSegment whichPart,bool Watermark = false)
        {            
            if (!backgroundImage.Contains("http") && !backgroundImage.Contains("wwwroot") && !backgroundImage.Contains("c:"))
            {
                backgroundImage = "wwwroot/" + backgroundImage;
            }
            ImageSize CoverImageSettings = new ImageSize();
            CoverImageSettings = GlobalImageSizes().Where(x => x.enumImageSegment == whichPart).SingleOrDefault();
            Image<Rgba32> img1 = Image.Load(backgroundImage);
            string coverImage = GetCoverImage(img1, GetCleanUrl(backgroundImage), CoverImageSettings, null, true,Watermark);
            return coverImage;
        }       
        private static string GetCoverImage(Image<Rgba32> img1, string SavePath, ImageSize ImageSize, List<Image<Rgba32>> MultipleImages = null,bool onlyResize = false,bool watermark=false)
        {
            Image<Rgba32> imageToReturn;
            using (Image<Rgba32> outputImage = new Image<Rgba32>(ImageSize.Width, ImageSize.Height))
            {
                int imageSeperationY = 0;
                int imageSeperationX = 0;
                if (img1 != null)
                {
                    if (!onlyResize)
                    {
                        img1.Mutate(o => o.Resize(new Size(100, 150)));
                    }
                    else
                    {
                        if (SavePath.Contains(".jpg"))
                        {
                            SavePath = SavePath.ToLower().Replace(".jpg","_" + ImageSize.Width + "_" + ImageSize.Height + ".jpg");
                        }
                        if (SavePath.Contains(".png"))
                        {
                            SavePath = SavePath.ToLower().Replace(".png", "_" + ImageSize.Width + "_" + ImageSize.Height + ".png");
                        }
                        if (watermark)
                        {
                        img1.Mutate(o =>
                        o.ApplyScalingWaterMark("c:/logo/")
                        .Resize(new Size(ImageSize.Width, ImageSize.Height)));
                        if (SavePath.Contains("c\\"))
                        {
                            SavePath = SavePath.Replace("c\\", "c:\\");
                        }
                        img1.Save(SavePath);
                        return SavePath.Replace("wwwroot/", "");
                        }
                        else
                        {
                            img1.Mutate(o =>                        
                                o.Resize(new Size(ImageSize.Width, ImageSize.Height)));
                            if (SavePath.Contains("c\\"))
                            {
                                SavePath = SavePath.Replace("c\\", "c:\\");
                            }
                            img1.Save(SavePath);
                            return SavePath.Replace("wwwroot/", "");
                        }
                    }
                    for (var y = 0; y < 20; y++)
                    {
                        for (var i = 0; i < 5; i++)
                        {
                            if (imageSeperationY <= 380)
                            {
                                outputImage.Mutate(o => o
                               .DrawImage(img1, new Point(imageSeperationX, imageSeperationY), 1));
                                imageSeperationY = imageSeperationY + img1.Height;
                            }
                        }
                        imageSeperationY = 0;
                        imageSeperationX = imageSeperationX + img1.Width;
                    }
                }

                if (img1 == null)
                {
                    for (var y = 0; y < 20; y++)
                    {
                        var imageToUse = MultipleImages.Skip(RandomNumber(0, MultipleImages.Count())).Take(1).FirstOrDefault();
                        imageToUse.Mutate(o => o.Resize(new Size(100, 150)));
                        for (var i = 0; i < 5; i++)
                        {
                            imageToUse = MultipleImages.Skip(RandomNumber(0, MultipleImages.Count())).Take(1).FirstOrDefault();
                            imageToUse.Mutate(o => o.Resize(new Size(100, 150)));
                            if (imageSeperationY <= 380)
                            {
                                outputImage.Mutate(o => o                                
                               .DrawImage(imageToUse, new Point(imageSeperationX, imageSeperationY), 1));
                                imageSeperationY = imageSeperationY + imageToUse.Height;
                            }
                        }
                        imageSeperationY = 0;
                        imageSeperationX = imageSeperationX + imageToUse.Width;
                    }
                }
                //WaterMark.ApplyScalingWaterMark(outputImage, "");
                outputImage.Save(SavePath);
                imageToReturn = outputImage;
            }
            return SavePath.Replace("Wwwroot/", "");
            //return imageToReturn;
        }
        public static int Pagesize()
        {
            return 10;
        }
        public static IActionResult SendAsItIs(GenericResult _result)
        {
            if (_result.Succeded) return new OkObjectResult(_result);
            return new BadRequestObjectResult(_result);
        }
        public static IActionResult SendAsItIs(object What)
        {
            return new OkObjectResult(What);
        }

        public static double ProcessCommission(double price)
        {
            //TODO Individual Comission Rate for individual Vendors for Individual Parts
            double commissionPrice = (CommissionPercentage / 100) * price;
            return price + commissionPrice;
        }
        public static GenericResult Error(Exception ex = null, string Message = null, string data = null, string[] arrayData = null)
        {
            if (ex != null)
            {
                string FileName = "C:/Errors/" + DateTime.Now.ToString().Replace(":", "").Replace(" ", "_").Replace("/", "-") + ".txt";
                var logPath = FileName;//System.IO.Path.GetTempFileName();
                var logFile = System.IO.File.Create(logPath);
                var logWriter = new System.IO.StreamWriter(logFile);
                List<string> newCodes = new List<string>();
                newCodes.Add("Exception" + ex);
                newCodes.Add("Exception Message" + ex.Message);
                // newCodes.Add("InnerException Source : " + ex.InnerException.Source);
                // newCodes.Add("InnerException Data : " + ex.InnerException.Data);
                newCodes.Add("Execption Source : " + ex.Source);
                newCodes.Add("Execption HelpLink : " + ex.HelpLink);
                newCodes.Add("Execption Stack Trace : " + ex.StackTrace);
                foreach (var items in newCodes)
                {
                    logWriter.WriteLine(items);
                }
                logWriter.Dispose();
            }

            return new GenericResult()
            {
                Succeded = false,
                Message = Message,
                //ArrayData = arrayData,
                //Data = data
            };
        }

        public static GenericResult Success(string Message = "", string Data = "")
        {
            return new GenericResult()
            {
                Succeded = true,
                Message = Message,
                Data = Data
            };
        }

        public static GenericResult Failed(string Message = "", string Data = "")
        {
            return new GenericResult()
            {
                Succeded = false,
                Message = Message,
                Data = Data
            };
        }

        public static GenericResult AccessDenied()
        {
            return new GenericResult()
            {
                Succeded = false,
                Message = "Permission Denied."
            };
        }
        public static string GetCleanUrl(string String)
        {

            String = ToTitleCase(String);
            String = String
                 .Trim()
                 .Replace(",", "")
                 .Replace("\"", "")
                 .Replace(" ", "-")
                 .Replace("|", "")
                 .Replace("(", "")
                 .Replace("’", "")
                 .Replace(":", "")
                 .Replace("‘", "")
                 .Replace(">", "")
                 .Replace("<", "")
                 .Replace(")", "")
                 .Replace("--", "-")
                 .Replace("--", "-")
                 .Replace("-I-", "-")
                 .Replace("\t", "")
                 .Replace("'", "")
                 ;
            if (String.EndsWith("-"))
                String = String.Substring(0, String.Length - 1);
            return String;
        }
        public static string ToTitleCase(this string str)
        {
            var tokens = str.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < tokens.Length; i++)
            {
                var token = tokens[i];
                tokens[i] = token.Substring(0, 1).ToUpper() + token.Substring(1);
            }

            return string.Join(" ", tokens);
        }
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
 (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
        public static int CacheDuration()
        {
            return 5;
        }
        public static void Live_Crash_Report(Exception ex)
        {
            new RockmanduErrorReporting("").ReportError(ex);
            //new RaygunClient("8pzeRhLbAqKgNIQN3slglA==").SendInBackground(ex);
        }
        public static string FilterEnumString(string enumValue)
        {
            return enumValue.Replace("_", " ");
        }

        public static string filterStartingNumbers(string id)
        {
            if (Char.IsLetter(id[0])) return id;
            id = filterStartingNumbers(id.Substring(1));
            return id;
        }
        public static ScriptCss obfuscate(ScriptCss s)
        {
            string media_query_style = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string background_image_styles = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string dynamic_styles = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string hangout = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string head = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string content_container = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string content = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string overlay = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string card_menu = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string header = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string adminimage = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string adminname = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string supportstaffinfo = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string offline_form = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string offline_form_header = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string form_header = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string form_container = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string input_container = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string IdName = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string IdEmail = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string IdPhone_Number = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string IdMessage = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string offline_form_button = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string message_sent = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string company_info = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string list_chat = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));
            string style_bg = filterStartingNumbers(Guid.NewGuid().ToString().Replace("-", "_"));



            s.HTML = s.HTML.Replace("media-query-style", media_query_style);
            s.Script = s.Script.Replace("media-query-style", media_query_style);

            s.HTML = s.HTML.Replace("background_image_styles", background_image_styles);
            s.Script = s.Script.Replace("background_image_styles", background_image_styles);

            s.HTML = s.HTML.Replace("dynamic-styles", dynamic_styles);
            s.Script = s.Script.Replace("dynamic-styles", dynamic_styles);


            s.HTML = s.HTML.Replace("hangout", hangout);
            s.Script = s.Script.Replace("#hangout", "#" + hangout);
            s.Css = s.Css.Replace("#hangout", "#" + hangout);

            s.HTML = s.HTML.Replace("'head'", "'" + head + "'");
            s.Css = s.Css.Replace("#head", "#" + head);
            s.Script = s.Script.Replace("#head", "#" + head);

            //s.Script = s.Script.Replace("'head'", "'"+head+"'");


            s.HTML = s.HTML.Replace("content-container", content_container);
            s.Script = s.Script.Replace("content-container", content_container);
            s.Css = s.Css.Replace("content-container", content_container);

            s.HTML = s.HTML.Replace("content", content);
            s.Script = s.Script.Replace("#content", "#" + content);
            s.Css = s.Css.Replace("#content", "#" + content);


            s.HTML = s.HTML.Replace("overlay", overlay);
            s.Css = s.Css.Replace(".overlay", "." + overlay);
            s.Script = s.Script.Replace(".overlay", "." + overlay);

            // s.HTML = s.HTML.Replace("card menu", card_menu);
            // s.Script = s.Script.Replace("card menu", card_menu);

            s.HTML = s.HTML.Replace("'header'", "'" + header + "'");
            s.Css = s.Css.Replace(".header", "." + header);

            s.HTML = s.HTML.Replace("adminimage", adminimage);
            s.Script = s.Script.Replace("adminimage", adminimage);

            s.HTML = s.HTML.Replace("adminname", adminname);
            s.Script = s.Script.Replace("adminname", adminname);

            s.HTML = s.HTML.Replace("supportstaffinfo", supportstaffinfo);
            s.Script = s.Script.Replace("supportstaffinfo", supportstaffinfo);

            s.HTML = s.HTML.Replace("offline-form-header", offline_form_header);
            //s.Script = s.Script.Replace("offline-form-header", offline_form_header);

            s.HTML = s.HTML.Replace("'offline-form'", "'" + offline_form + "'");
            s.Css = s.Css.Replace(".offline-form", "." + offline_form);
            s.Script = s.Script.Replace(".offline-form", "." + offline_form);

            s.HTML = s.HTML.Replace("'form-header'", "'" + form_header + "'");
            s.Css = s.Css.Replace(".form-header", "." + form_header);

            // s.HTML = s.HTML.Replace("form-container", form_container);
            // s.Script = s.Script.Replace("form-container", form_container);

            s.HTML = s.HTML.Replace("input-container", input_container);
            s.Css = s.Css.Replace(".input-container", "." + input_container);

            // s.HTML = s.HTML.Replace("id='Name'", "id='" + IdName + "'");
            // s.Script = s.Script.Replace("id='Name'", "id='" + IdName + "'");

            // s.HTML = s.HTML.Replace("id='Email'", "id='" + IdEmail + "'");
            // s.Script = s.Script.Replace("id='Email'", "id='" + IdEmail + "'");

            // s.HTML = s.HTML.Replace("id='Phone Number'", "id='" + IdPhone_Number + "'");
            // s.Script = s.Script.Replace("id='Phone Number'", "id='" + IdPhone_Number + "'");

            // s.HTML = s.HTML.Replace("id='Message'","id='"+ IdMessage +"'");
            // s.Script = s.Script.Replace("id='Message'","id='"+ IdMessage +"'");


            s.HTML = s.HTML.Replace("offline-form-button", offline_form_button);
            s.Script = s.Script.Replace("offline-form-button", offline_form_button);

            s.HTML = s.HTML.Replace("message-sent", message_sent);
            s.Script = s.Script.Replace("message-sent", message_sent);
            s.Css = s.Css.Replace("message-sent", message_sent);

            s.HTML = s.HTML.Replace("company-info", company_info);
            s.Css = s.Css.Replace("company-info", company_info);
            s.Script = s.Script.Replace("company-info", company_info);

            s.HTML = s.HTML.Replace("list-chat", list_chat);
            s.Css = s.Css.Replace(".list-chat", "." + list_chat);
            s.Script = s.Script.Replace(".list-chat", "." + list_chat);

            s.HTML = s.HTML.Replace("style-bg", style_bg);
            s.Script = s.Script.Replace("style-bg", style_bg);

            return s;
        }
        public static string SuperUser()
        {
            return "masterstill@gmail.com";
        }
        public static int PremiumUserDate()
        {
            return 30;
        }
        public static GenericResult AuthorizationFailureMessage()
        {
            return new GenericResult
            {
                Succeded = false,
                Message = "Not Authorized"
            };
        }
        public static string NoImage()
        {
            string protocol = "https";
            string domain = "sportsmandu.com";
            string ImageLoacation = "/images/noimage.png";
            return protocol + "://" + domain + ImageLoacation;
        }
        public static IActionResult AuthorizationFailureRoute()
        {
            return new BadRequestObjectResult(AuthorizationFailureMessage());
        }
        public static string DefaultChatWidgetBackgroundColour()
        {
            return "Green";
        }
        public static string DevelopmentUserName()
        {
            return "masterstill@gmail.com";
        }
        public static int FreeWebsiteLimit()
        {
            return 10;
        }

        public static bool DevelopmentMode()
        {
            return true;
        }
        public static string DefaultOfflineText()
        {
            return "Hey Hi we are not online now please leave a message.";
        }
        public static EmailFormat EmailTemplate(enumEmailTemplate template)
        {
            //TOODO multilingual stufffs
            EmailFormat e = new EmailFormat();
            string messageBody = System.IO.File.ReadAllText("EmailTemplates/" + template.ToString() + ".html");
            switch (template)
            {
                case enumEmailTemplate.AssignedToRole:
                    break;
                case enumEmailTemplate.ConfirmationEmail:
                    e.Subject = "Confirm your account";
                    e.Message = messageBody;
                    break;
                case enumEmailTemplate.ForgotyourPassword:
                    e.Subject = "Reset your Password";
                    e.Message = messageBody;
                    break;
                case enumEmailTemplate.InvitationForRoleNewUser:
                    e.Subject = "Invitation by {fullname}";
                    e.Message = messageBody;
                    break;
                case enumEmailTemplate.WelcomeEmail:
                    e.Subject = "Welcome to Live sportsmandu";
                    e.Message = messageBody;
                    break;
                case enumEmailTemplate.RefferalInvitation:
                    break;
                default:
                    Console.WriteLine("No Tempelate Found");
                    break;
            }
            return e;
        }
        public static string WidgetCss(string WidgetName)
        {
            string chatWidgetCss = string.Empty;
            try
            {
                chatWidgetCss = System.IO.File.ReadAllText("/Widgets/" + WidgetName + "/" + "style.css");
            }
            catch (Exception ex)
            {
                Global.Error(ex);
                // we need this stupid thing as VS reads from IIS
                chatWidgetCss = VSCODEURL(WidgetName, "style.css");
            }
            return "<style>" + chatWidgetCss + "</style>";
        }
        public static string VSCODEURL(string WidgetName, string fileName)
        {
            string VSCODEURL = System.IO.File.ReadAllText("C:/Users/MasterStill/source/repos/sportsmandu/sportsmandu/Widgets/" + WidgetName + "/" + fileName);
            return VSCODEURL;
        }
        public static string WidgetJs(string WidgetName)
        {
            string chatWidgetJs = string.Empty;
            try
            {
                chatWidgetJs = System.IO.File.ReadAllText("Widgets/" + WidgetName + "/" + "javascript.js");
            }
            catch (Exception ex)
            {
                Global.Error(ex);
                // we need this stupid thing as VS reads from IIS
                chatWidgetJs = VSCODEURL(WidgetName, "javascript.js");
            }
            return chatWidgetJs;
        }
        public static string WidgetHtml(string WidgetName)
        {
            string chatWidgetCss;
            try
            {
                chatWidgetCss = System.IO.File.ReadAllText("Widgets/" + WidgetName + "/" + "index.html");
            }
            catch (Exception ex)
            {
                Global.Error(ex);
                chatWidgetCss = VSCODEURL(WidgetName, "index.html");
            }
            return chatWidgetCss;

        }
        public static string WidgetIframe()
        {
            string chatWidgetCss;
            try
            {
                chatWidgetCss = System.IO.File.ReadAllText("Widgets/" + "iframe.html");
            }
            catch (Exception ex)
            {
                Global.Error(ex);
                chatWidgetCss = System.IO.File.ReadAllText("C:/Users/MasterStill/source/repos/sportsmandu/sportsmandu/Widgets/iframe.html");
            }
            return chatWidgetCss;
        }

        public static ScriptCss getWidgetScriptandMore(string widgetNAme = "widget1")
        {
            ScriptCss s = new ScriptCss();
            s.HTML = WidgetCss(widgetNAme) + WidgetHtml(widgetNAme);
            s.Script = WidgetJs(widgetNAme);
            s.Css = WidgetCss(widgetNAme);
            s.IFrame = WidgetIframe();
            return s;
        }

        public class EmailFormat
        {
            public string Subject { get; set; }
            public string Message { get; set; }
        }
        public class RockmanduErrorReporting
        {
            private string _applicationId;
            private static HttpClient client = new HttpClient();
            public RockmanduErrorReporting(string ApplicationId)
            {
                this._applicationId = ApplicationId;
            }
            public void ReportError(Exception ex)
            {
                try
                {
                    Uri uri = new Uri("http://localhost:5000/api/Error");
                    var data = JsonConvert.SerializeObject(ex);
                    client.PostAsync(uri, new StringContent(data, Encoding.UTF8, "application/json"));
                }
                catch
                {
                    Console.Write("Error Occured Report Error");
                }
            }
        }



        //sportsmandu Purano bata taneko !!
        public class MultiLingualGlobal
        {
            public MultilingualAI GenericMultiLingualRepo(IEnumerable<MultiLingualViewModel> multiObject, List<int> _userKnownCulture, int CultureId)
            {
                List<int> convertedIDs = new List<int>();
                IEnumerable<MultiLingualViewModel> objToCheck = multiObject;
                foreach (var items in objToCheck)
                {
                    if (items.CultureId == CultureId)
                    {
                        items.Converted = true;
                        if (items.multilingualid != items.Id)
                        {
                            convertedIDs.Add(items.multilingualid);
                        }
                    }
                }
                foreach (var items in objToCheck.Where(x => x.Converted == false))
                {
                    bool knownLanguageByUser = false;
                    foreach (var knownCulture in _userKnownCulture)
                    {
                        if (items.CultureId == knownCulture)
                        {
                            foreach (var sknownCulture in _userKnownCulture)
                            {
                                if (sknownCulture == CultureId)
                                {
                                    knownLanguageByUser = true;
                                    break;
                                }
                            }
                        }
                    }
                    foreach (var convertedItems in objToCheck.Where(x => x.Converted == true))
                    {
                        if (convertedItems.multilingualid == items.multilingualid)
                        {
                            convertedIDs.Add(items.Id);
                        }
                    }
                    if (!knownLanguageByUser)
                    {
                        convertedIDs.Add(items.Id);
                    }
                }
                IEnumerable<MultiLingualViewModel> results = from item in objToCheck
                                                             where !convertedIDs.Contains(item.Id)
                                                             //where _userKnownCulture.Contains(item.CultureId)
                                                             select item;
                MultilingualAI test = new MultilingualAI();
                test.items = results;
                test.idToRemove = convertedIDs; return test;
            }
        }
    }
    public static class MultiLingualGlobal
    {
        public static PlayerAdminViewModel GenericMultiLingualRepoNew(List<PlayerConvertedCulture> GenericConvertedCulture, List<int> _userKnownCulture, int CultureId, List<Player> PlayerAdminViewModel,IMapper _Mapper)
        {
            List<PlayerAdminViewModel> pavm = new List<PlayerAdminViewModel>();
            foreach(var items in PlayerAdminViewModel)
            {
                if (GenericConvertedCulture.Where(x=>x.PlayerId == items.Id).Where(x=>x.Culture.Id == CultureId).SingleOrDefault() == null)
                {
                    var objToReturn = _Mapper.Map<PlayerAdminViewModel>(items);
                    objToReturn.Converted = false;
                    pavm.Add(objToReturn);
                }
            }
            return new PlayerAdminViewModel();
        }
        public static MultilingualAI GenericMultiLingualRepo(IEnumerable<MultiLingualViewModel> multiObject, List<int> _userKnownCulture, int CultureId)
        {
            List<int> convertedIDs = new List<int>();
            IEnumerable<MultiLingualViewModel> objToCheck = multiObject;
            foreach (var items in objToCheck)
            {
                if (items.CultureId == CultureId)
                {
                    items.Converted = true;
                    if (items.multilingualid != items.Id)
                    {
                        convertedIDs.Add(items.multilingualid);
                    }
                }
            }
            foreach (var items in objToCheck.Where(x => x.Converted == false))
            {
                bool knownLanguageByUser = false;
                foreach (var knownCulture in _userKnownCulture)
                {
                    if (items.CultureId == knownCulture)
                    {
                        foreach (var sknownCulture in _userKnownCulture)
                        {
                            if (sknownCulture == CultureId)
                            {
                                knownLanguageByUser = true;
                                break;
                            }
                        }
                    }
                }
                foreach (var convertedItems in objToCheck.Where(x => x.Converted == true))
                {
                    if (convertedItems.multilingualid == items.multilingualid)
                    {
                        convertedIDs.Add(items.Id);
                    }
                }
                if (!knownLanguageByUser)
                {
                    convertedIDs.Add(items.Id);
                }
            }
            IEnumerable<MultiLingualViewModel> results = from item in objToCheck
                                                         where !convertedIDs.Contains(item.Id)
                                                         //where _userKnownCulture.Contains(item.CultureId)
                                                         select item;
            MultilingualAI test = new MultilingualAI();
            test.items = results;
            test.idToRemove = convertedIDs; return test;
        }
    }
    public static class WaterMark
    {
        public static IImageProcessingContext<TPixel> ApplyScalingWaterMark<TPixel>(
            this IImageProcessingContext<TPixel> processingContext,
            string cacheFolder)
            where TPixel : struct, IPixel<TPixel>
        {
            return processingContext.Apply(
                image =>
                {
                    string path = Path.Combine(cacheFolder, "logo.png");

                    if (!File.Exists(path))
                    {
                        return;
                    }

                        // Dispose of logo once used.
                        using (var logo = Image.Load<TPixel>(path))
                    {
                        bool worh = image.Width > image.Height;
                        double coff = worh
                                    ? image.Width * 0.3 / Math.Min(logo.Width, logo.Height)
                                    : image.Height * 0.3 / Math.Max(logo.Width, logo.Height);

                        var size = new Size((int)(logo.Width * coff), (int)(logo.Height * coff));
                        var location = new Point(image.Width - size.Width - 5, image.Height - size.Height - 5);

                            // Resize the logo
                            logo.Mutate(i => i.Resize(size));
                        image.Mutate(i => i.DrawImage(logo, location,PixelColorBlendingMode.Normal,PixelAlphaCompositionMode.SrcAtop ,0.5f));
                    }
                });
        }
    }
}