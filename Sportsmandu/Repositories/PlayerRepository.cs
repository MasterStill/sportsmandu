﻿using Sportsmandu.Data;
using Sportsmandu.Data.Infrastructure.Repository.Abstract;
using Sportsmandu.Model.Entities.Player;
namespace Community.Data.Infrastructure.Repository
{
    public class PlayerRepository : SportsmanduBaseRepository<Player>, IPlayerRepository
    {
        public PlayerRepository(AppDbContext context)
            : base(context)
        { }
    }
}