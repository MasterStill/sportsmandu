﻿using Sportsmandu.Data.Infrastructure.Repository.Abstract;
using Sportsmandu.Model.Entities.Globalization;
namespace Sportsmandu.Data.Infrastructure.Repository
{
    public class CultureRepository : SportsmanduBaseRepository<Culture>, ICultureRepository
    {
        public CultureRepository(AppDbContext context)
            : base(context)
        { }
    }
}