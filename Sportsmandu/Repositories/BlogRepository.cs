﻿using Sportsmandu.Data;
using Sportsmandu.Data.Infrastructure.Repository.Abstract;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Models.Entities.Blog;

namespace Community.Data.Infrastructure.Repository
{
    public class BlogRepository : SportsmanduBaseRepository<BlogPost>, IBlogRepository
    {
        public BlogRepository(AppDbContext context)
            : base(context)
        { }
    }
    public class BlogTagRepository : SportsmanduBaseRepository<BlogTag>, IBlogTagRepository
    {
        public BlogTagRepository(AppDbContext context)
            : base(context)
        { }
    }
    public class BlogCategoryRespository : SportsmanduBaseRepository<BlogCategory>, IBlogCategoryRepository
    {
        public BlogCategoryRespository(AppDbContext context)
            : base(context)
        { }
    }
}
