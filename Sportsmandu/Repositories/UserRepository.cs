﻿using System.Collections.Generic;
using System;
using Sportsmandu.Data.Infrastructure.Repository.Abstract;
using Sportsmandu.Models;
using Sportsmandu.Data;

namespace Community.Data.Infrastructure.Repository
{
    public class UserRepository : SportsmanduBaseRepository<ApplicationUser>, IUserRepository
    {
        IRoleRepository _roleReposistory;
        public UserRepository(AppDbContext context)
            : base(context)
        { 
            this._roleReposistory = _roleReposistory;
        }

        public ApplicationUser GetSingleByUsername(string username)
        {
            return this.GetSingle(x => x.UserName == username);
        }

        //public IEnumerable<Role> GetUserRoles(string username)
        //{
        //    List<Role> _roles = null;

        //    ApplicationUser _user = this.GetSingle(u => u.UserName == username, u => u.Roles);
        //    if(_user != null)
        //    {
        //        _roles = new List<Role>();
        //        foreach (var _userRole in _user.Roles)
        //            _roles.Add(_roleReposistory.GetSingle(_userRole.RoleId));
        //    }

        //    return _roles;
        //}
        public IEnumerable<ApplicationUserRole> GetUserRoles(string username,int website)
        {
            Console.WriteLine("Checking UserRole for Username : " + username + " For StoreId " + website);
            List<ApplicationUserRole> _roles = null;
            ApplicationUser _user = this.GetSingle(u => u.UserName == username, u => u.Roles);
            if(_user != null)
            {
                _roles = new List<ApplicationUserRole>();
                foreach (var _userRole in _user.Roles)
                    {
                        //Console.WriteLine(_roleReposistory.GetSingle(_userRole.RoleId).Name);
                    }
                    
                    //_roles.Add(_roleReposistory.GetSingle(_userRole.RoleId));
                    //var role = _roleReposistory.GetSingle(_userRole.RoleId);
                     
                    // if (role != null){
                    //     _roles.Add(role);
                    // }
                    //_roles.Add(_roleReposistory.GetAll().Where(x=>x.StoreId == website).Where(x=>x.RoleId == _userRole.RoleId));
            }
            return _roles;
        }

        //public List<int> GetUserCulture(int UserId){
        //    ApplicationUser _user = this.GetSingle(u => u.Id == UserId, u => u.UserCulture);
        //    List<int> userCulture = new List<int>();
        //    foreach(var Cultures in _user.UserCulture){
        //        userCulture.Add(Cultures.CultureId);
        //    }
        //    return userCulture;
        //}
    }
}