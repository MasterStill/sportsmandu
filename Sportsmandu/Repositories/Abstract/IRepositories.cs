﻿using Community.Model.Entities.Global;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Model.Entities.Global;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Models;
using Sportsmandu.Models.Entities.Blog;
using System.Collections.Generic;
namespace Sportsmandu.Data.Infrastructure.Repository.Abstract
{
    //public interface ILoggingRepository : ISportsmanduBaseRepository<Error> { }
    //public interface IPhotoRepository : ISportsmanduBaseRepository<PhotoAlbumPhoto> { }
    //public interface IPhotoAlbumRepository : ISportsmanduBaseRepository<PhotoAlbum> { }
    //public interface ISliderRepository : ISportsmanduBaseRepository<Slider> { }
    //public interface IPhotoAlbumCategoryRepository : ISportsmanduBaseRepository<PhotoAlbumCategory> { }
    //public interface IStoreRepository : ISportsmanduBaseRepository<Store>{}     
    public interface IUserRepository : ISportsmanduBaseRepository<ApplicationUser>
    {
        ApplicationUser GetSingleByUsername(string username);
        //IEnumerable<ApplicationUserRole> GetUserRoles(string username);
        IEnumerable<ApplicationUserRole> GetUserRoles(string username, int website);
        //List<int> GetUserCulture(int UserId);
    }
    public interface IUserRoleRepository { }//: ISportsmanduBaseRepository<ApplicationUserRole> { }
    public interface IRoleRepository { } //: ISportsmanduBaseRepository<ApplicationRole> { }
    //public interface ILoggingRepository : ISportsmanduBaseRepository<Error> { }
    public interface IMenuRepository : ISportsmanduBaseRepository<Menu> { }
    public interface ICultureRepository : ISportsmanduBaseRepository<Culture> { }
    public interface IBlogRepository : ISportsmanduBaseRepository<BlogPost> { }
    public interface IBlogCategoryRepository : ISportsmanduBaseRepository<BlogCategory> { }
    public interface IBlogTagRepository : ISportsmanduBaseRepository<BlogTag> { }
    public interface IPlayerRepository : ISportsmanduBaseRepository<Model.Entities.Player.Player>{}
    // public interface IGlobalizationRepository : ISportsmanduBaseRepository<StaticGlobalization> { }
}