﻿using Sportsmandu.Model;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
namespace Sportsmandu.Data.Infrastructure.Repository.Abstract
{
    public interface ISportsmanduBaseRepository<T> where T : class, ISportsmanduBase, new()
    {
        IEnumerable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAllByUser(int Id);
        int Count();
        T GetSingle(int id);
        T GetSingle(Expression<Func<T, bool>> predicate);
        T GetSingle(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void DeleteWhere(Expression<Func<T, bool>> predicate);
        
        bool Duplicate(Expression<Func<T, bool>> predicate);
        T ReturnIfExist(Expression<Func<T, bool>> predicate);
        void Commit();
    }
}