using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Models.League;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Models.Globalization;
using Sportsmandu.Services.Abstract;
using static Sportsmandu.MultiLingualGlobal;
using Z.EntityFramework.Plus;
using Sportsmandu.Models;
using Sportsmandu.Models.Association;

namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    [Area("SeasonMatch")]
    [Authorize]
    public class SeasonMatchEventsController : Controller
    {
        private AppDbContext _context;
        private IMapper _mapper { get; set; }
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        public SeasonMatchEventsController(AppDbContext _context, IMapper _mapper, IFileAndDirectoryService _fileService, IHostingEnvironment _hostingEnviroment)
        {
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
            this._context = _context;
            this._mapper = _mapper;
        }
        private void SetViewBagStuffs(int Culture)
        {
            ViewBag.Categories = GetCategories(Culture);
            ViewBag.Countries = GetCountries(Culture);
        }
        private IEnumerable<SelectListItem> GetCountries(int CultureId)
        {
            return _context.Countries.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();

        }
        public ActionResult SeasonMatchEvent(int Culture, int multilingualid)
        {
            // Multilingual ID = SeasonMatchID

            return View();
        }
        public IActionResult Index(int culture, int multilingualid)
        {
            return SeasonMatchEvent(culture,multilingualid);
            //SetViewBagStuffs(culture);
            //var _result = _context.Association
            ////.Include(x => x.Category)
            //.Where(x => x.Delflag == false).ToList();
            //List<int> _userKnownCulture = new List<int>();
            //_userKnownCulture.Add(1);
            //_userKnownCulture.Add(2);
            //MultilingualAI MAI = GenericMultiLingualRepo(_mapper.Map<IEnumerable<MultiLingualViewModel>>(_result), _userKnownCulture, culture);
            //var result = from item in _result
            //             where !MAI.idToRemove.Contains(item.Id)
            //             select item;
            //var objToReturn = _mapper.Map<IEnumerable<AssociationAdminViewModel>>(result);
            //foreach (var items in objToReturn)
            //{
            //    items.Converted = MAI.items.Where(x => x.Id == items.Id).Select(x => x.Converted).SingleOrDefault();
            //    if (items.Image == null || items.Image == "")
            //    {
            //        items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
            //    }
            //}
            //return View(objToReturn);
        }
        public class AssociationearchViewModel
        {
            public int CultureId { get; set; }
            public int CategoryId { get; set; }
            public int BrandId { get; set; }
            public string Term { get; set; }
        }

        [HttpPost]
        public IActionResult Search([FromForm]AssociationearchViewModel psvm)
        {
            if (psvm.CultureId == 0) psvm.CultureId = 1;
            var _result = _context.Association
                    // .Include(x => x.Brand)
                    .Include(x => x.Category)
                    // .Include(x => x.Parts)
                    .Where(x => x.Delflag == false);
            if (psvm != null)
            {
                if (psvm.CategoryId != 0)
                {
                    _result = _result.Where(x => x.CategoryId == psvm.CategoryId);
                }
            }
            SetViewBagStuffs(1);
            List<int> _userKnownCulture = new List<int>();
            _userKnownCulture.Add(1);
            _userKnownCulture.Add(2);
            MultilingualAI MAI = GenericMultiLingualRepo(_mapper.Map<IEnumerable<MultiLingualViewModel>>(_result), _userKnownCulture, psvm.CultureId);
            var result = from item in _result
                         where !MAI.idToRemove.Contains(item.Id)
                         select item;
            var objToReturn = _mapper.Map<IEnumerable<AssociationAdminViewModel>>(result);
            foreach (var items in objToReturn)
            {
                items.Converted = MAI.items.Where(x => x.Id == items.Id).Select(x => x.Converted).SingleOrDefault();
                if (items.Image == null || items.Image == "")
                {
                    items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                }
            }
            SetViewBagStuffs(1);
            return View("Index", objToReturn);
        }
        public ActionResult Create(int Culture)
        {
            //ViewData["Brand"] = new SelectList(_context.Brands.Where(x => x.CultureId == Culture).ToList().Select(x => new { x.multilingualid, x.Name }), "multilingualid", "Name");
            //ViewData["Category"] = new SelectList(_context.Categories.Where(x => x.CultureId == Culture).ToList().Select(x => new { x.multilingualid, x.Name }), "multilingualid", "Name");
            SetViewBagStuffs(Culture);
            AssociationCreateViewModel p = new AssociationCreateViewModel();
            return View(p);
        }
        public ActionResult Details(int Culture, int multilingualid)
        {

            return View();
        }

        [HttpGet]
        public ActionResult Report()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AssociationCreateViewModel brand, IFormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                Association o = _mapper.Map<Association>(brand);
                _context.Entry(o).State = EntityState.Added;
                o.CreatedDate = DateTime.Now;
                //##TodO
                o.CreatedById = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault().Id;
                //o. = 1;
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Associations", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        o.Image = _result.ArrayData[0];
                    }
                }
                if (formCollection.Files.Count == 1 && o.multilingualid != 0)
                {
                    o.Image = _context.Association.SingleOrDefault(x => x.Id == o.multilingualid).Image;
                }
                if (o.multilingualid != 0)
                {
                    o.CategoryId = _context.Association.SingleOrDefault(x => x.Id == o.multilingualid).CategoryId;
                }
                _context.Association.Add(o);
                _context.SaveChanges();
                if (o.multilingualid == 0)
                {
                    //_context.Entry(o).State = EntityState.Modified;
                    o.multilingualid = o.Id;
                    _context.Association.Update(o);
                    _context.SaveChanges();
                }
                return RedirectToAction("Index", "Associations", new { area = "Admin", Culture = o.CultureId });
            }
            return View();
        }
        public ActionResult Edit(int id)
        {
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            Association Association = _context.Association
            .Where(x => x.Id == id).SingleOrDefault();
            SetViewBagStuffs(1);
            return View(Association);
        }
        private IEnumerable<SelectListItem> GetCategories(int CultureId)
        {
            return _context.Categories.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();

        }
        // private IEnumerable<SelectListItem> GetCategories(int CultureId)
        //{
        //    return _context.Categories.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
        //    {
        //        Value = x.multilingualid.ToString(),
        //        Text = x.Name
        //    }).ToList();

        //}             
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Association Association, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(enumClaimTypeSegments.Seasons.ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), Association.Id, User.Claims)) return (Unauthorized());

            if (ModelState.IsValid)
            {
                _context.Entry(Association).State = EntityState.Modified;
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Associations", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        Association.Image = _result.ArrayData[0];
                    }
                }
                Association.CreatedById = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault().Id;
                _context.SaveChanges();
                return RedirectToAction("Index", "Associations", new { area = "Admin", Culture = Association.CultureId });
            }
            return View(Association);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Association about = _context.Association.Where(x => x.Id == id).SingleOrDefault();
            _context.Entry(about).State = EntityState.Modified;
            about.Delflag = true;
            _context.SaveChanges();
            return RedirectToAction("Index", "Associations", new { area = "Admin", Culture = about.CultureId });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
