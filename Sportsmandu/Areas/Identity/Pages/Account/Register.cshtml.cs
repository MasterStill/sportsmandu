﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Sportsmandu.Models;
using Sportsmandu.Data;
using System.Linq;
using Sportsmandu.Models.enums;
namespace Sportsmandu.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        //private readonly AuthMessageSender _emailSender;
        private Sportsmandu.Services.IEmailSender _emailSender;
        private AppDbContext _context;
        public RegisterModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<RegisterModel> logger,
            Sportsmandu.Services.IEmailSender _emailSender 
            ,AppDbContext _context
            )
        {
            this._context = _context;
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            this._emailSender = _emailSender;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public class InputModel
        {
            [Required]
            [Display(Name = "FirstName")]
            public string FirstName { get; set; }

            [Required]
            [Display(Name = "LastName")]
            public string LastName { get; set; }

            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }
         
            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }
        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }
        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {            
            returnUrl = returnUrl ?? Url.Content("~/");
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = Input.Email, Email = Input.Email,FirstName = Input.FirstName,LastName = Input.LastName};
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    var justRegisteredUser = _context.Users.Where(x => x.Email == Input.Email).SingleOrDefault();
                    justRegisteredUser.UserIdGuid = Guid.NewGuid().ToString();
                    _context.Entry(justRegisteredUser).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    _context.SaveChanges();
                    _logger.LogInformation("User created a new account with password.");
                    var isinvited = _context.EmailInvitation.Where(x => x.Email == Input.Email).Where(x => x.Accepted == false);
                    if (isinvited.Count() > 0)
                    {
                        try
                        {
                            foreach (var items in isinvited)
                            {
                                ApplicationUserRole role = new ApplicationUserRole();
                                //role.ClubId = items.ClubId;
                                //role.RoleId = items.RoleId;
                                role.UserId = user.Id;
                                _context.UserRoles.Add(role);
                                items.AcceptedDate = DateTime.Now;
                                items.Accepted = true;
                                _context.EmailInvitation.Update(items);
                            }
                            _context.SaveChanges();
                        }
                        catch { }
                    }
                    //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    //var callbackUrl = Url.Page(
                    //    "/Identity/Account/ConfirmEmail",
                    //    pageHandler: null,
                    //    values: new { userId = user.Id, code = code },
                    //    protocol: Request.Scheme);
                    //await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                    //    $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");
                    //string callbackUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}/Identity/Account/ConfirmEmail?userid={justRegisteredUser.UserIdGuid}&code={code}";
                    //var template = Global.EmailTemplate(enumEmailTemplate.ConfirmationEmail);
                    //_emailSender.SendEmailAsync(Input.Email, template.Subject,
                    //   template.Message.Replace("{callbackUrl}", callbackUrl),"accounts@sportsmandu.com","Sportsmandu Customer Service");                    
                    return LocalRedirect(returnUrl);
                }
                else
                {                    
                    var resultaaaa = await _signInManager.PasswordSignInAsync(Input.Email, Input.Password, true, lockoutOnFailure: true);
                    if (result.Succeeded)
                    {
                        _logger.LogInformation("User logged in.");
                        return LocalRedirect(returnUrl);
                    }
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}