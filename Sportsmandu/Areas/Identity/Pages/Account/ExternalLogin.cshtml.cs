﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Sportsmandu.Data;
using Sportsmandu.Models;
using Sportsmandu.Services.Abstract;

namespace Sportsmandu.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ExternalLoginModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<ExternalLoginModel> _logger;
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        private AppDbContext _context;
        public ExternalLoginModel(
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager,
            IFileAndDirectoryService _fileService,
            IHostingEnvironment _hostingEnviroment,
            AppDbContext _context,
            ILogger<ExternalLoginModel> logger)
        {
            this._context = _context;
            _signInManager = signInManager;
            _userManager = userManager;
            _logger = logger;
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string LoginProvider { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }
            [Required]            
            public string FirstName { get; set; }
            [Required]            
            public string LastName { get; set; }                    
            public string Avatar { get; set; }
            [Required]
            public string PhoneNo { get; set; }
        }

        public IActionResult OnGetAsync(string param)
        {
            if(param == "facebook")
            {
                //string baseUrl = "sportsmandu.ngrok.io/";
                string returnUrl = "/";
                var redirectUrl = Url.Page("./ExternalLogin", pageHandler: "Callback", values: new { returnUrl });
                var properties = _signInManager.ConfigureExternalAuthenticationProperties("Facebook", "sportsmandu.com" + redirectUrl);
                return new ChallengeResult("Facebook", properties);
            }
            return RedirectToPage("./Login");
        }

        public IActionResult OnPost(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            var redirectUrl = Url.Page("./ExternalLogin", pageHandler: "Callback", values: new { returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return new ChallengeResult(provider, properties);
        }      
        public async Task<IActionResult> OnGetCallbackAsync(string returnUrl = null, string remoteError = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (remoteError != null)
            {
                ErrorMessage = $"Error from external provider: {remoteError}";
                return RedirectToPage("./Login", new {ReturnUrl = returnUrl });
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                ErrorMessage = "Error loading external login information.";
                return RedirectToPage("./Login", new { ReturnUrl = returnUrl });
            }

            // Sign in the user with this external login provider if the user already has a login.
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor : true);
            if (result.Succeeded)
            {
                try
                {
                    if (info.Principal.HasClaim(c => c.Type == ClaimTypes.Anonymous))
                    {
                        try
                        {
                            //var neededUser = _context.Users.Where(x=>x.Email == )
                            //if(_context)
                            string avatar = Global.GetFbImage(info.Principal.FindFirstValue(ClaimTypes.Anonymous), Guid.NewGuid().ToString());
                            Input.Avatar = avatar;
                        }
                        catch (Exception ex)
                        {
                            Input.Avatar = "";
                        }

                    }
                }
                catch (Exception)
                {

                }
                _logger.LogInformation("{Name} logged in with {LoginProvider} provider.", info.Principal.Identity.Name, info.LoginProvider);
                return LocalRedirect(returnUrl);
            }
            if (result.IsLockedOut)
            {
                return RedirectToPage("./Lockout");
            }
            else
            {
                // If the user does not have an account, then ask the user to create an account.
                ReturnUrl = returnUrl;
                LoginProvider = info.LoginProvider;
                string FullName = info.Principal.FindFirstValue(ClaimTypes.Name);
                string FirstName = string.Empty;
                string LastName = string.Empty;
                try
                {
                    if (info.Principal.HasClaim(c => c.Type == ClaimTypes.Anonymous))
                    {
                        try
                        {
                            string avatar = Global.GetFbImage(info.Principal.FindFirstValue(ClaimTypes.Anonymous),Guid.NewGuid().ToString());
                            Input.Avatar = avatar;
                        }
                        catch (Exception ex)
                        {
                            Input.Avatar = "";
                        }

                    }
                }
                catch (Exception)
                {

                }
                if (FullName != null && FullName.IndexOf(" ") >= 0)
                {
                    FirstName = FullName.Split(" ")[0];
                    LastName = FullName.Split(" ")[1];
                }else
                {
                    FirstName = FullName;
                }
                if (info.Principal.HasClaim(c => c.Type == ClaimTypes.Email))
                {
                    try
                    {
                        Input = new InputModel
                        {
                            Email = info.Principal.FindFirstValue(ClaimTypes.Email),
                            FirstName = FirstName,
                            LastName = LastName
                        };
                        if (info.Principal.HasClaim(c => c.Type == ClaimTypes.Anonymous))
                        {
                            try
                            {
                                string avatar = Global.GetFbImage(info.Principal.FindFirstValue(ClaimTypes.Anonymous), info.Principal.FindFirstValue(ClaimTypes.Email).Replace("@", "").Replace(".", ""));
                                Input.Avatar = avatar;
                            }
                            catch (Exception ex)
                            {
                                Input.Avatar = "";
                            }

                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }else
                {
                    Input = new InputModel
                    {                        
                        FirstName = FirstName,
                        LastName = LastName
                    };
                    try
                    {
                        string avatar = Global.GetFbImage(info.Principal.FindFirstValue(ClaimTypes.Anonymous),Guid.NewGuid().ToString());
                        Input.Avatar = avatar;
                    }
                    catch (Exception ex)
                    {
                        Input.Avatar = "";
                    }
                }                
                return Page();
            }
        }

        public async Task<IActionResult> OnPostConfirmationAsync(string returnUrl = null,IFormCollection formCollection = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");            
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                ErrorMessage = "Error loading external login information during confirmation.";
                return RedirectToPage("./Login", new { ReturnUrl = returnUrl });
            }
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = Input.Email, Email = Input.Email, Image = Input.Avatar, FirstName = Input.FirstName, LastName = Input.LastName ,PhoneNumber = Input.PhoneNo};
                var result = _userManager.CreateAsync(user).Result;                
                if (result.Succeeded)
                {
                    if (formCollection.Files.Count > 0)
                    {
                        var _result = _fileService.CreateFiles("sportsmandu", "users", formCollection, _hostingEnviroment, 1, Input.Avatar,Input.Email);
                        if (_result.Succeded)
                        {
                            Input.Avatar = _result.ArrayData.Where(x => x.Contains("users/Image")).FirstOrDefault();
                        }
                    }
                    user.Image = Input.Avatar;
                    result = _userManager.AddLoginAsync(user, info).Result;
                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
                        return LocalRedirect(returnUrl);
                    }
                }else
                {
                    result = _userManager.AddLoginAsync(user, info).Result;
                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
                        return LocalRedirect(returnUrl);
                    }
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            LoginProvider = info.LoginProvider;
            ReturnUrl = returnUrl;
            return Page();
        }
    }
}