﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Sportsmandu.Data;
using Sportsmandu.Models;
using Sportsmandu.Models.enums;
using Sportsmandu.Services;

namespace Sportsmandu.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ConfirmEmailModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IEmailSender _emailSender;
        private AppDbContext _context;
        public ConfirmEmailModel(UserManager<ApplicationUser> userManager, IEmailSender _emailSender,AppDbContext _context)
        {
            _userManager = userManager;
            this._context = _context;
        }
        //public Task<IActionResult> OnGetAsync(string userId, string code)
        //{
        //    if (userId == null || code == null)
        //    {
        //        return RedirectToAction("/Index");
        //    }            
        //    var user = _context.Users.Where(x=>x.UserIdGuid == userId).SingleOrDefault();
        //    if (user == null)
        //    {
        //        return NotFound($"Unable to load user with ID '{userId}'.");
        //    }
        //    var result = await _userManager.ConfirmEmailAsync(user, code);
        //    if (!result.Succeeded)
        //    {
        //        throw new InvalidOperationException($"Error confirming email for user with ID '{userId}':");
        //    }
        //    else
        //    {
        //        //if (result.Succeeded)
        //        //{
        //            // Need to check if the user is invited to send his task email for now ok !                
        //            var template = Global.EmailTemplate(enumEmailTemplate.WelcomeEmail);
        //            _emailSender.SendEmailAsync(user.Email, template.Subject, template.Message.Replace("{firstname}", "user.Firstname").Replace(" {firstname},", ""));
        //        //}
        //    }
        //    return Page();
        //}
    }
}
