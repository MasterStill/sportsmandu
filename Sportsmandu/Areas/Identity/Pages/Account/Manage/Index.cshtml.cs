﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Sportsmandu.Data;
using Sportsmandu.Models;
using Sportsmandu.Services.Abstract;

namespace Sportsmandu.Areas.Identity.Pages.Account.Manage
{
    public partial class IndexModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly Sportsmandu.Services.IEmailSender _emailSender;
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        private AppDbContext _context;
        public IndexModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IFileAndDirectoryService _fileService,
            IHostingEnvironment _hostingEnviroment,
            Sportsmandu.Services.IEmailSender _emailSender,
            AppDbContext _context
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
            this._emailSender = _emailSender;
            this._context = _context;
        }

        public string Username { get; set; }
        public string Avatar { get; set; }
        public bool IsEmailConfirmed { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }

            [Phone]
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }
            public string Avatar { get; set; }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var userName = await _userManager.GetUserNameAsync(user);
            var email = await _userManager.GetEmailAsync(user);
            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            var avatar = user.Avatar;
            Username = userName;
            Input = new InputModel
            {
                Email = email,
                PhoneNumber = phoneNumber,
                Avatar = avatar
            };
            IsEmailConfirmed = await _userManager.IsEmailConfirmedAsync(user);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(IFormCollection formCollection = null)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var email = await _userManager.GetEmailAsync(user);
            if (Input.Email != email)
            {
                var setEmailResult = await _userManager.SetEmailAsync(user, Input.Email);
                if (!setEmailResult.Succeeded)
                {
                    var userId = await _userManager.GetUserIdAsync(user);
                    throw new InvalidOperationException($"Unexpected error occurred setting email for user with ID '{userId}'.");
                }
            }

            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            if (Input.PhoneNumber != phoneNumber)
            {
                var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, Input.PhoneNumber);
                if (!setPhoneResult.Succeeded)
                {
                    var userId = await _userManager.GetUserIdAsync(user);
                    throw new InvalidOperationException($"Unexpected error occurred setting phone number for user with ID '{userId}'.");
                }
            }
            if (formCollection.Files.Count > 0)
            {
                var _result = _fileService.CreateFiles("sportsmandu", "users", formCollection, _hostingEnviroment, 1, Input.Avatar,Guid.NewGuid().ToString());
                if (_result.Succeded)
                {
                    Input.Avatar = _result.ArrayData.Where(x => x.Contains("users/Image")).FirstOrDefault();
                    user.Image = Input.Avatar;
                    var userToModify = _context.Users.Find(user.Id);
                    userToModify.Image = user.Avatar;
                    _context.Entry(userToModify).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    _context.SaveChanges();
                }
            }
            await _signInManager.RefreshSignInAsync(user);


            StatusMessage = "Your profile has been updated";
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostSendVerificationEmailAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var userId = user.UserIdGuid;// await _userManager.GetUserIdAsync(user);
            var email = await _userManager.GetEmailAsync(user);
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = Url.Page(
                "/Account/ConfirmEmail",
                pageHandler: null,
                values: new { userId = userId, code = code },
                protocol: Request.Scheme);
            var template = Global.EmailTemplate(Models.enums.enumEmailTemplate.ConfirmationEmail);
            _emailSender.SendEmailAsync(email, template.Subject,
               template.Message.Replace("{callbackUrl}", HtmlEncoder.Default.Encode(callbackUrl)), "accounts@sportsmandu.com", "Sportsmandu Customer Service");
            //_emailSender.SendEmailAsync(
            //    email, 
            //    "Confirm your email",
            //    $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");
            StatusMessage = "Verification email sent. Please check your email.";
            return RedirectToPage();
        }
    }
}