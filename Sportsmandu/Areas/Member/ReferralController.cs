﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sportsmandu;
using Sportsmandu.Data;
using Sportsmandu.Models.Referral;
namespace Sportsmandu.Api.Areas.Members.Controllers
{
    [Route("api/Referral")]
    [Produces("application/json")]
    //[Authorize]
    public class ReferralController : Controller
    {
        private readonly AppDbContext _context;
        public ReferralController(AppDbContext _context)
        {

            this._context = _context;
        }
        private int GetUserId()
        {
            string UserName = User.Identity.Name;
            if (UserName == null)
            {
                UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            }
            int UserID = _context.Users.SingleOrDefault(x => x.UserName == UserName).Id;
            return UserID;
        }
        private bool IsInRole(string RoleName, Models.enumClaimTypeSegments Segment,string SegmentValue)
        {
            if (_context.UserRoles
                    .Where(x => x.Segment == Segment)
                    .Where(x => x.SegmentValue == SegmentValue)
                    .Where(x => x.Role.Name == RoleName)
                    .Where(x => x.User.UserName == GetUserName())
                    .SingleOrDefault() == null)
            {
                return false;
            }
            return true;
        }

        [HttpGet]
        public IActionResult GetAffiliates()
        {
            var result = _context.Referral.Include(y=>y.ReferralBy).Where(x=>x.ReferralBy.Id == GetUserId()).ToList();
            return new OkObjectResult(result);
        }

        private string GetUserName()
        {
            if (User.Identity.IsAuthenticated)
                return User.Identity.Name;
            return Global.DevelopmentUserName();
        }    
        [HttpPost]
        public IActionResult AddReferral([FromBody]ReferralCreateViewModel referral)
        {           
            Referral r = new Referral
            {
                ReferralById = GetUserId() ,// for timebeing ## To Do 
                FullName = referral.FullName,
                Email = referral.Email,
                Segment = referral.Segment,
                Status = enumReferral.Pending,
                ReferralDate = DateTime.Now
            };
            _context.Referral.Add(r);
            _context.SaveChanges();
            return new OkObjectResult(r);
        }      
    }
}