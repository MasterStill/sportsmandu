using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sportsmandu.Data;
using Sportsmandu.Models;

namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class HomeController : Controller
    {
        private AppDbContext _context;
        private IMapper _mapper { get; set; }
        public HomeController(AppDbContext _context, IMapper _mapper)
        {
            this._context = _context;
            this._mapper = _mapper;
        }
        public IActionResult Index(string culture)
        {

            //if (!Authorization.Authorized(enumClaimTypeSegments.Globalizations.ToString(), "Edit", 0, User.Claims)) return (View("PermissionDenied"));
            bool SuperAdmin = false;
            foreach (var items in User.Claims)
            {
                if (items.Value.Contains("Super_Admin"))
                {
                    SuperAdmin = true;
                    break;
                }else{                    
                }
            }
            if (SuperAdmin)
            {
                return View(_context.AdminMenu.ToList());
            }
            return View(new List<Model.Entities.Global.AdminMenu>());
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}