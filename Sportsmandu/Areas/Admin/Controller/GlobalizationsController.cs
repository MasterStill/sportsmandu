using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Models.League;
using Sportsmandu.Models.Globalization;
using Sportsmandu.Services.Abstract;
using static Sportsmandu.MultiLingualGlobal;
using Z.EntityFramework.Plus;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Models;

namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class GlobalizationsController : Controller
    {
        private AppDbContext _context;
        private IMapper _mapper { get; set; }
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        public GlobalizationsController(AppDbContext _context, IMapper _mapper, IFileAndDirectoryService _fileService, IHostingEnvironment _hostingEnviroment)
        {
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
            this._context = _context;
            this._mapper = _mapper;
        }
        private void SetViewBagStuffs(int Culture)
        {
            //bool Add = false;
            var globalizationsThatAreDone = _context.Globalizations.Include(x=>x.LanguageTitle).ToList();
            List<SelectListItem> l = new List<SelectListItem>();
            var allLanguageTitles = GetLanguageTitles(Culture);
            foreach(var items in allLanguageTitles)
            {
                if(globalizationsThatAreDone.Where(x=>x.LanguageTitle.Name == items.Text).FirstOrDefault() == null)
                {
                    l.Add(items);
                }
            }
            ViewBag.LanguageTitles = l;
        }
        public IActionResult Index(int culture)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Unauthorized());
            SetViewBagStuffs(culture);
            var _result = _context.Globalizations.Include(x => x.LanguageTitle)
            .Where(x => x.Delflag == false).ToList();
            List<int> _userKnownCulture = new List<int>();
            _userKnownCulture.Add(1);
            _userKnownCulture.Add(2);
            MultilingualAI MAI = GenericMultiLingualRepo(_mapper.Map<IEnumerable<MultiLingualViewModel>>(_result), _userKnownCulture, culture);
            var result = from item in _result
                         where !MAI.idToRemove.Contains(item.Id)
                         select item;
            var objToReturn = _mapper.Map<IEnumerable<GlobalizationAdminViewModel>>(result);
            foreach (var items in objToReturn)
            {
                items.Converted = MAI.items.Where(x => x.Id == items.Id).Select(x => x.Converted).SingleOrDefault();
            }
            return View(objToReturn);
        }
        public ActionResult Create(int Culture)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0 ,User.Claims)) return (Unauthorized());
            SetViewBagStuffs(Culture);
            GlobalizationCreateViewModel p = new GlobalizationCreateViewModel();
            return View(p);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GlobalizationCreateViewModel brand, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Unauthorized());
            if (ModelState.IsValid)
            {
                Globalization o = _mapper.Map<Globalization>(brand);
                _context.Entry(o).State = EntityState.Added;
                o.CreatedDate = DateTime.Now;
                o.CreatedById = _context.Users.Where(x=>x.UserName == User.Identity.Name).SingleOrDefault().Id;

                if (o.multilingualid != 0)
                {
                    o.LanguageTitleId = _context.Globalizations.Include(x => x.LanguageTitle)
                                        .SingleOrDefault(x => x.Id == o.multilingualid)
                                        .LanguageTitle.Id;
                }

                _context.Globalizations.Add(o);
                _context.SaveChanges();
                if (o.multilingualid == 0)
                {
                    //_context.Entry(o).State = EntityState.Modified;
                    o.multilingualid = o.Id;
                    _context.Globalizations.Update(o);
                    _context.SaveChanges();
                }

                return RedirectToAction("Index", "Globalizations", new { area = "Admin", Culture = o.CultureId });
            }
            return View();
        }
        public ActionResult Edit(int id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            Globalization about = _context.Globalizations
            .Where(x => x.Id == id).SingleOrDefault();
            SetViewBagStuffs(1);
            return View(about);
        }
        private IEnumerable<SelectListItem> GetLanguageTitles(int CultureId)
        {
            return _context.LanguageTitles.Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            }).ToList();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Globalization Globalization, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(enumClaimTypeSegments.Globalizations.ToString(), this.ControllerContext.RouteData.Values["action"].ToString(), Globalization.Id, User.Claims)) return (Unauthorized());
            if (ModelState.IsValid)
            {
                _context.Entry(Globalization).State = EntityState.Modified;
                Globalization.CreatedById = _context.Users.Where(x=>x.UserName == User.Identity.Name).SingleOrDefault().Id;
                _context.SaveChanges();
                return RedirectToAction("Index", "Globalizations", new { area = "Admin", Culture = Globalization.CultureId });
            }
            return View(Globalization);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            Globalization about = _context.Globalizations.Where(x => x.Id == id).SingleOrDefault();
            _context.Entry(about).State = EntityState.Modified;
            about.Delflag = true;
            _context.SaveChanges();
            return RedirectToAction("Index", "Associations", new { area = "Admin", Culture = about.CultureId });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
