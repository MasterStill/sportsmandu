using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Models.League;
using Sportsmandu.Models.Globalization;
using Sportsmandu.Services.Abstract;
using static Sportsmandu.MultiLingualGlobal;
using Z.EntityFramework.Plus;
using Mapster;
using Sportsmandu.Model.Entities.ViewModels;
namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class LeaguesController : Controller
    {
        private AppDbContext _context;
        private IMapper _mapper { get; set; }
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        public LeaguesController(AppDbContext _context, IMapper _mapper, IFileAndDirectoryService _fileService, IHostingEnvironment _hostingEnviroment)
        {
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
            this._context = _context;
            this._mapper = _mapper;
        }
        private void SetViewBagStuffs(int Culture)
        {
            ViewBag.Categories = GetCategories(Culture);
            ViewBag.Countries = GetCountries(Culture);
        }
        public IActionResult Index(int culture)
        {

            SetViewBagStuffs(culture);
            var _result = _context.Leagues
            .Include(x => x.Category)
            .Where(x => x.CultureId == culture)
            .Where(x => x.Delflag == false)
            .OrderByDescending(x=>x.CreatedDate)
            .Take(4)
            .ToList();
            var objToReturn = _result.Adapt<List<LeagueAdminViewModel>>();
            foreach (var items in objToReturn)
            {
                if (items.Image == null) items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                items.Converted = true;
            }
            return View(objToReturn.Adapt<List<AdminViewModel>>());
            //return View(objToReturn);
        }
        public class LeaguesearchViewModel
        {
            public int CultureId { get; set; }
            public int CategoryId { get; set; }
            public int BrandId { get; set; }
            public string Term { get; set; }
        }

        [HttpPost]
        public IActionResult Search([FromForm]LeaguesearchViewModel psvm)
        {
            if (psvm.CultureId == 0) psvm.CultureId = 1;
            var _result = _context.Leagues
                    // .Include(x => x.Brand)
                    .Include(x => x.Category)
                    // .Include(x => x.Parts)
                    .Where(x => x.Delflag == false);
            //if (psvm != null)
            {
                if (psvm.CategoryId != 0)
                {
                    _result = _result.Where(x => x.CategoryId == psvm.CategoryId);
                }
            }
            SetViewBagStuffs(1);
            List<int> _userKnownCulture = new List<int>();
            _userKnownCulture.Add(1);
            _userKnownCulture.Add(2);
            MultilingualAI MAI = GenericMultiLingualRepo(_mapper.Map<IEnumerable<MultiLingualViewModel>>(_result), _userKnownCulture, psvm.CultureId);
            var result = from item in _result
                         where !MAI.idToRemove.Contains(item.Id)
                         select item;
            var objToReturn = _mapper.Map<IEnumerable<LeagueAdminViewModel>>(result);
            foreach (var items in objToReturn)
            {
                items.Converted = MAI.items.Where(x => x.Id == items.Id).Select(x => x.Converted).SingleOrDefault();
                if (items.Image == null || items.Image == "")
                {
                    items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                }
            }
            SetViewBagStuffs(1);
            var finalResult = objToReturn.Adapt<List<AdminViewModel>>();
            return View("Index", finalResult);
        }
        int NoOfCulture = 2;

        public IActionResult ToBeTranslated(int culture)
        {
            SetViewBagStuffs(culture);
            var _mainResult = _context.Leagues
                .Include(x => x.ConvertedCulture)
                .Where(x => x.Delflag == false)
                .Where(x => x.ConvertedCulture.Count == NoOfCulture - 1)
                .Where(x => x.CultureId != culture)
                .Select(x => new { x.Category, x.Name, x.Image, x.multilingualid })
                .OrderBy(x => x.Name).Take(20);
            var objToReturn = _mainResult.Adapt<List<LeagueAdminViewModel>>();// (_mainResult.Take(20));
            foreach (var items in objToReturn)
            {
                items.Converted = false;
                if (items.Image == null || items.Image == "")
                {
                    items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                }
            }
            var resultT0send = objToReturn.Adapt<List<AdminViewModel>>();
            return View("Index", resultT0send);
        }

        public ActionResult Create(int Culture)
        {
            //ViewData["Brand"] = new SelectList(_context.Brands.Where(x => x.CultureId == Culture).ToList().Select(x => new { x.multilingualid, x.Name }), "multilingualid", "Name");
            //ViewData["Category"] = new SelectList(_context.Categories.Where(x => x.CultureId == Culture).ToList().Select(x => new { x.multilingualid, x.Name }), "multilingualid", "Name");
            SetViewBagStuffs(Culture);
            LeagueCreateViewModel p = new LeagueCreateViewModel();
            return View(p);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LeagueCreateViewModel brand, IFormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                League o = _mapper.Map<League>(brand);
                _context.Entry(o).State = EntityState.Added;
                o.CreatedDate = DateTime.Now;
                //##TodO
                o.CreatedById = _context.Users.Where(x=>x.UserName == User.Identity.Name)
                    .SingleOrDefault().Id;
                //o. = 1;
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Leagues", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        brand.Image = _result.ArrayData.Where(x => x.Contains("Leagues/Image")).FirstOrDefault();
                        brand.Thumbnail = _result.ArrayData.Where(x => x.Contains("Leagues/Thumbnail")).FirstOrDefault();
                        brand.CoverImage = _result.ArrayData.Where(x => x.Contains("Leagues/CoverImage")).FirstOrDefault();
                    }
                }
                if (formCollection.Files.Count == 1 && o.multilingualid != 0)
                {
                    o.Image = _context.Leagues.SingleOrDefault(x => x.Id == o.multilingualid).Image;
                }
                if (o.multilingualid != 0)
                {
                    o.CategoryId = _context.Leagues.SingleOrDefault(x => x.Id == o.multilingualid).CategoryId;
                }
                o.Guid = Global.UniqueGuid(this.ControllerContext.RouteData.Values["controller"].ToString(), _context);                
                _context.Leagues.Add(o);
                _context.SaveChanges();
                if (o.multilingualid == 0)
                {
                    //_context.Entry(o).State = EntityState.Modified;
                    o.multilingualid = o.Id;
                    _context.Leagues.Update(o);
                    _context.SaveChanges();
                }
                LeagueConvertedCulture pc = new LeagueConvertedCulture
                {
                    CultureId = o.CultureId.Value,
                    LeagueId = o.multilingualid,
                };
                _context.LeagueConvertedCulture.AddAsync(pc);
                _context.SaveChanges();
                return RedirectToAction("Index", "Leagues", new { area = "Admin", Culture = o.CultureId });
            }
            return View();
        }
        public ActionResult Edit(int Id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), Id, User.Claims)) return (Unauthorized());
            League League = _context.Leagues
            .Find(Id);
            SetViewBagStuffs(1); // ViewBag.Categories = GetCategories(about.CultureId.Value);
            return View(League);
        }
        private IEnumerable<SelectListItem> GetCategories(int CultureId)
        {
            return _context.Categories.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();

        }
        private IEnumerable<SelectListItem> GetCountries(int CultureId)
        {
            return _context.Countries.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();
        }
        // private IEnumerable<SelectListItem> GetCategories(int CultureId)
        //{
        //    return _context.Categories.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
        //    {
        //        Value = x.multilingualid.ToString(),
        //        Text = x.Name
        //    }).ToList();

        //}             
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(League League, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), League.Id, User.Claims)) return (Unauthorized());
            if (ModelState.IsValid)
            {
                var league = _context.Leagues.Find(League.Id);
                //_context.Entry(League).State = EntityState.Modified;
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Leagues", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        bool hasImage = _result.ArrayData.Where(x => x.Contains("Leagues/Image")).FirstOrDefault() != null;
                        bool hasCoverImage = _result.ArrayData.Where(x => x.Contains("Leagues/CoverImage")).FirstOrDefault() != null;
                        bool hasThumbnail = _result.ArrayData.Where(x => x.Contains("Leagues/Thumbnail")).FirstOrDefault() != null;
                        if (hasImage)
                            League.Image = _result.ArrayData.Where(x => x.Contains("Leagues/Image")).FirstOrDefault();
                        if (hasThumbnail)
                            League.Thumbnail = _result.ArrayData.Where(x => x.Contains("Leagues/Thumbnail")).FirstOrDefault();
                        if (hasCoverImage)
                            League.CoverImage = _result.ArrayData.Where(x => x.Contains("Leagues/CoverImage")).FirstOrDefault();
                    }
                } 
                League.CreatedById = _context.Users.Where(x=>x.UserName == User.Identity.Name).SingleOrDefault().Id;

                League.Guid = league.Guid;
                League.CreatedById = league.CreatedById;
                League.CreatedDate = league.CreatedDate;
                //League.ModifiedById.
                _context.Entry(league).CurrentValues.SetValues(League);
                _context.SaveChanges();
                return RedirectToAction("Index", "Leagues", new { area = "Admin", Culture = League.CultureId });
            }
            return View(League);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            League about = _context.Leagues.Where(x => x.Id == id).SingleOrDefault();
            _context.Entry(about).State = EntityState.Modified;
            about.Delflag = true;
            _context.SaveChanges();
            return RedirectToAction("Index", "Leagues", new { area = "Admin", Culture = about.CultureId });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
