﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
//using Microsoft.AspNetCore.Identity.UI.Services;
using Sportsmandu.Models;
using Sportsmandu.Services;
//using SignalRMasterServer.Hubs;
//using Microsoft.AspNetCore.SignalR.Infrastructure;

//Todo
// Services and Repository Implementation inside IndentityDb from .net core
// SignalR Core Implementation

namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    //[Authorize]
    public class RolesController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IEmailSender _emailSender;
        //private IConnectionManager _connectionManager;
        public RolesController(AppDbContext _context, IEmailSender emailSender)//,IConnectionManager _connecitonManager)
        {
            //this._connectionManager = _connecitonManager;
            _emailSender = emailSender;
            this._context = _context;
        }
        [HttpGet]
        public IActionResult AllRoles()
        {
            var AllRoles = _context.Roles.ToList().Select(x => x.Name);
            return new OkObjectResult(AllRoles);
        }

        public IActionResult Index(int Culture)
        {
            var userRoles = _context.ApplicaitonUserRole
                                .Include(x => x.Role)
                                .Include(x => x.User)
                                ;
            return View(userRoles.ToList());
        }

        [HttpPost]
        public IActionResult setUserToRole([FromBody] RoleViewModel r)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Unauthorized());
            if (!IsInRole("Admin", r.Segment))
            {
                return Global.AuthorizationFailureRoute();
            }
            ApplicationUserRole role = new ApplicationUserRole();
            role.Segment = r.Segment;
            role.RoleId = _context.Roles.SingleOrDefault(x => x.Name == r.RoleName).Id;
            role.UserId = GetUserId(r.UserEmail, r.Segment, r.SegmentValue, role.RoleId);
            if (role.UserId == 0)
            { // User Not signed up so send invitation email
                var invitedbyUser = _context.Users.SingleOrDefault(x => x.Email == GetUserName());
                var kk = Global.EmailTemplate(Sportsmandu.Models.enums.enumEmailTemplate.InvitationForRoleNewUser);
                kk.Subject = kk.Subject.Replace("{fullname}", (invitedbyUser.FullName != null) ? invitedbyUser.FullName : invitedbyUser.Email);
                kk.Message = kk.Message.Replace("{fullname}", invitedbyUser.FullName);
                kk.Message = kk.Message.Replace("{emailaddress}", invitedbyUser.Email);
                kk.Message = kk.Message.Replace("{rolename}", r.RoleName);
                //kk.Message = kk.Message.Replace("{websitename}", _context.Clubs.SingleOrDefault(x => x.Id == r.se).Name);
                kk.Message = kk.Message.Replace("{invitedemail}", r.UserEmail);
                _emailSender.SendEmailAsync(r.UserEmail, kk.Subject,
                       kk.Message);
            }
            try
            {
                _context.UserRoles.Add(role);
                _context.SaveChanges();

                try
                {
                    var RoleGivenToUser = _context.Users.Where(x => x.Id == role.UserId).SingleOrDefault();
                    //_connectionManager.GetHubContext<Custo\merSupportHub>().Clients.Group(RoleGivenToUser.SignalrGuid.ToString()).onUpdateMe(SignalrUpdate.RolesControllersetUserToRole.ToString());
                }
                catch (Exception ex)
                {
                    Global.Error(ex);
                }
            }
            catch
            {

            }
            return Ok();
        }

        private int GetUserId(string Email, enumClaimTypeSegments Segment, string SegmentValue, int RoleId)
        {
            var user = _context.Users.SingleOrDefault((x => x.UserName == Email));
            if (user == null)
            {
                return UserNotInDb(Email, Segment, SegmentValue, RoleId);
            }
            else
            {
                return user.Id;
            }

        }

        private int UserNotInDb(string Email, enumClaimTypeSegments Segment, string SegmentValue, int RoleId)
        {
            var user = new EmailInvitation
            {
                Email = Email,
                Segment = Segment,
                SegmentValue = SegmentValue,
                InvitedDate = DateTime.Now,
                RoleId = RoleId,
                AddedById = GetUserId()
            };
            try
            {
                _context.Add(user);
                _context.SaveChanges();
                //Invite the user for the Role
            }
            catch { }
            return 0;// Need to workaround this;
        }


        [HttpPost("RemoveUserFromRole")]
        public IActionResult RemoveUserFromRole([FromBody] RoleViewModel r)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Unauthorized());
            if (!IsInRole("Admin", r.Segment))
            {
                return Global.AuthorizationFailureRoute();
            }
            try
            {
                ApplicationUserRole role = _context.UserRoles.Where(x => x.Role.Name == r.RoleName).Where(x => x.User.Email == r.UserEmail).Where(x => x.Segment == r.Segment).SingleOrDefault(); //new ApplicationUserRole;
                var totalNoofAdminUser = _context.UserRoles.Where(x => x.Role.Name == "Admin").Where(x => x.Segment == r.Segment).Count();
                if (totalNoofAdminUser == 1 && role != null)
                {
                    return new BadRequestObjectResult(new GenericResult
                    {
                        Succeded = false,
                        Message = "There need to be atleast one Admin"
                    });
                }
                _context.UserRoles.Remove(role);
                _context.SaveChanges();
            }
            catch
            {
                //User Chaina so remove from invitation list            
                EmailInvitation e = _context.EmailInvitation.Where(x => x.Email == r.UserEmail).Where(x => x.Role.Name == r.RoleName).Where(x => x.Accepted == false).SingleOrDefault();
                _context.EmailInvitation.Remove(e);
                _context.SaveChanges();
                return Ok();
            }
            return Ok();
        }


        private string GetUserName()
        {
            string UserName = User.Identity.Name;
            if (UserName == null)
            {
                UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            }
            return UserName;
        }

        private int GetUserId()
        {
            string UserName = User.Identity.Name;
            if (UserName == null)
            {
                UserName = (Global.DevelopmentMode()) ? Global.DevelopmentUserName() : null;
            }
            int UserID = _context.Users.SingleOrDefault(x => x.UserName == UserName).Id;
            return UserID;
        }
        private bool IsInRole(string RoleName, enumClaimTypeSegments Segment)
        {
            if (_context.UserRoles.Where(x => x.Segment == Segment).Where(x => x.Role.Name == RoleName).Where(x => x.User.UserName == GetUserName()).SingleOrDefault() == null)
            {
                return false;
            }
            return true;
        }

        public class RoleViewModel
        {
            public enumClaimTypeSegments Segment;
            public string SegmentValue;
            public string UserEmail;
            public string RoleName;
        }
    }
}