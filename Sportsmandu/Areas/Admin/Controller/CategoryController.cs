using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Models.Globalization;
using Sportsmandu.Services.Abstract;
using static Sportsmandu.MultiLingualGlobal;
namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class CategoryController : Controller
    {
        private AppDbContext _context;
        private IMapper _mapper { get; set; }
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        public CategoryController(AppDbContext _context, IMapper _mapper, IFileAndDirectoryService _fileService, IHostingEnvironment _hostingEnviroment)
        {
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
            this._context = _context;
            this._mapper = _mapper;
        }
        public IActionResult Index(string culture)
        {
            var _result = _context.Categories.Where(x => x.Delflag == false).ToList();
            List<int> _userKnownCulture = new List<int>();
            _userKnownCulture.Add(1);
            _userKnownCulture.Add(2);
            MultilingualAI MAI = GenericMultiLingualRepo(_mapper.Map<IEnumerable<MultiLingualViewModel>>(_result), _userKnownCulture, Int32.Parse(culture));
            var result = from item in _result
                         where !MAI.idToRemove.Contains(item.Id)
                         select item;
            var objToReturn = _mapper.Map<IEnumerable<CategoryViewModel>>(result);
            foreach (var items in objToReturn)
            {
                items.Converted = MAI.items.Where(x => x.Id == items.Id).Select(x => x.Converted).SingleOrDefault();
                if (items.Image == null || items.Image == "")
                {
                    items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                }
            }
            return View(objToReturn);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryCreateViewModel brand, IFormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                Category o = _mapper.Map<Category>(brand);
                _context.Entry(o).State = EntityState.Added;
                o.CreatedDate = DateTime.Now;
                o.CreatedById = _context.Users.Where(x=>x.UserName == User.Identity.Name).SingleOrDefault().Id;
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Category", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        o.Image = _result.ArrayData[0];
                    }
                }
                if (formCollection.Files.Count == 1 && o.multilingualid != 0)
                {
                    o.Image = _context.Categories.SingleOrDefault(x => x.Id == o.multilingualid).Image;
                }
                o.Guid = Global.UniqueGuid("Categories", _context);
                _context.Categories.Add(o);
                _context.SaveChanges();
                if (o.multilingualid == 0)
                {
                    _context.Entry(o).State = EntityState.Modified;
                    o.multilingualid = o.Id;
                    _context.Categories.Update(o);
                    _context.SaveChanges();
                }
                return RedirectToAction("Index", "Category",new { area = "Admin", Culture = o.CultureId});
            }
            return View();
        }
        public ActionResult Edit(int id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            Category about = _context.Categories.Where(x => x.Id == id).SingleOrDefault();
            return View(about);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Category about, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), about.Id, User.Claims)) return (Unauthorized());
            if (ModelState.IsValid)
            {
                _context.Entry(about).State = EntityState.Modified;
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Category", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        about.Image = _result.ArrayData[0];
                    }
                }
                 about.CreatedById = _context.Users.Where(x=>x.UserName == User.Identity.Name).SingleOrDefault().Id;
                _context.SaveChanges();
               return RedirectToAction("Index", "Category",new { area = "Admin", Culture = about.CultureId});
            }
            return View(about);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            Category about = _context.Categories.Where(x => x.Id == id).SingleOrDefault();
            _context.Entry(about).State = EntityState.Modified;
            about.Delflag = true;
            _context.SaveChanges();
           return RedirectToAction("Index", "Category",new { area = "Admin", Culture = about.CultureId});
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
