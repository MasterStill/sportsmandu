using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Model;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Models.Globalization;
using Sportsmandu.Services.Abstract;
using static Sportsmandu.MultiLingualGlobal;
namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class SubCategoryController : Controller
    {
        private AppDbContext _context;
        private IMapper _mapper { get; set; }
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        public SubCategoryController(AppDbContext _context, IMapper _mapper, IFileAndDirectoryService _fileService, IHostingEnvironment _hostingEnviroment)
        {
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
            this._context = _context;
            this._mapper = _mapper;
        }
        public IActionResult Index(string culture)
        {
            var _result = _context.SubCategories.Include(x=>x.Category).Where(x => x.Delflag == false).ToList();
            List<int> _userKnownCulture = new List<int>();
            _userKnownCulture.Add(1);
            _userKnownCulture.Add(2);
            MultilingualAI MAI = GenericMultiLingualRepo(_result.Adapt<IEnumerable<MultiLingualViewModel>>(), _userKnownCulture, Int32.Parse(culture));
            var result = from item in _result
                         where !MAI.idToRemove.Contains(item.Id)
                         select item;
            var objToReturn = result.Adapt<IEnumerable<CategoryViewModel>>();
            return View(objToReturn);
        }
        private IEnumerable<SelectListItem> GetCategories(int CultureId)
        {
            
            return _context.Categories.Where(x => x.Delflag == false).Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();
        }
        public ActionResult Create()
        {
            ViewBag.Categories = GetCategories(1);
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SubCategoryCreateViewModel brand, IFormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                SubCategory o = brand.Adapt<SubCategory>();
                _context.Entry(o).State = EntityState.Added;
                o.CreatedDate = DateTime.Now;
                o.CreatedById = _context.Users.Where(x=>x.UserName == User.Identity.Name).SingleOrDefault().Id;               
                o.Guid = Global.UniqueGuid("SubCategories", _context);
                _context.SubCategories.Add(o);
                _context.SaveChanges();
                if (o.multilingualid == 0)
                {
                    _context.Entry(o).State = EntityState.Modified;
                    o.multilingualid = o.Id;
                    _context.SubCategories.Update(o);
                    _context.SaveChanges();
                }
                return RedirectToAction("Index", "SubCategory",new { area = "Admin", Culture = o.CultureId});
            }
            return View();
        }
        public ActionResult Edit(int id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            Category about = _context.Categories.Where(x => x.Id == id).SingleOrDefault();
            return View(about);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SubCategory about, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), about.Id, User.Claims)) return (Unauthorized());
            if (ModelState.IsValid)
            {
                _context.Entry(about).State = EntityState.Modified;               
                 about.CreatedById = _context.Users.Where(x=>x.UserName == User.Identity.Name).SingleOrDefault().Id;
                _context.SaveChanges();
               return RedirectToAction("Index", "SubCategory", new { area = "Admin", Culture = about.CultureId});
            }
            return View(about);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            SubCategory about = _context.SubCategories.Where(x => x.Id == id).SingleOrDefault();
            _context.Entry(about).State = EntityState.Modified;
            about.Delflag = true;
            _context.SaveChanges();
           return RedirectToAction("Index", "SubCategory", new { area = "Admin", Culture = about.CultureId});
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
