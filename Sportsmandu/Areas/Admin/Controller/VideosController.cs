using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Services.Abstract;
using Z.EntityFramework.Plus;
using Mapster;
using Sportsmandu.Models;
namespace Sportsmandu.Api.Areas.Admin.Controllers
{    
    [Area("Admin")]
    //[Authorize("Admin")]
    public class VideosController : Controller//: GenerericAdminController<Player>//Controller
    {
        private AppDbContext _context;
        private AppDbContext _context1;
        private IMapper _mapper { get; set; }
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        public VideosController(AppDbContext _context, AppDbContext _context1, IMapper _mapper, IFileAndDirectoryService _fileService, IHostingEnvironment _hostingEnviroment)
        {
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
            this._context = _context;
            this._context1 = _context1;
            this._mapper = _mapper;
        }        
        private IEnumerable<SelectListItem> GetCountries(int CultureId)
        {
            return _context.Countries.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();
        }
        //int NoOfCulture = 2;
        //public IActionResult ToBeTranslated(int culture)
        //{
        //    var _mainResult = _context.Players
        //        .Include(x => x.ConvertedCulture)
        //        .Where(x => x.ConvertedCulture.Count == NoOfCulture - 1)
        //        .Where(x => x.CultureId != culture)
        //        .Select(x=>new {x.Category,x.Name,x.Image,x.multilingualid})
        //        .OrderBy(x => x.Name).Take(20);
        //    var objToReturn = _mainResult.Adapt<List<PlayerAdminViewModel>>();// (_mainResult.Take(20));
        //    foreach (var items in objToReturn)
        //    {
        //        items.Converted = false;
        //        if (items.Image == null || items.Image == "")
        //        {
        //            items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
        //        }
        //    }
        //    return View("Index", objToReturn.Adapt<List<AdminViewModel>>());
        //}
        public IActionResult Index(int culture)
        {
            List<Video> objToReturn = new List<Video>();
            var _result = _context.Videos.ToList()            
            .Where(x=>x.Delflag == false)
            //.Take(4).ToList()
            //.Select(x => new { x.Id,x.Guid, x.Name, x.Thumbnail, x.Description, x.VideoType,x.URL })
            .ToList();
            return View(_result.Adapt<List<AdminViewModel>>());
        }
        public class PlayerSearchViewModel
        {
            public int CultureId { get; set; }
            public string Term { get; set; }
        }
        public ActionResult Create(int Culture)
        {            
            Video p = new Video();
            return View(p);
        }               
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Video Video, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Unauthorized());
            if (ModelState.IsValid)
            {
                Video.Delflag = false;
                Video.Verified = true;
                Video videoa = _mapper.Map<Video>(Video);
                _context.Entry(videoa).State = EntityState.Added;
                videoa.CreatedDate = DateTime.Now;                
                videoa.CreatedById = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault().Id;
                Player PlayerToBeModified = new Player();
                if (Video.MultiLingualId != 0)
                {
                    PlayerToBeModified = _context.Players.Where(x => x.Id == Video.MultiLingualId).SingleOrDefault();                    
                }
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Videos", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        videoa.Image = _result.ArrayData.Where(x => x.Contains("Videos/Image")).FirstOrDefault();
                        videoa.Thumbnail = _result.ArrayData.Where(x => x.Contains("Videos/Thumbnail")).FirstOrDefault();
                        videoa.CoverImage = _result.ArrayData.Where(x => x.Contains("Videos/CoverImage")).FirstOrDefault();
                    }
                }
                if (formCollection.Files.Count == 1 && videoa.MultiLingualId != 0)
                {
                    videoa.Image = _context.Players.SingleOrDefault(x => x.Id == videoa.MultiLingualId).Image;
                }
                if (videoa.MultiLingualId != 0)
                {                    
                    videoa.Image = videoa.Image != null ? videoa.Image : PlayerToBeModified.Image;
                    videoa.Thumbnail = videoa.Image != null ? videoa.Image : PlayerToBeModified.Thumbnail;
                    videoa.CoverImage = videoa.Image != null ? videoa.Image : PlayerToBeModified.CoverImage;
                }
                videoa.Guid = Global.UniqueGuid(this.ControllerContext.RouteData.Values["controller"].ToString(), _context1);
                _context.Videos.Add(videoa);
                _context.SaveChanges();
                if (videoa.MultiLingualId == 0)
                {                    
                    videoa.MultiLingualId = videoa.Id;
                    _context.Videos.Update(videoa);
                    _context.SaveChanges();
                }
                //PlayerConvertedCulture pc = new PlayerConvertedCulture
                //{
                //    CultureId = pcvm.CultureId,
                //    PlayerId = player.multilingualid,
                //};
                //_context.PlayerConvertedCulture.AddAsync(pc);
                _context.SaveChanges();
                return RedirectToAction("Index", "Videos", new { area = "Admin", Culture = videoa.CultureId });
            }
            return View();
        }      
        
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var result = _context.Videos.Where(x => x.Id == Id).SingleOrDefault();
            return View(result);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Video Player, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), Player.Id, User.Claims)) return (Unauthorized());
            if (ModelState.IsValid)
            {
                _context.Entry(Player).State = EntityState.Modified;
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Videos", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        bool hasImage = _result.ArrayData.Where(x => x.Contains("Videos/Image")).FirstOrDefault() != null;
                        bool hasCoverImage = _result.ArrayData.Where(x => x.Contains("Videos/CoverImage")).FirstOrDefault() != null;
                        bool hasThumbnail = _result.ArrayData.Where(x => x.Contains("Videos/Thumbnail")).FirstOrDefault() != null;
                        if (hasImage)
                            Player.Image = _result.ArrayData.Where(x => x.Contains("Videos/Image")).FirstOrDefault();
                        if (hasThumbnail)
                            Player.Thumbnail = _result.ArrayData.Where(x => x.Contains("Videos/Thumbnail")).FirstOrDefault();
                        if (hasCoverImage)
                            Player.CoverImage = _result.ArrayData.Where(x => x.Contains("Videos/CoverImage")).FirstOrDefault();
                    }
                }
                Player.CreatedById = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault().Id;
                _context.SaveChanges();
                return RedirectToAction("Index", "Videos", new { area = "Admin", Culture = Player.CultureId });
            }
            return View(Player);
        }            
    }
}