using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Models.Globalization;
using Sportsmandu.Services.Abstract;
using static Sportsmandu.MultiLingualGlobal;
using Z.EntityFramework.Plus;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Models;
using System.Diagnostics;
using System.Security.Claims;
using Mapster;
using Sportsmandu.Models.League;
using Sportsmandu.Models.enums;

namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class ClubsController : Controller
    {
        private AppDbContext _context;
        private IMapper _mapper { get; set; }
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        public ClubsController(AppDbContext _context, IMapper _mapper, IFileAndDirectoryService _fileService, IHostingEnvironment _hostingEnviroment)
        {
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
            this._context = _context;
            this._mapper = _mapper;
        }
        private void SetViewBagStuffs(int Culture)
        {
            ViewBag.Categories = GetCategories(Culture);
            ViewBag.Countries = GetCountries(Culture);
            ViewBag.Grounds = GetGrounds(Culture);
        }
        public IActionResult Index(int culture)
        {
            SetViewBagStuffs(culture);
            var _result = _context.Clubs
            .Where(x => x.Delflag == false)
            .Where(x => x.Culture.Id == culture)
            .Take(4)
            //.Select(x=> new {x.Id, x.Name, x.Image, x.Description, x.multilingualid})
            //.OrderBy(r => Guid.NewGuid())
            //.Take(100)
            .ToList();
            List<ClubAdminViewModel> objToReturn = _result.Adapt<List<ClubAdminViewModel>>();
            // var objToReturn = _mapper.Map<IEnumerable<ClubAdminViewModel>>(_result);
            foreach (var items in objToReturn)
            {
                if (items.Image == null) items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                items.Converted = true;
            }
            return View(objToReturn.Adapt<List<AdminViewModel>>());
            //return View(objToReturn);
        }
        public class ClubsearchViewModel
        {
            public int CultureId { get; set; }
            public string Term { get; set; }
        }

        [HttpPost]
        public IActionResult Search([FromForm]ClubsearchViewModel psvm)
        {
            if (psvm.CultureId == 0) psvm.CultureId = 1;
            var _result = _context.Clubs
                    .Where(x => x.Name.Contains(psvm.Term))
                    .Where(x => x.Delflag == false);

            SetViewBagStuffs(1);
            List<int> _userKnownCulture = new List<int>();
            _userKnownCulture.Add(1);
            _userKnownCulture.Add(2);
            MultilingualAI MAI = GenericMultiLingualRepo(_mapper.Map<IEnumerable<MultiLingualViewModel>>(_result), _userKnownCulture, psvm.CultureId);
            var result = from item in _result
                         where !MAI.idToRemove.Contains(item.Id)
                         select item;
            var objToReturn = _mapper.Map<IEnumerable<ClubAdminViewModel>>(result);
            foreach (var items in objToReturn)
            {
                items.Converted = MAI.items.Where(x => x.Id == items.Id).Select(x => x.Converted).SingleOrDefault();
                if (items.Image == null || items.Image == "")
                {
                    items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                }
            }
            SetViewBagStuffs(1);
            return View("Index", objToReturn.Adapt<List<AdminViewModel>>());
        }



        public ActionResult Create(int Culture)
        {
            //ViewData["Brand"] = new SelectList(_context.Brands.Where(x => x.CultureId == Culture).ToList().Select(x => new { x.multilingualid, x.Name }), "multilingualid", "Name");
            //ViewData["Category"] = new SelectList(_context.Categories.Where(x => x.CultureId == Culture).ToList().Select(x => new { x.multilingualid, x.Name }), "multilingualid", "Name");
            SetViewBagStuffs(Culture);
            ClubCreateViewModel p = new ClubCreateViewModel();
            return View(p);
        }
        public ActionResult Details(int Culture, int multilingualid)
        {

            return View();
        }

        [HttpGet]
        public ActionResult Report()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ClubCreateViewModel brand, IFormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                Club Club = _mapper.Map<Club>(brand);
                _context.Entry(Club).State = EntityState.Added;
                Club.CreatedDate = DateTime.Now;                
                Club.CreatedById = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault().Id;                
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Clubs", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        bool hasImage = _result.ArrayData.Where(x => x.Contains("Clubs/Image")).FirstOrDefault() != null;
                        bool hasCoverImage = _result.ArrayData.Where(x => x.Contains("Clubs/CoverImage")).FirstOrDefault() != null;
                        bool hasThumbnail = _result.ArrayData.Where(x => x.Contains("Clubs/Thumbnail")).FirstOrDefault() != null;
                        if (hasImage)
                            Club.Image = _result.ArrayData.Where(x => x.Contains("Clubs/Image")).FirstOrDefault();
                        if (hasThumbnail)
                            Club.Thumbnail = _result.ArrayData.Where(x => x.Contains("Clubs/Thumbnail")).FirstOrDefault();
                        if (hasCoverImage)
                            Club.CoverImage = _result.ArrayData.Where(x => x.Contains("Clubs/CoverImage")).FirstOrDefault();
                    }
                }
                if (formCollection.Files.Count == 1 && Club.multilingualid != 0)
                {
                    Club.Image = _context.Clubs.SingleOrDefault(x => x.Id == Club.multilingualid).Image;
                }
                //if (o.multilingualid != 0)
                //{
                //    o.CategoryId = _context.Clubs.SingleOrDefault(x => x.Id == o.multilingualid).CategoryId;
                //}
                // Club.GUID = Guid.NewGuid();
                // Club.StringGuid = Club.GUID.ToString();
                Club.Guid = Global.UniqueGuid(this.ControllerContext.RouteData.Values["controller"].ToString(), _context);
                _context.Clubs.Add(Club);
                _context.SaveChanges();
                if (Club.multilingualid == 0)
                {
                    //_context.Entry(o).State = EntityState.Modified;
                    Club.multilingualid = Club.Id;
                    _context.Clubs.Update(Club);
                    _context.SaveChanges();
                }
                ClubConvertedCulture pc = new ClubConvertedCulture
                {
                    CultureId = Club.CultureId.Value,
                    ClubId = Club.multilingualid,
                    Club = null,
                    Culture = null
                };
                _context.ClubConvertedCulture.Add(pc);
                _context.SaveChanges();
                return RedirectToAction("Index", "Clubs", new { area = "Admin", Culture = Club.CultureId });
            }
            return View();
        }

        //[Authorize(Policy = "Club")]
        //[HttpGet("GetRoles/{Category}")]        
        public ActionResult Edit(int id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            Club Club = _context.Clubs
                .Where(x => x.Id == id)
                .Include(x=>x.ClubSocial)
                .Include(x => x.Seasons)
                .ThenInclude(x => x.Season)
                .Include(x=>x.Fixtures)
                .ThenInclude(x=>x.SeasonFixture)
                .ThenInclude(x=>x.Category)
                .Include(x => x.Fixtures)
                .ThenInclude(x => x.SeasonFixture)
                .Include(x=>x.Seasons)
                //.ThenInclude(x=>x.)
                .SingleOrDefault();                
            SetViewBagStuffs(Club.CultureId.Value);
            ViewBag.Categories = GetCategories(Club.CultureId.Value);
            return View(Club);
        }
        public class SeasonClubPlayersViewModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string CategoryName { get; set; }
            public string Image { get; set; }
            public string Thumbnail { get; set; }
        }
        public class SeasonClubplayersCreateViewModel
        {
            public string ClubId { get; set; }
            public int PlayerId { get; set; }
            public int SeasonId { get; set; }
            public enumPlayerRole PlayerRole { get; set; }
        }
        private IEnumerable<SelectListItem> GetCategories(int CultureId)
        {
            return _context.Categories.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();

        }
        int NoOfCulture = 2;
        public IActionResult ToBeTranslated(int culture)
        {
            SetViewBagStuffs(culture);
            var _mainResult = _context.Clubs
                .Include(x => x.ConvertedCulture)
                .Where(x => x.ConvertedCulture.Count == NoOfCulture - 1)
                .Where(x => x.CultureId != culture).OrderBy(x => x.Name).Where(x=>x.Delflag == false)
                .Take(30).ToList();
            var objToReturn = _mainResult.Adapt<List<ClubAdminViewModel>>();// (_mainResult.Take(20));
            foreach (var items in objToReturn)
            {
                items.Converted = false;
                if (items.Image == null || items.Image == "")
                {
                    items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                }
            }
            return View("Index", objToReturn.Adapt<List<AdminViewModel>>());
        }
        private IEnumerable<SelectListItem> GetCountries(int CultureId)
        {
            return _context.Countries.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();

        }
        private IEnumerable<SelectListItem> GetGrounds(int CultureId)
        {
            return _context.Grounds.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();

        }
        // private IEnumerable<SelectListItem> GetCategories(int CultureId)
        //{
        //    return _context.Categories.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
        //    {
        //        Value = x.multilingualid.ToString(),
        //        Text = x.Name
        //    }).ToList();

        //}             
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Club Club, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), Club.Id, User.Claims)) return (Unauthorized());
            if (ModelState.IsValid)
            {
                var club = _context.Clubs.Find(Club.Id);
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Clubs", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        bool hasImage = _result.ArrayData.Where(x => x.Contains("Clubs/Image")).FirstOrDefault() != null;
                        bool hasCoverImage = _result.ArrayData.Where(x => x.Contains("Clubs/CoverImage")).FirstOrDefault() != null;
                        bool hasThumbnail = _result.ArrayData.Where(x => x.Contains("Clubs/Thumbnail")).FirstOrDefault() != null;
                        if (hasImage)
                            Club.Image = _result.ArrayData.Where(x => x.Contains("Clubs/Image")).FirstOrDefault();
                        if (hasThumbnail)
                            Club.Thumbnail = _result.ArrayData.Where(x => x.Contains("Clubs/Thumbnail")).FirstOrDefault();
                        if (hasCoverImage)
                            Club.CoverImage = _result.ArrayData.Where(x => x.Contains("Clubs/CoverImage")).FirstOrDefault();
                    }
                }
                // Club.GUID = club.GUID;
                Club.CreatedDate = club.CreatedDate;
                // Club.StringGuid = club.StringGuid;
                //club.ClubType = Club.ClubType;
                Club.CreatedById = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault().Id;
                Club.Guid = club.Guid;
                _context.Entry(club).CurrentValues.SetValues(Club);
                _context.SaveChanges();
                return RedirectToAction("Index", "Clubs", new { area = "Admin", Culture = Club.CultureId });
            }
            return View(Club);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            Club about = _context.Clubs.Where(x => x.Id == id).SingleOrDefault();
            _context.Entry(about).State = EntityState.Modified;
            about.Delflag = true;
            _context.SaveChanges();
            return RedirectToAction("Index", "Clubs", new { area = "Admin", Culture = about.CultureId });
        }
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        _context.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
