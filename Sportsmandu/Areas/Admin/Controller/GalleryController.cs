using System;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Services.Abstract;
using Z.EntityFramework.Plus;
using Sportsmandu.Models;
namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class GalleryController : Controller
    {
        private AppDbContext _context;
        private IMapper _mapper { get; set; }
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        public GalleryController(AppDbContext _context, IMapper _mapper, IFileAndDirectoryService _fileService, IHostingEnvironment _hostingEnviroment)
        {
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
            this._context = _context;
            this._mapper = _mapper;
        }       
        public IActionResult Index(int culture)
        {
            return View();
        }               
        public ActionResult Create(int Culture)
        {            
            ClubCreateViewModel p = new ClubCreateViewModel();
            return View(p);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ClubCreateViewModel brand, IFormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                Club Club = _mapper.Map<Club>(brand);
                _context.Entry(Club).State = EntityState.Added;
                Club.CreatedDate = DateTime.Now;
                //##TodO
                Club.CreatedById = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault().Id;
                //o. = 1;
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Clubs", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {                        
                        bool hasImage = _result.ArrayData.Where(x => x.Contains("Clubs/Image")).FirstOrDefault() != null;
                        bool hasCoverImage = _result.ArrayData.Where(x => x.Contains("Clubs/CoverImage")).FirstOrDefault() != null;
                        bool hasThumbnail = _result.ArrayData.Where(x => x.Contains("Clubs/Thumbnail")).FirstOrDefault() != null;
                        if (hasImage)
                            Club.Image = _result.ArrayData.Where(x => x.Contains("Clubs/Image")).FirstOrDefault();
                        if (hasThumbnail)
                            Club.Thumbnail = _result.ArrayData.Where(x => x.Contains("Clubs/Thumbnail")).FirstOrDefault();
                        if (hasCoverImage)
                            Club.CoverImage = _result.ArrayData.Where(x => x.Contains("Clubs/CoverImage")).FirstOrDefault();
                    }
                }
                if (formCollection.Files.Count == 1 && Club.multilingualid != 0)
                {
                    Club.Image = _context.Clubs.SingleOrDefault(x => x.Id == Club.multilingualid).Image;
                }
                Club.Guid = Global.UniqueGuid(this.ControllerContext.RouteData.Values["controller"].ToString(), _context);
                _context.Clubs.Add(Club);
                _context.SaveChanges();
                if (Club.multilingualid == 0)
                {
                    Club.multilingualid = Club.Id;
                    _context.Clubs.Update(Club);
                    _context.SaveChanges();
                }
                ClubConvertedCulture pc = new ClubConvertedCulture
                {
                    CultureId = Club.CultureId.Value,
                    ClubId = Club.multilingualid,
                    Club = null,
                    Culture = null
                };
                return RedirectToAction("Index", "Clubs", new { area = "Admin", Culture = Club.CultureId });
            }
            return View();
        }
    }
}
