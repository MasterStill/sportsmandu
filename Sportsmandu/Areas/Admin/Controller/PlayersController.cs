using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Services.Abstract;
using Z.EntityFramework.Plus;
using Mapster;
namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    public class GenerericAdminController<T> : Controller where T : class
    {
        AppDbContext _context;
        AppDbContext _context1;
        public GenerericAdminController(AppDbContext _context, IMapper _mapper, IFileAndDirectoryService _fileService, IHostingEnvironment _hostingEnviroment)
        {
            this._context = _context;
            this._context1 = _context;
        }
    }
    [Area("Admin")]
    //[Authorize("Admin")]
    public class PlayersController : Controller//: GenerericAdminController<Player>//Controller
    {
        private AppDbContext _context;
        private AppDbContext _context1;
        private IMapper _mapper { get; set; }
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        public PlayersController(AppDbContext _context, AppDbContext _context1, IMapper _mapper, IFileAndDirectoryService _fileService, IHostingEnvironment _hostingEnviroment)
        {
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
            this._context = _context;
            this._context1 = _context1;
            this._mapper = _mapper;
        }
        private void SetViewBagStuffs(int Culture)
        {
            ViewBag.Categories = GetCategories(Culture);
            ViewBag.Countries = GetCountries(Culture);
        }
        private IEnumerable<SelectListItem> GetCountries(int CultureId)
        {
            return _context.Countries.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();
        }
        int NoOfCulture = 2;
        public IActionResult ToBeTranslated(int culture)
        {
            SetViewBagStuffs(culture);
            var _mainResult = _context.Players
                .Include(x => x.ConvertedCulture)
                .Where(x=>x.Delflag == false)
                .Where(x => x.ConvertedCulture.Count == NoOfCulture - 1)
                .Where(x => x.CultureId != culture)
                .Select(x=>new {x.Category,x.Name,x.Image,x.multilingualid})
                .OrderBy(x => x.Name).Take(20);
            var objToReturn = _mainResult.Adapt<List<PlayerAdminViewModel>>();// (_mainResult.Take(20));
            foreach (var items in objToReturn)
            {
                items.Converted = false;
                if (items.Image == null || items.Image == "")
                {
                    items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                }
            }
            var resultT0send = objToReturn.Adapt<List<AdminViewModel>>();
            return View("Index", resultT0send);
        }
        public IActionResult Index(int culture)
        {
            List<PlayerAdminViewModel> objToReturn = new List<PlayerAdminViewModel>(); 
            SetViewBagStuffs(culture);
            var _result = _context.Players
            .Include(x => x.Country)
            .Include(x => x.Category)
            .Include(x => x.Culture)
            .Where(x => x.CultureId == culture)
            .Where(x => x.Image != null)
            .Where(x=>x.Delflag == false)
            //.OrderBy(r => Guid.NewGuid())
            .Take(4).ToList()
            .Select(x => new { x.Id, x.Name, x.Thumbnail, x.Description, x.Category,x.URL })
            .ToList();
            objToReturn = _result.Adapt<List<PlayerAdminViewModel>>();
            //objToReturn = _mapper.Map<IEnumerable<PlayerAdminViewModel>>(_result);
            foreach (var items in objToReturn)
            {
                if (items.Image == null) items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                items.Converted = true;
            }
            return View(objToReturn.Adapt<List<AdminViewModel>>());
        }
        public class PlayerSearchViewModel
        {
            public int CultureId { get; set; }
            public string Term { get; set; }
        }

        [HttpPost]
        public IActionResult Search([FromForm]PlayerSearchViewModel psvm)
        {
            Console.WriteLine("Search Player Started... " + DateTime.Now);
            if (psvm.CultureId == 0) psvm.CultureId = 1;
            var _result = _context.Players
                    .Where(x => x.Name.Contains(psvm.Term))
                    .Where(x => x.Delflag == false)
                    .Include(x => x.Category)
                    .Select(x => new { x.Id, x.Name, x.Gender, x.Image, x.Description, x.multilingualid, x.Category })
                    .ToList();
            //.Select(x=>new { x.Category,x.CategoryId,x.Name,x.Id,x.multilingualid,x.Thumbnail}).ToList();
            //if (psvm != null)
            // {
            // if (psvm.CategoryId != 0)
            // {
            //     _result = _result.Where(x => x.CategoryId == psvm.CategoryId);
            // }
            // }
            // SetViewBagStuffs(1);
            // List<int> _userKnownCulture = new List<int>();
            // _userKnownCulture.Add(1);
            // _userKnownCulture.Add(2);
            //var mappedResult = _mapper.Map<IEnumerable<MultiLingualViewModel>>(_result);
            //var mappedResult = _result.Adapt<IEnumerable<MultiLingualViewModel>>();
            //MultilingualAI MAI = GenericMultiLingualRepo(mappedResult, _userKnownCulture, psvm.CultureId);
            //var result = from item in _result
            //             where !MAI.idToRemove.Contains(item.Id)
            //             select item;
            //var objToReturn = _mapper.Map<IEnumerable<PlayerAdminViewModel>>(result);
            Console.WriteLine("Search Player Yielded... : " + DateTime.Now);
            //var objToReturn = _result.Adapt<IEnumerable<PlayerAdminViewModel>>();            
            Console.WriteLine("Search Mapping Completed !!" + DateTime.Now);
            //foreach (var items in objToReturn)
            //{
            ////    items.Converted = true;//MAI.items.Where(x => x.Id == items.Id).Select(x => x.Converted).SingleOrDefault();
            ////    if (items.Image == null || items.Image == "")
            //    {
            //        // if (items.Image == null) items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
            //        items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
            //    }
            //}
            // SetViewBagStuffs(1);
            return View("Index", _result.Adapt<List<AdminViewModel>>());
            //return View(, objToReturn);
        }
        public ActionResult Create(int Culture)
        {
            //ViewData["Brand"] = new SelectList(_context.Brands.Where(x => x.CultureId == Culture).ToList().Select(x => new { x.multilingualid, x.Name }), "multilingualid", "Name");
            //ViewData["Category"] = new SelectList(_context.Categories.Where(x => x.CultureId == Culture).ToList().Select(x => new { x.multilingualid, x.Name }), "multilingualid", "Name");
            SetViewBagStuffs(Culture);
            PlayerCreateViewModel p = new PlayerCreateViewModel();
            return View(p);
        }
        public ActionResult Details(int Culture, int multilingualid)
        {
            return View();
        }

        [HttpGet]
        public ActionResult Report()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PlayerCreateViewModel pcvm, IFormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                Player player = _mapper.Map<Player>(pcvm);
                _context.Entry(player).State = EntityState.Added;
                player.CreatedDate = DateTime.Now;
                //##TodO
                player.CreatedById = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault().Id;
                Player PlayerToBeModified = new Player();
                if (pcvm.MultiLingualId != 0)
                {
                    PlayerToBeModified = _context.Players.Where(x => x.Id == pcvm.MultiLingualId).SingleOrDefault();
                    player.CountryId = PlayerToBeModified.CountryId.Value;
                }
                //o. = 1;
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Players", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        player.Image = _result.ArrayData.Where(x => x.Contains("Players/Image")).FirstOrDefault();
                        player.Thumbnail = _result.ArrayData.Where(x => x.Contains("Players/Thumbnail")).FirstOrDefault();
                        player.CoverImage = _result.ArrayData.Where(x => x.Contains("Players/CoverImage")).FirstOrDefault();
                    }
                }
                if (formCollection.Files.Count == 1 && player.multilingualid != 0)
                {
                    player.Image = _context.Players.SingleOrDefault(x => x.Id == player.multilingualid).Image;
                }
                if (player.multilingualid != 0)
                {
                    player.CategoryId = PlayerToBeModified.CategoryId;
                    player.Image = player.Image != null ? player.Image : PlayerToBeModified.Image;
                    player.Thumbnail = player.Image != null ? player.Image : PlayerToBeModified.Thumbnail;
                    player.CoverImage = player.Image != null ? player.Image : PlayerToBeModified.CoverImage;
                }
                player.Guid = Global.UniqueGuid(this.ControllerContext.RouteData.Values["controller"].ToString(), _context1);
                _context.Players.Add(player);
                _context.SaveChanges();
                if (player.multilingualid == 0)
                {
                    //_context.Entry(o).State = EntityState.Modified;
                    player.multilingualid = player.Id;
                    _context.Players.Update(player);
                    _context.SaveChanges();
                }
                PlayerConvertedCulture pc = new PlayerConvertedCulture
                {
                    CultureId = pcvm.CultureId,
                    PlayerId = player.multilingualid,
                };
                _context.PlayerConvertedCulture.AddAsync(pc);
                _context.SaveChanges();
                return RedirectToAction("Index", "Players", new { area = "Admin", Culture = player.CultureId });
            }
            return View();
        }
        public ActionResult Edit(int Id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), Id, User.Claims)) return (Unauthorized());
            Player Player = _context.Players
                .Include(x=>x.Social)
                .Include(x => x.Culture)
                .Include(x => x.Category)
                .Include(x => x.Country)
            .Where(x => x.Id == Id).SingleOrDefault();
            SetViewBagStuffs(Player.CultureId.Value);
            return View(Player);
        }
        private IEnumerable<SelectListItem> GetCategories(int CultureId)
        {
            return _context.Categories.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();
        }
        // private IEnumerable<SelectListItem> GetCategories(int CultureId)
        //{
        //    return _context.Categories.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
        //    {
        //        Value = x.multilingualid.ToString(),
        //        Text = x.Name
        //    }).ToList();

        //}             
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit(Player Player, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), Player.Id, User.Claims)) return (Unauthorized());
            if (ModelState.IsValid)
            {
                //_context.Entry(Player).State = EntityState.Modified;
                var existingPlayer = _context.Players.Find(Player.Id);
                Player.Guid = existingPlayer.Guid;
                Player.CreatedById = existingPlayer.CreatedById;
                Player.CreatedDate = existingPlayer.CreatedDate;
                Player.StatsString = existingPlayer.StatsString;
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Players", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        bool hasImage = _result.ArrayData.Where(x => x.Contains("Players/Image")).FirstOrDefault() != null;
                        bool hasCoverImage = _result.ArrayData.Where(x => x.Contains("Players/CoverImage")).FirstOrDefault() != null;
                        bool hasThumbnail = _result.ArrayData.Where(x => x.Contains("Players/Thumbnail")).FirstOrDefault() != null;
                        if (hasImage)
                            Player.Image = _result.ArrayData.Where(x => x.Contains("Players/Image")).FirstOrDefault();
                        if (hasThumbnail)
                            Player.Thumbnail = _result.ArrayData.Where(x => x.Contains("Players/Thumbnail")).FirstOrDefault();
                        if (hasCoverImage)
                            Player.CoverImage = _result.ArrayData.Where(x => x.Contains("Players/CoverImage")).FirstOrDefault();
                    }
                }
                Player.CreatedById = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault().Id;
                //_context.Entry(existingPlayer).State = EntityState.Modified;
                _context.Entry(existingPlayer).CurrentValues.SetValues(Player);
                _context.SaveChanges();
                return RedirectToAction("Index", "Players", new { area = "Admin", Culture = Player.CultureId });
            }
            return View(Player);
        }
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        _context.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}        
    }
}