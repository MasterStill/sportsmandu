using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Sportsmandu.Services.Abstract;
using System.Text;
using System.Net.Http; 
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Sportsmandu.Data;
using Sportsmandu.Models.Messaging.Notification;

namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class OneSignalController : Controller
    {
        private AppDbContext _context;
        private IMapper _mapper { get; set; }        
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        static HttpClient client = new HttpClient();
        public string OneSignalAuthToken = "Basic NjFiMzE1MzgtN2Q1Ni00YzQ1LWIzYTAtNDBiZDg0NWNkZDNl";

        public OneSignalController(AppDbContext _context, IMapper _mapper, IFileAndDirectoryService _fileService, IHostingEnvironment _hostingEnviroment)
        {
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
            this._context = _context;
            this._mapper = _mapper;
        }
        [HttpPost]
        public IActionResult Index([FromBody]PushMessage Message)
        {
            SendPushNotification(Message);
            return View();
        }
        public class OneSignal
        {
            public string app_id { get; set; }
            public string contents { get; set; }
            public string[] included_segments;
        }
        //public class PushMessage{
        //    public string Message {get;set;}
        //    public string Title {get;set;}

        //    public MessageType MessageType {get;set;}
        //    public List<string> DeviceIds {get;set;}

        //}
        private void SendPushNotification(PushMessage p)
        {
            try
            {
                client.BaseAddress = new Uri("https://onesignal.com/api/v1/notifications");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", OneSignalAuthToken);
            }
            catch
            {
            }
                string deviceIds = string.Empty;
                foreach(var items in p.DeviceIds){
                    deviceIds += "'" + items + "'";
                }
                if (p.DeviceIds.Count > 0)deviceIds = "[" + deviceIds.Replace("''","','") + "]";

                string OnlyFew;
                if (p.MessageType == MessageType.All){
                    OnlyFew = "'included_segments':['All']";
                }
                else{
                    OnlyFew = "'include_player_ids':" + deviceIds; 
                }

                string json = "{'app_id':'ba74aad0-9409-4f33-bc1d-a96949e4ff6b','contents': {'en': '" + p.Message + "'}, " +  OnlyFew + ",'headings':{'en':'"+ p.Title +"'}}";
                string milekoData = json.Replace("'", "\"");
                Console.WriteLine(milekoData);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "https://onesignal.com/api/v1/notifications");
                request.Content = new StringContent(milekoData,
                                        Encoding.UTF8,
                                        "application/json");
                var asdasdas = client.SendAsync(request)
                 .ContinueWith(responseTask =>
                 {
                     var lamokha = responseTask.Result;
                 });

        }
        //public enum MessageType
        //{
        //    All, Few
        //}
    }
}
