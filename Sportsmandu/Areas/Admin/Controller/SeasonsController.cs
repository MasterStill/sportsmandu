using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Models.Globalization;
using Sportsmandu.Services.Abstract;
using static Sportsmandu.MultiLingualGlobal;
using Z.EntityFramework.Plus;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Models;
using Sportsmandu.Models.League;
using Mapster;

namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class SeasonsController : Controller
    {
        private AppDbContext _context;
        private IMapper _mapper { get; set; }
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        public SeasonsController(AppDbContext _context, IMapper _mapper, IFileAndDirectoryService _fileService, IHostingEnvironment _hostingEnviroment)
        {
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
            this._context = _context;
            this._mapper = _mapper;
        }
        private void SetViewBagStuffs(int Culture)
        {
            ViewBag.Leagues = GetCategories(Culture);
        }
        public IActionResult Index(int culture)
        {
            SetViewBagStuffs(culture);
            var _result = _context.Seasons
            .Where(x => x.Delflag == false)
            .Where(x=>x.CultureId == culture)
            .OrderByDescending(x=>x.CreatedDate)            
            .Take(4).ToList()
            .Select(x => new { x.Id, x.Name, x.Thumbnail, x.Description })
            .ToList();
            var objToReturn = _result.Adapt<List<SeasonAdminViewModel>>();
            foreach(var items in objToReturn)
            {
                items.Converted = true;
            }
            return View(objToReturn.Adapt<List<AdminViewModel>>());        
            //return View(objToReturn);
        }
        public class SeasonsearchViewModel
        {
            public int CultureId { get; set; }
            public int CategoryId { get; set; }
            public int BrandId { get; set; }
            public string Term { get; set; }
        }

        int NoOfCulture = 2;
        public IActionResult ToBeTranslated(int culture)
        {
            SetViewBagStuffs(culture);
            var _mainResult = _context.Seasons
                .Include(x => x.ConvertedCulture)
                .Where(x => x.Delflag == false)
                .Where(x => x.ConvertedCulture.Count == NoOfCulture - 1)
                .Where(x => x.CultureId != culture)
                .Select(x => new { x.Name, x.Image, x.multilingualid })
                .OrderBy(x => x.Name).Take(20);
            var objToReturn = _mainResult.Adapt<List<SeasonAdminViewModel>>();// (_mainResult.Take(20));
            foreach (var items in objToReturn)
            {
                items.Converted = false;
                if (items.Image == null || items.Image == "")
                {
                    items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                }
            }
            var resultT0send = objToReturn.Adapt<List<AdminViewModel>>();
            return View("Index", resultT0send);
        }

        [HttpPost]
        public IActionResult Search([FromForm]SeasonsearchViewModel psvm)
        {
            if (psvm.CultureId == 0) psvm.CultureId = 1;
            var _result = _context.Seasons
                    // .Include(x => x.Brand)
                    //.Include(x => x.Category)
                    // .Include(x => x.Parts)
                    .Where(x => x.Delflag == false);
            //if (psvm != null)
            {
                //if (psvm.CategoryId != 0)
                //{
                //    _result = _result.Where(x => x.CategoryId == psvm.CategoryId);                    
                //}               
            }
            SetViewBagStuffs(1);
            List<int> _userKnownCulture = new List<int>();
            _userKnownCulture.Add(1);
            _userKnownCulture.Add(2);
            MultilingualAI MAI = GenericMultiLingualRepo(_mapper.Map<IEnumerable<MultiLingualViewModel>>(_result), _userKnownCulture, psvm.CultureId);
            var result = from item in _result
                         where !MAI.idToRemove.Contains(item.Id)
                         select item;
            var objToReturn = _mapper.Map<IEnumerable<SeasonAdminViewModel>>(result);
            foreach (var items in objToReturn)
            {
                items.Converted = MAI.items.Where(x => x.Id == items.Id).Select(x => x.Converted).SingleOrDefault();
                if (items.Image == null || items.Image == "")
                {
                    items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                }
            }
            SetViewBagStuffs(1);
            return View("Index", objToReturn.Where(x=>x.Name.ToLower().Contains(psvm.Term.ToLower())).Adapt<List<AdminViewModel>>());
        }
        public ActionResult Create(int Culture)
        {
            SetViewBagStuffs(Culture);
            SeasonCreateViewModel p = new SeasonCreateViewModel();
            return View(p);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SeasonCreateViewModel brand, IFormCollection formCollection)
        {
            if(brand.multilingualid != 0)
            {
                var _season = _context.Seasons.Where(x => x.Id == brand.multilingualid).SingleOrDefault();
                brand.StartedDate = _season.StartedDate;
                brand.EndDate = _season.EndDate;
                brand.Image = _season.Image;
                brand.Thumbnail = _season.Thumbnail;
                brand.CoverImage = _season.CoverImage;
                brand.LeagueId = _season.LeagueId;                
            }
            if (brand.Name == null || brand.LeagueId == 0) return View();
            //if (ModelState.IsValid)
            {
                Season Season = _mapper.Map<Season>(brand);
                _context.Entry(Season).State = EntityState.Added;
                Season.CreatedDate = DateTime.Now;
                //##TodO
                Season.CreatedById = _context.Users.Where(x=>x.UserName == User.Identity.Name).SingleOrDefault().Id;
                //o. = 1;
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Seasons", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        bool hasImage = _result.ArrayData.Where(x => x.Contains("Seasons/Image")).FirstOrDefault() != null;
                        bool hasCoverImage = _result.ArrayData.Where(x => x.Contains("Seasons/CoverImage")).FirstOrDefault() != null;
                        bool hasThumbnail = _result.ArrayData.Where(x => x.Contains("Seasons/Thumbnail")).FirstOrDefault() != null;
                        if (hasImage)                        
                            Season.Image = _result.ArrayData.Where(x => x.Contains("Seasons/Image")).FirstOrDefault();
                        if(hasThumbnail)
                        Season.Thumbnail = _result.ArrayData.Where(x => x.Contains("Seasons/Thumbnail")).FirstOrDefault();
                        if (hasCoverImage)
                            Season.CoverImage = _result.ArrayData.Where(x => x.Contains("Seasons/CoverImage")).FirstOrDefault();
                    }                    
                }
                if (formCollection.Files.Count == 1 && Season.multilingualid != 0)
                {
                    Season.Image = _context.Seasons.SingleOrDefault(x => x.Id == Season.multilingualid).Image;
                }
                //if (o.multilingualid != 0)
                //{
                //    o.CategoryId = _context.Seasons.SingleOrDefault(x => x.Id == o.multilingualid).CategoryId;
                //}
                Season.Guid = Global.UniqueGuid(this.ControllerContext.RouteData.Values["controller"].ToString(), _context);
                _context.Seasons.Add(Season);
                _context.SaveChanges();
                if (Season.multilingualid == 0)
                {
                    //_context.Entry(o).State = EntityState.Modified;
                    Season.multilingualid = Season.Id;
                    _context.Seasons.Update(Season);
                    _context.SaveChanges();
                }
                 SeasonConvertedCulture pc = new SeasonConvertedCulture
                {
                    CultureId = Season.CultureId.Value,
                    SeasonId = Season.multilingualid,
                };
                _context.SeasonConvertedCulture.AddAsync(pc);
                _context.SaveChanges();
                return RedirectToAction("Index", "Seasons", new { area = "Admin", Culture = Season.CultureId });
            }
            //return View();
        }
        public ActionResult Edit(int Id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), Id, User.Claims)) return (Unauthorized());
            Season Season = _context.Seasons.Include(x => x.Culture)            
            .Where(x => x.Id == Id).SingleOrDefault();
            SetViewBagStuffs(Season.Culture.Id);
            return View(Season);
        }
        private IEnumerable<SelectListItem> GetCategories(int CultureId)
        {
            return _context.Leagues.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();

        }
        // private IEnumerable<SelectListItem> GetCategories(int CultureId)
        //{
        //    return _context.Categories.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
        //    {
        //        Value = x.multilingualid.ToString(),
        //        Text = x.Name
        //    }).ToList();

        //}             
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Season Season, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), Season.Id, User.Claims)) return (Unauthorized());
            if (ModelState.IsValid)
            {
                //_context.Entry(Season).State = EntityState.Modified;
                var season= _context.Seasons.Find(Season.Id);
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Seasons", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        bool hasImage = _result.ArrayData.Where(x => x.Contains("Seasons/Image")).FirstOrDefault() != null;
                        bool hasCoverImage = _result.ArrayData.Where(x => x.Contains("Seasons/CoverImage")).FirstOrDefault() != null;
                        bool hasThumbnail = _result.ArrayData.Where(x => x.Contains("Seasons/Thumbnail")).FirstOrDefault() != null;
                        if (hasImage)                        
                            Season.Image = _result.ArrayData.Where(x => x.Contains("Seasons/Image")).FirstOrDefault();
                        if(hasThumbnail)
                        Season.Thumbnail = _result.ArrayData.Where(x => x.Contains("Seasons/Thumbnail")).FirstOrDefault();
                        if (hasCoverImage)
                            Season.CoverImage = _result.ArrayData.Where(x => x.Contains("Seasons/CoverImage")).FirstOrDefault();
                    }    
                }

                Season.CreatedById = season.CreatedById;
                Season.Guid = season.Guid;               
                Season.CreatedDate = season.CreatedDate;
                Season.ModifiedById = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault().Id;
                _context.Entry(season).CurrentValues.SetValues(Season);
                _context.SaveChanges();

                return RedirectToAction("Index", "Seasons", new { area = "Admin", Culture = Season.CultureId });
            }
            return View(Season);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            Season about = _context.Seasons.Where(x => x.Id == id).SingleOrDefault();
            _context.Entry(about).State = EntityState.Modified;
            about.Delflag = true;
            _context.SaveChanges();
            return RedirectToAction("Index", "Seasons", new { area = "Admin", Culture = about.CultureId });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
