using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Models.Globalization;
using Sportsmandu.Services.Abstract;
using static Sportsmandu.MultiLingualGlobal;
using Z.EntityFramework.Plus;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Models;
using Sportsmandu.Models.League;
using Mapster;
using Sportsmandu.Models.Generic;

namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class PartnersController : Controller
    {
        private AppDbContext _context;
        private IMapper _mapper { get; set; }
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        public PartnersController(AppDbContext _context, IMapper _mapper, IFileAndDirectoryService _fileService, IHostingEnvironment _hostingEnviroment)
        {
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
            this._context = _context;
            this._mapper = _mapper;
        }
        public IActionResult Search([FromForm]PlayersController.PlayerSearchViewModel psvm)
        {
            Console.WriteLine("Search Partners Started... " + DateTime.Now);
            if (psvm.CultureId == 0) psvm.CultureId = 1;
            var _result = _context.Partners
                    .Where(x => x.Name.Contains(psvm.Term))
                    .Where(x => x.Delflag == false)
                    .Select(x => new { x.Id, x.Name,  x.Image, x.Description, x.multilingualid})
                    .ToList();
            return View("Index", _result.Adapt<List<AdminViewModel>>());
        }
        public IActionResult Index(int culture)
        {
            //SetViewBagStuffs(culture);
            var _result = _context.Partners
            //.Include(x => x.Brand)
            //.Include(x => x.Category).Include(x => x.Parts)
            .Where(x => x.Delflag == false)             
            .Where(x => x.CultureId == culture)
            .OrderByDescending(x => x.CreatedDate)
            .Take(4)
            .Select(x => new { x.Id, x.Name,x.Thumbnail, x.Image, x.Description, x.multilingualid})
            .ToList();
            var objToReturn = _result.Adapt<List<PartnerAdminViewModel>>();
            return View(objToReturn.Adapt<List<AdminViewModel>>());            
        }
        public ActionResult Create(int Culture)
        {
            PartnerCreateViewModel p = new PartnerCreateViewModel();
            return View(p);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PartnerCreateViewModel Partner, IFormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                Partner o = _mapper.Map<Partner>(Partner);
                _context.Entry(o).State = EntityState.Added;
                o.CreatedDate = DateTime.Now;
                //##TodO
                o.CreatedById = _context.Users.Where(x=>x.UserName == User.Identity.Name).SingleOrDefault().Id;
                //o. = 1;
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Partners", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        Partner.Image = _result.ArrayData.Where(x => x.Contains("Partners/Image")).FirstOrDefault();
                        Partner.Thumbnail = _result.ArrayData.Where(x => x.Contains("Partners/Thumbnail")).FirstOrDefault();
                        Partner.CoverImage = _result.ArrayData.Where(x => x.Contains("Partners/CoverImage")).FirstOrDefault();
                    }
                }
                if (formCollection.Files.Count == 1 && o.multilingualid != 0)
                {
                    o.Image = _context.Partners.SingleOrDefault(x => x.Id == o.multilingualid).Image;
                }
                //if (o.multilingualid != 0)
                //{
                //    o.CategoryId = _context.Partners.SingleOrDefault(x => x.Id == o.multilingualid).CategoryId;
                //}
                o.Guid = Global.UniqueGuid(this.ControllerContext.RouteData.Values["controller"].ToString(), _context);
                _context.Partners.Add(o);
                _context.SaveChanges();
                if (o.multilingualid == 0)
                {
                    //_context.Entry(o).State = EntityState.Modified;
                    o.multilingualid = o.Id;
                    _context.Partners.Update(o);
                    _context.SaveChanges();
                }
                PartnerConvertedCulture pc = new PartnerConvertedCulture
                {
                    CultureId = Partner.CultureId,
                    PartnerId = Partner.multilingualid,
                };
                _context.PartnerConvertedCulture.AddAsync(pc);
                return RedirectToAction("Index", "Partners", new { area = "Admin", Culture = o.CultureId });
            }
            return View();
        }
        public ActionResult Edit(int Id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), Id, User.Claims)) return (Unauthorized());
            Partner about = _context.Partners
            .Where(x => x.Id == Id).SingleOrDefault();
            //SetViewBagStuffs(1);
            return View(about);
        }
        private IEnumerable<SelectListItem> GetCategories(int CultureId)
        {
            return _context.Leagues.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();

        }
        // private IEnumerable<SelectListItem> GetCategories(int CultureId)
        //{
        //    return _context.Categories.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
        //    {
        //        Value = x.multilingualid.ToString(),
        //        Text = x.Name
        //    }).ToList();

        //}             
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Partner Partner, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), Partner.Id, User.Claims)) return (Unauthorized());
            if (ModelState.IsValid)
            {
                //_context.Entry(Partner).State = EntityState.Modified;
                var partner = _context.Partners.Find(Partner.Id);
                Partner.Guid = partner.Guid;
                Partner.CreatedDate = partner.CreatedDate;
                Partner.CreatedById = partner.CreatedById;
                Partner.LastModifiedDate = DateTime.Now;
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Partners", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        bool hasImage = _result.ArrayData.Where(x => x.Contains("Partners/Image")).FirstOrDefault() != null;
                        bool hasCoverImage = _result.ArrayData.Where(x => x.Contains("Partners/CoverImage")).FirstOrDefault() != null;
                        bool hasThumbnail = _result.ArrayData.Where(x => x.Contains("Partners/Thumbnail")).FirstOrDefault() != null;
                        if (hasImage)
                            Partner.Image = _result.ArrayData.Where(x => x.Contains("Partners/Image")).FirstOrDefault();
                        if (hasThumbnail)
                            Partner.Thumbnail = _result.ArrayData.Where(x => x.Contains("Partners/Thumbnail")).FirstOrDefault();
                        if (hasCoverImage)
                            Partner.CoverImage = _result.ArrayData.Where(x => x.Contains("Partners/CoverImage")).FirstOrDefault();
                    }
                }
                Partner.CreatedById = _context.Users.Where(x=>x.UserName == User.Identity.Name).SingleOrDefault().Id;
                _context.Entry(partner).CurrentValues.SetValues(Partner);
                _context.SaveChanges();
                return RedirectToAction("Index", "Partners", new { area = "Admin", Culture = Partner.CultureId });
            }
            return View(Partner);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            Partner about = _context.Partners.Where(x => x.Id == id).SingleOrDefault();
            _context.Entry(about).State = EntityState.Modified;
            about.Delflag = true;
            _context.SaveChanges();
            return RedirectToAction("Index", "Partners", new { area = "Admin", Culture = about.CultureId });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
