using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Services.Abstract;
using Community.Model.Entities.ViewModels;
using System.Linq;
using Sportsmandu.Model.Entities.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Sportsmandu.Data;
using Microsoft.EntityFrameworkCore;
using Mapster;
using System;
using System.Net;
using System.IO;
using Sportsmandu.Models.Generic;
using Microsoft.AspNetCore.SignalR;

namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    // [Route("api/Blogs")]
    [Authorize]
    [Area("Admin")]
    public class BlogsController : Controller
    {
        //private Global Global;
        private readonly ILogger _logger;
        private readonly JsonSerializerSettings _serializerSettings;
        private IBlogService _blogService;
        private IHostingEnvironment _hostingEnvironment;
        List<string> Errors = new List<string>();
        private IFileAndDirectoryService _fileService;
        private AppDbContext _context;
        private IHubContext<Hubs.SportsmanduHub> _connectionManager;
        private static readonly System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
        public BlogsController(AppDbContext _context ,ILoggerFactory loggerFactory, IBlogService _blogService, IHostingEnvironment HostingEnvironment, IFileAndDirectoryService _fileService, IHubContext<Hubs.SportsmanduHub> _connectionManager)
        {
            this._fileService = _fileService;
            this._hostingEnvironment = HostingEnvironment;
            this._connectionManager = _connectionManager;
            this._blogService = _blogService;
            _logger = loggerFactory.CreateLogger<BlogsController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting  = Formatting.Indented
            };
            this._context = _context;
        }
        [AllowAnonymous]
        [HttpGet("Culture/{CultureName}/{Type}/Category/{Category}/{page}")]
        public IEnumerable<TestViewModel> GetBlogForWebsiteForCategory(string WebsiteName, string CultureName, BlogType Type, int page, string Category)
        {
            return _blogService.GetBlogPostsForCategory(CultureName, Category, page, Type);
        }
        [AllowAnonymous]
        [HttpGet("AllBlogs/Culture/{CultureName}/{page}")]
        public IEnumerable<TestViewModel> GetAllBlogForWebsite(string WebsiteName, string CultureName, int page)
        {
            return _blogService.GetBlogPostsForWebsite(WebsiteName, CultureName, page, Global.Pagesize(), BlogType.Blog);
        }
        int NoOfCulture = 2;
        public IActionResult ToBeTranslated(int culture)
        {            
            var _mainResult = _context.BlogPosts.Include(x => x.ConvertedCulture).Where(x => x.ConvertedCulture.Count == NoOfCulture - 1).Where(x => x.CultureId != culture).OrderBy(x => x.Title).Where(x=>x.Delflag == false).Take(20);
            var objToReturn = _mainResult.Adapt<List<BlogAdminViewModel>>();// (_mainResult.Take(20));
            foreach (var items in objToReturn)
            {
                items.Converted = false;
                if (items.Image == null || items.Image == "")
                {
                    items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                }
            }
            return View("Index", objToReturn);
        }
        public class NewsSource
        {
            public string Name { get; set; }
            public string Url { get; set; }
            public enumNewsSource Source { get; set; }            
        }
        public enum enumNewsSource { 
                Annapurna
        }
        private BlogPostVMFORES GetAllBlogPostFORES(int id)
        {
            BlogPost blogPost = _context.BlogPosts
                .Include(x => x.Culture)
                .Where(x => x.Delflag == false)
                .Where(x => x.Id == id)
                .SingleOrDefault();

            if (blogPost.multilingualid != blogPost.Id)
            {
                var dbBlogPost = _context.BlogPosts.Where(x => x.Id == blogPost.multilingualid).SingleOrDefault();
                if (blogPost.Image == null)
                {
                    blogPost.Image = dbBlogPost.Image;
                }
                if (blogPost.Thumbnail == null)
                {
                    blogPost.Thumbnail = dbBlogPost.Thumbnail;
                }
                if (blogPost.CoverImage == null)
                {
                    if (dbBlogPost == null)
                    {
                        blogPost.CoverImage = blogPost.Image;
                    }
                    else
                    {
                        blogPost.CoverImage = dbBlogPost.CoverImage;
                    }
                }
            }
            BlogPostVMFORES PVM = blogPost.Adapt<BlogPostVMFORES>();
            return PVM;
        }

        [AllowAnonymous]
        public IActionResult FetchNews(int Culture)
        {
            string ThumbnailPath = string.Empty;
            Console.WriteLine("Fetching News From Annapurna To Save...");
            Models.AnnapurnaPostScrapResult p = new Models.AnnapurnaPostScrapResult();
            Models.AnnapurnaPostSingleScrapResult singleResult = new Models.AnnapurnaPostSingleScrapResult();
            WebClient webClient = new WebClient();
            ICollection<BlogPost> blogPosts = new List<BlogPost>();
            List<AdminViewModel> blogToSend = new List<AdminViewModel>();                
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync("http://bg.annapurnapost.com/api/news/list?page=1&per_page=40&category_alias=sports&isCategoryPage=1");
            var msg = stringTask.Result;
            var allnews = _context.BlogPosts.ToList();
            p = JsonConvert.DeserializeObject<Models.AnnapurnaPostScrapResult>(msg);
            foreach (var annapurnaNews in p.Data)
            {
                if (allnews.Where(x=>x.Title == annapurnaNews.Title).FirstOrDefault() == null)
                {
                    string THUMBNAIL = "http://bg.annapurnapost.com" + annapurnaNews.FeaturedImage;
                    ThumbnailPath = "useruploads/1/blogs/thumbnail/" + annapurnaNews.Id + ".jpg";

                    var uploadsPath = Path.Combine(_hostingEnvironment.WebRootPath, ThumbnailPath);
                    webClient.DownloadFile(THUMBNAIL, uploadsPath);
                    //if(annapurnaNews.PublishedOnEnglish != null)
                    blogPosts.Add(new BlogPost
                    {
                        CultureId = 2,
                        //PublishDate = annapurnaNews.PublishedOnEnglish == null ? DateTime.Now : DateTime.Parse(annapurnaNews.PublishedOnEnglish),// DateTime.Now,
                        PublishDateNp = annapurnaNews.PublishOn,
                        Thumbnail = Global.FrontEndUseChangeBackground(ThumbnailPath, Models.SportsmanduImage.enumImageSegment.player_profile),
                        Title = annapurnaNews.Title,
                        Summary = annapurnaNews.IntroText,
                        Source = "http://annapurnapost.com/news/" + annapurnaNews.Id,
                        Api = "http://bg.annapurnapost.com/api/news/" + annapurnaNews.Id,
                        CategoryId = 1,
                        MiscId = annapurnaNews.Id
                    });
                }
            }
            foreach(var items in blogPosts)
            {
                //string CoverImage = "http://bg.annapurnapost.com" + annapurnaNews.FeaturedImage;
                //string CoverImagePath = "useruploads/1/blogs/thumbnail/" + Global.GetCleanUrl(annapurnaNews.Title) + ".jpg";
                //var uploadsPath = Path.Combine(_hostingEnvironment.WebRootPath, ThumbnailPath);
                //webClient.DownloadFile(THUMBNAIL, uploadsPath);
                client.DefaultRequestHeaders.Accept.Clear();
                var stringTaska = client.GetStringAsync(items.Api);
                var msga = stringTaska.Result;
                singleResult = JsonConvert.DeserializeObject<Models.AnnapurnaPostSingleScrapResult>(msga);
                items.Description = singleResult.News.Content;                
                items.Image = "http://bg.annapurnapost.com" + singleResult.News.FeaturedImage ;                
                items.PublishDate = singleResult.News.PublishedOnEnglish == null ? DateTime.Now : DateTime.Parse(singleResult.News.PublishedOnEnglish);// DateTime.Now,
                items.Guid = Global.UniqueGuid("BlogPosts", _context);
                ThumbnailPath = "useruploads/1/blogs/thumbnail/" + items.MiscId + ".jpg";
                items.CoverImage = Global.FrontEndUseChangeBackground(ThumbnailPath, Models.SportsmanduImage.enumImageSegment.ground_coverImage);
                items.AuthorImageUrl = "http://bg.annapurnapost.com" + singleResult.Author.ImageUrl;
                items.AuthorName = singleResult.Author.Name;
                items.CreatedById = 1;
                items.Verified = true;
                if(items.Summary == null)
                {
                    items.Summary = singleResult.News.MetaDescriptions;
                }
                try
                {
                    _context.Entry(items).State = EntityState.Added;
                    _context.SaveChanges();
                    UniversalDataContainer universalDataContainer = new UniversalDataContainer
                    {
                        Command = enumCommand.BlogPost.ToString(),
                        Param = enumParam.UPDATE.ToString(),
                        Result = GetAllBlogPostFORES(items.Id)
                    };
                    _connectionManager.Clients.Group("console").SendAsync("onUniversalGet", universalDataContainer);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Global.Error(ex);
                }
            }
                blogToSend = blogPosts.Adapt<List<BlogPostViewModel>>().Adapt<List<AdminViewModel>>();                       
            return View("Index", blogToSend);
        }
        public IActionResult Index(int Culture)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Unauthorized());
            List<AdminViewModel> _result = _blogService.GetBlogPosts(GetCurrentUserId(), 0, Global.Pagesize(), 1, Culture, BlogType.News);
            return View(_result.Adapt<List<AdminViewModel>>());
        }
        [AllowAnonymous]
        [HttpGet("{BlogId}")]
        public BlogPostCreateViewModel Get(int BlogId)
        {
            return _blogService.GetBlogPost(BlogId);
        }
        [HttpDelete("{BlogId}")]
        public IActionResult Delete(int BlogId)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", BlogId.ToString()), this.ControllerContext.RouteData.Values["action"].ToString(), BlogId, User.Claims)) return (Unauthorized());
            var _result = _blogService.DeleteBlog(BlogId, GetCurrentUserId());
            return Global.SendAsItIs(_result);
        }

        [HttpGet("Culture/{CultureId}/Type/{Type}/Categories")]
        public IActionResult GetCategories(int StoreId, int CultureId, BlogType Type)
        {
            return new ObjectResult(_blogService.GetCategories(GetCurrentUserId(), 0, Global.Pagesize(), StoreId, CultureId, Type));
        }
        private IEnumerable<SelectListItem> GetBlogCategories(int CultureId)
        {
            return _blogService.GetCategoriesForWebsite(null,"En",10,BlogType.News).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Title
            }).ToList();
        }
        [HttpGet]
        public IActionResult Create(int CultureId){
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Unauthorized());
            ViewBag.Categories = GetBlogCategories(CultureId);
            return View();
        }
        [HttpGet]
        public IActionResult Edit(int Id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", "0"), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Unauthorized());
            return View(_blogService.GetBlogPost(Id));
        }
        [HttpPost]
        public IActionResult Create( BlogPostCreateViewModel bLogVm, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Unauthorized());
            if (formCollection.Files.Count > 0)
            {
                var aaaaa = _fileService.CreateFiles("Sportsmandu", "Blogs", formCollection, _hostingEnvironment, 1);
                if (aaaaa.Succeded)
                { 
                    bool hasImage = aaaaa.ArrayData.Where(x => x.Contains("Blogs/Image")).FirstOrDefault() != null;
                    bool hasCoverImage = aaaaa.ArrayData.Where(x => x.Contains("Blogs/CoverImage")).FirstOrDefault() != null;
                    bool hasThumbnail = aaaaa.ArrayData.Where(x => x.Contains("Blogs/Thumbnail")).FirstOrDefault() != null;
                    if (hasImage)
                        bLogVm.Image = aaaaa.ArrayData.Where(x => x.Contains("Blogs/Image")).FirstOrDefault();
                    if (hasThumbnail)
                        bLogVm.Thumbnail = aaaaa.ArrayData.Where(x => x.Contains("Blogs/Thumbnail")).FirstOrDefault();
                    if (hasCoverImage)
                        bLogVm.CoverImage = aaaaa.ArrayData.Where(x => x.Contains("Blogs/CoverImage")).FirstOrDefault();
                }
            }
            var _result = _blogService.CreateBlogPost(GetCurrentUserId(), bLogVm);
            return Global.SendAsItIs(_result);
        }
         [HttpPost]
        public IActionResult Edit( BlogPostCreateViewModel bLogVm, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Unauthorized());
            if (formCollection.Files.Count > 0)
            {
                var aaaaa = _fileService.CreateFiles("Sportsmandu", "Blogs", formCollection, _hostingEnvironment, 1);
                if (aaaaa.Succeded)
                { 
                    bool hasImage = aaaaa.ArrayData.Where(x => x.Contains("Blogs/Image")).FirstOrDefault() != null;
                    bool hasCoverImage = aaaaa.ArrayData.Where(x => x.Contains("Blogs/CoverImage")).FirstOrDefault() != null;
                    bool hasThumbnail = aaaaa.ArrayData.Where(x => x.Contains("Blogs/Thumbnail")).FirstOrDefault() != null;
                    if (hasImage)
                        bLogVm.Image = aaaaa.ArrayData.Where(x => x.Contains("Blogs/Image")).FirstOrDefault();
                    if (hasThumbnail)
                        bLogVm.Thumbnail = aaaaa.ArrayData.Where(x => x.Contains("Blogs/Thumbnail")).FirstOrDefault();
                    if (hasCoverImage)
                        bLogVm.CoverImage = aaaaa.ArrayData.Where(x => x.Contains("Blogs/CoverImage")).FirstOrDefault();
                }
            }
            var _result = _blogService.CreateBlogPost(GetCurrentUserId(), bLogVm);
            return Global.SendAsItIs(_result);
        }
        
        public IActionResult Verify()
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Unauthorized());
            var _result = _blogService.GetUnverifiedPosts();
            return View("Index", _result.Adapt<List<AdminViewModel>>());
        }
   
        [HttpPost("Category")]
        public IActionResult CreateCategory([FromBody] BlogCategoryCreateViewModel photos)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), 0, User.Claims)) return (Unauthorized());
            var _result = _blogService.CreateCategory(GetCurrentUserId(), photos);
            return Global.SendAsItIs(_result);
        } 
        int GetCurrentUserId()
        {
            return 1;// _global.UserIdFromJWT(User)
        }
    }
}