using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Sportsmandu.Services.Abstract;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.ComponentModel.DataAnnotations;

namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    [Authorize]
    [Route("api/FilesAndDirectory")]
    [Produces("application/json")]
    public class FilesAndDirectoryController : Controller
    {
        private readonly ILogger _logger;
        //private readonly JwtIssuerOptions _jwtOptions;
        private readonly JsonSerializerSettings _serializerSettings;
        //private GlobalUser _global = new GlobalUser();
        private IHostingEnvironment _hostingEnviroment;
        private IFileAndDirectoryService _fdService;
        //private IUserService _userService;
        public FilesAndDirectoryController(IHostingEnvironment _hostingEnviroment, IFileAndDirectoryService _fdService, ILoggerFactory loggerFactory)//, IUserService _userService)//, IOptions<JwtIssuerOptions> jwtOptions,
        {
            this._fdService = _fdService;
            this._hostingEnviroment = _hostingEnviroment;
            //this._userService = _userService;

            //_jwtOptions = jwtOptions.Value;
            //Global.ThrowIfInvalidOptions(_jwtOptions);
            _logger = loggerFactory.CreateLogger<FilesAndDirectoryController>();
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }
        [HttpPost("{StoreId}/{Path}")]
        public IActionResult UploadContents(int StoreId, IFormCollection formCollection, string Path = "")
        {
            var _result = _fdService.CreateFiles(StoreId, Path, formCollection, _hostingEnviroment, UserId());
          return  Global.SendAsItIs(_result);
            // if (_result.Succeeded) return new OkObjectResult(_result);
            // return BadRequest(_result);
        }

        public class FileSearchOption
        {
            public string SearchTerm { get; set; }
            public string Path { get; set; }
        }
        [Authorize]
        [HttpGet("{StoreId}/Path/{Path}/SearchTerm/{SearchTerm}")]
        public FileAndDirectoryViewModel SearchContents(int StoreId, string SearchTerm, string Path)
        {
            //if (!_userService.IsInRole(UserId(), StoreId, (new string[] { "Admin", "Owner", "Editor" }))) return new FileAndDirectoryViewModel();
            return _fdService.Search(StoreId, "", Path, SearchTerm, _hostingEnviroment, UserId());//_global.UserIdFromJWT(User));
        }
        public class DeleteContentViewModel
        {
            
            [Required]
            public string FileName { get; set; }
        }
        [HttpPost("Store/{StoreId}/Delete")]
        //[Validate]
        public IActionResult DeleteContent([FromBody] DeleteContentViewModel d, int StoreId)
        {
            var _result = _fdService.DeleteContent(UserId(), StoreId, d.FileName);
            return Global.SendAsItIs(_result);
        }
        private int UserId()
        {
            return 0;
            //_global.UserIdFromJWT(User)
        }
    }
}