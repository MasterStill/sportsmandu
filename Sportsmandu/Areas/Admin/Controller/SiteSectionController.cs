
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sportsmandu;
using Sportsmandu.Data;
using Sportsmandu.Model.Entities.Globalization;

namespace MandirDarsan.Areas.Admin.Controllers
{
    [Authorize(Roles = "Super_Admin")]
    [Area("Admin")]
    public class SiteSectionsController : Controller
    {
        private static AppDbContext _context;
        private AppDbContext siteSectionRepository;
        public SiteSectionsController(AppDbContext context)
        {
            _context = context;
            siteSectionRepository = context;
        }

        // GET: Admin/SiteSections
        public ActionResult Index(int? pageNo, string searchTerm = "")
        {
            var siteSection = siteSectionRepository.SiteSections;
            return View(siteSection);
        }

        // GET: Admin/SiteSections/Create
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Create(SiteSection siteSection)
        {
            if (ModelState.IsValid)
            {
                siteSectionRepository.SiteSections.Add(siteSection);
                siteSectionRepository.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(siteSection);
        }

        public ActionResult Edit(int Id)
        {            
            var siteSection = siteSectionRepository.SiteSections.Find(Id);
            if (siteSection == null)
            {
                //return HttpNotFound();
            }
            return View(siteSection);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Edit(SiteSection siteSection)
        {            
            if (ModelState.IsValid)
            {
                _context.SiteSections.Update(siteSection);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(siteSection);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var siteSection = siteSectionRepository.SiteSections.Find(id);
            siteSectionRepository.Remove(siteSection);
            siteSectionRepository.SaveChanges();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}