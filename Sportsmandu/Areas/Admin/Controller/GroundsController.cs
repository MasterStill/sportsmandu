using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Models.Globalization;
using Sportsmandu.Services.Abstract;
using static Sportsmandu.MultiLingualGlobal;
using Z.EntityFramework.Plus;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Models;
using Mapster;
using Sportsmandu.Model.Entities.Ground;

namespace Sportsmandu.Api.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class GroundsController : Controller
    {
        private AppDbContext _context;
        private IMapper _mapper { get; set; }
        private IFileAndDirectoryService _fileService;
        private IHostingEnvironment _hostingEnviroment;
        public GroundsController(AppDbContext _context, IMapper _mapper, IFileAndDirectoryService _fileService, IHostingEnvironment _hostingEnviroment)
        {
            this._hostingEnviroment = _hostingEnviroment;
            this._fileService = _fileService;
            this._context = _context;
            this._mapper = _mapper;
        }
        private void SetViewBagStuffs(int Culture)
        {
            ViewBag.Categories = GetCategories(Culture);
            ViewBag.Countries = GetCountries(Culture);
        }
        private IEnumerable<SelectListItem> GetCountries(int CultureId)
        {
            return _context.Countries.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();
        }
        int NoOfCulture = 2;
        public IActionResult ToBeTranslated(int culture)
        {
            SetViewBagStuffs(culture);
            var _mainResult = _context.Grounds
                .Include(x => x.ConvertedCulture)
                .Where(x => x.Delflag == false)
                .Where(x => x.ConvertedCulture.Count == NoOfCulture - 1)
                .Where(x => x.CultureId != culture)
                .Select(x => new { x.Name, x.Thumbnail ,x.Image, x.multilingualid })
                .OrderBy(x => x.Name).Take(20);
            var objToReturn = _mainResult.Adapt<List<GroundAdminViewModel>>();// (_mainResult.Take(20));
            foreach (var items in objToReturn)
            {
                items.Converted = false;
                if (items.Image == null || items.Image == "")
                {
                    items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                }
            }
            var resultT0send = objToReturn.Adapt<List<AdminViewModel>>();
            return View("Index", resultT0send);
        }
        public IActionResult Index(int culture)
        {
            SetViewBagStuffs(culture);
            var _result = _context.Grounds
            // .Include(x => x.Brand).Include(x => x.Category).Include(x => x.Parts)
            .Where(x => x.Delflag == false).ToList();
            List<int> _userKnownCulture = new List<int>();
            _userKnownCulture.Add(1);
            _userKnownCulture.Add(2);
            MultilingualAI MAI = GenericMultiLingualRepo(_mapper.Map<IEnumerable<MultiLingualViewModel>>(_result), _userKnownCulture, culture);
            var result = from item in _result
                         where !MAI.idToRemove.Contains(item.Id)
                         select item;
            var objToReturn = _mapper.Map<IEnumerable<GroundAdminViewModel>>(result);
            foreach (var items in objToReturn)
            {
                items.Converted = MAI.items.Where(x => x.Id == items.Id).Select(x => x.Converted).SingleOrDefault();
                if (items.Image == null || items.Image == "")
                {
                    items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                }
            }
            return View(objToReturn.Adapt<List<AdminViewModel>>());
            //return View(objToReturn);
        }
        public class GroundsearchViewModel
        {
            public int CultureId { get; set; }
            public int CategoryId { get; set; }
            public int BrandId { get; set; }
            public string Term { get; set; }
        }
        [HttpPost]
        public IActionResult Search([FromForm]GroundsearchViewModel psvm)
        {
            if (psvm.CultureId == 0) psvm.CultureId = 1;
            var _result = _context.Grounds
                    // .Include(x => x.Brand)
                    //.Include(x => x.Category)
                    // .Include(x => x.Parts)
                    .Where(x => x.Delflag == false);
            //if (psvm != null)
            {
                //if (psvm.CategoryId != 0)
                //{
                //    _result = _result.Where(x => x.CategoryId == psvm.CategoryId);                    
                //}               
            }
            SetViewBagStuffs(1);
            List<int> _userKnownCulture = new List<int>();
            _userKnownCulture.Add(1);
            _userKnownCulture.Add(2);
            MultilingualAI MAI = GenericMultiLingualRepo(_mapper.Map<IEnumerable<MultiLingualViewModel>>(_result), _userKnownCulture, psvm.CultureId);
            var result = from item in _result
                         where !MAI.idToRemove.Contains(item.Id)
                         select item;
            var objToReturn = _mapper.Map<IEnumerable<GroundAdminViewModel>>(result);
            foreach (var items in objToReturn)
            {
                items.Converted = MAI.items.Where(x => x.Id == items.Id).Select(x => x.Converted).SingleOrDefault();
                if (items.Image == null || items.Image == "")
                {
                    items.Image = "https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png";
                }
            }
            SetViewBagStuffs(1);
            return View("Index", objToReturn);
        }
        public ActionResult Create(int Culture)
        {
            //ViewData["Brand"] = new SelectList(_context.Brands.Where(x => x.CultureId == Culture).ToList().Select(x => new { x.multilingualid, x.Name }), "multilingualid", "Name");
            //ViewData["Category"] = new SelectList(_context.Categories.Where(x => x.CultureId == Culture).ToList().Select(x => new { x.multilingualid, x.Name }), "multilingualid", "Name");
            SetViewBagStuffs(Culture);
            GroundCreateViewModel p = new GroundCreateViewModel();
            return View(p);
        }
        public ActionResult Details(int Culture, int multilingualid)
        {
            return View();
        }
        [HttpGet]
        public ActionResult Report()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GroundCreateViewModel brand, IFormCollection formCollection)
        {
            Ground neededGround = new Ground();
            if(brand.MultiLingualId != 0)
            {
                neededGround = _context.Grounds.Where(x => x.Id == brand.MultiLingualId).SingleOrDefault();
                brand.CountryId = neededGround.CountryId;
            }
            if (ModelState.IsValid)
            {
                Ground o = _mapper.Map<Ground>(brand);
                _context.Entry(o).State = EntityState.Added;
                o.CreatedDate = DateTime.Now;
                //##TodO
                o.CreatedById = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault().Id;
                //o. = 1;
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Grounds", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        o.Image = _result.ArrayData.Where(x => x.Contains("Grounds/Image")).FirstOrDefault();
                        o.Thumbnail = _result.ArrayData.Where(x => x.Contains("Grounds/Thumbnail")).FirstOrDefault();
                        o.CoverImage = _result.ArrayData.Where(x => x.Contains("Grounds/CoverImage")).FirstOrDefault();
                    }
                }
                if (formCollection.Files.Count == 1 && o.multilingualid != 0)
                {
                    if(o.Image == null)
                    o.Image = neededGround.Image;
                    if (o.Thumbnail== null)
                        o.Thumbnail = neededGround.Thumbnail;
                    if (o.CoverImage== null)
                        o.CoverImage = neededGround.CoverImage;
                }               
                o.Guid = Global.UniqueGuid(this.ControllerContext.RouteData.Values["controller"].ToString(), _context);
                _context.Grounds.Add(o);
                _context.SaveChanges();                
                if (o.multilingualid == 0)
                {
                    //_context.Entry(o).State = EntityState.Modified;
                    o.multilingualid = o.Id;
                    _context.Grounds.Update(o);
                    _context.SaveChanges();
                }
               GroundConvertedCulture pc = new GroundConvertedCulture
               {
                    CultureId = o.CultureId.Value,
                    GroundId = o.multilingualid,
                };
                _context.GroundConvertedCulture.AddAsync(pc);
                _context.SaveChanges();
                return RedirectToAction("Index", "Grounds", new { area = "Admin", Culture = o.CultureId });
            }
            return View();
        }
        
        public ActionResult Edit(int Id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), Id, User.Claims)) return (Unauthorized());
            Ground Ground = _context.Grounds
            .Where(x => x.Id == Id).SingleOrDefault();
            SetViewBagStuffs(Ground.CultureId.Value);            
            return View(Ground);
        }
        private IEnumerable<SelectListItem> GetCategories(int CultureId)
        {
            return _context.Categories.Where(x => x.CultureId == CultureId).Select(x => new SelectListItem
            {
                Value = x.multilingualid.ToString(),
                Text = x.Name
            }).ToList();

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Ground Ground, IFormCollection formCollection)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), Ground.Id, User.Claims)) return (Unauthorized());
            if (ModelState.IsValid)
            {
                //_context.Entry(Ground).State = EntityState.Modified;
                var ground = _context.Grounds.Find(Ground.Id);
                if (formCollection.Files.Count > 0)
                {
                    var _result = _fileService.CreateFiles("Sportsmandu", "Grounds", formCollection, _hostingEnviroment, 1);
                    if (_result.Succeded)
                    {
                        bool hasImage = _result.ArrayData.Where(x => x.Contains("Grounds/Image")).FirstOrDefault() != null;
                        bool hasCoverImage = _result.ArrayData.Where(x => x.Contains("Grounds/CoverImage")).FirstOrDefault() != null;
                        bool hasThumbnail = _result.ArrayData.Where(x => x.Contains("Grounds/Thumbnail")).FirstOrDefault() != null;
                        if (hasImage)
                            Ground.Image = _result.ArrayData.Where(x => x.Contains("Grounds/Image")).FirstOrDefault();
                        if (hasThumbnail)
                            Ground.Thumbnail = _result.ArrayData.Where(x => x.Contains("Grounds/Thumbnail")).FirstOrDefault();
                        if (hasCoverImage)
                            Ground.CoverImage = _result.ArrayData.Where(x => x.Contains("Grounds/CoverImage")).FirstOrDefault();
                        // Ground.Image = _result.ArrayData.Where(x => x.Contains("Grounds/Image")).FirstOrDefault();
                        // Ground.Thumbnail = _result.ArrayData.Where(x => x.Contains("Grounds/Thumbnail")).FirstOrDefault();
                        // Ground.CoverImage = _result.ArrayData.Where(x => x.Contains("Grounds/CoverImage")).FirstOrDefault();
                    }
                }

                Ground.CreatedById = ground.CreatedById;
                Ground.CreatedDate = ground.CreatedDate;
                Ground.Guid = ground.Guid;
                _context.Entry(ground).CurrentValues.SetValues(Ground);
                Ground.CreatedById = _context.Users.Where(x => x.UserName == User.Identity.Name).SingleOrDefault().Id;
                _context.SaveChanges();
                return RedirectToAction("Index", "Grounds", new { area = "Admin", Culture = Ground.CultureId });
            }
            return View(Ground);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            Ground about = _context.Grounds.Where(x => x.Id == id).SingleOrDefault();
            _context.Entry(about).State = EntityState.Modified;
            about.Delflag = true;
            _context.SaveChanges();
            return RedirectToAction("Index", "Grounds", new { area = "Admin", Culture = about.CultureId });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}