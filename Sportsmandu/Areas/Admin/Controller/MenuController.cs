using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Sportsmandu.Data.Infrastructure;
using Sportsmandu.Model.Entities.Global;
using Sportsmandu.Models;

namespace Sportsmandu.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class MenuController : Controller
    {
        private AppDbContext _context;
        //private UserManager<ApplicationUser> _userManager;
        //private string pathToCreate = "upload/image/menu/";
        private IHostingEnvironment _hostingEnvironment;
        public MenuController(AppDbContext _context, IHostingEnvironment _hostingEnvironment)//UserManager<ApplicationUser> _userManager
        {
            this._hostingEnvironment = _hostingEnvironment;
            this._context = _context;
        }
        public IActionResult Index()
        {
            return View(_context.AdminMenu.ToList());
        }
        public IActionResult Details(int id)
        {
            AdminMenu adminmenu = _context.AdminMenu.Where(x => x.Id == id).SingleOrDefault();
            return View(adminmenu);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AdminMenu adminmenu, IFormFile ImageUrl)
        {
            if (ModelState.IsValid)
            {
                _context.Entry(adminmenu).State = EntityState.Added;
                if (ImageUrl != null)
                {
                    //adminmenu.ImageUrl = Global.SaveAndGetImageUrl(pathToCreate, ImageUrl, _hostingEnvironment);
                }
                _context.AdminMenu.Add(adminmenu);
                _context.SaveChanges();
                return RedirectToAction("Index", "Menu", new { area = "Admin", Culture = adminmenu.CultureId });
            }
            return View(adminmenu);
        }
        public ActionResult Edit(int? id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id.Value, User.Claims)) return (Unauthorized());
            AdminMenu adminmenu = _context.AdminMenu.Where(x => x.Id == id).SingleOrDefault();
            return View(adminmenu);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AdminMenu AdminMenu, IFormFile ImageUrl)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), AdminMenu.Id, User.Claims)) return (Unauthorized());
            if (ModelState.IsValid)
            {
                _context.Entry(AdminMenu).State = EntityState.Modified;
                if (ImageUrl != null)
                {
                    //adminmenu.ImageUrl = Global.SaveAndGetImageUrl(pathToCreate, ImageUrl, _hostingEnvironment);
                }
                _context.SaveChanges();

                return RedirectToAction("Index", "Menu", new { area = "Admin", culture = AdminMenu.Culture });
            }
            return View(AdminMenu);
        }
        public ActionResult Delete(int id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            AdminMenu adminmenu = _context.AdminMenu.Where(x => x.Id == id).SingleOrDefault();
            return View(adminmenu);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), id, User.Claims)) return (Unauthorized());
            AdminMenu about = _context.AdminMenu.Where(x => x.Id == id).SingleOrDefault();
            _context.Entry(about).State = EntityState.Modified;
            _context.SaveChanges();
            return RedirectToAction("Index", "Menu", new { area = "Admin", Culture = about.CultureId });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
