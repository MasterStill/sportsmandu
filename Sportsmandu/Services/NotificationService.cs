﻿using Sportsmandu.Data.Infrastructure.Repository.Abstract;
using Sportsmandu.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Model.Entities.Global;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using Sportsmandu.Models.Messaging.Notification;
using Sportsmandu.Data;

namespace Sportsmandu.Services
{
    public class NotificationService : INotificationService
    {
        static HttpClient client = new HttpClient();
        private IMapper Mapper { get; set; }
        //private IConnectionManager _connectionManager;
        private AppDbContext _context;        
        public string PostUrl = "https://onesignal.com/api/v1/notifications";
        public string OneSignalAuthToken = "Basic NjFiMzE1MzgtN2 Q1Ni00YzQ1LWIzYTAtNDBiZDg0NWNkZDNl";
        public string AppId = "ba74aad0-9409-4f33-bc1d-a96949e4ff6b";
        public NotificationService(AppDbContext _context)
        {
            this._context = _context;
            this.Mapper = Mapper;
        }
        public class OneSignal
        {
            public string app_id { get; set; }
            public string contents { get; set; }
            public string[] included_segments;
        }

        public void SendPushNotification(PushMessage p)
        {
            try
            {
                client.BaseAddress = new Uri(PostUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", OneSignalAuthToken);
            }
            catch
            {
            }
            string deviceIds = string.Empty;
            foreach (var items in p.DeviceIds)
            {
                deviceIds += "'" + items + "'";
            }
            if (p.DeviceIds.Count > 0) deviceIds = "[" + deviceIds.Replace("''", "','") + "]";

            string OnlyFew = string.Empty;
            if (p.MessageType == MessageType.All)
            {
                OnlyFew = "'included_segments':['All']";
            }
            else
            {
                OnlyFew = "'include_player_ids':" + deviceIds;
            }
            string Data = "'data':null";
            string DatatoREplace = string.Empty;
            if (p.Data.Count > 0)
            {
                foreach (var items in p.Data)
                {
                    DatatoREplace = "'" + items.Key + "':'" + items.Value + "'";
                }
                DatatoREplace = "{" + DatatoREplace.Replace("''", "','") + "}";
                Data = Data.Replace("null",DatatoREplace);
            }
            string json = "{'app_id':'"+ AppId +"','contents': {'en': '" + p.Message + "'}, " + OnlyFew + ",'headings':{'en':'" + p.Title + "'}," + Data + "}";
            string milekoData = json.Replace("'", "\"");
            Console.WriteLine(milekoData);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post,PostUrl);
            request.Content = new StringContent(milekoData,
                                    Encoding.UTF8,
                                    "application/json");
            var asdasdas = client.SendAsync(request)
             .ContinueWith(responseTask =>
             {
                 var lamokha = responseTask.Result;
             });
        }
    }
}

