﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Identity.UI.V3.Pages.Account.Internal;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Sportsmandu;
//using Sportsmandu.Api.Options;//TODO
using Sportsmandu.Models;
using Sportsmandu.Models.Account.AccountViewModels;
using Sportsmandu.Models.Messaging.Notification;
namespace WebApi.Services
{
    public interface IJWTUserService
    {
        GenericResult Authenticate(Sportsmandu.Models.AccountViewModels.LoginViewModel loginModel);        
    }
    public class JWTUserService : IJWTUserService
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        //private readonly AppSettings _appSettings;

        //public JWTUserService(IOptions<AppSettings> appSettings, SignInManager<ApplicationUser> signInManager)
        //{
        //    _signInManager = signInManager;
        //    _appSettings = appSettings.Value;
        //}//TODO

        public GenericResult Authenticate(Sportsmandu.Models.AccountViewModels.LoginViewModel l)
        {
            var result = _signInManager.PasswordSignInAsync(l.Email, l.Password, l.RememberMe, lockoutOnFailure: true);
            
            if (result.Result.Succeeded)
            {                    
            // authentication successful so generate jwt token
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("asdasd");//TODO//_appSettings.Secret);
                int NoofDays = 1;
                NoofDays = l.RememberMe == true ? 7 : NoofDays;
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name,"1")
                    }),
                    Expires = DateTime.UtcNow.AddDays(NoofDays),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                UserViewModel User = new UserViewModel();
                User.Token = tokenHandler.WriteToken(token);


                //try
                //{
                //    UserNotification a = new UserNotification();
                //    a.DeviceId = User.DeviceId;
                //    a.UserId = 
                //    _context.UserNotification.Add(a);
                //    _context.SaveChanges();

                //}
                //catch { }
                return new GenericResult
                {
                    Succeded = true,
                    Data = User
                };
            }
            else
            {
                return new GenericResult
                {
                    Succeded = false,
                    Message = "Error : Invalid Username / Password"
                };
                //return Global.AccessDenied(Global.Error(null, "Error : Invalid Username / Password"));
            }
        }       
    }
}