﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using System;
using Sportsmandu.Services.Abstract;
using Sportsmandu.Models;
using Community.Model.Entities.Global;
using Community.Services.Abstract;
using Community.Model.Entities.ViewModels;
using Sportsmandu.Data.Infrastructure.Repository.Abstract;

namespace Sportsmandu.Services
{
    public class MenuService : IMenuService
    {
        private readonly IMenuRepository _menuRepository;
        private IMapper Mapper { get; set; }
        private IUserService _userService;
        //private Global Global = new Global();
        public MenuService(IMapper Mapper, IMenuRepository _menuRepository, IUserService _userService)
        {
            this._userService = _userService;
            this._menuRepository = _menuRepository;
            this.Mapper = Mapper;
        }
        // All Menu For WebSite
        public IEnumerable<MenuViewModel> GetMenus(int UserId, int page, int pageSize, int StoreId, int CultureId)
        {
            IEnumerable<MenuViewModel> websites = Mapper.Map<IEnumerable<MenuViewModel>>(_menuRepository.GetAll()
            .Where(x => x.Delflag == false)
            .Where(x => x.CultureId == CultureId)
            );
            return websites.Skip(page * pageSize).Take(pageSize);
        }
        public List<MenuViewModel> GetMenus(string CultureName, string WebsiteName)
        {

            List<MenuViewModel> Menus = new List<MenuViewModel>();
            try
            {
                List<MenuViewModel> menus = Mapper.Map<List<MenuViewModel>>(_menuRepository
                    .AllIncluding( x => x.Culture)
                    .Where(x => x.Delflag == false)
                    .Where(x => x.Verified == true)                    
                    .Where(x => x.Culture.Title.ToLower() == CultureName.ToLower())
                    .OrderByDescending(x => x.Order)
                    );
                return menus;
            }
            catch
            {
                return Menus;
            }
        }
        public GenericResult DeleteMenu(int userId, int MenuId)
        {
            Menu _menutoDelete = _menuRepository.GetSingle(x => x.Id == MenuId);
            //if (!_userService.IsInRole(userId, _menutoDelete.StoreId, (new string[] { "Admin", "Owner" }))) return Global.AccessDenied();
            try
            {
                _menuRepository.Delete(_menutoDelete);
                _menuRepository.Commit();
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Success : Menu Deleted.",
                };
            }
            catch (Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "Error : Menu Deletion Failed." + ex.Message,
                };
            }
        }
        public GenericResult DeleteMenu(int UserId, int StoreId, string MenuTitle)
        {
            try
            {
                var _menuToRemove = SingleMenu(UserId, StoreId, MenuTitle);
                _menuRepository.Delete(_menuToRemove);
                _menuRepository.Commit();
                return Global.Success("Deleted");
            }
            catch (Exception ex)
            {
                return Global.Error(ex, "Cannot Delete Menu");
            }
        }
        public Menu SingleMenu(int UserId, int StoreId, string MenuTitle)
        {
            return _menuRepository.GetAll().Where(x => x.Name.ToLower() == MenuTitle.ToLower()).SingleOrDefault();
        }
        public GenericResult CreateMenu(int UserId, MenuCreateViewModel Menu)
        {
            Menu menu = Mapper.Map<Menu>(Menu);
            try
            {

                //if (!_userService.IsInRole(UserId, Menu.StoreId, (new string[] { "Admin", "Owner", "Editor", "Content Writer" }))) return Global.AccessDenied();
                menu.CreatedById = UserId;
                //menu.Verified = _userService.IsInRole(UserId, menu.CreatedById, (new string[] { "Admin", "Editor", "Owner" }));
                if (_menuRepository.ReturnIfExist(x => x.Name.ToLower() == menu.Name.ToLower() && x.Delflag == false) != null)
                {
                    return new GenericResult()
                    {
                        Succeded = false,
                        Message = "Error : Menu Already Exist.",
                    };
                }

            }
            catch (Exception ex)
            {
                return Global.Error(ex);
            }

            try

            {
                if (menu.Id > 0) goto EditMenu;
                _menuRepository.Add(menu);
                _menuRepository.Commit();
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Success : Menu Created.",
                    Data = menu.Id.ToString(),
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "Error : Menu could not be Created." + ex.Message
                };
            }
            EditMenu:
            try
            {
                var oldMenu = _menuRepository.GetSingle(x => x.Id == Menu.Id);
                var editMenu = Mapper.Map<Menu>(Menu);
                editMenu = Mapper.Map<MenuCreateViewModel, Menu>(Menu, oldMenu);
                _menuRepository.Update(editMenu);
                _menuRepository.Commit();
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Success : Menu Updated "
                };
            }
            catch (Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "Error : Menu could not be Updated." + ex.Message
                };
            }
        }
    }
}