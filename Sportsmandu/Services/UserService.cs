﻿using Community.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Sportsmandu.Models;
using Sportsmandu.Models.enums;
using Sportsmandu.Data.Infrastructure.Repository.Abstract;

namespace Community.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private IRoleRepository _roleReposistory;
        private IMapper Mapper { get; set; }
        public UserService(IMapper Mapper,IUserRepository _userRepository,IRoleRepository _roleReposistory)
        {
            this._roleReposistory = _roleReposistory;
            this._userRepository = _userRepository;
            this.Mapper = Mapper;
        }
        public ApplicationUser GetSingleByUsername(string username){
            return _userRepository.GetSingleByUsername(username);
        }
  
        public List<int> GetUserCulture(int UserId){
            return new List<int>();// return _userRepository.GetUserCulture(UserId);
        }
        public ApplicationUser GetSingleById(int UserId){
            return new ApplicationUser();
            //return _userRepository.GetAll().Where(x=>x.Id == UserId).SingleOrDefault();
        }

        //public IEnumerable<ApplicaitonRole> GetUserRoles(string username){
        //    return _userRepository.GetUserRoles(username);
        //}

        public bool IsCommunityAdmin(int UserId){
            //if(_userRepository.GetSingle(UserId).UserType == enumUserType.CommunityAdmin) return true;
            return false;            
        }
        public bool IsInRole(int UserID, enumClaimTypeSegments Segment, string SegmentValue)
        {
            //if (IsCommunityAdmin(UserID))return true;
            //IEnumerable<ApplicationRole> _roles = GetUserRoles(UserID,Website);
            //var role = _roles.Where(x=>x.Name.ToLower() == RoleName.ToLower());
            //if (role!=null){
            //    return true;
            //}
            return false;
        }

        public bool IsInRole(int UserID, int Website,string[] RoleName){
            //if (IsCommunityAdmin(UserID))return true;
            //IEnumerable<ApplicationUserRole> _roles = GetUserRoles(UserID,Website);
            //foreach (var roleToCheck in RoleName)
            //{
            //    var role = _roles.Where(x=>x.Name.ToLower() == roleToCheck.ToLower());
            //    if (role.Any()){
            //        return true;
            //    }    
            //}
            return false;
        }

        public IEnumerable<ApplicationRole> GetUserRoles(string username,int website){
           Console.WriteLine("Checking UserRole for Username : " + username + " For StoreId " + website);
            List<ApplicationRole> _roles = null;
            //ApplicationUser _user = _userRepository.GetSingle(u => u.Username == username, u => u.Roles);
            //if(_user != null)
            //{
            //    _roles = new List<ApplicationRole>();
            //    foreach (var _userRole in _user.Roles.Where(x=>x.StoreId == website))
            //        {
            //            _roles.Add(_roleReposistory.GetSingle(_userRole.RoleId));
            //        }
            //}
            return _roles;
        }
        public IEnumerable<ApplicationRole> GetUserRoles(int userId,int website){
            Console.WriteLine("Checking UserRole for UserID : " + userId + " For StoreId " + website);
            List<ApplicationRole> _roles = null;
            //ApplicationUser _user = _userRepository.GetSingle(u => u.Id == userId, u => u.Roles);
            //if(_user != null)
            //{
            //    _roles = new List<ApplicationRole>();
            //    foreach (var _userRole in _user.Roles.Where(x=>x.StoreId == website))
            //        {
            //            _roles.Add(_roleReposistory.GetSingle(_userRole.RoleId));
            //        }
            //}
            return _roles;
        }
        public int GetUserId(string username){
             //var User =  = _userRepository.GetAll().Where(x=>x.Username == username).Select(x=>x.Id).SingleOrDefault();
            var User = GetSingleByUsername(username);

             Console.WriteLine("Service bata ayeko userId " + User.Id + " For Username : " + username);
             return Convert.ToInt32(User.Id);
        }
    } 
}