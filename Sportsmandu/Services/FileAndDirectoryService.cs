﻿using Sportsmandu.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Community.Services.Abstract;
using Sportsmandu.Models;

namespace Sportsmandu.Services
{
    public class FileAndDirectoryService : IFileAndDirectoryService
    {
        private IUserService _userService;
        private IMapper Mapper { get; set; }                
        public FileAndDirectoryService(IMapper Mapper, IUserService _userService)
        {            
            this.Mapper = Mapper;            
            this._userService = _userService;
        }

        private string getStoreId()
        {
            return "";// _websiteService.GetStoreName(StoreId)
        }

        public FileAndDirectoryViewModel GetDirectoriesandFiles(int StoreId, string filetype, string path, IHostingEnvironment _hostingEnvironment, int UserId)
        {
            string website = getStoreId();
            return GetDirectoriesandFiles(website, filetype, path, _hostingEnvironment, UserId);
        }

        public FileAndDirectoryViewModel Search(int StoreId, string filetype, string path, string SearchTerm, IHostingEnvironment _hostingEnvironment, int UserId)
        {                       
            if (path.Trim().ToLower() == "default")
            {
                path = "";
            }
            string website = getStoreId();
            FileAndDirectoryViewModel fd = new FileAndDirectoryViewModel();
            List<string> Images = new List<string>();
            List<string> Files = new List<string>();
            List<string> Directories = new List<string>();

            path = "UserUploads/" + website.Replace(":", "") + "/" + path;
            string toBeReplacedPath = _hostingEnvironment.WebRootPath.Replace("\\", "/");
            var fdPath = Path.Combine(_hostingEnvironment.WebRootPath, path);
            fdPath = fdPath.Replace("\\", "/");
            try
            {
                foreach (string file in Directory.EnumerateFiles(fdPath, "*" + SearchTerm + "*", SearchOption.AllDirectories))
                {
                    if (!file.Contains("settings.ini"))
                        //Files.Add(file.Replace(toBeReplacedPath, "").Replace("\\", "/"));
                        if (file.Contains(".jpg") || file.Contains(".png") || file.Contains(".jpeg"))
                        {
                            Images.Add(string.Concat("http://" + "masterstill.com", file.Replace(toBeReplacedPath, "").Replace("\\", "/")));
                            
                        }
                        else
                        {
                            Files.Add(string.Concat("http://" + "masterstill.com", file.Replace(toBeReplacedPath, "").Replace("\\", "/")));
                        }
                }
            }
            catch
            {

            }
            fd.Images = Images;
            fd.Files = Files;
            return fd;
        }
        public FileAndDirectoryViewModel GetDirectoriesandFiles(string website, string filetype, string path, IHostingEnvironment _hostingEnvironment, int UserId)
        {
            if (path.Trim().ToLower() == "default")
            {
                path = "";
            }
            try
            {
                CreateFolder(website, path, _hostingEnvironment, UserId);
            }
            catch { }
            FileAndDirectoryViewModel fd = new FileAndDirectoryViewModel();
            List<string> Files = new List<string>();
            List<string> Directories = new List<string>();
            if (website == null) return fd;

            path = "UserUploads/" + website + "/" + path;
            string toBeReplacedPath = _hostingEnvironment.WebRootPath.Replace("\\", "/");
            var fdPath = Path.Combine(_hostingEnvironment.WebRootPath, path);
            fdPath = fdPath.Replace("\\", "/");
            try
            {
                foreach (string file in Directory.EnumerateFiles(fdPath, "*", SearchOption.TopDirectoryOnly))
                {
                    if (!file.Contains("settings.ini"))
                        //Files.Add(file.Replace(toBeReplacedPath, "").Replace("\\", "/"));
                        Files.Add(string.Concat("http://" + "masterstill.com", file.Replace(toBeReplacedPath, "").Replace("\\", "/")));
                }
            }
            catch { }
            try
            {
                foreach (string directory in Directory.EnumerateDirectories(fdPath, "*", SearchOption.TopDirectoryOnly))
                {
                    //Directories.Add(directory);
                    Directories.Add(directory.Replace(toBeReplacedPath, "").Replace("\\", "/"));
                }
            }
            catch { }
            fd.Files = Files;
            fd.Directories = Directories;
            return fd;
        }
        public GenericResult CreateFolder(string website, string path, IHostingEnvironment _hostingEnvironment, int UserId)
        {
            try
            {
                //if (!_userService.IsInRole(UserId, getStoreId(), (new string[] { "Admin", "Owner","Editor" }))) return Global.AccessDenied();
                path = "UserUploads/" + website + "/" + path;
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, path);
                Directory.CreateDirectory(uploads);
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Directory Created."
                };
            }
            catch (Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "Error : Directory Could Not Be Created." + ex.Message
                };
            }
        }
        public GenericResult DeleteContent(int UserId, int StoreId, string FileName)
        {
            if (!_userService.IsInRole(UserId, StoreId, (new string[] { "Admin", "Owner", "Editor" }))) return Global.AccessDenied();
            try
            {
                //var _photoAlbumResults = _photoAlbumService.CheckIfImageIsUsed(FileName);
                //if (_photoAlbumResults.Any())
                //{
                //    return Global.Error(null, "Image Cannot Be Deleted Because Its Used in Photo Album ", _photoAlbumResults.FirstOrDefault().AlbumId.ToString());
                //}
                return Global.Success("Ensured it can be deleted now will code to delete from filestruction");
            }
            catch (Exception ex)
            {
                return Global.Error(ex);
            }
        }
        public GenericResult CreateFiles(string website, string path, IFormCollection formCollection, IHostingEnvironment _hostingEnvironment, int UserId,string subPath = null,string fileName = null)
        {
            //if (!_userService.IsInRole(UserId,_websiteService.GetStoreId(website), (new string[] { "Admin", "Owner","Editor","Content Writer"}))) return Global.AccessDenied();            
            try
            {                
                if (formCollection.Files.Count == 0)
                {
                    return new GenericResult()
                    {
                        Succeded = false,
                        Message = "No Files Selected !!."
                    };
                }
                // if (_websiteService.spaceExceeded(website) == true)
                // {
                //     return new GenericResult()
                //     {
                //         Succeeded = false,
                //         Message = "Error : Space Exceeded. Please contact Administrator or write open a Support Ticket"
                //     };
                // }
                List<string>SavedPath = new List<string>();
                string PathToSend = string.Empty; 
                string PredefinedFileName = string.Empty;
                //if (path.ToLower().Contains("users")){
                     if(website.ToLower().Contains("sportsmandu")){
                        if(subPath != null)
                        {
                            PathToSend = "Images/"  + path + "/" + subPath;
                        }
                        else
                        {
                            PathToSend = "Images/" + path;
                        }
                     }
                     else{
                        PathToSend = "UserUploads/" + UserId + "/" + path;
                     }
                //}
                //else{
                //PathToSend =  "Users";
                if(fileName!= null)
                {
                    PredefinedFileName = fileName.Replace(".","@");
                }
                //}

                foreach (var file in formCollection.Files)
                {
                    //SavedPath.Add("http://localhost:5000/" + Global.SaveAndGetImage(file, PathToSend, _hostingEnvironment,PredefinedFileName));
                    SavedPath.Add(Global.SaveAndGetImage(file, PathToSend + "/" + file.Name, _hostingEnvironment,PredefinedFileName));
                }
                //string[] sendSavedPaths = SavedPath.ToArray();
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Files Uploaded.",
                    ArrayData = SavedPath.ToArray()              
                };
            }
            catch (Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "Error : Files Could Not Be Uploaded ." + ex.Message
                };
            }
        }
        public GenericResult CreateFiles(int StoreId, string path, IFormCollection formCollection, IHostingEnvironment _hostingEnvironment, int UserId,string subPath = null)
        {
            string website = getStoreId();
            return CreateFiles(website, path, formCollection, _hostingEnvironment, UserId,subPath); 
        }
    }
}