﻿using Community.Services.Abstract;
using AutoMapper;
using Sportsmandu.Models;
using Sportsmandu;
using Sportsmandu.Data.Infrastructure.Repository.Abstract;
using Sportsmandu.Data;
namespace Community.Services
{
    public class UserRoleService : IUserRoleService  
    {
        private readonly IUserRoleRepository _userRoleRepository;
        private IMapper Mapper { get; set; }
        private IUserService _userService;
        AppDbContext _context;
        public UserRoleService(IMapper Mapper,IUserRoleRepository _userRoleRepository,IUserService _userService, AppDbContext _context)
        {
            this._context = _context;
            this._userService = _userService;
            this._userRoleRepository = _userRoleRepository;
            this.Mapper = Mapper;
        }

        //Create Role
        public GenericResult CreateUserRole(int UserId ,ApplicationUserRole userRole)
        {
//            if (!Authorization.Authorized(this.ControllerContext.RouteData.Values["controller"].ToString().Replace("Controller", ""), this.ControllerContext.RouteData.Values["action"].ToString(), Id, User.Claims)) return (Unauthorized());
            if (DuplicateRole(userRole)){
                 return new GenericResult()
                    {
                        Succeded = false,
                        Message = "Error : UserRole Already Exist.",
                    };
                 }
            try
            {
                _context.UserRoles.Add(userRole);
                _context.SaveChanges();
                //_userRoleRepository.Add(userRole);
                //_userRoleRepository.Commit();
                 return new GenericResult()
                {
                    Succeded = true,
                    Message = "Success : " + "UserRole" + " Created.",
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "Error : UserRole could not be Created." + ex.Message 
                };   
            }
        }
        public bool DuplicateRole(ApplicationUserRole bc){
            //  int category = _userRoleRepository.GetAll().Where(x=>x.CreatedById == bc.CreatedById).Where(x=>x.Name.ToString().ToLower() == bc.Name.ToLower()).Where(x=>x.Delflag == false).Count();  
            //  if (category > 0 ){
            //      return true; 
            //  }
            return false;
        }
    }
}