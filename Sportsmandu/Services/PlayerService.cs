﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Newtonsoft.Json;
using Community.Model.Entities.Blog;
using Microsoft.EntityFrameworkCore;
using Community.Model.Entities.ViewModels;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Models;
using Sportsmandu.Models.Globalization;
using Sportsmandu.Models.Entities.Global;
using Sportsmandu.Models.Blog;
using Sportsmandu.Services.Abstract;
using Sportsmandu.Data;
using Sportsmandu.Models.Entities.Blog;
using static Sportsmandu.MultiLingualGlobal;
using Sportsmandu.Data.Infrastructure.Repository.Abstract;
using Community.Services.Abstract;
using Sportsmandu.Model.Entities.ViewModels;
using Mapster;
namespace Sportsmandu.Services
{
    public class PlayerService : IPlayerService
    {        
        private IMapper Mapper { get; set; }
        private IUserService _userService;
        private AppDbContext _context;        
        public PlayerService(IMapper Mapper,IUserService _userService, AppDbContext _context)
        {                        
            this.Mapper = Mapper;
            this._userService = _userService;
            this._context = _context;
        }
        public PlayerViewModel GetPlayer(int Id){
            return _context.Players.Where(x=>x.Id == Id).SingleOrDefault().Adapt<PlayerViewModel>();
        }
        public bool DuplicatePlayerId(string Id)
        {
            int category = _context.Players.SingleOrDefault().Id;                            
            if (category > 0)
            {
                return true;
            }
            return false;
        }
    }
}