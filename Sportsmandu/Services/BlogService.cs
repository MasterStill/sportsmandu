﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Newtonsoft.Json;
using Community.Model.Entities.Blog;
using Microsoft.EntityFrameworkCore;
using Community.Model.Entities.ViewModels;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Models;
using Sportsmandu.Models.Globalization;
using Sportsmandu.Models.Entities.Global;
using Sportsmandu.Models.Blog;
using Sportsmandu.Services.Abstract;
using Sportsmandu.Data;
using Sportsmandu.Models.Entities.Blog;
using static Sportsmandu.MultiLingualGlobal;
using Sportsmandu.Data.Infrastructure.Repository.Abstract;
using Community.Services.Abstract;
using Sportsmandu.Model.Entities.ViewModels;
using Mapster;

namespace Sportsmandu.Services
{
    public class BlogService : IBlogService
    {
        private readonly IBlogRepository _blogRepository;
        private IMapper Mapper { get; set; }
        private IUserService _userService;        
        //private IBlogTagRepository _blogTagRepository;
        //        private readonly IBlogCategoryRepository _categoryRepository;
        private AppDbContext _context;
        //public BlogService(IMapper Mapper, IBlogRepository _blogRepository, IBlogTagRepository _blogTagRepository, IUserService _userService, IBlogCategoryRepository _categoryRepository,  AppDbContext _context)
            public BlogService(IMapper Mapper, IBlogRepository _blogRepository, IUserService _userService, AppDbContext _context)
        {            
          //  this._categoryRepository = _categoryRepository;
            this._blogRepository = _blogRepository;
            //this._blogTagRepository = _blogTagRepository;
            this.Mapper = Mapper;
            this._userService = _userService;
            this._context = _context;
        }

        public void SeedBlogPost(int StoreId, int UserId)
        {
            int intCultureEnglish = 1;
            BlogCategoryCreateViewModel bcvm = new BlogCategoryCreateViewModel();
            bcvm.Title = "General";
            //bcvm.StoreId = StoreId;
            bcvm.CultureId = intCultureEnglish;
            bcvm.Type = BlogType.Blog;
            GenericResult _newCategory = CreateCategory(UserId, bcvm);
            //need to workaround for below 1 line
            int intblogCategoryId = GetCategories(UserId, 0, 10, StoreId, intCultureEnglish, BlogType.Blog).Select(x => x.Id).SingleOrDefault();
            BlogPostCreateViewModel bpcvm = new BlogPostCreateViewModel();
            bpcvm.CategoryId = intblogCategoryId;
            bpcvm.CommentAllowed = false;
            bpcvm.CultureId = intCultureEnglish;
            bpcvm.PublishDate = DateTime.Now;
            bpcvm.Description = "Welcome Content English";
            bpcvm.Summary = "Welcome Summary English";
            bpcvm.Title = "Welcome to Community CMS";
            bpcvm.Image = "http://sermonspiceuploads.s3.amazonaws.com/3265/fp_69207/stagelightswelcomehd_full.jpg";
            bpcvm.Thumbnail = "http://amnplify.com.au/wp-content/uploads/2015/01/Cute_Fall_Welcome_CMN-HD.jpg";
            bpcvm.Type = BlogType.Blog;
            //bpcvm.StoreId = StoreId;
            BlogPostCreateViewModel bpcvm1 = new BlogPostCreateViewModel();
            bpcvm1.CategoryId = intblogCategoryId;
            bpcvm1.CommentAllowed = true;
            bpcvm1.CultureId = intCultureEnglish;
            bpcvm1.PublishDate = DateTime.Now;
            bpcvm1.Summary = "Welcome Summary English";
            bpcvm1.Description = "About Site Constent";
            bpcvm1.Title = "Know How's About Site";
            bpcvm1.Image = "http://www.microiways.com/How_To/images/howto.jpg";
            bpcvm1.Thumbnail= "http://cdn2.pcadvisor.co.uk/cmsdata/features/3572640/How-to-start-a-blog.jpg";
            bpcvm1.Type = BlogType.Blog;
            //bpcvm1.StoreId = StoreId;
            BlogPostCreateViewModel bpcvm2 = new BlogPostCreateViewModel();
            bpcvm2.CategoryId = intblogCategoryId;
            bpcvm2.CommentAllowed = true;
            bpcvm2.CultureId = intCultureEnglish;
            bpcvm2.PublishDate = DateTime.Now;
            bpcvm2.Description = "Monetization Contents Here";
            bpcvm2.Summary = "Amazing Summary of Things That Can Be Acheived including Monetization";
            bpcvm2.Title = "Amaazing Things To Do Now !";
            bpcvm2.Image = "http://tiffanyteeworld.com/wp-content/uploads/2016/06/Awesome-People-Up-to-Amazing-Things-800x675.jpg";
            bpcvm2.Thumbnail = "http://images6.fanpop.com/image/photos/36900000/Amazing-Things-quotes-36902984-500-334.jpg";
            bpcvm2.Type = BlogType.Blog;
            //bpcvm2.StoreId = StoreId;
            CreateBlogPost(UserId, bpcvm2);
            CreateBlogPost(UserId, bpcvm1);
            CreateBlogPost(UserId, bpcvm);
        }
        public IEnumerable<TestViewModel> GetBlogPostsForWebsite(string WebsiteName, string CultureCode, int page, int pageSize, BlogType Type)
        {
            var result = _blogRepository.AllIncluding(x => x.Culture, x => x.Category)
                .Where(x => x.Culture.Code.ToLower() == CultureCode.ToLower())
                .Where(x => x.Delflag == false)
                .Where(x => x.Verified == true)
                //.Where(x => x.BlogType == Type)
                .OrderByDescending(x => x.CreatedDate)
                .Skip(page * pageSize).Take(pageSize).ToList();
            var objToReturn = Mapper.Map<IEnumerable<TestViewModel>>(result);
            return objToReturn;
        }
        public List<BlogAdminViewModel> GetUnverifiedPosts()
        {
            var result = _blogRepository
                .AllIncluding(x => x.Culture, x => x.Category)
                .Where(x => x.Delflag == false)
                .Where(x => x.Verified == false);                        
            var objToReturn = result.Adapt<List<BlogAdminViewModel>>();            
            return objToReturn;
        }
        public List<BlogAdminViewModel> VerifyPosts(int UserId,int PostId)
        {
            var blogPost = _blogRepository
                .FindBy(x => x.Id == PostId).SingleOrDefault();
            blogPost.Verified = true;
            _blogRepository.Update(blogPost);
            _blogRepository.Commit();
            var result = _blogRepository
               .AllIncluding(x => x.Culture, x => x.Category)
               .Where(x => x.Delflag == false)
               .Where(x => x.Verified == false);
            var objToReturn = result.Adapt<List<BlogAdminViewModel>>();
            return objToReturn;
        }
        // All BlogPost by Admin End
        public List<AdminViewModel> GetBlogPosts(int UserId, int page, int pageSize, int StoreId, int CultureId, BlogType Type)
        {
            var result = _blogRepository
                .AllIncluding(x => x.Culture, x => x.Category)
                .Where(x => x.Delflag == false)
                .Where(x=>x.CultureId == CultureId)
                //.Where(x => x.BlogType == Type)
                .OrderByDescending(x=>x.PublishDate)
                .Skip(page * pageSize).Take(pageSize);          
            return result.Adapt<List<AdminViewModel>>();
        }       
        public GenericResult DeleteBlog(int BlogId, int UserId)
        {            
            BlogPost blogPost = _blogRepository.GetSingle(u => u.Id == BlogId);//, u => u.Roles);
            try
            {
                //if (!_userService.IsInRole(UserId, (new string[] { "Admin", "Owner" }))) return Global.AccessDenied();
                _blogRepository.Delete(blogPost);
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Success : Blog Deleted.",
                };
            }
            catch
            {
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Success : Blog Deletion Failed.",
                };
            }
        }
        public BlogPostSingleViewModel GetBlogPost(int BlogId, string Culture)
        {
            BlogPostSingleViewModel blogPost = new BlogPostSingleViewModel();
            var blogpost = _context.BlogPosts
               .Include(x => x.Tags)
               .ThenInclude(v => v.Tag)
               .Include(x => x.Category)
               .Include(x => x.Culture)
               //.Include(x => x.Website)
               .Where(x => x.multilingualid == BlogId)
               .Where(x => x.Delflag == false);

            try
            {
                var multilingualBlogPost = blogpost.Where(x => x.Culture.Title.ToLower() == Culture.ToLower()).SingleOrDefault();
                if (multilingualBlogPost == null)
                {

                    blogPost = Mapper.Map<BlogPostSingleViewModel>(blogpost.FirstOrDefault());
                }
                else
                {
                    blogPost = Mapper.Map<BlogPostSingleViewModel>(multilingualBlogPost);
                    foreach (var items in multilingualBlogPost.Tags)
                    {
                        blogPost.Tag = items.Tag;
                    }

                    blogPost.Translated = true;
                }
            }
            catch (Exception ex)
            {
                Global.Error(ex);
            }
            //if(blogpo)
            //blogPost.BlogTags = (Tags.ToArray());
            return blogPost;
        }

        public BlogPostVMFORES GetBlogPost(string BlogId)
        {
            //if (BlogId == 0) return new BlogPostCreateViewModel();
            var blogpost = _blogRepository.AllIncluding(x => x.Tags, x => x.Category)
            .Where(x => x.Guid == BlogId)
                .Where(x => x.Delflag == false).SingleOrDefault();
            BlogPostVMFORES blogPost = blogpost.Adapt<BlogPostVMFORES>();
            //List<string> Tags = new List<string>();
            //if (blogpost.Tags != null)
            //{
            //    foreach (var items in blogpost.Tags)
            //    {
            //        Tags.Add(_context.BlogTags.Where(x => x.Id == items.TagId).Select(x => x.Name).SingleOrDefault());
            //    }
            //    //blogPost.Tag = (Tags.ToArray());
            //}
            return blogPost;
        }

        public BlogPostCreateViewModel GetBlogPost(int BlogId)
        {
            //if (BlogId == 0) return new BlogPostCreateViewModel();
            var blogpost = _blogRepository.AllIncluding(x => x.Tags, x => x.Category)
            .Where(x => x.Id == BlogId)
                .Where(x => x.Delflag == false).SingleOrDefault();
            BlogPostCreateViewModel blogPost = blogpost.Adapt<BlogPostCreateViewModel>();
            //List<string> Tags = new List<string>();
            //if (blogpost.Tags != null)
            //{
            //    foreach (var items in blogpost.Tags)
            //    {
            //        Tags.Add(_context.BlogTags.Where(x => x.Id == items.TagId).Select(x => x.Name).SingleOrDefault());
            //    }
            //    //blogPost.Tag = (Tags.ToArray());
            //}
            return blogPost;
        }
        //All BlogPost By Category
        public IEnumerable<BlogPostViewModel> GetBlogPostsForCategory(int UserId, int CategoryId, int page, int pageSize, int StoreId)
        {
            IEnumerable<BlogPostViewModel> websites = Mapper.Map<IEnumerable<BlogPostViewModel>>(_blogRepository
                //.AllIncluding(x => x.CreatedBy)
                .AllIncluding()
                .Where(x => x.Delflag == false)
                .Where(x => x.CategoryId == CategoryId));
            return websites.Skip(page * pageSize).Take(pageSize);
        }

        public IEnumerable<TestViewModel> GetBlogPostsForCategory(string Culture, string Category, int page, BlogType Type)
        {
            Category = Category.Replace("_", " ");
            int pageSize = 10;
            var _result = _blogRepository
                .AllIncluding(x => x.Category, x => x.Culture)
                .Where(x => x.Delflag == false)
                .Where(x => x.Category.Title.ToLower() == Category.ToLower())                
                .Where(x => x.BlogType.ToString().ToLower() == Type.ToString().ToLower())
                .Where(x => x.Culture.Title.ToLower() == Culture.ToLower())
                .OrderByDescending(x => x.PublishDate)
                .ToList();
            IEnumerable<BlogPostViewModel> blogPostWithCategory = Mapper.Map<IEnumerable<BlogPostViewModel>>(_result);
            //  IEnumerable<TestViewModel> bptvm ;            
            return Mapper.Map<IEnumerable<TestViewModel>>(blogPostWithCategory.Skip(page * pageSize).Take(pageSize));
        }

        public PaginationSet<BlogPost> GetPagedBlogPostForTags(string Website, string Culture, string Tags, int page, BlogType Type)
        {
            Tags = Tags.Replace("_", " ");            
            var _result = _context.BlogPosts
                .Where(x => x.BlogType == Type)
                .Where(x => x.Delflag == false)
                .Where(x => x.Culture.Title == Culture)
                .Include(x => x.Tags)
                .ThenInclude(y => y.Tag)
                .Include(x => x.Category)
                .Include(x => x.Culture)
                .ToList();
            List<int> blogToSend = new List<int>();
            foreach (var blog in _result)
            {
                foreach (var items in blog.Tags)
                {
                    if (items.Tag.Name == Tags)
                    {
                        blogToSend.Add(blog.Id);
                    }
                }
            }
            var _finalResult = from item in _result
                               where blogToSend.Contains(item.Id)
                               select item;
            IEnumerable<BlogPostViewModel> blogPostWithCategory = Mapper.Map<IEnumerable<BlogPostViewModel>>(_finalResult);
            PaginationSet<BlogPost> pbpvm = new PaginationSet<BlogPost>();
            //pbpvm.Count = blogPostWithCategory.Count();
            pbpvm.Items = _finalResult;
            pbpvm.Page = page;    
            return pbpvm;
        }

        public IEnumerable<BlogPostViewModel> GetBlogPostsForTags(string Website, string Culture, string Tags, int page, BlogType Type)
        {
            return Mapper.Map<IEnumerable<BlogPostViewModel>>(GetPagedBlogPostForTags(Website, Culture, Tags, page, Type).Items);                        
        }        
        public List<BlogPostBlogTag> SaveTags(string[] Tags, int UserId, int CultureId, bool Verified, int? blogId = 0)
        {
            try
            {
                if (blogId != 0)
                {
                    var _tagstoRemove = _context.BlogPostBlogTag.Where(x => x.PostId == blogId);
                    _context.BlogPostBlogTag.RemoveRange(_tagstoRemove);
                    _context.SaveChanges();
                }
            }
            catch { }

            List<BlogPostBlogTag> bpbt = new List<BlogPostBlogTag>();
            foreach (var tag in Tags)
            {
                var dbResultforDuplication = _context.BlogTags.Where(x=>x.Name == tag).SingleOrDefault();
                if (dbResultforDuplication == null)
                {
                    BlogTag bt = new BlogTag();
                    bt.Name = tag;
                    bt.CreatedById = UserId;//NewblogPost.CreatedById;
                    bt.CultureId = CultureId;
                    bt.Verified = Verified;
                    _context.BlogTags.Add(bt);
                    _context.SaveChanges();

                    BlogPostBlogTag blogpostblogtag = new BlogPostBlogTag();
                    blogpostblogtag.Tag = bt;
                    if (blogId > 0)
                    {
                        blogpostblogtag.PostId = blogId.Value;
                    }
                    bpbt.Add(blogpostblogtag);
                }
                else
                {
                    BlogPostBlogTag blogpostblogtag = new BlogPostBlogTag();
                    blogpostblogtag.Tag = dbResultforDuplication;
                    if (blogId > 0)
                    {
                        blogpostblogtag.PostId = blogId.Value;
                    }
                    bpbt.Add(blogpostblogtag);
                }
            }
            if (blogId > 0)
            {
                _context.BlogPostBlogTag.AddRange(bpbt);
                _context.SaveChanges();
            }
            return bpbt;
        }

        public List<SiteMap> SiteMapBlogTags(string WebsiteName)
        {
            var _result = _context.BlogPostBlogTag
                        .Include(x => x.Post)
                        //.ThenInclude(y => y.Website)
                        .Include(x => x.Tag)
                        //.Where(x => x.Post.Website.Name.Contains(WebsiteName))
                        //.Where(x => x.Post.Website.Construction == false)
                        //.Where(x => x.Post.Website.Name == Website)
                        //.Where(x => x.Post.BlogType == Type)
                        ;
            List<SiteMap> tags = new List<SiteMap>();
            foreach (var items in _result)
            {
                SiteMap btvm = new SiteMap();
                btvm.URL = "http://" + "Sportsmandu.com" + "/" + items.Tag.URL;
                btvm.WebsiteName = "Sportsmandu.com";
                tags.Add(btvm);
            }

            return tags;
        }
        public List<SiteMap> SiteMapBlogCategory(string WebsiteName)
        {
            List<SiteMap> _siteMap = new List<SiteMap>();
            //var _blogCategory = new object();

            var _blogCategory = _context.BlogCategory
                                .Include(x => x.Culture)
                                //.Include(x => x.Website)
                                //.Where(x => x.Website.Construction == false)
                                .Where(x => x.Verified == true)
                                .Where(x => x.Delflag == false)
                                //.Where(x => x.Website.Name.Contains(WebsiteName))
                                .ToList()
                                .Select(x => new { x.Culture.Title, x.URL });


            foreach (var items in _blogCategory.OrderBy(x => x.Title))
            {
                SiteMap sm = new SiteMap();
                sm.WebsiteName = items.Title;
                sm.URL = items.URL;
                _siteMap.Add(sm);
            }
            return _siteMap;
        }
        public List<SiteMap> SiteMapBlogPost(string WebsiteName)
        {
            List<SiteMap> _siteMap = new List<SiteMap>();
            var _blogsUrl = _context.BlogPosts
                    .Include(x => x.Culture)
                    .Include(x => x.Category)
                    .Where(x => x.Delflag == false)
                    .Where(x => x.Verified == true)
                    .Where(x => x.Title != "Amaazing Things To Do Now !")
                    .Where(x => x.Title != "Know How's About Site")
                    .Where(x => x.Title != "Welcome to Community CMS")
                    .ToList()
                    .Select(x => new { x.BlogType, x.URL })
                    ;

            //.Select(x=>new {x.Website.Name , x.Url});
            foreach (var items in _blogsUrl.OrderBy(x => x.BlogType))
            {
                SiteMap sm = new SiteMap();
                sm.WebsiteName = items.BlogType.ToString();
                sm.URL = items.URL;
                _siteMap.Add(sm);
            }
            return _siteMap;
        }
        public GenericResult CreateBlogPost(int UserId, BlogPostCreateViewModel blogPost)
        {
            if (blogPost.Id > 0)
            { // Updated Blog Post
                BlogPost oldBlog = _blogRepository.GetAll().Where(x => x.Id == blogPost.Id).SingleOrDefault();
                blogPost.CategoryId = oldBlog.CategoryId;
                BlogPost newBlog = Mapper.Map<BlogPost>(oldBlog);
                newBlog = Mapper.Map<BlogPostCreateViewModel, BlogPost>(blogPost, newBlog);
                newBlog.BlogType = blogPost.Type;
                if(newBlog.Tags.Count>0)
                newBlog.Tags = SaveTags(blogPost.Tag, UserId, blogPost.CultureId, newBlog.Verified, newBlog.Id);
                //newBlog = Mapper.Map<BlogPostCreateViewModel, BlogPost>(blogPost);
                //if (!_userService.IsInRole(UserId, blogPost.StoreId, (new string[] { "Admin", "Editor", "Owner" }))) return Global.AccessDenied();
                try
                {
                    try
                    {
                        //_auditService.SaveAuditData(oldBlog.Id, UserId, oldBlog, newBlog);
                    }
                    catch (Exception ex)
                    {
                        Global.Error(ex);
                        Console.WriteLine("Audit Data Could Not Be Proceeed !!");
                        // return new GenericResult()
                        // {
                        //     Succeeded = false,
                        //     Message = "Error : Audit Data Could Not Be Processed" + ex.Message,
                        // };
                    }
                    _blogRepository.Update(newBlog);
                    _blogRepository.Commit();
                    return new GenericResult()
                    {
                        Succeded = true,
                        Message = "Success : " + blogPost.Type + " Updated.",
                    };
                }
                catch (Exception ex)
                {
                    return new GenericResult()
                    {
                        Succeded = false,
                        Message = "Error : " + blogPost.Type + " Couldnot Be Updated." + ex.Message,
                    };
                }
            }

            BlogPost NewblogPost = Mapper.Map<BlogPost>(blogPost);
            try
            {
                //if (!_userService.IsInRole(UserId, blogPost.StoreId, (new string[] { "Admin", "Owner", "Editor", "Content Writer" }))) return Global.AccessDenied();                
                NewblogPost.BlogType = blogPost.Type;
                NewblogPost.CreatedById = UserId;
                NewblogPost.Guid = Global.UniqueGuid("BlogPosts", _context);
                //NewblogPost.Verified = _userService.IsInRole(UserId, blogPost.StoreId, (new string[] { "Admin", "Editor", "Owner" }));
                List<BlogPostBlogTag> bpbt = new List<BlogPostBlogTag>();
                if (blogPost.Tag != null)
                {                    
                    NewblogPost.Tags = SaveTags(blogPost.Tag, UserId, blogPost.CultureId, NewblogPost.Verified);
                }
                _context.Entry(NewblogPost).State = EntityState.Added;
                _context.BlogPosts.Add(NewblogPost);
                _context.SaveChanges();
                if (NewblogPost.CultureId == 1) NewblogPost.multilingualid = NewblogPost.Id;
                _context.Entry(NewblogPost).State = EntityState.Modified;
                _context.SaveChanges();
                BlogConvertedCulture pc = new BlogConvertedCulture
                {
                    CultureId = NewblogPost.CultureId.Value,
                    BlogId = NewblogPost.multilingualid,
                    Blog = null,
                    Culture = null
                };
                _context.BlogConvertedCulture.Add(pc);
                _context.SaveChanges();
                //_blogRepository.Add(NewblogPost);
                //_blogRepository.Commit();
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Success : " + blogPost.Type + " Created.",
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "Error : " + blogPost.Type + " could not be Created." + ex.Message
                };
            }
        }
        public List<BlogPostViewModel> CheckIfImageIsUsed(string ImagePath)
        {
            var _result = _blogRepository.AllIncluding(x => x.Culture, x => x.Category)
                    .Where(x => x.Image == ImagePath || x.Thumbnail == ImagePath);
            List<BlogPostViewModel> blogPost = Mapper.Map<List<BlogPostViewModel>>(_result);
            return blogPost;
        }
        public IEnumerable<BlogCategoryViewModel> GetCategories(int UserId, int page, int pageSize, int StoreId, int cultureId, BlogType Type)
        {
            IEnumerable<BlogCategoryViewModel> websites = Mapper.Map<IEnumerable<BlogCategoryViewModel>>(_context.BlogCategory
                .Include(x => x.Culture)
                .Where(x => x.Delflag == false)
                .Where(x => x.CultureId == cultureId)
                .Where(x => x.Type == Type)
                //.Where(x => x.StoreId == StoreId)
                );
            return websites.Skip(page * pageSize).Take(pageSize);            
        }
        public IEnumerable<BlogCategoryViewModel> GetCategoriesForWebsite(string Website, string Culture, int Pagesize, BlogType type)
        {
            IEnumerable<BlogCategoryViewModel> websites = Mapper.Map<IEnumerable<BlogCategoryViewModel>>(_context.BlogCategory
                .Include(x => x.Culture)
                .Where(x => x.Delflag == false)                
                //.Where(x => x.Type == type)
                //.Where(x => x.Culture.Title == Culture)
                );
            return websites.Take(Pagesize);
        }
        public GenericResult CreateCategory(int UserId, BlogCategoryCreateViewModel BlogCategory)
        {
            if (BlogCategory.Title.Trim() == null)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "Error : " + BlogCategory.Type + " Category Cannot Be Empty",
                };
            }
            BlogCategory blogCategory = Mapper.Map<BlogCategory>(BlogCategory);
            blogCategory.CreatedById = UserId;
            if (DuplicateCategory(blogCategory))
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "Error : " + blogCategory.Type + " " + blogCategory.Title + " Category Already Exist.",
                };
            }
            try
            {
                _context.BlogCategory.Add(blogCategory);
                _context.SaveChanges();                
                BlogCategoryViewModel returnedCategory = Mapper.Map<BlogCategoryViewModel>(_context.BlogCategory
                            .Include(x => x.Culture)
                            .Where(x => x.Id == blogCategory.Id)
                            .SingleOrDefault());
                return new GenericResult()
                {
                    Succeded = true,
                    Message = "Success : " + blogCategory.Type + " " + blogCategory.Title + "  Category Created.",
                    Data = JsonConvert.SerializeObject(returnedCategory),
                };
            }
            catch (System.Exception ex)
            {
                return new GenericResult()
                {
                    Succeded = false,
                    Message = "Error : " + blogCategory.Type + " " + blogCategory.Title + " Category could not be Created." + ex.Message
                };
            }
        }
        public List<BlogTagViewModel> GetTagsForWebsite(string Website, string Culture, int Pagesize, BlogType Type)
        {
            var _result = _context.BlogPostBlogTag
                 .Include(x => x.Post)
                 .Include(x => x.Tag)
                 .Where(x => x.Post.BlogType == Type);
            List<BlogTagViewModel> tags = new List<BlogTagViewModel>();
            foreach (var items in _result)
            {
                BlogTagViewModel btvm = new BlogTagViewModel();

                btvm.Url = "http://" + Website + "/" + items.Tag.URL;

                btvm.Name = items.Tag.Name;
                tags.Add(btvm);
            }
            return tags;
        }
        public bool DuplicateCategory(BlogCategory bc)
        {
            int category = _context.BlogCategory.Include(x => x.Culture).Where(x => x.Title.ToString()
                                 .ToLower() == bc.Title.ToLower())
                            .Where(x => x.Delflag == false)
                            .Where(x => x.CultureId == bc.CultureId)
                            .Where(x => x.Type == bc.Type).Count();
            if (category > 0)
            {
                return true;
            }
            return false;
        }
    }
}