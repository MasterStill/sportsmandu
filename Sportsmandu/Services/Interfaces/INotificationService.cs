﻿using Sportsmandu.Models.Messaging.Notification;

namespace Sportsmandu.Services.Abstract
{
    public interface INotificationService
    {
        void SendPushNotification(PushMessage Messgae);
    }
}