﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Sportsmandu.Models;

namespace Sportsmandu.Services.Abstract
{
    public interface IFileAndDirectoryService
    {
      GenericResult CreateFolder(string website,string path,IHostingEnvironment _hostingEnvironment,int UserId);
      GenericResult CreateFiles(int StoreId,string path,IFormCollection formCollection,IHostingEnvironment _hostingEnvironment,int UserId,string subPath=null);
      GenericResult CreateFiles(string websiteName,string path,IFormCollection formCollection,IHostingEnvironment _hostingEnvironment,int UserId,string subPath = null,string fileName = null);
      FileAndDirectoryViewModel GetDirectoriesandFiles(string website,string filetype,string Path,IHostingEnvironment _hostingEnvironment,int UserId);   
      FileAndDirectoryViewModel GetDirectoriesandFiles(int StoreId,string filetype,string Path,IHostingEnvironment _hostingEnvironment,int UserId);
      FileAndDirectoryViewModel Search(int StoreId, string filetype, string path,string SearchTerm, IHostingEnvironment _hostingEnvironment,int UserId);
      GenericResult DeleteContent(int UserId,int StoreId,string FileName);
    }
}