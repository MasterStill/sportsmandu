﻿using System.Collections.Generic;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Models;
namespace Sportsmandu.Services.Abstract
{
    public interface ICultureService
    {
        List<Culture> GetCultures();
        int GetCultureId (string CultureName);
        List<CultureViewModel> GetCultures(int CultureId);
        List<CultureViewModel> GetCultures(int StoreId,int CultureId);
        GenericResult CreateCulture(int UserId,Culture culture);
        List<CultureViewModel> GetCulturesInCultureLanguage();
    }
}  