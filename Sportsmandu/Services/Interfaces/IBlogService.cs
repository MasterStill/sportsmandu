﻿using Community.Model.Entities.ViewModels;
using System.Collections.Generic;
using Community.Model.Entities.Blog;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Models;
using Sportsmandu.Models.Blog;
using Sportsmandu.Model.Entities.ViewModels;

namespace Sportsmandu.Services.Abstract
{
    public interface IBlogService
    {
        List<AdminViewModel> GetBlogPosts(int UserId,int page, int pageSize,int StoreId,int CultureId,BlogType type);
        List<BlogAdminViewModel> GetUnverifiedPosts();
        List<BlogAdminViewModel> VerifyPosts(int Userid,int Id);
        IEnumerable<TestViewModel> GetBlogPostsForWebsite(string WebsiteName,string Culture,int page, int pageSize,BlogType Type);
        // BlogPostSingleViewModel GetBlogPost(int BlogId,int CultureId); 
        BlogPostVMFORES GetBlogPost(string BlogId);
        BlogPostCreateViewModel GetBlogPost(int BlogId);
        BlogPostSingleViewModel GetBlogPost(int BlogId,string Culture);

        IEnumerable<BlogPostViewModel> GetBlogPostsForCategory(int UserId,int CategoryId,int page, int pageSize,int StoreId);
        IEnumerable<TestViewModel> GetBlogPostsForCategory(string Culture,string Category,int page,BlogType type);
         IEnumerable<BlogPostViewModel> GetBlogPostsForTags(string Website,string Culture,string Tags,int page,BlogType Type);
        GenericResult CreateBlogPost(int UserId, BlogPostCreateViewModel BlogPost);
        GenericResult DeleteBlog(int BlogId,int UserId);

        IEnumerable<BlogCategoryViewModel> GetCategories(int UserId,int page, int pageSize,int StoreId,int cultureId,BlogType type);
        IEnumerable<BlogCategoryViewModel> GetCategoriesForWebsite(string Website,string Culture,int Pagesize,BlogType type);
        List<BlogTagViewModel> GetTagsForWebsite(string Website,string Culture,int Pagesize,BlogType Type);
        GenericResult CreateCategory(int UserId, BlogCategoryCreateViewModel BlogCategory);
        List<BlogPostViewModel> CheckIfImageIsUsed(string ImagePath);
         List<SiteMap> SiteMapBlogTags(string SiteName ="");
        List<SiteMap> SiteMapBlogCategory(string SiteName ="");
        List<SiteMap> SiteMapBlogPost(string SiteName ="");
        void SeedBlogPost(int StoreId, int UserId);
    }
}