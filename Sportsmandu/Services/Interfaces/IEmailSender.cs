﻿using System.Threading.Tasks;
namespace Sportsmandu.Services
{
    public interface IEmailSender
    {
        void SendEmailAsync(string email, string subject, string message, string FromAddress = "noreply@Sportsmandu.com", string FromAdressTitle = "Sportsmandu Customer Service");
    }
}
