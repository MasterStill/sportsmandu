﻿using Community.Model.Entities.Global;
using Community.Model.Entities.ViewModels;
using Sportsmandu.Models;
using System.Collections.Generic;
namespace Sportsmandu.Services.Abstract
{
    public interface IMenuService
    {
        IEnumerable<MenuViewModel> GetMenus(int UserId, int page, int pageSize, int Website, int CultureId);
        List<MenuViewModel> GetMenus(string CultureName, string WebsiteName);
        GenericResult CreateMenu(int UserId, MenuCreateViewModel menuCreateViewModel);
        GenericResult DeleteMenu(int UserId, int MenuId);
        GenericResult DeleteMenu(int UserId, int StoreId, string MenuTitle);
        Menu SingleMenu(int UserId, int StoreId, string MenuTitle);
    }
}