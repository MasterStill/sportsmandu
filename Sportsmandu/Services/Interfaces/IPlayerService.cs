﻿using Community.Model.Entities.ViewModels;
using System.Collections.Generic;
using Community.Model.Entities.Blog;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Models;
using Sportsmandu.Models.Blog;
using Sportsmandu.Model.Entities.ViewModels;
namespace Sportsmandu.Services.Abstract
{
    public interface IPlayerService
    {
        PlayerViewModel GetPlayer(int PlayerId);
    }
}