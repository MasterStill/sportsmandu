﻿using Sportsmandu.Models;
namespace Community.Services.Abstract
{
    public interface IUserRoleService
    {
        GenericResult CreateUserRole(int UserId,ApplicationUserRole userrole);
    }
}
