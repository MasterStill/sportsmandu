﻿using Community.Model.Entities.Global;
using System.Collections.Generic;
using Community.Model.Entities;
using Sportsmandu.Models;

namespace Community.Services.Abstract
{
    public interface IUserService
    {
        ApplicationUser GetSingleByUsername(string username);
        ApplicationUser GetSingleById(int UserId);
        //IEnumerable<ApplicationRole> GetUserRoles(string username);
        IEnumerable<ApplicationRole> GetUserRoles(string username,int StoreId);
        IEnumerable<ApplicationRole> GetUserRoles(int UserID,int StoreId);
        List<int> GetUserCulture(int UserId);
        bool IsInRole(int UserID, enumClaimTypeSegments Segment,string SegmentValue);
        bool IsInRole(int UserID, int Website,string[] RoleName);
        int GetUserId(string username);
    }
}