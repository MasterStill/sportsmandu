﻿var sportsmanduApp = new Vue({
    el: '#sportsAdmin',
    data: {
        seasonGroup: [],
        //baseUrl: 'https://sportsmandu.com/',
        baseUrl: 'https://localhost:44373/',
        connected: false,
        stats: [],
        sportsmanduYoutube: '',
        playerGameStatsFields: [], // Player ko stats >> ADMIN >> PLAYER
        seasonMatch: [], // Seasonmatch >> MAtch
        clubCategory: [], // Clubs >> Edit ko lagi
        seasonMatchTeamIndexEdit: null,
        seasonMatchIndexEdit: null, // Seasonmatch >> Edit ko lagi
        YoutubeDetails: [],        
        loading: false,
        seasonPartner: [],
        playerRoles: [],// Used to store all PlayerRoles
        seasonPartnerEditIndex: null,
        allPartnerTitles: [], // Season >> Edit ko lagi
        allClubs: [], // Season >> Edit ko lagi
        clubs: [], // Season >> Edit ko lagi
        categories: [],
        allCategories: [], // Season >> Edit ko lagi
        selectedCategory: null, // Clubs ma matra use bhayeko cha
        selectedCategory1: null, // Clubs ma matra use bhayeko cha ///TODO RemoveAbove because it holds only ID
        categoryIndexEdit: null,
        seasonCategoryIndexEdit: null,
        galleryIndexEdit: null,
        clubsIndexEdit: null, // Season >> Edit ko lagi
        currentSeason: null, // Season >> Edit ko lagi // Season ID ho yo
        allPlayers: [], // Clubs >> Edit ko lagi
        players: [], // Clubs >> Edit ko lagi
        clubPlayers: [], // Clubs >> Edit ko lagi
        allEmployees: [],
        selectedEmployee:null,
        employeeSearch:'',
        clubEmployees: [], // Clubs >> Edit ko lagi
        allEmployeeDesignation: [], // Clubs >> Edit ko lagi
        clubEmployeesIndexEdit: null,// Clubs >> Edit ko lagi
        seasonClubPlayers: [], // Clubs >> Edit ko lagi
        selectedSeason: null,  // Clubs >> Edit ko lagi
        seasonClubPlayerIndexEdit: null,// Clubs >> Edit ko lagi
        clubCategoryIndexEdit: null,// Clubs >> Edit ko lagi
        clubSeasonFixturePlayerIndexEdit: null,// Clubs >> Fixtures Edit ko lagi
        selectedPlayer: null, // Clubs >> Edit ko lagi
        playersIndexEdit: null, // Clubs >> Edit ko lagi
        seasonGroupIndexEdit: null,
        currentClub: null, // Clubs >> Edit ko lagi  // Club Id ho yo
        seasonGround: [],
        seasonGroundEditIndex: null,
        allGrounds: [],
        myGallery: [],
        selectedGallery: null,
        myGalleryPhotos: [],
        route: ['basic'],
        mainRoute: ['index'],
        socialIndexEdit: null,
        social: [],
        allSocial: [
            {
                id: 1,
                name: "Facebook"
            }, {
                id: 2,
                name: "Youtube"
            }
        ],
        currentSeasonMatchForStats: [],
        enumFixtureStatus: [
            {
                name: 'Not_Started'
            }, {
                name: 'Started'
            }, {
                name: 'Completed'
            }
        ],
        enumTeamFixtureStatus: [
            {
                name: 'Not_Started'
            },
            {
                name: 'Won'
            },
            {
                name: 'Lost'
            },
            {
                name: 'Draw'
            }
        ],
        enumSeasonFixtureRound: [
            {
                name: 'General'
            }, {
                name: 'Quater_Final'
            }, {
                name: 'Semi_Final'
            }, {
                name: 'KnockOut'
            }, {
                name: 'Final'
            }, {
                name: 'For_Third_Place'
            }
        ],
        enumSeasonMatchType: [
            {
                name: 'Default'
            },
            {
                name: 'Male'
            },
            {
                name: 'Female'
            }
        ],
        seasonFixturesPlayersOfClub: [],
        selectedSeasonFixture: {
            guid: null,
            category: null
        }
        // showseasonClubPlayers: false // Club edit ko seasons ko lagi
    },
    computed: {
    },
    destroyed: function () { },
    methods: {
        onSearchEmployeeName: function () {
            var currentSearchTerm = this.employeeSearch;
            if (currentSearchTerm.length > 3) {
                axios.get(this.baseUrl + 'Api/Employee/AllEmployee/' + currentSearchTerm)
                    .then(function (response) {
                        console.log("Loading Employees", response.data);
                        sportsmanduApp.allEmployees = response.data;
                        sportsmanduApp.loading = false;
                    })
                    .catch(function (error) {
                        console.log("onSearchEmployeeName() Error: " + error);
                    });
            }
        },
        initDropZone: function () {
            var segment = window.location.href.toLowerCase().split('admin/')[1].split("/")[0];
            var id = window.location.href.toLowerCase().split('/')[window.location.href.toLowerCase().split('/').length - 1];
            console.log("INit DropZoneeee !!");
            setTimeout(function () {
                $("div#myDropZone").dropzone({ url: "/Api/SaveGalleryImages/" + segment + "/" + id  +"/" + sportsmanduApp.selectedGallery.id });
            },1000);
            //var myDropzone = new Dropzone("div#myDropZone", { url: "/file/post" });
        },
        addGallery: function () {
            console.log("Adding new Gallery");
            //var segment = "Grounds";
            var segment = window.location.href.toLowerCase().split('admin/')[1].split("/")[0];
            var segmentId = window.location.href.toLowerCase().split('/')[window.location.href.toLowerCase().split('/').length - 1];
            sportsmanduApp.myGallery.unshift({                
                id:null,
                guid: null,
                name: 'Untitled Gallery',
                segment: segment,
                segmentId: segmentId
            });
            sportsmanduApp.galleryIndexEdit = 0;
        },
        saveGallery: function (gallery) {
            var dataToSend = gallery;
            console.log("SAVING gallery ", dataToSend);
            var fullLink = this.baseUrl + 'Api/SaveGallery/';
            axios({
                method: 'post',
                url: fullLink,
                data: dataToSend
            })
                .then(function (response) {
                    if (!response.data.succeded) {
                        toastr.error(response.data.message);
                    } else {
                        toastr.success(response.data.message);                        
                        console.log("saveGallery", response.data);
                        console.log("this.galleryIndexEdit", this.galleryIndexEdit);
                        sportsmanduApp.myGallery[sportsmanduApp.galleryIndexEdit].id = parseInt(response.data.data);
                        sportsmanduApp.galleryIndexEdit = null;
                    }
                })
                .catch(function (error) {
                });
        },
        deleteGallery: function (id) {      
            if (confirm("Are you sure you want to Delete")) {
                axios.get(this.baseUrl + 'Api/DeleteGallery/' + id)
                    .then(function (response) {
                        toastr.success(response.data.message);
                        console.log("Gallery Deleted", response.data);
                        loadGallery();
                    })
                    .catch(function (error) {
                        console.log("loadAllPlayers() Error: " + error);
                    });   
            } else {
                return;
            }            
        },
        deleteGalleryPhotos: function (id) {
            if (confirm("Are you sure you want to Delete")) {
            axios.get(this.baseUrl + 'Api/deleteGalleryPhotos/' + id)
                .then(function (response) {
                    toastr.success(response.data.message);
                    console.log("Gallery Photo Deleted", response.data);                    
                })
                .catch(function (error) {
                    console.log("loadAllPlayers() Error: " + error);
                    });
            } else {
                return;
            }       
        },
        loadGallery: function () {
            sportsmanduApp.loading = true;
            var segment = window.location.href.toLowerCase().split('admin/')[1].split("/")[0];
            var id = window.location.href.toLowerCase().split('/')[window.location.href.toLowerCase().split('/').length - 1];
            var Param = segment;
            var ParamId = id;
            console.log("Loading " + Param);
            axios.get(this.baseUrl + 'Api/Gallery/' + Param + '/' + ParamId)
                .then(function (response) {
                    console.log("Loading All Galleries", response.data);
                    sportsmanduApp.myGallery = response.data;
                    sportsmanduApp.loading = false;
                })
                .catch(function (error) {
                    console.log("loadAllPlayers() Error: " + error);
                });
        },
        loadGalleryImages: function () {
            axios.get(this.baseUrl + 'Api/Gallery/' + this.selectedGallery.id)
                .then(function (response) {
                    console.log("Loading All Galleries", response.data);
                    sportsmanduApp.myGalleryPhotos = response.data;
                    sportsmanduApp.loading = false;
                })
                .catch(function (error) {
                    console.log("loadGalleryImages() Error: " + error);
                });
        },
        saveGalleryImages: function () {
            sportsmanduApp.loading = true;
            var segment = window.location.href.toLowerCase().split('admin/')[1].split("/")[0];
            var id = window.location.href.toLowerCase().split('/')[window.location.href.toLowerCase().split('/').length - 1];
            var Param = segment;
            var ParamId = id;
            console.log("Loading " + Param);
            var datatoSend = [];
            var fullLink = this.baseUrl + 'Api/SaveGalleryImages/' + Param + '/' + ParamId;
            axios({
                method: 'post',
                url: fullLink,
                data: dataToSend
            })
                .then(function (response) {
                    if (!response.data.succeded) {
                        toastr.error(response.data.message);
                    } else {
                        toastr.success(response.data.message);
                    }
                })
                .catch(function (error) {
                });
        },
        onTeamFixtureStatusChange: function (team, currentSeasonMatchForStats) {
            var dataToSend = {
                SeasonId: currentSeasonMatchForStats.seasonId,
                ClubId: team.id,
                Guid: currentSeasonMatchForStats.guid,
                Status: team.status
            };
            console.log("SAVING TeamFixtureStatusChange Stat: ", dataToSend);
            var fullLink = this.baseUrl + 'Api/TeamFixtureStatusChange/';
            axios({
                method: 'post',
                url: fullLink,
                data: dataToSend
            })
                .then(function (response) {
                    if (!response.data.succeded) {
                        toastr.error(response.data.message);
                    } else {
                        toastr.success(response.data.message);
                    }
                })
                .catch(function (error) {
                });
        },
        filteredSeasonFixtureClubPlayers: function () { // Used in clubs >> edit >> fixtures            
            console.log("Filtering Items", sportsmanduApp.selectedSeasonFixture);
            var club = this.seasonClubPlayers.filter(function (item) {
                return item.categoryName === sportsmanduApp.selectedSeasonFixture.category;
            });
            console.log("Found Season Club Players !", club);
            return club;
        },
        addNewSocial: function () {
            var location = window.location.href.toLowerCase();
            var type = "";
            if (location.indexOf("clubs") !== -1) {
                type = "Clubs";
            } else if (location.indexOf("players") !== -1) {
                type = "Players";
            } else if (location.indexOf("partners") !== -1) {
                type = "Partners";
            } else if (location.indexOf("leagues") !== -1) {
                type = "Leagues";
            } else if (location.indexOf("association") !== -1) {
                type = "Association";
            } else if (location.indexOf("grounds") !== -1) {
                type = "Grounds";
            }
            var currentId = window.location.href.split("Edit/")[1].replace("#", "");
            console.log("Adding New Social for ", type);
            this.social.unshift(
                {
                    social: 1,
                    link: "",
                    id: currentId,
                    type: type
                });
            this.socialIndexEdit = 0;
        },
        addNewPlayer: function () {
            this.loadAllPlayers();
            console.log("Adding New Player!");
            this
                .players
                .unshift({ playerId: "", name: "", joinedDate: null, resignedDate: null, shirtNumber: "", clubId: this.currentClub });
            this.playersIndexEdit = 0;
            setTimeout(() => {
                jQuery('.datetimepicker').datetimepicker();
                $(".chosen-select").chosen();
            }, 2000);
        }, addNewClubEmployee: function () {
            console.log("Adding New Club Employee!");
            this
                .clubEmployees
                .unshift({ id: "", name: "", joinedDate: '', resignedDate: '' });
            this.clubEmployeesIndexEdit = 0;
            setTimeout(() => {
                jQuery('.datetimepicker').datetimepicker();
                $(".chosen-select").chosen();
            }, 2000);
        },
        addNewClubCategory: function () {
            console.log("Adding New addNewClubCategory!");
            this
                .clubCategory
                .unshift({ id: "", name: "", image: '', clubId: this.currentClub });
            this.clubCategoryIndexEdit = 0;
        }, addNewClubFixturePlayer: function () {
            console.log("Adding New addNewClubFixturePlayer!");
            this
                .seasonFixturesPlayersOfClub
                .unshift(
                    {
                        playerId: '',
                        clubId: this.currentClub,
                        seasonFixtureGuid: this.selectedSeasonFixture.guid
                    });
            this.clubSeasonFixturePlayerIndexEdit = 0;
        },
        addNewSeasonGround: function () {
            console.log("Adding New Ground!");
            this.seasonGround
                .unshift({
                    id: "",
                    ground: [],
                    seasonId: 3031
                });
            sportsmanduApp.loadAllGrounds();
            this.seasonGroundEditIndex = 0;
        },
        addNewClub: function () {
            console.log("Adding New Club!");
            this
                .clubs
                .unshift({ clubId: "", name: "", seasonId: this.currentSeason });
            this.clubsIndexEdit = 0;
            sportsmanduApp.loadAllClubs();

        },
        addNewMatch: function () {
            console.log("Adding New Match!");
            this
                .seasonMatch
                .unshift({
                    date: Date(),
                    seasonId: this.currentSeason,
                    ground: [],
                    category: [],
                    teams: [{
                        id: null
                    }]
                });
            this.seasonMatchIndexEdit = 0;
            setTimeout(() => {
                jQuery('.datetimepicker').datetimepicker();
            }, 3000);
        },
        addNewSeasonMatchTeam: function (teamId) {
            var thisTranIndex = sportsmanduApp.seasonMatch.map(function (match) {
                return match.id;
            }).indexOf(teamId);
            var neededSeason = sportsmanduApp.seasonMatch[thisTranIndex];
            neededSeason.teams.unshift({
                id: "",
                name: null,
                players: []
            });
            //alert("This Match Fixture" + teamId);
        },
        addNewSeasonClubPlayer: function (seasonId) {
            sportsmanduApp.seasonClubPlayers.unshift({
                id: null,
                name: null
            });
            this.seasonClubPlayerIndexEdit = 0;
        },
        // addNewSeasonMatch: function () {            
        //     console.log("Adding New Teams");
        //     this
        //         .seasonMatchTeam
        //         .unshift({
        //             teams: [],                                        
        //             category: [],
        //         });
        //     this.seasonMatchIndexEdit = 0;
        //     setTimeout(() => {
        //         jQuery('.datetimepicker').datetimepicker();                
        //     }, 3000);
        // }
        addNewSeasonPartner: function () {
            console.log("Adding New Partner to Season!");
            this
                .seasonPartner
                .unshift({
                    id: "",
                    seasonId: this.currentSeason,
                    partnerTitle: [],
                    partner: [],
                });
            sportsmanduApp.loadAllPartners();
            sportsmanduApp.loadAllPartnerTitles();
            this.seasonPartnerEditIndex = 0;
        },
        addNewCategory: function () {
            console.log("addNewCategory!!!!");
            this
                .categories
                .unshift({ categoryId: "", name: "", seasonId: this.currentSeason });
            this.categoryIndexEdit = 0; //TODO to filter below three lines
            this.clubCategoryIndexEdit = 0;
            this.seasonCategoryIndexEdit = 0;
            sportsmanduApp.loadAllCategories();
        },
        addNewSeasonGroup: function () {
            console.log("Adding addNewSeasonGroup!!!!");
            this
                .seasonGroup
                .unshift({
                    seasonId: this.currentSeason,
                    categoryId: "",
                    clubId: "",
                    groupName: ""
                });
            this.seasonGroupIndexEdit = 0;
        },
        savePlayers: function () {
            loading = true;
            console.log("Saving Player !");
            //alert("saving");
            SaveClubPlayer(this.players);
            setTimeout(() => {
                sportsmanduApp.loadClubPlayers();
            }, 2000);
        },
        saveSocial: function (social) {
            loading = true;
            console.log("Saving Social!", social);
            SaveSocial(social);
        },
        saveEmployee: function (employee) {
            if (sportsmanduApp.selectedEmployee === null) {                
                var segment = window.location.href.toLowerCase().split('admin/')[1].split("/")[0];
                var segmentId = window.location.href.toLowerCase().split('/')[window.location.href.toLowerCase().split('/').length - 1].replace("#","");
                console.log(segment, segmentId);
                var dataToSend = {
                    Name: sportsmanduApp.employeeSearch,
                    DesignationId: employee.designationId,
                    SegmentId: segmentId,
                    EmployeeId: null,
                    Segment: segment
                };
                //var dataToSend = sportsmanduApp.currentSeasonMatchForStats;
                console.log("SAVING saveEmployee", dataToSend);
                var fullLink = this.baseUrl + 'Api/SaveEmployee/';
                axios({
                    method: 'post',
                    url: fullLink,
                    data: dataToSend
                })
                    .then(function (response) {
                        if (!response.data.succeded) {
                            toastr.error(response.data.message);
                        } else {
                            toastr.success(response.data.message);
                        }
                    })
                    .catch(function (error) {
                    });
                }
        },
        saveClubFixturePlayer: function (player, state) {
            loading = true;
            console.log("Saving Season Fixture Club Players !");
            SaveClubFixturePlayer(player, state);
        },
        saveClubCategory: function () {
            loading = true;
            console.log("Saving Category !");
            //alert("saving");
            SaveClubCategory(this.clubCategory);
            setTimeout(() => {
                sportsmanduApp.loadClubCategories();
            }, 2000);
            sportsmanduApp.clubCategoryIndexEdit = null;
        },
        saveSeasonClubPlayer: function (seasonClubPlayer, currentState) { // Club >> EDit 
            loading = true;
            SaveSeasonClubPlayer(seasonClubPlayer, currentState);
        },
        saveSeasonMatch: function () {
            console.log("Saving Season Match");
            SaveSeasonMatch(this.seasonMatch[this.seasonMatchIndexEdit]);;
        },
        saveSeasonMatchTeam: function () {
            console.log("Saving Season Match Teams");
            SaveSeasonMatchTeam(this.seasonMatch[this.seasonMatchIndexEdit].teams[this.seasonMatchTeamIndexEdit]);;
        },
        saveSeasonPartner: function () {
            console.log("Saving Season Partner");
            SaveSeasonPartner(this.seasonPartner[this.seasonPartnerEditIndex]);
        },
        saveSeasonGround: function () {
            console.log("Saving Season Ground");
            SaveSeasonGround(this.seasonGround[this.seasonGroundEditIndex]);
        },
        saveSeasonGroup: function () {
            console.log("Saving Season Group");
            SaveSeasonGroup(this.seasonGroup[this.seasonGroupIndexEdit]);
        },
        deleteSeasonGround: function (testyy) {
            if (confirm('Are you sure you want to Delete' + testyy.name + " From the Match")) {
                console.log("Deleting Season Ground");
                console.log(testyy);
                DeleteSeasonGround(testyy);;
            } else {
                return;
            }
        },
        deleteClubPlayer: function (testyy) {
            console.log("Deleting Club Player");
            console.log(testyy);
            if (confirm('Are you sure you want to Delete' + testyy.name + " From the Match")) {
                DeleteClubPlayer(testyy);
            } else {
                return;
            }
        },
        deleteEmployee: function (testyy) {
            console.log("Deleting Employee");
            console.log(testyy);
            var segment = window.location.href.toLowerCase().split('admin/')[1].split("/")[0];
            var segmentId = window.location.href.toLowerCase().split('/')[window.location.href.toLowerCase().split('/').length - 1];
            if (confirm('Are you sure you want to Delete ' + testyy.name + " from List")) {
                axios.get(this.baseUrl + 'Api/DeleteEmployee/' + segment + "/" + segmentId + "/" + testyy.id)
                    .then(function (response) {
                        console.log("Deleting Employee Result : ", response.data);
                        sportsmanduApp.loading = false;
                    })
                    .catch(function (error) {
                        console.log("Deleting Employee() Error: " + error);
                    });
            } else {
                return;
            }
        },
        deletePartnerFromSeason: function (testyy) {
            if (confirm('Are you sure you want to Delete' + testyy.name + " From the Match")) {
                DeletePartnerFromSeason(testyy);
            } else {
                return;
            }
        },
        deleteClubCategory: function (testyy) {
            console.log("Deleting Club Category");
            console.log(testyy);
            if (confirm("Are you sure you want to Delete")) {
                // Save it!
            } else {
                return;
            }
            DeleteClubCategory(testyy);;
        },
        deleteSeasonFixture: function (testyy) {
            DeleteSeasonFixture(testyy);
        },
        deleteSeasonFixtureTeam: function (testyy, match) {
            console.log("Deleting Season Fixture Team");
            console.log("Team : ", testyy);
            console.log("Match : ", match);
            if (confirm('Are you sure you want to Delete' + testyy.name + " From the Match")) {
                DeleteSeasonFixtureTeam(testyy, match);;
            } else {
                return;
            }
        },
        saveClubs: function () {
            console.log("Saving Clubs !");
            SaveSeasonClub(this.clubs);
            setTimeout(() => {
                sportsmanduApp.loadSeasonClubs();
            }, 2000);
        },
        saveCategories: function () {
            console.log("Saving categories !", this.categories);
            SaveSeasonCategory(this.categories);
            setTimeout(() => {
                sportsmanduApp.loadSeasonCategories();
            }, 2000);
        },
        getMomentTime: function (date) {
            // return date;
            return moment(date).format('ll')
            //return moment(date, "YYYY-MM-DDTHH:mm:ss").format("dddd, MMMM Do YYYY, h:mm:ss a");
        },
        checkifSeasonClubPlayer: function (id) {
            // console.log("Checking : ", id);
            id = parseInt(id);
            // console.log("this.seasonClubPlayers" ,this.seasonClubPlayers);
            var player = this.seasonClubPlayers.filter(function (items) {
                // console.log(items.id);
                return items.id === id;
            })[0];
            // console.log("SeasonClubPlayer :" , player);
            if (player !== null && player !== undefined) {
                // console.log("Returning True");
                return true;
            } else {
                return false;
            }
        },
        checkifSeasonFixturePlayer: function (id) {
            console.log("Checking : ", id);
            id = parseInt(id);
            console.log("this.seasonClubPlayers", this.seasonFixturesPlayersOfClub);
            var player = this.seasonFixturesPlayersOfClub.filter(function (items) {
                console.log(items.playerId);
                return items.playerId === id;
            })[0];
            console.log("SeasonClubPlayer :", player);
            if (player !== null && player !== undefined) {
                console.log("Returning True");
                return true;
            } else {
                return false;
            }
        },
        //getSocialName: function (social) {
        //    alert(social);
        //    var sociala = this.allSocial.filter(function (item) {
        //        return item.id === id;
        //    })[0];
        //    return sociala.name;
        //},        
        getPlayerImage: function (id) {
            //alert(id);
            var player = this.allPlayers.filter(function (item) {
                return item.id === id;
            });
            if (player[0] !== null && player[0] !== undefined)
                return player[0].image !== null ? player[0].image : this.baseUrl + "/images/noimageavatar.jpg";
            return this.baseUrl + "/images/noimageavatar.jpg";
        }, getEmployeeImage: function (id) {
            //alert(id);
            var player = this.clubEmployees.filter(function (item) {
                return item.id === id;
            });
            if (player[0] !== null && player[0] !== undefined)
                return player[0].image !== null ? player[0].image : this.baseUrl + "/images/noimageavatar.jpg";
            return this.baseUrl + "/images/noimageavatar.jpg";
        }
        , getPartnerImage: function (id) {
            // console.log("GetPartnetImage")
            //alert(id);
            var partner = this.allPartners.filter(function (item) {
                return item.id === id;
            })[0];
            // console.log("Id : ", id)
            // console.log("Partner : ", partner)
            if (partner !== null && partner !== undefined)
                return partner.image !== null ? this.baseUrl + partner.image : this.baseUrl + "/images/noimageavatar.jpg";
            return this.baseUrl + "/images/noimageavatar.jpg";
        },
        getGroundImage: function (id) {
            //alert(id);
            var player = this.allGrounds.filter(function (item) {
                return item.id === id;
            })
            if (player[0] !== null && player[0] !== undefined)
                return player[0].image !== null ? player[0].image : this.baseUrl + "/images/noimageavatar.jpg";
            return this.baseUrl + "/images/noimageavatar.jpg";
        },
        getClubImage: function (id) {
            console.log("Fetching Image for : " + id);
            var club = this.allClubs.filter(function (item) {
                return item.id === id;
            });
            console.log("Fetching Club Image !", club);
            if (club[0] !== null && club[0] !== undefined)
                return club[0].image;
            return this.baseUrl + "/images/noimageavatar.jpg";
        },
        getClubName: function (id) {
            console.log("Fetching ClubName for : " + id);
            var club = this.clubs.filter(function (item) {
                return item.clubId === id;
            });
            console.log("Found Club Name !", club);
            if (club[0] !== null && club[0] !== undefined)
                return club[0].name;
            return name;
        },
        getCategoryName: function (id) {
            console.log("Fetching getCategoryName for : " + id);
            var club = this.categories.filter(function (item) {
                return item.categoryId === id;
            });
            console.log("Found Category !", club);
            if (club[0] !== null && club[0] !== undefined)
                return club[0].name;
            return name;
        },
        getCategoryImage: function (id) {
            //alert(id);
            var categories = this.allCategories.filter(function (item) {
                return item.id === id;
            })
            if (categories[0] !== null && categories[0] !== undefined)
                return categories[0].image;
            return this.baseUrl + "/images/noimageavatar.jpg";
        },
        loadAllPlayers: function () {
            console.log("Loading All Players!!");
            sportsmanduApp.loading = true;
            var selectedCate = sportsmanduApp.selectedCategory !== null ? sportsmanduApp.selectedCategory : sportsmanduApp.clubCategory[0];
            axios.get(this.baseUrl + 'Api/players/' + selectedCate)
                .then(function (response) {
                    console.log("Loading All Players", response.data);
                    response.data.forEach(player => {
                        var thisTranIndex = sportsmanduApp.players.map(function (playertoo) {
                            return playertoo.Id;
                        }).indexOf(player.id);
                        console.log("This tran index: " + thisTranIndex);
                        if (thisTranIndex === -1) {
                            sportsmanduApp.allPlayers.unshift(player);
                        }
                    });
                    sportsmanduApp.loading = false;
                })
                .catch(function (error) {
                    console.log("loadAllPlayers() Error: " + error);
                });
        }, fetchYoutubeDetails: function () {
            sportsmanduApp.loading = true;
            var baseURLL = this.baseUrl + 'Api/FetchVideoInformation/' + sportsmanduApp.sportsmanduYoutube.split('=')[1];
            axios.get(baseURLL)
                .then(function (response) {
                    console.log("Fetching Youtube Information", response.data);
                    sportsmanduApp.YoutubeDetails = response.data
                    sportsmanduApp.loading = false;
                })
                .catch(function (error) {
                    console.log("fetchYoutubeDetails() Error: " + error);
                });
        }
        , loadAllPlayerRoles: function (category) {
            console.log("Loading All Player roles for : " + category);
            sportsmanduApp.loading = true;
            axios.get(this.baseUrl + 'Api/GetAllCategoryRoles/' + category)
                .then(function (response) {
                    console.log("Player Roles Response : ", response.data);
                    sportsmanduApp.playerRoles = response.data
                    sportsmanduApp.loading = false;
                })
                .catch(function (error) {
                    console.log("loadAllPlayers() Error: " + error);
                });
        }
        , loadAllPartners: function () {
            console.log("Loading All Partners!!");
            sportsmanduApp.loading = true;
            axios.get(this.baseUrl + 'Api/getallpartners/')
                .then(function (response) {
                    sportsmanduApp.allPartners = response.data;
                    console.log(response.data);
                })
                .catch(function (error) {
                    console.log("Partners Error: " + error);
                });
        }, getSeasonGroup: function () {
            console.log("Loading All Season Groups!!");
            sportsmanduApp.loading = true;
            sportsmanduApp.currentSeason = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
            axios.get(this.baseUrl + 'Api/GetSeasonCategoryClubGroup/' + sportsmanduApp.currentSeason)
                .then(function (response) {
                    sportsmanduApp.seasonGroup = response.data;
                    console.log("sportsmanduApp.seasonGroup", response.data);
                })
                .catch(function (error) {
                    console.log("Partners Error: " + error);
                });
        }, getSeasonFixtureTeamPlayers: function (fixtureGuid) {
            sportsmanduApp.loading = true;
            sportsmanduApp.currentClub = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
            console.log("Loading All Players for Club : " + sportsmanduApp.currentClub + " in Season Match " + fixtureGuid);
            axios.get(this.baseUrl + 'Api/GetSeasonFixtureTeamPlayers/' + sportsmanduApp.currentClub + "/" + fixtureGuid)
                .then(function (response) {
                    sportsmanduApp.seasonFixturesPlayersOfClub = response.data;
                    console.log("sportsmanduApp.seasonFixturesPlayersOfClub", response.data);
                })
                .catch(function (error) {
                    console.log("seasonFixturesPlayersOfClub Error: " + error);
                });
        }
        , loadPlayerRoles: function (category) { // Used in Clubs / Edit
            console.log("Loading Roles!!");
            sportsmanduApp.loading = true;
            axios.get(this.baseUrl + 'Admin/Clubs/GetCategoryRoles/' + category)
                .then(function (response) {
                    sportsmanduApp.playerRoles = response.data;
                    console.log(response.data);
                    sportsmanduApp.loading = false;
                })
                .catch(function (error) {
                    console.log("Role Loading Error: " + error);
                    sportsmanduApp.loading = false;
                });
        }, loadAllPartnerTitles: function () {
            console.log("Loading All Partner Titles !!");
            sportsmanduApp.loading = true;
            axios.get(this.baseUrl + 'Api/getallpartnerstitle/')
                .then(function (response) {
                    sportsmanduApp.allPartnerTitles = response.data;
                    console.log(response.data);
                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });
        }
        , loadAllCategories: function () {
            //alert("Here !!");
            sportsmanduApp.loading = true;
            axios.get(this.baseUrl + 'Api/GetAllCategories')
                .then(function (response) {
                    sportsmanduApp.allCategories = response.data;
                    console.log(response.data);
                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });
            sportsmanduApp.loading = false;
        }, loadAllGrounds: function () {
            //alert("Here !!"); 
            console.log("Loading All Grounds");
            sportsmanduApp.loading = true;
            axios.get(this.baseUrl + 'Api/GetAllGrounds/1') // GetallGrounds / CountryId
                .then(function (response) {
                    sportsmanduApp.allGrounds = response.data;
                    console.log("Grounds", response.data);
                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });;
        },
        loadAllClubs: function () {
            //alert("Here !!");
            sportsmanduApp.loading = true;
            axios.get(this.baseUrl + 'Api/Clubs/' + window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1])
                .then(function (response) {
                    sportsmanduApp.allClubs = response.data;
                    console.log("Loading All Clubs ", response.data);
                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });
        },
        loadSeasonGround: function () {
            sportsmanduApp.loading = true;
            sportsmanduApp.currentSeason = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
            console.log("Loading Season Ground");
            axios.get(this.baseUrl + 'Api/GetGroundsFromSeason/' + sportsmanduApp.currentSeason)
                .then(function (response) {
                    sportsmanduApp.seasonGround = response.data;
                    playersIndexEdit = null;
                    console.log("sportsmanduApp.Ground", response.data);
                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });
            sportsmanduApp.loading = false;
        },
        getSeasonFixtureTeamStatsFields: function (category, seasonMatch) {
            sportsmanduApp.loading = true;
            axios.get(this.baseUrl + 'Api/GetPlayerGameStatsFields/' + 'SeasonMatch' + '/' + category)
                .then(function (response) {
                    console.log(response.data);
                    //sportsmanduApp.playerGameStatsFields = response.data;
                    //sportsmanduApp.stats = response.data; // To Use  in Match Stats But Will be generic in Future!;

                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });
            $('#exampleModal').on('shown.bs.modal', function () {
                //$('#myInput').trigger('focus');                    
            });
            sportsmanduApp.loading = false;
        },
        getPlayerGameStatsFields: function (category, alreadyHaveAStat, segment) { // Used in Admin >> Player For Now
            // console.log(alreadyHaveAStat);
            sportsmanduApp.playerGameStatsFields = [];
            sportsmanduApp.stats = []; // To Use  in Match Stats But Will be generic in Future!;
            console.log("SEASONSTATS : " + segment + " : " + category + " : " + alreadyHaveAStat);
            if (alreadyHaveAStat !== '' && alreadyHaveAStat !== null) {
                alreadyHaveAStat = JSON.parse(alreadyHaveAStat, toCamelCase);
                console.log("I ALREADY HAVE STATS", alreadyHaveAStat);
                sportsmanduApp.playerGameStatsFields = alreadyHaveAStat.stats;
            } else {
                sportsmanduApp.loading = true;
                //var currentPlayer = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
                axios.get(this.baseUrl + 'Api/GetPlayerGameStatsFields/' + segment + '/' + category)// + sportsmanduApp.currentSeason)
                    .then(function (response) {
                        console.log(response.data);
                        sportsmanduApp.playerGameStatsFields = response.data;
                        sportsmanduApp.stats = response.data; // To Use  in Match Stats But Will be generic in Future!;
                    })
                    .catch(function (error) {
                        console.log("Error: " + error);
                    });
                $('#exampleModal').on('shown.bs.modal', function () {
                    //$('#myInput').trigger('focus');                    
                });
                sportsmanduApp.loading = false;
            }
            //alert("Here");
        },
        loadSeasonMatch: function () {
            console.log("Loading Season Match");
            sportsmanduApp.clubs = [];
            sportsmanduApp.loading = true;
            sportsmanduApp.currentSeason = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
            axios.get(this.baseUrl + 'Api/seasons/GetSeasonMatch/' + sportsmanduApp.currentSeason + '/1') //TODO CategoryID Not implemented
                .then(function (response) {
                    sportsmanduApp.seasonMatch = response.data;
                    playersIndexEdit = null;
                    console.log("sportsmanduApp.Match", response.data);
                    sportsmanduApp.loadSeasonCategories();
                    sportsmanduApp.loadSeasonClubs();
                    sportsmanduApp.loadSeasonGround();
                    sportsmanduApp.loading = false;
                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });
        }
        , loadSeasonPartner: function () {
            sportsmanduApp.loading = true;
            sportsmanduApp.currentSeason = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
            console.log("Loading Season Partner");
            axios.get(this.baseUrl + 'Api/seasons/GetSeasonPartner/' + sportsmanduApp.currentSeason) //TODO CategoryID Not implemented
                .then(function (response) {
                    sportsmanduApp.seasonPartner = response.data;
                    seasonPartnerEditIndex = null;
                    console.log("sportsmanduApp.SeasonPartner", response.data);
                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });
            loading = true;
        },
        loadSeasonClubs: function () {
            // alert("here");
            sportsmanduApp.clubs = [];
            sportsmanduApp.loading = true;
            sportsmanduApp.currentSeason = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
            axios.get(this.baseUrl + 'Api/seasons/' + sportsmanduApp.currentSeason)
                .then(function (response) {
                    console.log("Season Clubs", sportsmanduApp.season);
                    console.log("sportsmanduApp.clubs", response.data);
                    response.data.forEach(element => {
                        sportsmanduApp.clubs.unshift({
                            seasonId: element.seasonId,
                            name: element.clubName,
                            clubId: element.clubId,
                            image: element.clubImage,
                        });
                    });
                    sportsmanduApp.loading = false;
                    playersIndexEdit = null;
                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });
        }, loadSeasonCategories: function () {
            console.log("Loading Season Categories");
            sportsmanduApp.Categories = [];
            sportsmanduApp.loading = true;
            sportsmanduApp.currentSeason = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
            axios.get(this.baseUrl + 'Api/GetCategoriesFromSeason/' + sportsmanduApp.currentSeason)
                .then(function (response) {
                    console.log("Season Categories", sportsmanduApp.currentSeason);
                    console.log("sportsmanduApp.Categories", response.data);
                    response.data.forEach(element => {
                        sportsmanduApp.categories.unshift({
                            seasonId: element.seasonId,
                            name: element.categoryName,
                            categoryId: element.categoryId,
                            image: element.categoryImage,
                        });
                    });
                    playersIndexEdit = null;
                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });
            sportsmanduApp.loading = false;
        },
        loadSeasonClubPlayers: function (seasonId) {
            sportsmanduApp.loading = true;
            sportsmanduApp.seasonClubPlayers = [];
            sportsmanduApp.currentSeason = seasonId;
            sportsmanduApp.selectedSeason = seasonId;
            sportsmanduApp.currentClub = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
            axios.get(this.baseUrl + 'Api/seasonClubPlayers/' + sportsmanduApp.currentClub + '/' + seasonId)
                .then(function (response) {
                    console.log("Player ", sportsmanduApp.seasonClubPlayers)
                    console.log("sportsmanduApp.seasonClubPlayers", response.data);

                    response.data.forEach(element => {
                        sportsmanduApp.seasonClubPlayers = response.data;
                    });
                    playersIndexEdit = null;
                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });
            sportsmanduApp.loading = false;
        },
        loadClubPlayers: function () {
            sportsmanduApp.players = [];
            sportsmanduApp.loading = true;
            sportsmanduApp.currentClub = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
            console.log("current club is ", sportsmanduApp.currentClub);
            if (sportsmanduApp.selectedCategory === null) return;
            var fullLink = sportsmanduApp.baseUrl + 'Api/players/clubplayers/' + sportsmanduApp.currentClub + '/' + sportsmanduApp.selectedCategory;
            console.log(fullLink);
            axios.get(fullLink)
                .then(function (response) {
                    console.log("Result", response.data);
                    sportsmanduApp.clubPlayers = [];
                    sportsmanduApp.players = [];
                    response.data.forEach(element => {
                        sportsmanduApp.players.unshift({
                            playerId: element.playerId,
                            name: element.playerName,
                            joinedDate: element.joinedDate,
                            resignedDate: element.resignedDate,
                            shirtNumber: element.shirtNumber,
                            clubId: element.clubId,
                            image: element.playerImage,
                            playerRole: element.playerRole,
                            playerCategory: element.playerCategoryName,
                            //category: element.playerCategoryName
                        });

                        sportsmanduApp.clubPlayers.unshift({
                            playerId: element.playerId,
                            name: element.playerName,
                            joinedDate: element.joinedDate,
                            resignedDate: element.resignedDate,
                            shirtNumber: element.shirtNumber,
                            clubId: element.clubId,
                            image: element.playerImage,
                            playerRole: element.playerRole,
                            playerCategory: element.playerCategoryName,
                            //category: element.playerCategoryName
                        });
                    });
                    playersIndexEdit = null;
                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });
        },
        // GetPlayerStats: function (stat) {
        //     console.log("STATSSSS", stat);
        // },
        loadAllDesignation: function () {
            var fullLink = this.baseUrl + 'Api/AllEmployeeDesignation';
            console.log(fullLink);
            axios.get(fullLink)
                .then(function (response) {
                    console.log("Result", response.data);
                    sportsmanduApp.allEmployeeDesignation = response.data;
                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });
        },
        loadClubEmployee: function () {
            var segment = window.location.href.toLowerCase().split('admin/')[1].split("/")[0];
            var segmentId = window.location.href.toLowerCase().split('/')[window.location.href.toLowerCase().split('/').length - 1];
            sportsmanduApp.clubEmployee = [];
            sportsmanduApp.loading = true;
            //sportsmanduApp.currentClub = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
            console.log(sportsmanduApp.currentClub);
            var fullLink = this.baseUrl + 'Api/Employee/' + segment + '/' + segmentId;
            console.log(fullLink);
            axios.get(fullLink)
                .then(function (response) {
                    console.log("Result", response.data);
                    sportsmanduApp.clubEmployees = response.data;                    
                    employeeIndexEdit = null;
                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });
        }, SaveSeasonMatchStats: function () {
            dataToSend = sportsmanduApp.currentSeasonMatchForStats;
            console.log("SAVING SeasonMatch Stat: ", dataToSend);
            var fullLink = this.baseUrl + 'Api/SaveSeasonMatchStats/';
            axios({
                method: 'post',
                url: fullLink,
                data: dataToSend
            })
                .then(function (response) {
                    if (!response.data.succeded) {
                        toastr.error(response.data.message);
                    } else {
                        toastr.success(response.data.message);
                    }
                })
                .catch(function (error) {
                });
        },
        SaveStats: function (whose, param) {
            console.log("SAVING STATS : " + whose + " ", param);
            var currentId = '';
            var dataToSend = null;
            console.log("playerGameStatsFields : ", this.playerGameStatsFields);
            if (whose === "Player") {
                currentId = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
                dataToSend = this.playerGameStatsFields;
            } else if (whose === "SeasonMatch") {
                param = this.currentSeasonMatchForStats;
                //console.log(param.guid);
                currentId = param.guid;
                dataToSend = this.currentSeasonMatchForStats;
            }
            console.log("PARAM : ", this.playerGameStatsFields);
            var fullLink = this.baseUrl + 'Api/SaveStats/' + whose + '/' + currentId;
            axios({
                method: 'post',
                url: fullLink,
                data: dataToSend
            })
                .then(function (response) {
                    if (!response.data.succeded) {
                        toastr.error(response.data.message);
                    } else {
                        toastr.success(response.data.message);
                    }
                })
                .catch(function (error) {
                });

        },
        loadClubCategories: function () {
            sportsmanduApp.clubCategory = [];
            sportsmanduApp.loading = true;
            sportsmanduApp.currentClub = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
            console.log("current club is ", sportsmanduApp.currentClub);
            axios.get(this.baseUrl + 'Api/ClubCategories/' + sportsmanduApp.currentClub)
                .then(function (response) {
                    console.log("ClubCategories", response.data);
                    sportsmanduApp.clubCategory = response.data;
                    sportsmanduApp.selectedCategory = response.data[0].id;
                    sportsmanduApp.loadClubPlayers();
                })
                .catch(function (error) {
                    console.log("Error: " + error);
                });
        },
        'handleSellCommand': function (tranId) {
            console.logCollapsed("Sell Signal Received For Order Id : " + tranId);
            var object = ({
                signal: 'sell',
            });
            console.log(object);;
        }
    }
});
function toCamelCase(key, value) {
    if (value && typeof value === 'object') {
        for (var k in value) {
            if (/^[A-Z]/.test(k) && Object.hasOwnProperty.call(value, k)) {
                value[k.charAt(0).toLowerCase() + k.substring(1)] = value[k];
                delete value[k];
            }
        }
    }
    return value;
}
sportsmanduApp.currentClub = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length];
//sportsmanduApp.getPlayerGameStatsFields();
if (window.location.href.replace("#", "").replace("?", "").toLowerCase().indexOf("edit") !== -1 && window.location.href.replace("#", "").replace("?", "").toLowerCase().indexOf("seasons") !== -1) {
    //alert("here")    
    // sportsmanduApp.loadSeasonMatch();
    // sportsmanduApp.loadSeasonGround();
    // sportsmanduApp.loadSeasonClubs();
    //sportsmanduApp.loadSeasonPartner();
    // sportsmanduApp.loadSeasonCategories();
    // sportsmanduApp.loadAllClubs();
    // sportsmanduApp.loadAllPartners();
    // sportsmanduApp.loadAllPartnerTitles();
    // sportsmanduApp.loadAllGrounds();
    // sportsmanduApp.loadAllCategories();
    sportsmanduApp.loading = false;
} else if (window.location.href.replace("#", "").replace("?", "").toLowerCase().indexOf("edit") !== -1 && window.location.href.replace("#", "").replace("?", "").toLowerCase().indexOf("clubs") !== -1) {
    // sportsmanduApp.loadAllCategories();
    // setTimeout(() => {
    //     sportsmanduApp.selectedCategory = sportsmanduApp.allCategories[0];
    //     sportsmanduApp.loadClubPlayers();
    // }, 2000);
}
function SaveSocial(social) {
    sportsmanduApp.currentClub = window.location.href.split("Edit/")[1].replace("#", "");
    console.log("Saving Club >> Social Data", social);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'api/AddSocial',
        data: social
    })
        .then(function (response) {
            sportsmanduApp.playersIndexEdit = null;
            if (!response.data.succeded) {
                toastr.error(response.data.message);
            } else {
                toastr.success(response.data.message);
            }
        })
        .catch(function (error) {
        });
}
function SaveClubPlayer(playerdata) {
    playerdata = playerdata[0];//JSON.stringify(playerdata[0]).replace(/"/g, "'")
    sportsmanduApp.currentClub = window.location.href.split("Edit/")[1].replace("#", "");
    playerdata.clubId = sportsmanduApp.currentClub;
    console.log("Saving Club >> Player Playerdata ");
    console.log(playerdata);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'api/AddPlayersToClub/',
        data: playerdata
    })
        .then(function (response) {
            sportsmanduApp.playersIndexEdit = null;
            if (!response.data.succeded) {
                toastr.error(response.data.message);
            } else {
                toastr.success(response.data.message);
            }
        })
        .catch(function (error) {
        });
}
function SaveSeasonClubPlayer(playerdata, currentState) {
    //playerdata = playerdata[0];//JSON.stringify(playerdata[0]).replace(/"/g, "'")
    if (currentState === false) {
        console.log("SaveSeasonClubPlayer Data :  ", playerdata);
        playerdata.seasonId = sportsmanduApp.currentSeason;
        playerdata.clubId = sportsmanduApp.currentClub;
        playerdata.playerId = playerdata.playerId;
        console.log("SaveSeasonClubPlayer Data :  ", playerdata);
        axios({
            method: 'post',
            url: sportsmanduApp.baseUrl + 'Api/AddPlayersToSeasonClub/',
            data: playerdata
        })
            .then(function (response) {
                if (!response.data.succeded) {
                    toastr.error(response.data.message);
                } else {
                    toastr.success(response.data.message);
                    console.log("##ToDo Here")
                    console.log("sportsmanduApp.seasonClubPlayer", sportsmanduApp.seasonClubPlayers);
                    sportsmanduApp.seasonClubPlayers
                        .unshift(
                            {
                                id: parseInt(playerdata.playerId),
                                categoryName: playerdata.playerCategory,
                                playerRole: playerdata.playerRole,
                                name: playerdata.name,
                                image: playerdata.image,
                                thumbnail: playerdata.thumbnail,
                            });
                }
            })
            .catch(function (error) {
            });
    } else {
        if (confirm('Are you sure you want to Delete the player From the Match')) {
            DeleteSeasonClubPlayer(playerdata);;
        } else {
            return;
        }
    }
}

function SaveClubFixturePlayer(player, state) {
    var dataTosend = {
        SeasonFixtureGuid: sportsmanduApp.selectedSeasonFixture.guid,
        ClubId: sportsmanduApp.currentClub,
        PlayerId: player.id
    };
    if (state === false) {
        console.log("Save Received for ", dataTosend);
        axios({
            method: 'post',
            url: sportsmanduApp.baseUrl + 'api/AddPlayersToSeasonFixture/',
            data: dataTosend
        })
            .then(function (response) {
                if (!response.data.succeded) {
                    toastr.error(response.data.message);
                } else {
                    toastr.success(response.data.message);
                    sportsmanduApp.seasonFixturesPlayersOfClub
                        .unshift(
                            {
                                playerId: player.id,
                            });
                }
            })
            .catch(function (error) {
            });
    } else {
        if (confirm('Are you sure you want to Delete the player From the Match')) {
            DeleteSeasonFixturePlayer(player);
        } else {
            return;
        }
    }
}
function SaveClubCategory(playerdata) {
    playerdata = playerdata[0];
    sportsmanduApp.currentClub = window.location.href.split("Edit/")[1].replace("#", "");
    playerdata.clubId = sportsmanduApp.currentClub;
    playerdata.categoryId = playerdata.id;
    console.log("Saving Club >> Player Playerdata ");
    console.log(playerdata);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'api/AddCategoryToClub/',
        data: playerdata
    })
        .then(function (response) {
            sportsmanduApp.playersIndexEdit = null;
            if (!response.data.succeded) {
                toastr.error(response.data.message);
            } else {
                toastr.success(response.data.message);
            }
        })
        .catch(function (error) {
        });
}
function SaveSeasonMatch(seasonMatchData) {
    console.log("SeasonMatchData :  ", seasonMatchData);
    var seasonMatch = [{
        Guid: seasonMatchData.id,
        GroundId: seasonMatchData.ground.id,
        CategoryId: seasonMatchData.category.id,
        SeasonId: seasonMatchData.seasonId,
        Date: seasonMatchData.date,
        Teams: [],
        MatchType: seasonMatchData.matchType,
        Status: seasonMatchData.status,
        CurrentRound: seasonMatchData.currentRound
    }];
    seasonMatchData.teams.forEach(team => {
        seasonMatch[0].Teams.unshift({
            id: team.id
        }
        );
    });
    console.log("seasonMatch Tteam", seasonMatch[0].Teams)
    // seasonMatch[0].Teams  = seasonMatch[0].Team[0];
    console.log("SeasonMatch :  ", seasonMatch[0]);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/AddMatchToSeason/',
        data: seasonMatch[0]
    })
        .then(function (response) {
            if (!response.data.succeded) {
                toastr.error(response.data.message);
            } else {
                sportsmanduApp.loadSeasonMatch();
                toastr.success(response.data.message);
                sportsmanduApp.seasonMatchIndexEdit = null;
            }
        })
        .catch(function (error) {
        });
}
function SaveSeasonPartner(seasonMatchData) {
    console.log("SeasonPartnerData :  ", seasonMatchData);
    var seasonMatch = [{
        partnerId: seasonMatchData.partner.id,
        seasonId: seasonMatchData.seasonId,
        partnerTitleId: seasonMatchData.partnerTitle.id
    }];
    console.log("SeasonPartner :  ", seasonMatch[0]);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/AddPartnerToSeason/',
        data: seasonMatch[0]
    })
        .then(function (response) {
            if (!response.data.succeded) {
                toastr.error(response.data.message);
            } else {
                sportsmanduApp.seasonPartnerEditIndex = null;
                sportsmanduApp.loadSeasonPartner();
                toastr.success(response.data.message);
            }
        })
        .catch(function (error) {
        });
}
function SaveSeasonGroup(seasonGroupData) {
    console.log("seasonGroupData :  ", seasonGroupData);
    var seasonGroup = {
        SeasonId: sportsmanduApp.currentSeason,
        ClubId: seasonGroupData.clubId,
        CategoryId: seasonGroupData.categoryId,
        GroupName: seasonGroupData.groupName
    };
    console.log("seasonGroupData :  ", seasonGroupData);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/SaveSeasonGroup/',
        data: seasonGroup
    })
        .then(function (response) {
            if (!response.data.succeded) {
                toastr.error(response.data.message);
            } else {
                toastr.success(response.data.message);
                sportsmanduApp.seasonGroundEditIndex = null;
                setTimeout(() => {
                    sportsmanduApp.loadSeasonGround();
                }, 1000);
            }
        })
        .catch(function (error) {
        });
}
function SaveSeasonGround(seasonGroundData) {
    console.log("SeasonGroundData :  ", seasonGroundData);
    var seasonGround = {
        SeasonId: sportsmanduApp.currentSeason,
        GroundId: seasonGroundData.ground.id
    };
    console.log("seasonGround :  ", seasonGround);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/AddGroundToSeason/',
        data: seasonGround
    })
        .then(function (response) {
            if (!response.data.succeded) {
                toastr.error(response.data.message);
            } else {
                toastr.success(response.data.message);
                sportsmanduApp.seasonGroundEditIndex = null;
                setTimeout(() => {
                    sportsmanduApp.loadSeasonGround();
                }, 1000);
            }
        })
        .catch(function (error) {
        });
}
function DeleteClubCategory(ClubCategory) {
    console.log("DeleteClubCategory :  ", ClubCategory);
    var seasonGround = {
        CategoryId: ClubCategory,
        ClubId: sportsmanduApp.currentClub
    };
    console.log("DeleteClubCategory :  ", seasonGround);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/DeleteCategoryFromClub/',
        data: seasonGround
    })
        .then(function (response) {
            if (!response.data.succeded) {
                console.log(response.data.message);
                toastr.error(response.data.message);
            } else {
                sportsmanduApp.loadClubCategories();
                toastr.success(response.data.message);
            }
        })
        .catch(function (error) {
        });
}
function DeleteClubPlayer(ClubPlayer) {
    console.log("DeleteClubPlayer :  ", ClubPlayer);
    var seasonGround = {
        PlayerId: ClubPlayer.playerId,
        ClubId: sportsmanduApp.currentClub
    };
    console.log("DeleteClubPlayer :  ", seasonGround);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/DeletClubPlayer/',
        data: seasonGround
    })
        .then(function (response) {
            if (!response.data.succeded) {
                console.log(response.data.message);
                toastr.error(response.data.message);
            } else {
                sportsmanduApp.loadClubPlayers();
                sportsmanduApp.seasonClubPlayerIndexEdit = null;
                toastr.success(response.data.message);
            }
        })
        .catch(function (error) {
        });
}
function DeletePartnerFromSeason(SeasonPartner) {
    SeasonPartner.PartnerId = SeasonPartner.partner.id;
    console.log("DeletePartnerFromSeason :  ", SeasonPartner);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/DeletePartnerFromSeason/',
        data: SeasonPartner
    })
        .then(function (response) {
            if (!response.data.succeded) {
                console.log(response.data.message);
                toastr.error(response.data.message);
            } else {
                sportsmanduApp.seasonPartnerEditIndex = null;
                sportsmanduApp.loadSeasonPartner();
                toastr.success(response.data.message);
            }
        })
        .catch(function (error) {
        });
}
function DeleteSeasonFixture(SeasonFixture) {
    console.log("Delete Season Fixture :  ", SeasonFixture);
    SeasonFixture.guid = SeasonFixture.id;
    console.log("DeleteSeasonFixture :  ", SeasonFixture);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'api/DeleteSeasonFixture/',
        data: SeasonFixture
    })
        .then(function (response) {
            if (!response.data.succeded) {
                console.log(response.data.message);
                toastr.error(response.data.message);
            } else {
                sportsmanduApp.loadSeasonMatch();
                seasonMatchIndexEdit = null;
                toastr.success(response.data.message);

            }
        })
        .catch(function (error) {
        });
}
function DeleteSeasonFixturePlayer(player) {
    var dataToSend = {
        PlayerId: player.id,
        ClubId: sportsmanduApp.currentClub,
        SeasonFixtureGuid: sportsmanduApp.selectedSeasonFixture.guid
    };
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'api/DeleteSeasonFixturePlayer/',
        data: dataToSend
    })
        .then(function (response) {
            if (!response.data.succeded) {
                console.log(response.data.message);
                toastr.error(response.data.message);
            } else {
                toastr.success(response.data.message);
                console.log("TODO HERE FInd and REmove from seasonFixturesPlayersOfClub");
                console.log("sportsmanduApp.seasonFixturesPlayersOfClub", sportsmanduApp.seasonFixturesPlayersOfClub);
                console.log("player.id", player.id);
                var thisTranIndex = sportsmanduApp.seasonFixturesPlayersOfClub.map(function (player) {
                    return player.playerId;
                }).indexOf(player.id);
                console.log("thisTranIndex: " + thisTranIndex);
                sportsmanduApp.seasonFixturesPlayersOfClub.splice(thisTranIndex, 1)
            }
        })
        .catch(function (error) {
        });
}
function DeleteSeasonGround(seasonGroundData) {
    console.log("SeasonGroundData :  ", seasonGroundData);
    var seasonGround = {
        SeasonId: sportsmanduApp.currentSeason,
        GroundId: seasonGroundData.ground.id
    };
    console.log("seasonGround :  ", seasonGround);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/DeleteGroundFromSeason/',
        data: seasonGround
    })
        .then(function (response) {
            if (!response.data.succeded) {
                console.log(response.data.message);
                toastr.error(response.data.message);
            } else {
                sportsmanduApp.loadSeasonGround();
                toastr.success(response.data.message);
            }
        })
        .catch(function (error) {
        });
}
function DeleteSeasonClubPlayer(seasonClubPlayerData) {
    seasonClubPlayerData.playerId = parseInt(seasonClubPlayerData.playerId);
    console.log("DeleteSeasonClubPlayer :  ", seasonClubPlayerData);
    var seasonClubPlayer = {
        SeasonId: sportsmanduApp.currentSeason,
        PlayerId: seasonClubPlayerData.playerId,
        ClubId: sportsmanduApp.currentClub
    };
    console.log("seasonClubPlayerData :  ", seasonClubPlayer);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/DeleteSeasonClubPlayer/',
        data: seasonClubPlayer
    })
        .then(function (response) {
            if (!response.data.succeded) {
                console.log(response.data.message);
                toastr.error(response.data.message);
            } else {
                toastr.success(response.data.message);
                var thisTranIndex = sportsmanduApp.seasonClubPlayers.map(function (player) {
                    return player.id;
                }).indexOf(seasonClubPlayerData.playerId);
                console.log("thisTranIndex: " + thisTranIndex);
                sportsmanduApp.seasonClubPlayers.splice(thisTranIndex, 1);
                //sportsmanduApp.loadSeasonClubPlayers();
            }
        })
        .catch(function (error) {
        });
}
function DeleteSeasonFixtureTeam(team, match) {
    console.log("SeasonFixtureTeamData :  ", team);
    console.log("SeasonFixtureMatch :  ", match);
    var seasonMatchTeam = {
        SeasonFixtureGuid: match.id,
        ClubId: team.id
    };
    console.log("seasonMatchTeam :  ", seasonMatchTeam);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/DeleteTeamFromSeasonMatchFixture/',
        data: seasonMatchTeam
    })
        .then(function (response) {
            if (!response.data.succeded) {
                console.log(response.data.message);
                toastr.error(response.data.message);
            } else {
                sportsmanduApp.loadSeasonMatch();
                toastr.success(response.data.message);
            }
        })
        .catch(function (error) {
        });
}

function SaveSeasonClub(playerdata) {
    playerdata = playerdata[0];//JSON.stringify(playerdata[0]).replace(/"/g, "'")
    playerdata.seasonId = sportsmanduApp.currentSeason;
    console.log("ClubData ", playerdata);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/AddClubToSeason/',
        data: playerdata
    })
        .then(function (response) {
            if (!response.data.succeded) {
                toastr.error(response.data.message);
            } else {
                toastr.success(response.data.message);
                sportsmanduApp.playersIndexEdit = null;
            }
        })
        .catch(function (error) {
        });
}


function SaveSeasonCategory(playerdata) {
    playerdata = playerdata[0];//JSON.stringify(playerdata[0]).replace(/"/g, "'")
    playerdata.seasonId = sportsmanduApp.currentSeason;
    // console.log(playerdata);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/AddCategoryToSeason/',
        data: playerdata
    })
        .then(function (response) {
            if (!response.data.succeded) {
                toastr.error(response.data.message);
            } else {
                toastr.success(response.data.message);
                sportsmanduApp.categoryIndexEdit = null;
            }
        })
        .catch(function (error) {
        });
}

// $(document).ready(function () {
// setTimeout(() => {

// }, timeout);
//TODO only in players for now
$(document).ready(function () {
    RemoveUnwantedRoles();
});
function RemoveUnwantedRoles() {
    $('#PlayerRole option').each(function (index, option) {
        console.log(option.text);
    });
    var categoryRole = [];
    var category = $("#CategoryId option:selected").text();
    console.log(category);
    if (category === null || category === '') return;
    sportsmanduApp.loading = true;
    axios.get(sportsmanduApp.baseUrl + 'Api/GetCategoryRoles/' + category)
        .then(function (response) {
            console.log(response.data);
            categoryRole = response.data;
            $('#PlayerRole option').each(function (index, option) {
                roleFound = false;
                roleValue = option.value;
                response.data.forEach(element => {
                    if (element.name === option.text) {
                        roleFound = true;
                        //Donot Remove
                    }
                });
                if (roleFound === false) {
                    console.log("Remove : " + option.text);
                    $("#PlayerRole option[value='" + roleValue + "']").remove();
                }
            });
            sportsmanduApp.loading = false;
            return response.data;
        })
        .catch(function (error) {
            console.log("loadAllPlayers() Error: " + error);
        });
    $('#PlayerRole option').each(function (index, option) {
        categoryRole.forEach(element => {
            if (element.name === option.text) {
                //Donot Remove
            }
        });
    });
}
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

$(document).ready
    (function () {
        var currentCulture = "";
        try {
            currentCulture = window.location.href.split("/")[3];
        } catch (error) {
            console.log(error);
        }
        sportsmanduApp.currentCulture = currentCulture;
        if (sportsmanduApp.currentCulture === "") {
            sportsmanduApp.currentCulture = "En";
        }
        sportsmanduApp.currentClub = window.location.href.split("/")[window.location.href.split("/").length];
        
    });