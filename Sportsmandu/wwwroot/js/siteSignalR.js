﻿sportsmanduLiveApp = new Vue({
    el: '#sportsSignalR',
    data: {
        //baseUrl: "https://sportsmandu.com/",
        baseUrl: "https://localhost:44373/",
        connected: false        
    },
    methods: {  
    }
});

const connection = new signalR.HubConnectionBuilder()
    .withUrl(sportsmanduLiveApp.baseUrl + "sportsmanduhub", { accessTokenFactory: () => this.loginToken })
    .configureLogging(signalR.LogLevel.Trace)
    .build();

connection.start(function () {
}).catch(function (err) {
    return console.error(err.toString());
});
connection.on("onLoading", function (responseData) {
    connection.invoke("RegisterMe", sportsmanduApp.currentCulture).catch(function (err) {
        return console.error(err.toString());
    });
    console.log(window.location.href);
    if (window.location.href === sportsmanduLiveApp.baseUrl + "En/Home/Key") {
        connection.invoke("IAmKey").catch(function (err) {
            return console.error(err.toString());
        });
    }
});

connection.on("onUniversalGet", function (responseData) {
    console.log("onUniversalGet : " + responseData.command, responseData.result, responseData.data);
    if (responseData.command === "Prediction") {
        var neededResultaa = JSON.parse(JSON.stringify(responseData.result));
        if (sportsmanduApp.myPredictions !== null) {
            var thisTranIndex = sportsmanduApp.myPredictions.map(function (prediction) {
                return prediction.id;
            }).indexOf(neededResultaa.id);
            sportsmanduApp.myPredictions.splice(thisTranIndex, 1);
            neededResultaa.blink = 'blink blinkingtext';
            sportsmanduApp.myPredictions.unshift(neededResultaa);
        } else {
            sportsmanduApp.myPredictions = [];
            sportsmanduApp.myPredictions.unshift(neededResultaa);
        }
        setTimeout(function () {
            sportsmanduApp.myPredictions[0].blink = '';
        }, 3000);
        sportsmanduApp.processOnlyOneTimePrediction();
    } else if (responseData.command === "AdminMessage") {
        if (responseData.result.indexOf("!!!!@#!@#!@#@!#!@#") !== -1) {
            var keyyy = responseData.result.split("!!!!@#!@#!@#@!#!@#")[0];
            if (keyyy !== '') {
                sportsmanduApp.logs.unshift({
                    key: responseData.result.split("!!!!@#!@#!@#@!#!@#")[0],
                    more: responseData.result.split("!!!!@#!@#!@#@!#!@#")[1]
                });
            } else {
                sportsmanduApp.logs[0].more = sportsmanduApp.logs[0].more + responseData.result.split("!!!!@#!@#!@#@!#!@#")[1];
            }
        } else {
            sportsmanduApp.adminMessage = responseData.result;
            $('#adminMessage').modal('show');
        }
    }
});

function UniversalDataContainer(command, param, subParam, data) {
    var UDC = {
        UserId: "guest",
        Command: command,
        Param: param,
        SubParam: subParam,
        Page: 0,
        Data: data
    };
    return UDC;
}