﻿sportsmanduApp = new Vue({
    el: '#sportsGlobalization',
    data: {
        baseUrl: 'https://sportsmandu.com/',
        name: 'Miraj',
        siteSections: [],
        siteSectionLanguageTitle: [],
        siteSectionsEditIndex: null,
        siteSectionLanguageTitlesEditIndex: null,
        languageTitles: [],
        languageTitlesEditIndex: null,
        route:"globalization",
    },
    computed: {
    },
    destroyed: function () { },
    methods: {
        addNewSiteSection: function () {
            console.log("Adding SiteSection !");
            this.siteSections.unshift({
                id: 0,
                name: null
            });
            this.siteSectionsEditIndex = 0;
        },
        addNewSiteSectionLanguageTitle: function () {
            console.log("Adding SiteSectionLanguageTitle !");
            this.siteSectionLanguageTitle.unshift({
                siteSectionId: 0,
                languageTitleId: 0
            });
            this.siteSectionLanguageTitlesEditIndex = 0;
        },
        addNewLanguageTitle: function () {
            console.log("Adding LanguageTitle !");
            this.languageTitles.unshift({
                id: 0,
                name: null
            });
            this.languageTitlesEditIndex = 0;
        },
        saveSiteSection: function () {
            loading = true;
            console.log("Saving SiteSection !");
            SaveSitesection();
        },
        saveSiteSectionLanguageTitle: function () {
            loading = true;
            console.log("Saving SiteSectionLanguageTitle !");
            SaveSiteSectionLanguageTitle();
        },
        saveLanguageTitle: function () {
            loading = true;
            console.log("Saving LanguageTitle !");
            SaveLanguageTitle();
        }
        , getAllSiteSection: function () {
            console.log("Loading All GetAllSiteSection");
            sportsmanduApp.loading = true;
            axios.get(this.baseUrl + 'Api/Globalization/GetAllSiteSections/')
                .then(function (response) {
                    console.log("GetAllSiteSection Response : ", response.data);
                    sportsmanduApp.siteSections = response.data
                    sportsmanduApp.loading = false;
                })
                .catch(function (error) {
                    console.log("GetAllSiteSection() Error: " + error);
                });
        },
        getAllLanguageTitles: function () {
            console.log("Loading All GetAllLanguageTitles");
            sportsmanduApp.loading = true;
            axios.get(this.baseUrl + 'Api/Globalization/GetAllLanguageTitles/')
                .then(function (response) {
                    console.log("GetAllLanguageTitles Response : ", response.data);
                    sportsmanduApp.languageTitles = response.data
                    sportsmanduApp.loading = false;
                })
                .catch(function (error) {
                    console.log("GetAllLanguageTitles() Error: " + error);
                });
        },
        getAllSiteSectionLanguageTitle: function () {
            console.log("Loading All GetAllSiteSectionLanguageTitle");
            sportsmanduApp.loading = true;
            axios.get(this.baseUrl + 'Api/Globalization/GetAllSiteSectionLanguageTitle/')
                .then(function (response) {
                    console.log("GetAllSiteSectionLanguageTitle Response : ", response.data);
                    sportsmanduApp.siteSectionLanguageTitle = response.data
                    sportsmanduApp.loading = false;
                })
                .catch(function (error) {
                    console.log("GetAllSiteSectionLanguageTitle() Error: " + error);
                });
        },
        getSiteSectionName: function (id) {
            var name = this.siteSections.filter(function (item) {
                return item.id == id;
            });
            console.log(name.length);
            return name.length != 0 ? name[0].name : "";
        }, getlanguageTitleName: function (id) {
            var name = this.languageTitles.filter(function (item) {
                return item.id == id;
            });
            console.log(name.length);
            return name.length != 0 ? name[0].name : "";
        }
    }
});
sportsmanduApp.getAllLanguageTitles();
sportsmanduApp.getAllSiteSection();
sportsmanduApp.getAllSiteSectionLanguageTitle();
function SaveSitesection() {
    SiteSection = sportsmanduApp.siteSections[sportsmanduApp.siteSectionsEditIndex];
    console.log("Saving Site Sections");
    console.log(SiteSection);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'api/Globalization/AddNewSiteSection',
        data: SiteSection
    })
        .then(function (response) {
            if (!response.data.succeded) {
                toastr.error(response.data.message);
            } else {
                setTimeout(() => {
                    sportsmanduApp.loadAllSiteSections();
                }, 2000);
                sportsmanduApp.siteSectionsEditIndex = null;
                toastr.success(response.data.message);
            }
        })
        .catch(function (error) {
        });
}
function SaveLanguageTitle() {
    languageTitle = sportsmanduApp.languageTitles[sportsmanduApp.languageTitlesEditIndex];
    console.log("SaveLanguageTitle....");
    console.log(languageTitle);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'api/Globalization/AddNewlanguageTitle',
        data: languageTitle
    })
        .then(function (response) {
            if (!response.data.succeded) {
                toastr.error(response.data.message);
            } else {
                setTimeout(() => {
                    sportsmanduApp.loadAlllanguageTitles();
                }, 2000);
                sportsmanduApp.languageTitlesEditIndex = null;
                toastr.success(response.data.message);
            }
        })
        .catch(function (error) {
        });
}
function SaveSiteSectionLanguageTitle() {
    languageTitle = sportsmanduApp.siteSectionLanguageTitle[sportsmanduApp.siteSectionLanguageTitlesEditIndex];
    console.log("SaveSiteSectionLanguageTitle....");
    console.log(languageTitle);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'api/Globalization/AddNewSiteSectionLanguageTitle',
        data: languageTitle
    })
        .then(function (response) {
            if (!response.data.succeded) {
                console.log(response.data);
                toastr.error(response.data.message);
            } else {
                setTimeout(() => {
                    sportsmanduApp.loadAlllanguageTitles();
                }, 2000);
                sportsmanduApp.languageTitlesEditIndex = null;
                toastr.success(response.data.message);
                console.log(response.data);
            }
        })
        .catch(function (error) {
        });
}