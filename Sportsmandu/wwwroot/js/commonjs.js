﻿var utils = {
    getHostName: function () {
        return window.location.host;
    },
    getCookie: function (cname) {
        cname = cname.replace(':', '');
        var name = cname + '=';
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },
    setCookie: function (cname, cvalue, exdays) {
        cname = cname.replace(':', '');
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
    }
}
function ConsoleInit() {
    var log = console.log;
    console.log, function () {
        if (utils.getCookie('SportsmanduAdmin') === 'true')
            log.apply(this, Array.prototype.slice.call(arguments));
    };
}
function Weed() {
    utils.setCookie('SportsmanduAdmin', 'false', 1);
    console.log("Settings Stuffs")
}
Weed();
this.ConsoleInit();