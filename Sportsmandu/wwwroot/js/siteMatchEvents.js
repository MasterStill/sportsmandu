﻿//alert("SiteAdmin Loaded!!");
sportsmanduApp = new Vue({
    el: '#sportsAdminMatchEvents',
    data: {
        // baseUrl: 'http://sportsmandu.ngrok.io/',
        baseUrl: 'https://sportsmandu.com/',
        connected: false,
        allClubs: [], // Season >> Edit ko lagi
        clubs: [], // Season >> Edit ko lagi
        categories: [],
        allcategories: [], // Season >> Edit ko lagi
        categoryIndexEdit: null,
        clubsIndexEdit: null, // Season >> Edit ko lagi
        currentSeason: null, // Season >> Edit ko lagi // Season ID ho yo

        allPlayers: [], // Clubs >> Edit ko lagi
        players: [], // Clubs >> Edit ko lagi
        selectedPlayer: null, // Clubs >> Edit ko lagi
        playersIndexEdit: null, // Clubs >> Edit ko lagi
        currentClub: null // Clubs >> Edit ko lagi  // Club Id ho yo 
    },
    computed: {
    },
    destroyed: function () { },
    methods: {
        addNewPlayer: function () {
            console.log("Adding New Player!");
            this
                .players
                .unshift({ playerId: "", name: "", joinedDate: '2014-01-02T11:42:13.510', resignedDate: '2014-01-02T11:42:13.510', shirtNumber: "", clubId: this.currentClub });
            this.playersIndexEdit = 0;
        },
        addNewClub: function () {
            console.log("Adding New Club!");
            this
                .clubs
                .unshift({ clubId: "", name: "", seasonId: this.currentSeason });
            this.clubsIndexEdit = 0;
        },
        addNewCategory: function () {
            console.log("Adding New Club!");
            this
                .categories
                .unshift({ categoryId: "", name: "", seasonId: this.currentSeason });
            this.categoryIndexEdit = 0;
        },
        savePlayers: function () {
            console.log("Saving Player !");
            alert("saving");
            SaveClubPlayer(this.players);
            setTimeout(() => {
                sportsmanduApp.loadClubPlayers();
            }, 2000);
        },
        saveClubs: function () {
            console.log("Saving Clubs !");
            SaveSeasonClub(this.clubs);
            setTimeout(() => {
                sportsmanduApp.loadSeasonClubs();
            }, 2000);
        },
        saveCategories: function () {
            console.log("Saving categories !", this.categories);
            SaveSeasonCategory(this.categories);
            setTimeout(() => {
                sportsmanduApp.loadSeasonCategories();
            }, 2000);
        },
        getPlayerImage: function (id) {
            alert(id);
            var player = this.allPlayers.filter(function (item) {
                return item.id == id;
            })
            if (player[0] !== null && player[0] !== undefined)
                return player[0].image !== null ? player[0].image : this.baseUrl + "/images/noimageavatar.jpg";
            return this.baseUrl + "/images/noimageavatar.jpg";
        },
        getClubImage: function (id) {
            //alert(id);
            var club = this.allClubs.filter(function (item) {
                return item.id == id;
            })
            console.log("Fetching Club Image !", club);
            if (club[0] !== null && club[0] !== undefined)
                return club[0].image;
            return this.baseUrl + "/images/noimageavatar.jpg";
        },
        getCategoryImage: function (id) {
            //alert(id);
            var categories = this.allCategories.filter(function (item) {
                return item.id == id;
            })
            if (categories[0] !== null && categories[0] !== undefined)
                return categories[0].image;
            return this.baseUrl + "/images/noimageavatar.jpg";
        },
        loadAllPlayers: function () {
            console.log("Loading All Players!!");
            sportsmanduApp.loading = true;
            axios.get(this.baseUrl + 'Api/players/')
                .then(function (response) {
                    sportsmanduApp.allPlayers = response.data;
                    console.log(response.data);
                })
                .catch(function (error) {
                    console.log(error);
                });
        }, loadAllCategories: function () {
            //alert("Here !!");
            sportsmanduApp.loading = true;
            axios.get(this.baseUrl + 'Api/GetAllCategories')
                .then(function (response) {
                    sportsmanduApp.allCategories = response.data;
                    console.log(response.data);
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        loadAllClubs: function () {
            //alert("Here !!");
            sportsmanduApp.loading = true;
            axios.get(this.baseUrl + 'Api/Clubs/' + window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1])
                .then(function (response) {
                    sportsmanduApp.allClubs = response.data;
                    console.log("Loading All Clubs ", response.data);
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        loadSeasonClubs: function () {
            sportsmanduApp.clubs = [];
            sportsmanduApp.loading = true;
            sportsmanduApp.currentSeason = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
            axios.get(this.baseUrl + 'Api/seasons/' + sportsmanduApp.currentSeason)
                .then(function (response) {
                    console.log("Season Clubs", sportsmanduApp.season)
                    console.log("sportsmanduApp.clubs", response.data);
                    response.data.forEach(element => {
                        sportsmanduApp.clubs.unshift({
                            seasonId: element.seasonId,
                            name: element.clubName,
                            clubId: element.clubId,
                            image: element.clubImage,
                        });
                    });
                    playersIndexEdit = null;
                })
                .catch(function (error) {
                    console.log(error);
                });
        }, loadSeasonCategories: function () {
            sportsmanduApp.Categories = [];
            sportsmanduApp.loading = true;
            sportsmanduApp.currentSeason = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
            axios.get(this.baseUrl + 'Api/GetCategoriesFromSeason/' + sportsmanduApp.currentSeason)
                .then(function (response) {
                    console.log("Season Categories", sportsmanduApp.currentSeason)
                    console.log("sportsmanduApp.Categories", response.data);
                    response.data.forEach(element => {
                        sportsmanduApp.categories.unshift({
                            seasonId: element.seasonId,
                            name: element.categoryName,
                            categoryId: element.categoryId,
                            image: element.categoryImage,
                        });
                    });
                    playersIndexEdit = null;
                })
                .catch(function (error) {
                    console.log(error);
                });
        },

        loadClubPlayers: function () {
            sportsmanduApp.players = [];
            sportsmanduApp.loading = true;
            // if(sportsmanduApp.currentClub === undefined){
            sportsmanduApp.currentClub = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1];
            // }
            console.log("current club is ", window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length - 1])
            axios.get(this.baseUrl + 'Api/players/' + sportsmanduApp.currentClub)
                .then(function (response) {
                    console.log("Player ", sportsmanduApp.players)
                    console.log("sportsmanduApp.players", response.data);
                    response.data.forEach(element => {
                        sportsmanduApp.players.unshift({
                            playerId: element.playerId,
                            name: element.playerName,
                            joinedDate: element.joinedDate,
                            resignedDate: element.resignedDate,
                            shirtNumber: element.shirtNumber,
                            clubId: element.clubId,
                            image: element.playerImage,
                            category: element.playerCategoryName
                        });
                    });
                    playersIndexEdit = null;
                })
                .catch(function (error) {
                    console.log(error);
                });
        },

        'handleSignalSubmit': function () {
            this.signalForm.UserId = _UserId;
            this.signalForm.Exchange = "Bittrex";
            this.signalForm.Strategy = 'SELF';
            this.signalForm.Blob = '';
            this.signalForm.Source = _UserId;
            console.groupCollapsed("New Signal Sending...!");
            alert("Signal Sent");
            console.groupEnd();
        },
        'handleSellCommand': function (tranId) {
            console.groupCollapsed("Sell Signal Received For Order Id : " + tranId);
            var object = ({
                signal: 'sell',
                orderuid: tranId,
                exchange: 'Bittrex',
                market: market
            });
            console.log(object);
            //broadcaster.server.signalGiven(this.signalForm)
            console.groupEnd();
        }
    }
});
sportsmanduApp.currentClub = window.location.href.replace("#", "").replace("?", "").split("/")[window.location.href.replace("#", "").replace("?", "").split("/").length];
if (window.location.href.replace("#", "").replace("?", "").toLowerCase().indexOf("edit") !== -1 && window.location.href.replace("#", "").replace("?", "").toLowerCase().indexOf("seasons") !== -1) {
    //alert("here")    
    sportsmanduApp.loadSeasonClubs();
    sportsmanduApp.loadSeasonCategories();
    sportsmanduApp.loadAllClubs();
    sportsmanduApp.loadAllCategories();
} else if (window.location.href.replace("#", "").replace("?", "").toLowerCase().indexOf("edit") !== -1 && window.location.href.replace("#", "").replace("?", "").toLowerCase().indexOf("clubs") !== -1) {
    sportsmanduApp.loadClubPlayers();
    sportsmanduApp.loadAllPlayers();
}
function SaveClubPlayer(playerdata) {
    playerdata = playerdata[0];//JSON.stringify(playerdata[0]).replace(/"/g, "'")
    playerdata.clubId = sportsmanduApp.currentClub;
    console.log("Saving Club >> Player Playerdata ");
    console.log(playerdata);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/AddPlayersToClub/',
        data: playerdata
    })
        .then(function (response) {
            if (!response.data.succeded) {
                toastr.error(response.data.message);
            } else {
                toastr.success(response.data.message);
                sportsmanduApp.playersIndexEdit = null;
            }
        })
        .catch(function (error) {
        });
}

function SaveSeasonClub(playerdata) {
    playerdata = playerdata[0];//JSON.stringify(playerdata[0]).replace(/"/g, "'")
    playerdata.seasonId = sportsmanduApp.currentSeason;
    console.log("ClubData ", playerdata);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/AddClubToSeason/',
        data: playerdata
    })
        .then(function (response) {
            if (!response.data.succeded) {
                toastr.error(response.data.message);
            } else {
                toastr.success(response.data.message);
                sportsmanduApp.playersIndexEdit = null;
            }
        })
        .catch(function (error) {
        });
}

function SaveSeasonCategory(playerdata) {
    playerdata = playerdata[0];//JSON.stringify(playerdata[0]).replace(/"/g, "'")
    playerdata.seasonId = sportsmanduApp.currentSeason;
    // console.log(playerdata);
    axios({
        method: 'post',
        url: sportsmanduApp.baseUrl + 'Api/AddCategoryToSeason/',
        data: playerdata
    })
        .then(function (response) {
            if (!response.data.succeded) {
                toastr.error(response.data.message);
            } else {
                toastr.success(response.data.message);
                sportsmanduApp.categoryIndexEdit = null;
            }
        })
        .catch(function (error) {
        });
}

// $(document).ready(function () {
// setTimeout(() => {

// }, timeout);    
document.addEventListener("DOMContentLoaded", function () {
    var lazyloadImages;

    if ("IntersectionObserver" in window) {
        lazyloadImages = document.querySelectorAll(".lazy");
        var imageObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (entry) {
                if (entry.isIntersecting) {
                    var image = entry.target;
                    image.src = image.dataset.src;
                    image.classList.remove("lazy");
                    imageObserver.unobserve(image);
                }
            });
        });

        lazyloadImages.forEach(function (image) {
            imageObserver.observe(image);
        });
    } else {
        var lazyloadThrottleTimeout;
        lazyloadImages = document.querySelectorAll(".lazy");

        function lazyload() {
            if (lazyloadThrottleTimeout) {
                clearTimeout(lazyloadThrottleTimeout);
            }

            lazyloadThrottleTimeout = setTimeout(function () {
                var scrollTop = window.pageYOffset;
                lazyloadImages.forEach(function (img) {
                    if (img.offsetTop < (window.innerHeight + scrollTop)) {
                        img.src = img.dataset.src;
                        img.classList.remove('lazy');
                    }
                });
                if (lazyloadImages.length == 0) {
                    document.removeEventListener("scroll", lazyload);
                    window.removeEventListener("resize", lazyload);
                    window.removeEventListener("orientationChange", lazyload);
                }
            }, 20);
        };

        document.addEventListener("scroll", lazyload);
        window.addEventListener("resize", lazyload);
        window.addEventListener("orientationChange", lazyload);
    }
});
// });