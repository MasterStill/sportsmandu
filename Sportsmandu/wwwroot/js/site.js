﻿var sportsmanduApp = null;
sportsmanduApp = new Vue({
    el: '#sportsManduMainPage',
    data: {        
        //baseUrl: 'https://sportsmandu.com/',
        baseUrl: 'https://localhost:44373/',
        currentCulture: '',
        broswer: '',
        adminMessage: '',
        adminHtml: '',
        adminTitle: '',
        loading: false,
        connected: false,        
        allMatches: [],
        completedMatches:[],
        currentSegment: null,
        currentSegmentId: null,
        selectedFixture: null,
        gallery:[],
        selectedGallery: {
            id:null
        },
        myPredictions: null,
        logs: [
            {
                key: '',
                more:''
            }
        ],
        processingPrediciton:true,
        Search: {
            SearchTerm: null,
            Culture: sportsmanduApp !== null ? sportsmanduApp.currentCulture : null
        },
        SearchResult: {
            players: []
        },
        newsLetter: {
            name: null,
            email: null,
            subscribed: false
        },
        contactForm: {
            firstName: null,
            lastName: null,
            email: null,
            phoneNumber: null,
            message: null,
            messageSent: false
        },
        predictedFixture: {
            fixtureGuid: null,
            teamId: null,
            team: null,
            prediction: null
        },
        predictions: [
            {
                name: 'Please select one'
            },
            {
                name: 'Win'
            }, {
                name: 'Draw'
            }]
    },
    computed: {
    },
    destroyed: function () { },
    methods: {
        processOnlyOneTimePrediction: function () {        
            this.processingPrediciton = false;
            if (this.myPredictions !== null && this.myPredictions !== undefined) {
                this.myPredictions.forEach(function (element) {                    
                    pos = sportsmanduApp.allMatches.map(function (e) { return e.guid; }).indexOf(element.fixtureGuid);
                    if (pos >= 0) {
                        sportsmanduApp.allMatches[pos].myPrediction = element.predictionText;
                    } else {
                        console.log("");
                    }
                });
            }
        },
        getPrediction: function (guid) {
            if (this.myPredictions !== null && this.myPredictions !== undefined) {
                var prediction = this.myPredictions.filter(function (item) {
                    return item.fixtureGuid === guid;
                });
                if (prediction[0] !== null && prediction[0] !== undefined) {                    
                    return "Already Predicted";
                }
            }
            return "Predict";
        },
        myPredictedPoints: function () {
            if (this.myPredictions !== null && this.myPredictions !== undefined) {
                if (this.myPrediction !== null) {
                    var prediction = this.myPredictions.filter(function (item) {
                        return item.predictionSucceded === true;
                    });
                    return prediction.length * 3;
                }
            }
            return 0;
        },
        getTeamById: function (id) {
            console.log(id);
        },
        setPrediction: function (guid) {
            if (this.myPredictions !== null && this.myPredictions !== undefined) {
                var prediction = this.myPredictions.filter(function (item) {
                return item.fixtureGuid === guid;
            })[0];
                if (prediction !== null && prediction !== undefined) {
                    this.predictedFixture.team = prediction.team;
                    this.predictedFixture.teamId = prediction.teamId;
                    this.predictedFixture.prediction = prediction.prediction;
                }
            }
            return null;
        },
        savePrediction: function () {
            sportsmanduApp.adminMessage = "Saving Predictions";
            //$('#adminMessage').modal('show');
            dataToSend = this.predictedFixture;
            var fullLink = this.baseUrl + 'Api/SavePrediction/';
            axios({
                method: 'post',
                url: fullLink,
                data: dataToSend
            })
                .then(function (response) {
                    if (!response.data.succeded) {
                        if (response.data.message === "Not Authorized") {
                            window.location.href = "https://sportsmandu.com/Identity/Account/Login?ReturnUrl=%2FNp%2FHome%2FFbPredictions";
                        }
                        toastr.error(response.data.message);
                    } else {
                        toastr.success("Great Prediction Saved !");
                        //setTimeout(function () {
                            //location.reload();
                        //},8000);
                        this.predictedFixture = {
                            fixtureGuid: null,
                            teamId: null,
                            team: null,
                            prediction: null
                        };
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },        
        modifyUrl: function (url) {
            return "/" + url;
        },                
        SubscribeMe: function () {
            this.Search.Culture = this.currentCulture;
            axios({
                method: 'get',
                url: this.baseUrl + 'Api/Subscribe/' + this.currentSegment + "/" + this.currentSegmentId
            })
                .then(function (response) {
                    if (response.data.succeded) {
                        toastr.success("Added to Subscription List");
                    } else {
                        toastr.error("Error Adding To Subscription List");
                    }
                })
                .catch(function (error) {
                    toastr.error(error.message);
                });
        }, SaveNewsletter: function () {
            this.loading = true;
            if (this.newsLetter.name === null) {
                toastr.error("Please Input Your Name");
                return;
            }
            if (this.newsLetter.email === null) {
                toastr.error("Please Input Your Email");
                return;
            }
            dataToSend = this.newsLetter;
            var fullLink = this.baseUrl + 'Api/SaveNewsLetter/';
            axios({
                method: 'post',
                url: fullLink,
                data: dataToSend
            })
                .then(function (response) {
                    if (!response.data.succeded) {
                        toastr.error(response.data.message);
                    } else {
                        sportsmanduApp.newsLetter.subscribed = true;
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
            this.loading = false;
        }, SaveContactForm: function () {
            this.loading = true;
            if (this.contactForm.firstName === null) {
                toastr.error("Please Input Your First Name");
                return;
            }
            if (this.contactForm.email === null) {
                toastr.error("Please Input Your Email");
                return;
            }
            //if (this.contactForm.phoneNumber === null) {
            //    toastr.error("Please Input Your Phone Number");
            //    return;
            //}
            if (this.contactForm.message === null) {
                toastr.error("Please Input Your Message");
                return;
            }
            dataToSend = this.contactForm;
            var fullLink = this.baseUrl + 'Api/SaveContactForm/';
            axios({
                method: 'post',
                url: fullLink,
                data: dataToSend
            })
                .then(function (response) {
                    if (!response.data.succeded) {
                        toastr.error(response.data.message);
                    } else {
                        sportsmanduApp.contactForm.messageSent = true;
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
            this.loading = false;
        }
        //,
        //'handleSearchSubmit': function () {
        //    console.groupCollapsed("New Search Initiated...!");
        //    //console.log(sportsmanduApp.Search);
        //    sportsmanduApp.Search.Culture = sportsmanduApp.currentCulture;
        //    axios({
        //        method: 'post',
        //        url: sportsmanduApp.baseUrl + sportsmanduApp.currentCulture + '/Home/Search',
        //        data: sportsmanduApp.Search
        //    })
        //        .then(function (response) {
        //            //console.log(response.data);
        //            sportsmanduApp.SearchResult = response.data;
        //        })
        //        .catch(function (error) {
        //        });
        //    console.groupEnd();
        //}
    }
});
function SendMyDeviceId() {
    var OnesignalPlayerId;
    OneSignal.getUserId(function (id) {
        OnesignalPlayerId = id;
        axios({
            method: 'get',
            url: sportsmanduApp.baseUrl + 'Api/RegisterDeviceId/WebBrowser/' + OnesignalPlayerId
        })
            .then(function (response) {
                if (response.data.succeded) {
                    toastr.success("Added To DataBase");
                } else {
                    toastr.error("Error : Not Added To Database");
                }
            })
            .catch(function (error) {
                toastr.error(error.message);
            });
    });
}
$(document).ready
    (function () {
        var currentCulture = "";
        try {
            currentCulture = window.location.href.split("/")[3];
        } catch (error) {
            console.log(error);
        }
        sportsmanduApp.currentCulture = currentCulture;
        if (sportsmanduApp.currentCulture === "") {
            sportsmanduApp.currentCulture = "En";
        }
        sportsmanduApp.currentClub = window.location.href.split("/")[window.location.href.split("/").length];
        try {
            if (window.location.href.split('/')[4] === "Season") {
                sportsmanduApp.currentSegment = "Season";
                sportsmanduApp.currentSegmentId = window.location.href.split('/')[5];
            }
            else if (window.location.href.split('/')[5] === "Team") {
                sportsmanduApp.currentSegment = "Team";
                sportsmanduApp.currentSegmentId = window.location.href.split('/')[6];
            }
            else if (window.location.href.split('/')[5] === "Ground") {
                sportsmanduApp.currentSegment = "Ground";
                sportsmanduApp.currentSegmentId = window.location.href.split('/')[6];
            }
            else if (window.location.href.split('/')[6] === "Player") {
                sportsmanduApp.currentSegmentId = window.location.href.split('/')[7];
                sportsmanduApp.currentSegment = "Player";
            }
        }
        catch (error) {            
            console.log(error);
        }
    });
$(document).ready(function () {
    setTimeout(function () {
        if (sportsmanduApp !== null) {
            sportsmanduApp.processOnlyOneTimePrediction();
        }
    }, 3000);
});