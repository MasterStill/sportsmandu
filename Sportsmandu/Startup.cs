﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using AutoMapper;
using Sportsmandu.Models;
using Microsoft.AspNetCore.SignalR;
//using Swashbuckle.AspNetCore.Swagger;
using Sportsmandu.Services;
using Sportsmandu.Data.Infrastructure.Repository;
using Sportsmandu.Services.Abstract;
using Sportsmandu.Data.Infrastructure.Repository.Abstract;
using Community.Data.Infrastructure.Repository;
using Community.Services;
using Community.Services.Abstract;
using Sportsmandu.Api;
using System.Text;
using Sportsmandu.Hubs;
using Microsoft.AspNetCore.Mvc.NewtonsoftJson;
//;using Newtonsoft.Json.Serialization;
//using Newtonsoft.Json;
using Audit.Core;
using Audit.Elasticsearch.Providers;
using System;
using Mapster;
using System.Linq;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using WebMarkupMin.AspNetCore2;
using WebMarkupMin.Core;
using WebMarkupMin.AspNet.Common.Compressors;
using System.IO.Compression;
using System.Collections.Generic;
using Mindscape.Raygun4Net;
using Mindscape.Raygun4Net.AspNetCore;
using Microsoft.AspNetCore.Authentication.OAuth;
using System.Security.Claims;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Sportsmandu.Model.Entities.ViewModels;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

namespace Sportsmandu
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private readonly SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));
        private const string SecretKey = "c1d50d31-d114-45dd-bea6-2dd076c1e1d8";
        private MapperConfiguration _mapperConfiguration { get; set; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            _mapperConfiguration = new MapperConfiguration(cfg =>
            {
                //cfg.AllowNullDestinationValues = false;
                cfg.AddProfile(new AutoMapperProfileConfiguration());
                cfg.CreateMissingTypeMaps = true;
            });

            services.Configure<CookiePolicyOptions>(options =>
            {
                 // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                 options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.None;
            });


            //services.AddTransient<IJWTUserService, JWTUserService>();

            //Audit.Core.Configuration.Setup()
            //.UseSqlServer(options => options
            //    .ConnectionString("Server=(localdb)\\mssqllocaldb;Database=Audit_Sportsmandu;Trusted_Connection=True;MultipleActiveResultSets=true;")
            //    .Schema("dbo")
            //        .TableName("Event")
            //        .IdColumnName("EventId")
            //        .JsonColumnName("Data")
            //        .LastUpdatedColumnName("LastUpdatedDate"));

            //Audit.Core.Configuration.Setup()
            //    .UseElasticsearch(config => config
            //        .ConnectionSettings(new Uri("http://localhost:9200"))
            //        .Index(auditEvent => auditEvent.EventType.ToLower())
            //        .Id(ev => Guid.NewGuid()));

            services.AddSingleton<IMapper>(sp => _mapperConfiguration.CreateMapper());

            //services.AddDbContext<AppDbContext>(options =>
            //    options.UseSqlServer(
            //        Configuration.GetConnectionString("DefaultConnection")));
                
            services.AddDbContext<AppDbContext>(options =>
            //options.UseLazyLoadingProxies()
            options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.Configure<DataProtectionTokenProviderOptions>(o =>
               o.TokenLifespan = TimeSpan.FromHours(3));
            services.AddIdentity<ApplicationUser, ApplicationRole>
            (config =>
            {
                config.SignIn.RequireConfirmedEmail = false;
            })
            .AddEntityFrameworkStores<AppDbContext>()
            .AddUserStore<ApplicationUserStore>()
            .AddUserManager<ApplicationUserManager>()
            .AddRoleStore<ApplicationRoleStore>()
            //.AddRoleManager<ApplicationRoleManager>()
            //.AddSignInManager<ApplicationSignInManager>()
            //.AddDefaultUI(UIFramework.Bootstrap4)
            //.AddDefaultUI()
            .AddDefaultTokenProviders();

            //.AddJsonOptions(opt =>
            // {
            //     var resolver = opt.SerializerSettings.ContractResolver;
            //     if (resolver != null)
            //     {
            //         resolver = new LowercaseContractResolver();
            //         var res = resolver as DefaultContractResolver;
            //         res.NamingStrategy = null;
            //     }
            // })

            services.AddAuthentication()
                .AddFacebook(facebookoptions => {
                    facebookoptions.AppId = "2086666778115750";
                    facebookoptions.AppSecret = "b354b848fda6f16e0caaebfc414f4197";
                    facebookoptions.Fields.Add("picture");
                    facebookoptions.Events = new OAuthEvents
                    {
                        OnCreatingTicket = context =>
                        {
                            var identity = (ClaimsIdentity)context.Principal.Identity;
                            //#NEW
                            //var profileImg = context.User["picture"]["data"].Value<string>("url");
                            //identity.AddClaim(new Claim(ClaimTypes.Anonymous, profileImg));
                            return Task.CompletedTask;
                        }
                    };
                });
            services.AddAuthentication()
            .AddGoogle(googleOptions =>
            {
                googleOptions.ClientSecret = "cUyYW8JgCTuIg_q-q2O9Ap6V";
                googleOptions.ClientId = "925803071256-45120d6q91fmhb9ui77akohgbab65me5.apps.googleusercontent.com";
                googleOptions.Scope.Add("profile");
                googleOptions.Events = new OAuthEvents
                {
                    OnCreatingTicket = context =>
                    {
                        var identity = (ClaimsIdentity)context.Principal.Identity;
                        //#NEW
                        //var profileImg = context.User["picture"];
                        string kka = JObject.Parse(context.User.ToString())["picture"].ToString();
                        identity.AddClaim(new Claim(ClaimTypes.Anonymous, kka));
                        return Task.CompletedTask;
                    }
                };
            });
            //#New
            //.AddMicrosoftAccount(microsoftOptions =>
            //{
            //    microsoftOptions.ClientId = "7880fd57-1678-43e6-bd0a-7907e7ae850a";
            //    microsoftOptions.ClientSecret = "fwnvOWAH08!(-qnuXCS255=";
            //    microsoftOptions.SaveTokens = true;
            //    microsoftOptions.Events = new OAuthEvents
            //    {
            //        OnCreatingTicket = context =>
            //        {
            //            var identity = (ClaimsIdentity)context.Principal.Identity;
            //            var httpClient = new HttpClient();
            //            var pictureResult = httpClient.GetAsync("https://graph.microsoft.com/v1.0/me/photo/$value").Result;
            //            var profileImg = context.User["picture"];
            //            string kka = JObject.Parse(context.User.ToString())["picture"].ToString();
            //            identity.AddClaim(new Claim(ClaimTypes.Anonymous, kka));
            //            return Task.CompletedTask;
            //        }
            //    };
            //});
            //services.AddAuthentication(x =>
            //{
            //    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //})
            //.AddJwtBearer(options =>
            //{
            //    options.TokenValidationParameters =
            //     new TokenValidationParameters
            //     {
            //         LifetimeValidator = (before, expires, token, param) =>
            //         {
            //             return expires > DateTime.UtcNow;
            //         },
            //         ValidateAudience = false,
            //         ValidateIssuer = false,
            //         ValidateActor = false,
            //         ValidateLifetime = true,
            //         IssuerSigningKey = _signingKey,
            //     };
            //    options.Events = new JwtBearerEvents
            //    {
            //        OnMessageReceived = context =>
            //        {
            //            var accessToken = context.Request.Query["access_token"];

            //            // If the request is for our hub...
            //            var path = context.HttpContext.Request.Path;
            //            if (!string.IsNullOrEmpty(accessToken) &&
            //                (path.StartsWithSegments("/Sportsmanduhub")))
            //            {
            //                // Read the token out of the query string
            //                context.Token = accessToken;
            //            }
            //            return Task.CompletedTask;
            //        }
            //    };
            //    options.SaveToken = true;
            //    options.RequireHttpsMetadata = false;
            //    options.Audience = "http://localhost:5000/";
            //    options.Authority = "http://localhost:5000/";
            //});

            services.AddMvc()
                //#NEW
                //Naya ma modified
                //.AddJsonOptions(options =>
                //{
                //    options.ContractResolver = new CamelCasePropertyNamesContractResolver();
                //    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                //})


                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddRaygun(Configuration);
            services.AddCors(options => {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    //.SetIsOriginAllowed((host) => true)
                    .AllowAnyOrigin()
                    //.WithOrigins(new string[] { "http://localhost:5000", "http://sportsmandu.ngrok.io" })
                    .AllowAnyMethod()
                    .AllowAnyHeader());
                    //#new
                    //.AllowCredentials());
            });

            services.AddSignalR(o =>
            {
                o.EnableDetailedErrors = true;
            });

            services.AddTransient<SportsmanduHub, SportsmanduHub>();
            services.AddTransient<IUserIdProvider, EmailBasedUserIdProvider>();
            services.AddTransient<IUserClaimsPrincipalFactory<ApplicationUser>, UserClaimService>();

            //2086666778115750 facebook
            //b354b848fda6f16e0caaebfc414f4197 facebook
            //
            //services.AddAuthorization(options =>
            //{
            //    options.AddPolicy("Admin",
            //                      policy => policy.RequireClaim("Role", "Admin"));
            //    //options.AddPolicy("AllUser",
            //    //                  policy => policy.RequireClaim("AllUser", "AllUser"));
            //});
            //var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));
            //services.Configure<JwtIssuerOptions>(options =>
            //{
            //    options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
            //    options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
            //    options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            //});
            services.AddWebMarkupMin(options =>
            {
                options.AllowMinificationInDevelopmentEnvironment = true;
                options.AllowCompressionInDevelopmentEnvironment = true;
            })
            //.AddHtmlMinification(o =>
            //{
            //    var settings = o.MinificationSettings;
            //    //var jsSettings = new NUglifyJsMinificationSettings
            //    //{
            //    //    LocalRenaming = LocalRenaming.CrunchAll,
            //    //    RemoveUnneededCode = true
            //    //};

            //    settings.RemoveEmptyAttributes = false;
            //    settings.MinifyAngularBindingExpressions = true;
            //    settings.RemoveHtmlComments = true;
            //    settings.RemoveOptionalEndTags = true;
            //    settings.WhitespaceMinificationMode = WhitespaceMinificationMode.Aggressive;
            //    settings.AttributeQuotesRemovalMode = HtmlAttributeQuotesRemovalMode.Html5;
            //    //o.CssMinifierFactory = new NUglifyCssMinifierFactory();
            //    //o.JsMinifierFactory = new NUglifyJsMinifierFactory(jsSettings);
            //})
            .AddHttpCompression(o =>
            {
                o.CompressorFactories = new List<ICompressorFactory>
                {
                    //new BrotliCompressorFactory(new BrotliCompressionSettings { Level = CompressionLevel.Fastest }),
                    new DeflateCompressorFactory(new DeflateCompressionSettings { Level = CompressionLevel.Fastest }),
                    new GZipCompressorFactory(new GZipCompressionSettings { Level = CompressionLevel.Fastest }),
                };
            });
            //services.AddAuthorization(options =>
            //{
            //    options.DefaultPolicy = new AuthorizationPolicyBuilder(
            //    JwtBearerDefaults.AuthenticationScheme).RequireAuthenticatedUser().Build();
            //});
            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            //});
            // services.AddSwaggerGen(c =>
            // {
            //     c.EnableAnnotations();
            //     c.SwaggerDoc("v1", new Info
            //     {
            //         Version = "v1",
            //         Title = "Sportsmandu API",
            //         Description = "Compiled Api for Sportsmandu",
            //         TermsOfService = "None",
            //         Contact = new Contact
            //         {
            //             Name = "Master Still",
            //             Email = string.Empty,
            //             Url = "https://masterstill.com"
            //         },
            //         License = new License
            //         {
            //             Name = "Freeware",
            //             Url = "https://example.com/license"
            //         }
            //     });
            // });

            services.AddTransient<UserManager<IdentityUser>, UserManager<IdentityUser>>();


            services.AddTransient<IFileAndDirectoryService, FileAndDirectoryService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IRoleRepository, RoleRepository>();
            services.AddTransient<ICultureRepository, CultureRepository>();
            services.AddTransient<ICultureService, CultureService>();
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<INotificationService, NotificationService>();

            services.AddTransient<IBlogRepository, BlogRepository>();
            services.AddTransient<IBlogService, BlogService>();            
            //services.AddTransient<IBlogTagRepository, BlogTagRepository>();            
            //services.AddTransient<IBlogCategoryRepository, IBlogCategoryRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            TypeAdapterConfig<Models.League.SeasonFixture, Models.League.SeasonMatchViewModel>.NewConfig()
                           .Map(dest => dest.Teams, src => src.Teams.Select(x => x.Club))
                           .Map(dest => dest.Id, src => src.Guid);


            TypeAdapterConfig<ClubEmployee, EmployeeViewModelApi>.NewConfig()
                .Map(dest => dest.Id, src => src.EmployeeId)
                .Map(dest => dest.Name, src => src.Employee.Name)
                .Map(dest => dest.Image, src => src.Employee.Image)
                .Map(dest => dest.Thumbnail, src => src.Employee.Thumbnail)
                .Map(dest => dest.DesignationId, src => src.DesignationId)
                .Map(dest => dest.DesignationName, src => src.Designation.Name);

            TypeAdapterConfig<Predict, PredictionVMFORES>.NewConfig()
                           .Map(dest => dest.Teams, src => src.Fixture.Teams.Select(x => x.Club));                           


            TypeAdapterConfig<Models.League.SeasonFixtureTeams, Models.League.SeasonFixtureTeamsViewModel>.NewConfig()
                           .Map(dest => dest.Name, src => src.Club.Name)
                           .Map(dest => dest.Id, src => src.Club.Id)
                           .Map(dest => dest.Stats, src=> src.StatsString != null ? JsonConvert.DeserializeObject<List<Model.Generic.ClassFieldTypeForView>>(src.StatsString) : new List<Model.Generic.ClassFieldTypeForView>())
                           .Map(dest => dest.Thumbnail, src => src.Club.Thumbnail);

            TypeAdapterConfig<Models.League.SeasonClubs, Models.League.ClubVMForSeasonVMFORES>.NewConfig()
                           .Map(dest => dest.Name, src => src.Club.Name)
                           .Map(dest => dest.URL, src => src.Club.URL)
                           .Map(dest => dest.Id, src => src.Club.Id)
                           //.Map(dest => dest.Image, src => src.Club.Image)
                           .Map(dest => dest.Thumbnail, src => src.Club.Thumbnail)
                           .Map(dest => dest.Players, src => src.Club.Players)
                           ;


            TypeAdapterConfig<Model.SubCategory, Model.Entities.ViewModels.CategoryViewModel>.NewConfig()
                          .Map(dest => dest.Name, src => src.Category.Name +  " > "  + src.Name);


            TypeAdapterConfig<Model.Entities.Blog.BlogPost, Model.Entities.ViewModels.AdminViewModel>.NewConfig()
                         .Map(dest => dest.Name, src => src.Title);

            TypeAdapterConfig<Club, Model.Club.ViewModels.ClubViewModelFORES>.NewConfig()
                          .Map(dest => dest.Seasons, src => src.Seasons.Select(x => x.Season))
                          .Map(dest => dest.Employee, src => src.Employee.Select(x => x.Employee))
                          .Map(dest => dest.Social, src => src.ClubSocial);

            //.Map(dest => dest.Id, src => src.Guid);
            //TypeAdapterConfig<Models.League.SeasonFixtureTeamsViewModel, Models.League.SeasonMatchViewModel>.NewConfig()
            //              .Map(dest => dest.Teams.Select(y => y.Players), src => src.Players);

            //TypeAdapterConfig<Models.League.SeasonFixture, Models.League.SeasonMatchViewModel>.NewConfig()
            //                .Map(dest => dest.Teams.Select(x=>x.Players), src => src.Teams.Select(y => y.Club.Players.Select(z=>z.Player)));


            

            app.UseCors("CorsPolicy");

            var supportedCultures = new[]
                {
                    new CultureInfo("En"),
                    new CultureInfo("Np"),
                };


                app.UseRequestLocalization(new RequestLocalizationOptions
                {
                    DefaultRequestCulture = new RequestCulture("Np"),
                    // Formatting numbers, dates, etc.
                    SupportedCultures = supportedCultures,
                    // UI strings that we have localized.
                    SupportedUICultures = supportedCultures
                });



            // if (env.IsDevelopment())
            // {
            //     app.UseDeveloperExceptionPage();
            //     app.UseDatabaseErrorPage();
            // }
            // else
            app.UseStatusCodePagesWithReExecute("/Error/{0}");            
            //app.UseExceptionHandler("/Error/500");
            //app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            
            app.UseRaygun();
            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            //app.UseWebMarkupMin();


            // app.UseStaticFiles(new StaticFileOptions
            // {
            //     OnPrepareResponse = ctx =>
            //     {
            //         const int durationInSeconds = 60 * 60 * 24;
            //         ctx.Context.Response.Headers[HeaderNames.CacheControl] =
            //             "public,max-age=" + durationInSeconds;
            //     }
            // });

            //app.UseCookiePolicy();
            //app.UseCors(builder =>
            //{
            //    builder.WithOrigins("http://sportsmandu.ngrok.io")
            //        .AllowAnyHeader()
            //        .WithMethods("GET", "POST")
            //        .AllowCredentials();
            //});


            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<SportsmanduHub>("/sportsmanduhub", options =>
                {
                    // 30Kb message buffer
                    options.ApplicationMaxBufferSize = 102004 * 1024;
                });
            });

            //app.UseSignalR(routes =>
            //{
            //    routes.MapHub<SportsmanduHub>("/sportsmanduhub", options =>
            //    {
            //        // 30Kb message buffer
            //        options.ApplicationMaxBufferSize = 102004 * 1024;
            //    });
            //});
            //app.UseSwagger();


            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            //});
            //string defaultCulture = "En";

            app.UseEndpoints(routes =>
            {
                {
                    //routes.MapRoute(
                    //        name: "store",
                    //         template: "{area:exists}/{controller}/{action}/{id?}",
                    //        defaults: new { Areas = "Store", controller = "Home", action = "Index" });
                    routes.MapControllerRoute(
                                name: "admin",
                                pattern: "{area:exists}/{culture}/{controller}/{action}/{multilingualid?}",
                                defaults: new { Areas = "Admin", controller = "Home", action = "Index" });
                    routes.MapControllerRoute(
                          name: "normalAdmin",
                          pattern: "{area:exists}/{controller}/{action}/{id?}",
                          defaults: new { Areas = "Admin", controller = "Home", action = "Index" });



                    //routes.MapRoute(
                    //       name: "store",
                    //       template: "{area:exists}/{controller}/{action}",
                    //       defaults: new { Areas = "Store", controller = "Home", action = "Index" });
                    routes.MapControllerRoute(
                                       name: "homepage",
                                       pattern: "{Culture=En}/{controller=Home}/{action=Index}");
                    routes.MapControllerRoute(
                        name: "PlayersSingle",
                        pattern: "{culture=En}/{controller=Home}/{action=player}/{id?}");

                    routes.MapControllerRoute(
                    name: "BlogsSingle",
                    pattern: "{culture}/{controller}/{action}/{id}/{title}");


                    routes.MapControllerRoute(
                        name: "TeamsSingle",
                        pattern: "{culture=En}/{controller=Home}/{action=team}/{id?}");

                    routes.MapControllerRoute(
                        name: "GroundsSingle",
                        pattern: "{culture=En}/{controller=Home}/{action=Ground}/{id?}");

                    routes.MapControllerRoute(
                        name: "AssociationSingle",
                        pattern: "{culture=En}/{countryname}/Association/{id?}/{AssociationName}",
                        defaults: new { controller = "Home", Action = "Association" });

                    routes.MapControllerRoute(
                       name: "VenueSingle",
                       pattern: "{culture=En}/{countryname}/Venue/{id?}/{VenueName}",
                       defaults: new { controller = "Home", Action = "Venue" });

                    //routes.MapRoute(
                    //    name: "SeasonSingle",
                    //    template: "{culture=En}/{controller=Home}/{action=Season}/{id?}");
                    routes.MapControllerRoute(
                        name: "LeagueSingle",
                        pattern: "{culture=En}/{controller=Home}/{action=League}/{id?}");
                    //routes.MapRoute(
                    // name: "Identity",
                    // template: "{culture}/{area:exists}/{controller}/{action}/{id}",
                    // defaults: new { Areas = "Identity" });
                    routes.MapControllerRoute(
                               name: "HomePagePlayersSingle",
                               pattern: "{culture}/{countryName}/{categoryName}/{action}/{id}/{playerName}",
                               defaults: new { controller = "Home" });

                    routes.MapControllerRoute(
                              name: "HomePageCLubSingle",
                              pattern: "{culture}/{countryName}/Team/{id}/{teamName}",
                              defaults: new { controller = "Home", Action = "Team" });

                    routes.MapControllerRoute(
                              name: "HomePageGroundSingle",
                              pattern: "{culture}/{countryName}/Ground/{id}/{groundName}",
                              defaults: new { controller = "Home", Action = "Ground" });

                    routes.MapControllerRoute(
                              name: "HomePageSeasonSingle",
                              pattern: "{culture}/Season/{id}/{SeasonName}",
                              defaults: new { controller = "Home", Action = "Season" });

                    routes.MapControllerRoute(
                     name: "Identity",
                     pattern: "{cul" +
                     "ture}/{area:exists}/{controller}/{action}/{id}",
                     defaults: new { Areas = "Identity" });
                }
            });
            //app.UseMvc(routes =>
            //{
            //    //routes.MapRoute(
            //    //        name: "store",
            //    //         template: "{area:exists}/{controller}/{action}/{id?}",
            //    //        defaults: new { Areas = "Store", controller = "Home", action = "Index" });
            //    routes.MapRoute(
            //                name: "admin",
            //                template: "{area:exists}/{culture}/{controller}/{action}/{multilingualid?}",
            //                defaults: new { Areas = "Admin", controller = "Home", action = "Index" });
            //    routes.MapRoute(
            //          name: "normalAdmin",
            //          template: "{area:exists}/{controller}/{action}/{id?}",
            //          defaults: new { Areas = "Admin", controller = "Home", action = "Index" });



            //    //routes.MapRoute(
            //    //       name: "store",
            //    //       template: "{area:exists}/{controller}/{action}",
            //    //       defaults: new { Areas = "Store", controller = "Home", action = "Index" });
            //    routes.MapRoute(
            //                       name: "homepage",
            //                       template: "{Culture=En}/{controller=Home}/{action=Index}");
            //    routes.MapRoute(
            //        name: "PlayersSingle",
            //        template: "{culture=En}/{controller=Home}/{action=player}/{id?}");

            //        routes.MapRoute(
            //        name: "BlogsSingle",
            //        template: "{culture}/{controller}/{action}/{id}/{title}");


            //    routes.MapRoute(
            //        name: "TeamsSingle",
            //        template: "{culture=En}/{controller=Home}/{action=team}/{id?}");

            //    routes.MapRoute(
            //        name: "GroundsSingle",
            //        template: "{culture=En}/{controller=Home}/{action=Ground}/{id?}");

            //    routes.MapRoute(
            //        name: "AssociationSingle",
            //        template: "{culture=En}/{countryname}/Association/{id?}/{AssociationName}",
            //        defaults: new { controller = "Home",Action = "Association"});

            //    routes.MapRoute(
            //       name: "VenueSingle",
            //       template: "{culture=En}/{countryname}/Venue/{id?}/{VenueName}",
            //       defaults: new { controller = "Home", Action = "Venue" });

            //    //routes.MapRoute(
            //    //    name: "SeasonSingle",
            //    //    template: "{culture=En}/{controller=Home}/{action=Season}/{id?}");
            //    routes.MapRoute(
            //        name: "LeagueSingle",
            //        template: "{culture=En}/{controller=Home}/{action=League}/{id?}");
            //    //routes.MapRoute(
            //    // name: "Identity",
            //    // template: "{culture}/{area:exists}/{controller}/{action}/{id}",
            //    // defaults: new { Areas = "Identity" });
            //    routes.MapRoute(
            //               name: "HomePagePlayersSingle",
            //               template: "{culture}/{countryName}/{categoryName}/{action}/{id}/{playerName}",
            //               defaults: new { controller = "Home" });

            //    routes.MapRoute(
            //              name: "HomePageCLubSingle",
            //              template: "{culture}/{countryName}/Team/{id}/{teamName}",
            //              defaults: new { controller = "Home",Action ="Team"});

            //    routes.MapRoute(
            //              name: "HomePageGroundSingle",
            //              template: "{culture}/{countryName}/Ground/{id}/{groundName}",
            //              defaults: new { controller = "Home", Action = "Ground" });

            //    routes.MapRoute(
            //              name: "HomePageSeasonSingle",
            //              template: "{culture}/Season/{id}/{SeasonName}",
            //              defaults: new { controller = "Home", Action = "Season" });
            //});
        }
    }
}