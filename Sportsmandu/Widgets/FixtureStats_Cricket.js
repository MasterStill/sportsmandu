function loadJquery() {
    return new Promise((resolve, reject) => {
        var script = document.createElement('script');
        script.src = "https://code.jquery.com/jquery-3.4.1.min.js";
        document.getElementsByTagName('head')[0].appendChild(script);
        script.onload = () => {
            resolve();
        };
    });
}

//get the data from batting or balling table
function scrapeDataFromBBTable(lists, type) {
    var rowData = lists.children();
    if (type === 'batting') {
        rowData = rowData.slice(0, -1);
    }
    var headingTable = jQuery(rowData[0])
    var heading = jQuery(headingTable.find('th')[0]).text();

    var scoreTitles = headingTable.find('th.imspo_tps__psc')
    scoreTitles = scoreTitles.map((key, title) => {
        return $(title).text()
    });
    //get rid of first heading
    rowData = rowData.slice(1);
    var finalData = { scores: [], heading };
    for (var i = 0; i < rowData.length; i++) {
        let row = rowData[i]
        let dataTr = $(row).find('tr')[1];
        let name = $(dataTr).find('td')[1]
        let toReturn = {
            name: $(name).text(),
        }
        var scoreTitleValues = $(dataTr).find('span.imspo_tps__stat-val');
        var _finalValues = {}
        scoreTitles.map((key, title) => {
            _finalValues[title] = $(scoreTitleValues[key]).text();
        });
        finalData.scores.push({
            name: $(name).text(),
            scores: _finalValues
        })
    }
    return finalData;
}

(function () {
    loadJquery().then(() => {
        var lists1 = jQuery('#second-team-boxscore-round-0_scorecard_1 .imspo_tps__tb');
        var lists2 = jQuery('#first-team-boxscore-round-0_scorecard_0 .imspo_tps__tb');

        var countryNames = $('g-tabs:first > div > div a')
        var list1CName = $(countryNames[1]).text();
        var list2CName = $(countryNames[0]).text();

        //bowling is in 1 and batting is in two
        //go through list 1
        var data = [];
        var batting = scrapeDataFromBBTable($(lists1[0]), 'batting') //batting 
        var bowling = scrapeDataFromBBTable($(lists1[1]), 'bowling') //bowling

        data.push({
            bowling,
            batting,
            country: list1CName
        })

        batting = scrapeDataFromBBTable($(lists1[0]), 'batting') //batting 
        bowling = scrapeDataFromBBTable($(lists1[1]), 'bowling') //bowling

        data.push({
            bowling,
            batting,
            country: list2CName
        })

        console.log(data)
    });
})();