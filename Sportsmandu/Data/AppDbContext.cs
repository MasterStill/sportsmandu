﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Audit.EntityFramework;
using Mapster;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sportsmandu.Model;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Model.Entities.Global;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Entities.Ground;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Generic;
using Sportsmandu.Models;
using Sportsmandu.Models.Association;
using Sportsmandu.Models.Entities.Blog;
using Sportsmandu.Models.League;
using Sportsmandu.Models.Messaging.Notification;
using Sportsmandu.Models.Referral;
using Sportsmandu.Models.Stats;
namespace Sportsmandu.Data
{
    public class AppDbContext : ApplicationDbContext<ApplicationUser, ApplicationRole, int, ApplicationUserClaim, ApplicationUserRole, ApplicationUserLogin, ApplicationRoleClaim, ApplicationUserToken>
    {
        private static DbContextHelper _helper = new DbContextHelper();
        private readonly IAuditDbContext _auditContext;
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
            _auditContext = new DefaultAuditContext(this);
            _helper.SetConfig(_auditContext);
        }
        public override int SaveChanges()
        {
            try
            {
                var AddedEntities = ChangeTracker.Entries<Entity>().Where(E => E.State == EntityState.Added).ToList();
                var ModifiedEntities = ChangeTracker.Entries<Entity>().Where(E => E.State == EntityState.Modified).ToList();
                AddedEntities.ForEach(E =>
                {
                    E.Entity.CreatedDate = DateTime.Now;
                });
                ModifiedEntities.ForEach(E =>
                {
                    E.Entity.LastModifiedDate = DateTime.Now;
                });
                try
                {
                    int x = _helper.SaveChanges(_auditContext, () => base.SaveChanges());
                    return x;
                }
                catch (Exception)
                {
                    return 0;
                }

            }
            catch (Exception ex)
            {
                Global.Error(ex);
                return 0;
            }
        }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _helper.SaveChangesAsync(_auditContext, () => base.SaveChangesAsync(cancellationToken));
        }
        public DbSet<AdminMenu> AdminMenu { get; set; }
        //public DbSet<Advert> Advert { get; set; }
        public DbSet<EmailInvitation> EmailInvitation { get; set; }
        public DbSet<UserNotification> UserNotification { get; set; }
        public DbSet<ApplicationUserRole> ApplicaitonUserRole { get; set; }
        public DbSet<ApplicationUserClaim> ApplicationUserClaim { get; set; }
        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<Alexa> Alexa { get; set; }
        public DbSet<BlogPostBlogTag> BlogPostBlogTag { get; set; }
        public DbSet<BlogCategory> BlogCategory { get; set; }
        public DbSet<BlogTag> BlogTags { get; set; }
        public DbSet<Culture> Cultures { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<ApplicationUserGames> ApplicationUserGames { get; set; }
        public DbSet<LeagueSocial> LeagueSocials { get; set; }
        public DbSet<ClubsSocial> ClubSocials { get; set; }
        public DbSet<AssociationsSocial> AssociationSocials { get; set; }
        public DbSet<PartnersSocial> PartnerSocials { get; set; }
        public DbSet<PlayerSocial> PlayerSocials { get; set; }
        public DbSet<GroundSocial> GroundSocials { get; set; }
        public DbSet<Globalization> Globalizations { get; set; }
        public DbSet<SiteSection> SiteSections { get; set; }
        public DbSet<LanguageTitle> LanguageTitles { get; set; }
        public DbSet<SiteSectionLanguageTitle> SiteSectionLanguageTitles { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Newsletter> Newsletters { get; set; }
        public DbSet<ContactForm> ContactForms { get; set; }
        //public DbSet<PhotoVerification> PhotoVerification { get; set; }
        public DbSet<PhotoVerifications> PhotoVerifications { get; set; }
        public DbSet<ClubCategory> ClubCategory { get; set; }
        public DbSet<ClubEmployee> ClubEmployee { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<StatsDB> StatsDB { get; set; }
        public DbSet<Designation> Designation { get; set; }
        //public DbSet<Sportsmandu.Models.Video> Videosa { get; set; }
        public DbSet<Sportsmandu.Models.Video> Videos { get; set; }

        public DbSet<PlayerConvertedCulture> PlayerConvertedCulture { get; set; }
        public DbSet<GroundConvertedCulture> GroundConvertedCulture { get; set; }
        public DbSet<ClubConvertedCulture> ClubConvertedCulture { get; set; }
        public DbSet<BlogConvertedCulture> BlogConvertedCulture { get; set; }
        public DbSet<SeasonConvertedCulture> SeasonConvertedCulture { get; set; }
        public DbSet<LeagueConvertedCulture> LeagueConvertedCulture { get; set; }
        public DbSet<PartnerConvertedCulture> PartnerConvertedCulture { get; set; }
        public DbSet<Predict> Predict { get; set; }
        public DbSet<AssociationConvertedCulture> AssociationConvertedCulture { get; set; }
        public DbSet<Club> Clubs { get; set; }
        public DbSet<ClubPlayers> ClubPlayers { get; set; }
        public DbSet<SeasonGround> SeasonGround { get; set; }
        public DbSet<Ground> Grounds { get; set; }
        public DbSet<Referral> Referral { get; set; }
        // public DbSet<Brand> Brands { get; set; }
        public DbSet<League> Leagues { get; set; }
        public DbSet<Association> Association { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<SeasonClubs> SeasonClubs { get; set; }
        public DbSet<SeasonClubPlayers> SeasonClubPlayers { get; set; }

        public DbSet<SeasonMatchEvent> SeasonMatchEvent { get; set; }

        //public DbSet<SeasonMatchPlayers> SeasonMatchPlayers { get; set; }



        public DbSet<SeasonCategories> SeasonCategories { get; set; }
        public DbSet<SeasonCategoryClubGroup> SeasonCategoryClubGroups { get; set; }
        public DbSet<SeasonMatch> SeasonMatch { get; set; }
        public DbSet<SeasonFixture> SeasonFixture { get; set; }
        //public DbSet<SeasonStatsDb> SeasonStatistics { get; set; }
        public DbSet<SeasonStatsDb> SeasonStats { get; set; }
        //public DbSet<SeasonStatsDb> SeasonStatistics { get; set; }
        public DbSet<SeasonFixtureTeams> SeasonFixtureTeams { get; set; }
        public DbSet<SeasonFixtureTeamPlayers> SeasonFixtureTeamPlayers { get; set; }

        public DbSet<Partner> Partners { get; set; }
        public DbSet<PartnerTitle> PartnerTitles { get; set; } // Title Sponsor // Medical Sponsor // Dental Sponsor        
        public DbSet<SeasonPartners> SeasonPartners { get; set; }
        public DbSet<PlayerSubscription> PlayerSubscriptions { get; set; }
        public DbSet<ClubSubscription> ClubSubscriptions { get; set; }       
        public DbSet<LeagueSubscription> LeagueSubscriptions { get; set; }
        public DbSet<GroundSubscription> GroundSubscriptions { get; set; }
        public DbSet<SeasonFixtureSubscription> SeasonFixtureSubscriptions { get; set; }
        public DbSet<Models.Gallery.Gallery> Gallery { get; set; }
        public DbSet<FixturePlayerStatDb> FixturePlayerStat { get; set; }
        public DbSet<Models.Gallery.GalleryPhoto> GalleryPhoto { get; set; }
        public List<BlogPostVMFORES> GetAllBlogPostFORES()
        {            
            List<BlogPost> BlogPost = BlogPosts
                .Include(x => x.Culture)
                .Where(x => x.Delflag == false).ToList();
            foreach (BlogPost blogPost in BlogPosts)
            {
                if (blogPost.multilingualid != blogPost.Id)
                {
                    if (blogPost.Image == null)
                    {
                        var image = BlogPosts.Where(x => x.Id == blogPost.multilingualid).SingleOrDefault();
                        blogPost.Image = image.Image;
                    }
                    if (blogPost.Thumbnail == null)
                    {
                        var image = BlogPosts.Where(x => x.Id == blogPost.multilingualid).SingleOrDefault();
                        blogPost.Thumbnail = image.Thumbnail;
                    }
                    if (blogPost.CoverImage == null)
                    {
                        var image = BlogPosts.Where(x => x.Id == blogPost.multilingualid).SingleOrDefault();
                        if (image == null)
                        {
                            blogPost.CoverImage = blogPost.Image;
                        }
                        else
                        {
                            blogPost.CoverImage = image.CoverImage;
                        }
                    }
                }
            }
            List<BlogPostVMFORES> PVM = BlogPosts.Adapt<List<BlogPostVMFORES>>();
            return PVM;
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            //Database.Lof = (query) => Debug.Write(query);
            base.OnModelCreating(builder);
        }
    }

    public class ApplicationRoleStore : RoleStore<ApplicationRole, AppDbContext, int, ApplicationUserRole, ApplicationRoleClaim>
    {
        public ApplicationRoleStore(AppDbContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }
        protected override ApplicationRoleClaim CreateRoleClaim(ApplicationRole role, Claim claim)
        {
            throw new NotImplementedException();
        }
    }
    public class ApplicationUserStore : UserStore<ApplicationUser, ApplicationRole, AppDbContext, int, ApplicationUserClaim, ApplicationUserRole, ApplicationUserLogin, ApplicationUserToken, ApplicationRoleClaim>
    {
        public ApplicationUserStore(AppDbContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }
        private bool _disposed;
        private DbSet<ApplicationUserLogin> _logins;
        private DbSet<ApplicationUserRole> _roles;
        private DbSet<ApplicationUserClaim> _claims;
        public virtual enumClaimTypeSegments Segment { get; set; }
        public virtual string SegmentValue { get; set; }
        private DbSet<ApplicationUserLogin> Logins
        {
            get { return _logins ?? (_logins = Context.Set<ApplicationUserLogin>()); }
        }
        private DbSet<ApplicationUserRole> Roles
        {
            get { return _roles ?? (_roles = Context.Set<ApplicationUserRole>()); }
        }
        private DbSet<ApplicationUserClaim> Claims
        {
            get { return _claims ?? (_claims = Context.Set<ApplicationUserClaim>()); }
        }
        private void ThrowIfInvalid()
        {
            if (_disposed)
                throw new ObjectDisposedException(GetType().Name);
            if (EqualityComparer<enumClaimTypeSegments>.Default.Equals(Segment, default(int)))
                throw new InvalidOperationException("The TenantId has not been set.");
        }

        protected override ApplicationUserClaim CreateUserClaim(ApplicationUser user, Claim claim)
        {
            ApplicationUserClaim c = new ApplicationUserClaim();
            c.ClaimType = claim.Type;
            c.ClaimValue = claim.Value;
            c.UserId = user.Id;
            return c;
        }
        protected override ApplicationUserLogin CreateUserLogin(ApplicationUser user, UserLoginInfo login)
        {
            ApplicationUserLogin a = new ApplicationUserLogin();
            a.UserId = user.Id;
            a.LoginProvider = login.LoginProvider;
            a.ProviderDisplayName = login.ProviderDisplayName;
            a.ProviderKey = login.ProviderKey;
            return a;
        }
        protected override ApplicationUserToken CreateUserToken(ApplicationUser user, string loginProvider, string name, string value)
        {
            ApplicationUserToken t = new ApplicationUserToken();
            t.UserId = user.Id;
            t.LoginProvider = loginProvider;
            t.Value = value;
            t.Name = name;
            return t;
        }
        protected override ApplicationUserRole CreateUserRole(ApplicationUser user, ApplicationRole role)
        {
            ApplicationUserRole r = new ApplicationUserRole();
            r.Segment = Segment;
            r.SegmentValue = SegmentValue;
            r.RoleId = role.Id;
            Context.UserRoles.Add(r);
            Context.SaveChanges();
            return r;
        }
    }
    interface IEntityDate
    {
        DateTime CreatedDate { set; get; }
        DateTime LastModifiedDate { set; get; }
    }
    class Entity : IEntityDate
    {
        public DateTime CreatedDate { set; get; }
        public DateTime LastModifiedDate { set; get; }
    }
    public class UserClaimService : UserClaimsPrincipalFactory<ApplicationUser, ApplicationRole>
    {
        private readonly AppDbContext _dbContext;
        public UserClaimService(AppDbContext dbContext, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager, IOptions<IdentityOptions> optionsAccessor) : base(userManager, roleManager, optionsAccessor)
        {
            _dbContext = dbContext;
        }
        public override async Task<ClaimsPrincipal> CreateAsync(ApplicationUser user)
        {
            var principal = await base.CreateAsync(user);
            var userClaims = _dbContext.ApplicationUserClaim.Where(x => x.UserId == user.Id).ToList();
            foreach (var claim in userClaims)
            {
                ((ClaimsIdentity)principal.Identity).AddClaim(new Claim(claim.ClaimType, claim.ClaimValue));
            }
            return principal;
        }
    }
}