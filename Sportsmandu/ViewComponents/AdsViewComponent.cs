﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Sportsmandu.Model.Entities.Ground;
using System;
using System.Collections.Generic;
using System.Linq;
namespace Sportsmandu.ViewComponents
{
    [AllowAnonymous]
    public class AdsViewComponent : ViewComponent
    {
        IMemoryCache _cache;

        public AdsViewComponent(IMemoryCache cache)
        {
            _cache = cache;
        }
        public IViewComponentResult Invoke()
        {
            if (this.RouteData.Values["area"] != null && this.RouteData.Values["area"] == "Admin") return View();
            string Culture = this.RouteData.Values["culture"] != null ? this.RouteData.Values["culture"].ToString() : null;
            Culture = Culture == "1" ? Culture = "En" : Culture;
            Culture = Culture == "2" ? Culture = "Np" : Culture;
            if (Culture == null)
            {
                //var KKa= this.HttpContext.Response.Cookies;
                var rqf = Request.HttpContext.Features.Get<IRequestCultureFeature>();
                var culture = rqf.RequestCulture.Culture;
                Culture = culture.Name;
                ;// return View();
            }
            var rqfa = Request.HttpContext.Features.Get<IRequestCultureFeature>();
            var culturea = rqfa.RequestCulture.Culture;
            if (Culture.ToLower() != culturea.Name)
            {
                this.HttpContext.Features.Set<IRequestCultureFeature>(new RequestCultureFeature(new RequestCulture(culture: Culture.ToLower(), uiCulture: Culture.ToLower()), null));
                //SetCurrentThreadCulture(requestCulture)
                this.HttpContext.Response.Cookies.Append(
                    CookieRequestCultureProvider.DefaultCookieName,
                    CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(Culture)),
                    new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
                );
            }
            var _result = Sportsmandu.Api.Controllers.ProductController.SiteSectionLanguageTitle(Culture, "layout",_cache);
            if (_result == null) return View();
            //var KK = this.HttpContext.Response.Cookies;
            foreach (var items in _result)
            {
                try
                {
                    var itsValue = items.LanguageTitleGlobalizations.Where(x => x.CultureCode == Culture).SingleOrDefault();
                    this.HttpContext.Items[items.LanguageTitleName] = itsValue == null ? "ITS NULL" : itsValue.Value;
                }
                catch (Exception) { }
                //ViewData.Add(new KeyValuePair<string, object>(items.LanguageTitleName, items.LanguageTitleGlobalizations
                //            .Where(x => x.CultureCode == Culture).SingleOrDefault().Value));
            }
            //if (this.RouteData.Values["area"] != null && this.RouteData.Values["area"] != "Admin")
            {
                var news = Sportsmandu.Api.Controllers.ProductController.AllBlogPost(Culture,_cache);
                if(news != null)
                {
                    var blogPosts = news.Take(2).ToList();//_context.BlogPosts.Include(x=>x.Culture).OrderByDescending(x => x.PublishDate).Where(x=>x.Culture.Code == Culture).Take(2).ToList();
                    this.HttpContext.Items["Footer_News"] = blogPosts ;
                }
                // var resultTosend = blogPosts.Adapt<List<BlogPostViewModel>>();
            }
            List<GroundVMFORES> venues = Sportsmandu.Api.Controllers.ProductController.GetVenues(Culture).ToList();
            if (venues != null)
            {
                var blogPosts = venues.Where(x=>x.Type == enumVenueType.Venue).Take(8).ToList();//_context.BlogPosts.Include(x=>x.Culture).OrderByDescending(x => x.PublishDate).Where(x=>x.Culture.Code == Culture).Take(2).ToList();
                this.HttpContext.Items["Footer_Venues"] = blogPosts;
            }

            var associations = Sportsmandu.Api.Controllers.ProductController.GetAssociations(Culture);
            this.HttpContext.Items["association_list"] = associations;
            return View();
        }
    }
}