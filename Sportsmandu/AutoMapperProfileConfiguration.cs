using AutoMapper;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Model.Entities.Globalization;
using System.Linq;
using Sportsmandu.Models.Globalization;
using Sportsmandu.Model;
using Sportsmandu.Models;
using Sportsmandu.Model.Club.ViewModels;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Models.League;
using Sportsmandu.Model.Entities.Blog;
using Community.Model.Entities.ViewModels;
using Sportsmandu.Model.Entities.Ground;
using Sportsmandu.Models.Association;

namespace Sportsmandu
{
    public class AutoMapperProfileConfiguration : Profile
    {
        public AutoMapperProfileConfiguration()
        {
            // CreateMap<Brand, MultiLingualViewModel>();
            // CreateMap<Brand, BrandViewModel>();
            // CreateMap<BrandViewModel, Brand>();
            // CreateMap<BrandCreateViewModel, Brand>();

            CreateMap<Category, MultiLingualViewModel>();
            CreateMap<Category, CategoryViewModel>();
            CreateMap<Category, ApiCategoryViewModel>();
            CreateMap<CategoryViewModel, Category>();
            CreateMap<CategoryCreateViewModel, Category>();

            CreateMap<ClubCreateViewModel, Club>();
            CreateMap<ClubViewModel, Club>();
            CreateMap<Club, ClubViewModel>();
            CreateMap<Club, MultiLingualViewModel>();
            //CreateMap<Club, ClubViewModelFORES>()
            //    .ForMember(x => x.Players,opts => opts.MapFrom(x => x.Players));

            CreateMap<Club, ClubViewModelFORES>()
                .ForMember(x => x.Seasons, opts => opts.MapFrom(x => x.Seasons.Select(y => y.Season)))
                .ForMember(x => x.Players, opts => opts.MapFrom(x => x.Players))
                .ForMember(x=>x.Social , opts=>opts.MapFrom(x=>x.ClubSocial))
                .ForMember(x => x.Id, opts => opts.MapFrom(x => x.Guid));
            //;
            //;
            //CreateMap<Club, ClubViewModelFORES>()
                  

            CreateMap<CultureViewModel, Culture>();
            CreateMap<Culture, CultureViewModel>();

            CreateMap<Player, MultiLingualViewModel>();
            CreateMap<Player, PlayerAdminViewModel>();
            CreateMap<PlayerAdminViewModel, Player>();
            CreateMap<PlayerCreateViewModel, Player>();
            CreateMap<Player, PlayerCreateViewModel>();
            CreateMap<Player, PlayerViewModel>();
            CreateMap<PlayerViewModel, Player>();

            //CreateMap<Player, PlayerViewModelEs>()
            //       .ForMember(x => x.Id,
            //          opts => opts.MapFrom(x => x.Guid));
            CreateMap<Player, PlayerViewModelEs>()
                   .ForMember(x => x.Id,
                      opts => opts.MapFrom(x => x.Id)
                      )
                      .ForMember(x => x.Guid,
                      opts => opts.MapFrom(x => x.Guid)
                      )
                   .ForMember(x => x.Seasons,
                                     opts => opts.MapFrom(x => x.Season.Select(y => y.Season)));

            CreateMap<SeasonClubPlayers, SeasonVMForPlayerViewModelEs>() // UsedFor FE
                  .ForMember(x => x.Id,
                     opts => opts.MapFrom(x => x.Season.Guid));


            CreateMap<ClubPlayers, ClubPlayerViewModel>() // UsedFor FE
                  .ForMember(x => x.Id,
                     opts => opts.MapFrom(x => x.Club.Guid))
                     .ForMember(x => x.Name,
                     opts => opts.MapFrom(x => x.Club.Name))
                     .ForMember(x => x.Image,
                     opts => opts.MapFrom(x => x.Club.Image))
                     .ForMember(x => x.URL,
                     opts => opts.MapFrom(x => x.Club.URL))
                     .ForMember(x => x.Thumbnail,
                     opts => opts.MapFrom(x => x.Club.Thumbnail));
            
            CreateMap<BlogPost, BlogPost>();
            CreateMap<BlogPostCreateViewModel, BlogPost>();
            CreateMap<BlogPost, BlogPostViewModel>();
            CreateMap<BlogPost, MultiLingualViewModel>();
            CreateMap<BlogPost, TestViewModel>();



            CreateMap<League, MultiLingualViewModel>();
            CreateMap<League, LeagueAdminViewModel>();
            CreateMap<LeagueAdminViewModel, League>();
            CreateMap<LeagueCreateViewModel, League>();
            CreateMap<League, LeagueCreateViewModel>();
            CreateMap<League, LeagueViewModel>();
            CreateMap<LeagueViewModel, League>();

            CreateMap<Partner, MultiLingualViewModel>();
            CreateMap<Partner, PartnerAdminViewModel>();
            CreateMap<PartnerAdminViewModel, Partner>();
            CreateMap<PartnerCreateViewModel, Partner>();
            CreateMap<Partner, PartnerCreateViewModel>();
            // CreateMap<Partner, PartnerVMFORES>()
            //     .ForMember(x => x.Seasons,
            //     opts => opts.MapFrom(x => x.Seasons));
            // CreateMap<Partner, PartnerViewModel>();
            // CreateMap<PartnerViewModel, Partner>();

            CreateMap<Club, MultiLingualViewModel>();
            CreateMap<Club, ClubAdminViewModel>();
            CreateMap<ClubAdminViewModel, Club>();
            CreateMap<ClubCreateViewModel, Club>();
            CreateMap<Club, ClubCreateViewModel>();
            CreateMap<Club, ClubViewModel>();
            CreateMap<ClubViewModel, Club>();
            CreateMap<Club, ApiClubViewModel>();


            CreateMap<Globalization, MultiLingualViewModel>();
            CreateMap<Globalization, GlobalizationAdminViewModel>();
            CreateMap<GlobalizationCreateViewModel, Globalization>();


            CreateMap<SiteSectionLanguageTitle, SiteSectionLanguageTitleForEs>(); 

            CreateMap<Season, MultiLingualViewModel>();
            CreateMap<Season, SeasonAdminViewModel>();
            CreateMap<SeasonAdminViewModel, Season>();
            CreateMap<SeasonCreateViewModel, Season>();
            CreateMap<Season, SeasonCreateViewModel>();
            CreateMap<Season, SeasonViewModel>();
            CreateMap<SeasonViewModel, Season>();
            CreateMap<SeasonFixture, SeasonMatchViewModel>();
            CreateMap<SeasonMatchCreateViewModel, SeasonFixture>();


            CreateMap<Season, SeasonVMFORES>()
                     .ForMember(x => x.Clubs,
                         opts => opts.MapFrom(x => x.Clubs.Select(y => y.Club)))

                        //  .ForMember(x => x.Clubs.Select(y=>y.Players),
                        //      opts => opts.MapFrom(x => x.Clubs.Select(y => y.Club.Players)))

                        // .ForMember(x => x.Partners,
                        //     opts => opts.MapFrom(x => x.Partners.Select(y => y.Partner)))                                            
                        ;

            CreateMap<SeasonFixture, SeasonMatchViewModel>()
               .ForMember(x => x.Teams,
                       opts => opts.MapFrom(x => x.Teams.Select(y => y.Club)));

            CreateMap<SeasonFixture, SeasonMatchViewModel>()
              .ForMember(x => x.Id,
                      opts => opts.MapFrom(x => x.Guid));

            //CreateMap<SeasonFixture, SeasonMatchViewModel>()
            //   .ForMember(x => x.Teams.Select(z=>z.Players),
            //           opts => opts.MapFrom(x => x.Teams.Select(y => y.Club.Players)));

            CreateMap<ClubPlayers, MultiLingualViewModel>();
            // CreateMap<ClubPlayers, ClubPlayersAdminViewModel>();
            // CreateMap<ClubPlayersAdminViewModel, ClubPlayers>();
            // CreateMap<ClubPlayersCreateViewModel, ClubPlayers>();
            // CreateMap<ClubPlayers, ClubPlayersCreateViewModel>();
            // CreateMap<ClubPlayers, ClubPlayersViewModel>();
            // CreateMap<ClubPlayersViewModel, ClubPlayers>();

            CreateMap<Association, MultiLingualViewModel>();
            CreateMap<Association, AssociationAdminViewModel>();
            CreateMap<AssociationAdminViewModel, Association>();
            CreateMap<AssociationCreateViewModel, Association>();
            CreateMap<Association, AssociationCreateViewModel>();
            CreateMap<Association, AssociationViewModel>();
            CreateMap<AssociationViewModel, Association>();

            CreateMap<Ground, MultiLingualViewModel>();

            CreateMap<Ground, GroundAdminViewModel>();

            CreateMap<GroundAdminViewModel, Ground>();

            CreateMap<GroundCreateViewModel, Ground>();

            CreateMap<Ground, GroundCreateViewModel>();

            CreateMap<Ground, GroundViewModel>();

            CreateMap<GroundViewModel, Ground>();


        }
    }
}