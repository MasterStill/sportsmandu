﻿using Microsoft.AspNetCore.SignalR.Client;
using Mindscape.Raygun4Net.AspNetCore;
using Sportsmandu.Models.Generic;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Sportsmandu.Models;
namespace SportsmanduJyami
{
    public static class JyamiProgram
    {
        private static string WorkerGuid { get; set; }
        private static string Version = "0.001";
        static RaygunClient _raygunClient = new RaygunClient("vABPZc4KddKSDv1YE4NrQ");
        private static bool _disconnectedFromServer = false;
        private static string signalRbaseUrl = "";
        //private static string signalRbaseUrlInternal = "http://localhost:44393/";
        private static string signalRbaseUrlInternal = "http://40.88.142.249/";
        private static readonly HttpClient client = new HttpClient();
        private static HubConnection _connection;
        private static ConnectionState _connectionState = ConnectionState.Disconnected;
        private static HubConnection _Internalconnection;
        private static ConnectionState _internalConnectionState = ConnectionState.Disconnected;
        public static string[] Topics;
        private static string MyName;
        private static string _accessId;
        //private static enumSuperVisorRole _supervisorRole = enumSuperVisorRole.User_Verification;        
        private static enumSuperVisorRole _supervisorRole =  enumSuperVisorRole.Roome_Creation;
        private enum ConnectionState
        {
            Connected,
            Disconnected,
            Faulted
        }
        static void Main(string[] args)
        {
            if (args.Length > 0)
             {
                try
                {
                    if(args[0] == "User_Verification")
                    {
                        _supervisorRole =  enumSuperVisorRole.User_Verification;
                    }
                    else
                    {
                        _supervisorRole = enumSuperVisorRole.Roome_Creation;
                    }
                }
                catch (Exception) { }
            }            
            MyName = Environment.MachineName;
            Console.Write("My Name : " + MyName);
            signalRbaseUrlInternal = "https://localhost:44373/";//MichelCommon.MichelCommon.GetWebUrl();
            WriteLogs("Connecting To URL : " + signalRbaseUrlInternal);
            WriteLogs("Starting TaskForce!!!!" + DateTime.Now.Minute + ":" + DateTime.Now.Second);
            WorkerGuid = MyName;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Maina(args);
        }
        static async void Maina(string[] args)
        {
            StartConnectionAsyncInternal();
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            while (true)
            {
            }
        }
        public static async Task StartConnectionAsyncInternal()
        {
            Console.WriteLine("Console Init....");
            _Internalconnection = new HubConnectionBuilder()
            .WithUrl(signalRbaseUrlInternal + "sportsmanduhub", options =>
            {
                options.Headers["ClientMessage"] = "Console";
                options.Headers["GUID_TO_CONNECT"] = "GUID_TO_CONNECT";
            })
                .Build();
            //_Internalconnection.On<dynamic>("Reload", (ReceivedMessagea) =>
            //{
            //    Reload();
            //});
            _Internalconnection.On<UniversalDataContainer>("onUniversalGet", (UniversalDataContainer) =>
            {
                UniversalGet(UniversalDataContainer);
            });
            //_connection.On<dynamic, dynamic>("Reload", (ASD, dynamic) =>
            //{
            //    string AccessId = GetAccessId();
            //    win(AccessId);
            //});
            _Internalconnection.Closed += async (error) =>
            {
            aa:
                Console.WriteLine("Internal Client Disconnected !");
                _internalConnectionState = ConnectionState.Disconnected;
                await Task.Delay(new Random().Next(5, 10) * 1000);
                try
                {
                    ConnectInternal();
                }
                catch (Exception) { }
                if (_internalConnectionState != ConnectionState.Connected)
                    goto aa;
            };
        bb:
            ConnectInternal();
            if (_internalConnectionState != ConnectionState.Connected)
            {
                await Task.Delay(new Random().Next(10, 15) * 1000);
                goto bb;
            }
        }

        async static void ConnectInternal()
        {
            try
            {
                if (_internalConnectionState != ConnectionState.Connected)
                {
                    Console.WriteLine("Trying To Connect...." + DateTime.Now.Minute + ":" + DateTime.Now.Second + " : " + signalRbaseUrlInternal);
                    await _Internalconnection.StartAsync();
                    _internalConnectionState = ConnectionState.Connected;
                    var kk = new MoreInfoAboutAutmationSupervisor
                    {
                        Guid = "GUID_TO_CONNECT",
                        Role = _supervisorRole,
                        Version = Version,
                        WorkerGuid = WorkerGuid
                    };
                    //await _Internalconnection.InvokeAsync<UniversalDataContainer>("IAmSupervisorForAutomation", $"GUID_TO_CONNECT||{WorkerGuid}||{Version}||{_supervisorRole}");
                    await _Internalconnection.InvokeAsync<UniversalDataContainer>("IAmSupervisorForAutomation", kk);
                    INITInternal();
                    Console.WriteLine("Connected Internal " + DateTime.Now.Minute + ":" + DateTime.Now.Second);
                }
            }
            catch (Exception ex)
            {
                _disconnectedFromServer = true;
                Console.WriteLine("Server Service Not Ready.... " + DateTime.Now.Minute + ":" + DateTime.Now.Second + " " + ex.Message);
            }
        }
        private static void INITInternal()
        {
            if (_disconnectedFromServer == true)
            {

            }
            _disconnectedFromServer = false;
        }
        public static void WriteLogs(string Log)
        {
            Console.WriteLine(Log);
            {
                var environment = AppDomain.CurrentDomain.BaseDirectory;
                string fileName = $"{environment}{"logs.txt"}";
                using (StreamWriter writer = File.AppendText(fileName))
                {
                    writer.WriteLine(Log);
                    if (!Log.Contains("*******") && Log.Trim() != "")
                    {
                        writer.WriteLine(Log + " : " + DateTime.Now);
                    }
                    else
                    {
                    }
                }
            }
        }
        //public static async Task ProcessReceivedMessage(string ReceivedMessagea)
        //{
        //    OnlyMessageType MessageType = JsonConvert.DeserializeObject<OnlyMessageType>((ReceivedMessagea));
        //    UniversalDataContainer UniversalDataContainer = new UniversalDataContainer
        //    {
        //        //AccountId = "ALL",
        //        Command = MessageType.MessageType,
        //        Param = "List",
        //        //Result = JsonConvert.SerializeObject(ReceivedMessage, Formatting.None)
        //        Result = ReceivedMessagea
        //    };
        //    _Internalconnection.InvokeAsync<UniversalDataContainer>("UniversalGet", UniversalDataContainer);                        
        //}       
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            _raygunClient.Send(e.ExceptionObject as Exception);
        }
        static async Task<string> PostURI(Uri u, HttpContent c)
        {
            var response = string.Empty;
            using (var client = new HttpClient())
            {
                HttpResponseMessage result = await client.PostAsync(u, c);
                if (result.IsSuccessStatusCode)
                {
                    response = result.Content.ReadAsStringAsync().Result;
                }
            }
            return response;
        }
        public static async Task UniversalGet(UniversalDataContainer UniversalDataContainer)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();                    
            switch (Enum.Parse<enumCommandAutomationCommand>(UniversalDataContainer.Command))
            {
                case enumCommandAutomationCommand.CREATE_ROOM:
                    Console.WriteLine("INside CREATE_ROOM");
                    string ResponseBackRoomId = string.Empty;
                    process.StartInfo.FileName = @"C:\Users\maste\source\repos\upworkrelated\AutoitAutomation\CompletelyNew\pubg.exe";
                    process.StartInfo.Arguments = "CREATE_ROOM RESPONSEBACKTOKEN PARAM1 PARAM2 PARAM3 PARAM3";
                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.OutputDataReceived += (sender, data) => OutputData(data.Data, UniversalDataContainer);                    
                    process.Start();
                    process.WaitForExit();
                    process.BeginOutputReadLine();
                    process.BeginErrorReadLine();                    
                    Console.WriteLine("STARTGAME FINISHED");
                    break;
                case enumCommandAutomationCommand.VERIFY_USER:
                    Console.WriteLine($"VERIFY_USER_REQUEST");
                    //System.Diagnostics.Process process = new System.Diagnostics.Process();
                    process.StartInfo.FileName = @"C:\Users\maste\source\repos\upworkrelated\AutoitAutomation\CompletelyNew\pubg.exe";
                    var finalArgs = $"VERIFY_USER {UniversalDataContainer.SubParam}";
                    process.StartInfo.Arguments = finalArgs;
                    Console.WriteLine("FINAL_ARGS : " + finalArgs);
                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.OutputDataReceived += (sender, data) => VERIFY_USER_OUTPUT(data.Data, UniversalDataContainer);
                    process.ErrorDataReceived += (sender, data) => ERRORVERIFY_USER_OUTPUT(data, UniversalDataContainer);
                    process.Start();
                    process.WaitForExit();
                    process.BeginOutputReadLine();
                    process.BeginErrorReadLine();                    
                    Console.WriteLine("VERIFY_USER_REQUEST Completed");
                    break;                
                default:
                    Console.WriteLine(UniversalDataContainer.Command + " Received !!");
                    break;
            }
        }
        private static void ERRORVERIFY_USER_OUTPUT(dynamic data, UniversalDataContainer UniversalDataContainer)
        {

        }
        private static void VERIFY_USER_OUTPUT(string data, UniversalDataContainer UniversalDataContainer)
        {
            UniversalDataContainer.Command = enumCommandAutomationCommandResponseBack.USER_VERIFIED.ToString();
            UniversalDataContainer.Message = data;
            _Internalconnection.InvokeAsync<UniversalDataContainer>("UniversalGet", UniversalDataContainer);            
            UniversalDataContainer.Command = "Worker_Free";
            _Internalconnection.InvokeAsync<UniversalDataContainer>("UniversalGet", UniversalDataContainer);
            Console.WriteLine("USER VERIFICATION COMPLETED");
        }
        private static void OutputData(string data, UniversalDataContainer UniversalDataContainer)
        {
            Console.WriteLine("OutputData : " + data);
            if (data != null && data.Trim().Length > 3)
            {
                if (data.Contains("FAILED"))
                {
                    Console.WriteLine(data);
                    goto Exit;
                }
                else if (data.Contains("FINISHED"))
                {
                    Console.WriteLine(data);
                    Console.WriteLine("EXITED : CREATEROOM");
                    UniversalDataContainer.Command = "Worker_Free";
                    _Internalconnection.InvokeAsync<UniversalDataContainer>("UniversalGet", UniversalDataContainer);
                    goto Exit;
                }
                UniversalDataContainer.Command = enumCommandAutomationCommandResponseBack.ROOM_CREATED.ToString();
                UniversalDataContainer.SubParam = data.Trim();
                _Internalconnection.InvokeAsync<UniversalDataContainer>("UniversalGet", UniversalDataContainer);
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                //System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.FileName = @"C:\Users\maste\source\repos\upworkrelated\AutoitAutomation\CompletelyNew\Start_game.exe";
                process.StartInfo.Arguments = "START_GAME RESPONSEBACKTOKEN PARAM1 PARAM2 PARAM3 PARAM3";
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.OutputDataReceived += (sender, data) => OutputData(data.Data, UniversalDataContainer);
                process.Start();
                process.WaitForExit();
                process.BeginOutputReadLine();
                while (process.HasExited)
                {
                    goto Exit;
                }
            Exit:
                Console.WriteLine("");
            }
        }
    }
}