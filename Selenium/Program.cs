﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Diagnostics;
using System.Net.Http;

namespace Selenium
{
    public class Program
    {
        //private static IWebDriver driver;
        private static  string homeURL;
        private static readonly HttpClient client = new HttpClient();
        static void Main(string[] args)
        {
            processEspnInfo();
            return;
            IWebDriver driver = new ChromeDriver(Environment.CurrentDirectory);
            string url = "https://www.alexa.com/siteinfo/sportsmandu.com"; // Google doesn't seem to work properly in IE at the moment...
            string browserName = "chrome";
            //using (var drivera = CreateWebDriver(browserName))
            //{
            driver.Navigate().GoToUrl(url);
            var kk = driver.FindElement(By.Id("card_rank")).Text;
            var globalRank = kk.Split("\r\n")[4].Replace(",","");
            var countryRank = kk.Split("\r\n")[10].Replace(",", "").Replace("#", "");
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync($"https://localhost:44373/Api/Alexa/sportsmandu.com/{globalRank}/{countryRank}");
            var msg = stringTask.Result;
            Debug.WriteLine("sdfdsf");            
        }
        static void processEspnInfo()
        {
            IWebDriver driver = new ChromeDriver(Environment.CurrentDirectory);
            string url = "http://www.espn.in/cricket/series/8039/game/1144489/afghanistan-vs-sri-lanka-7th-match-icc-cricket-world-cup-2019"; // Google doesn't seem to work properly in IE at the moment...
            driver.Navigate().GoToUrl(url);
            var kk = driver.FindElement(By.ClassName("cscore cscore--live cricket")).Text;

        }
        private static IWebDriver CreateWebDriver(string browserName)
        {
            switch (browserName.ToLowerInvariant())
            {
                case "chrome":
                    return new OpenQA.Selenium.Chrome.ChromeDriver();

                case "edge":
                    return new OpenQA.Selenium.Edge.EdgeDriver();

                case "firefox":
                    return new OpenQA.Selenium.Firefox.FirefoxDriver();

                case "internetexplorer":
                    var options = new OpenQA.Selenium.IE.InternetExplorerOptions() { IgnoreZoomLevel = true };
                    return new OpenQA.Selenium.IE.InternetExplorerDriver(options);

                default:
                    throw new NotSupportedException($"The browser '{browserName}' is not supported.");
            }
        }
    }
}