Get /defaultauditcontext/_search
{
    "_source": {
        "includes": [
            "environment.userName",
            // "entityFrameworkEvent.entries",
            "entityFrameworkEvent.entries.action",
            "entityFrameworkEvent.entries.table",
            "entityFrameworkEvent.entries.columnValues"
        ]
    }
}