﻿using Sportsmandu.Models.Telegram;
using Sportsmandu.Models.Telegram.QuestionAnswer;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Reflection;
using Telegram.Bot.Types;
namespace SportsTelegram.ProcessingQuestions
{    
    class FootballEvents
    {
        public static string ProcessForFootBall(Message Message, List<QuestionAnswers> QA, TelegramUserMessageHistory telegramUserMessageHistory)
        {
            long chatId = Message.Chat.Id;
            string username = Message.Chat.Username;
            enumGameEventTypeFootball enumEventType;
            var kk = (Enum.TryParse(telegramUserMessageHistory.Route, out enumEventType));
            string MessageToReturn = string.Empty;
            dynamic MyDynamic = new System.Dynamic.ExpandoObject();
            foreach (var items in QA)
            {
                string route = telegramUserMessageHistory.SubRoute != null && telegramUserMessageHistory.SubRoute != "" ? telegramUserMessageHistory.SubRoute : telegramUserMessageHistory.Route;
                string TypeQuery = $"Sportsmandu.Models.Telegram.QuestionAnswer.{telegramUserMessageHistory.Category}Questions+{route}, Sportsmandu.Models, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
                    Type myType = Type.GetType(TypeQuery);
                var prop = myType .GetProperty(items.Question,
                                       BindingFlags.Public
                                       | BindingFlags.Instance
                                       | BindingFlags.IgnoreCase);
                var question = prop.Name;
                var type = prop.PropertyType.FullName;
                var Answer = SportsTelegram.Global.FetchInnerAnswerForGames(prop.PropertyType.FullName, telegramUserMessageHistory, Message, items.Answer);                
                ((IDictionary<String, Object>)MyDynamic).Add(items.Question, Answer);
            }
            //Type myType = Sportsmandu.Api.Controllers.ProductController.FootballGoalQuestionEffects.GetType();
            //Type myTypea = typeof(Sportsmandu.Api.Controllers.ProductController);
            //string methodName = $"{telegramUserMessageHistory.Category}{telegramUserMessageHistory.Route}QuestionEffects";
            //var kak = myTypea.GetMethod(methodName);
            //MethodInfo dynMethod = Sportsmandu.Api.Controllers.ProductController.GetType().GetMethod(methodName,
            //    BindingFlags.Default | BindingFlags.OptionalParamBinding | BindingFlags.Public | BindingFlags.Instance);
            FootballEvents footballEvents = new FootballEvents();
            //kak.Invoke(footballEvents, new System.Dynamic.ExpandoObject[] { MyDynamic });            
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(MyDynamic);
            string URLTOPost = $"{Global.BaseUrl()}/Api/QuestionEffects/{telegramUserMessageHistory.Category}{telegramUserMessageHistory.Route}QuestionEffects/";
            Uri requestUri = new Uri(URLTOPost);
            var objClint = new System.Net.Http.HttpClient();
            var response = objClint.PostAsync(requestUri, new StringContent(json, System.Text.Encoding.UTF8, "application/json")).Result;
            string responJsonText = response.Content.ReadAsStringAsync().Result;            
            return "sdafadsFS";            
        }
    }
    //class CricketEvents
    //{
    //    public static string ProcessForCricket(Message Message, List<QuestionAnswers> QA, TelegramUserMessageHistory telegramUserMessageHistory)
    //    {
    //        long chatId = Message.Chat.Id;
    //        string username = Message.Chat.Username;
    //        enumGameEventTypeCricket enumEventType;
    //        var kk = (Enum.TryParse(telegramUserMessageHistory.Route, out enumEventType));
    //        string MessageToReturn = string.Empty;
    //        switch (enumEventType)
    //        {
    //            case enumGameEventTypeCricket.Run:
    //                break;
    //            case enumGameEventTypeCricket.Caught_Out:
    //                CricketQuestions.Caught_Out CricketCaughtOutQuestions= new CricketQuestions.Caught_Out();
    //                foreach (var items in QA)
    //                {
    //                    var prop = typeof(CricketQuestions.Caught_Out).GetProperty(items.Question,
    //                                           BindingFlags.Public
    //                                           | BindingFlags.Instance
    //                                           | BindingFlags.IgnoreCase);
    //                    var question = prop.Name;
    //                    var type = prop.PropertyType.FullName;
    //                    var Answer = SportsTelegram.Global.FetchInnerAnswerForGames(prop.PropertyType.FullName, telegramUserMessageHistory, Message, items.Answer);
    //                    CricketCaughtOutQuestions.GetType().GetProperty(question).SetValue(CricketCaughtOutQuestions, Answer, null);
    //                }                    
    //                MessageToReturn = CricketCaughtOutQuestions.Message;
    //                break;
    //            case enumGameEventTypeCricket.Settings:
    //                break;
    //            default:
    //                break;
    //        }
    //      return MessageToReturn;
    //    }
    //}


    //public static string ProcessForFootBall(Message Message, List<QuestionAnswers> QA, TelegramUserMessageHistory telegramUserMessageHistory)
    //{
    //    long chatId = Message.Chat.Id;
    //    string username = Message.Chat.Username;
    //    enumGameEventTypeFootball enumEventType;
    //    var kk = (Enum.TryParse(telegramUserMessageHistory.Route, out enumEventType));
    //    string MessageToReturn = string.Empty;
    //    string TypeQuery = $"Sportsmandu.Models.Telegram.QuestionAnswer.{telegramUserMessageHistory.Category}Questions+{telegramUserMessageHistory.Route}, Sportsmandu.Models, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
    //    Type myType = Type.GetType(TypeQuery);
    //    Type footballGoalQuestions = myType;
    //    foreach (var items in QA)
    //    {
    //        var prop = Type.GetType(TypeQuery).GetProperty(items.Question,
    //                               BindingFlags.Public
    //                               | BindingFlags.Instance
    //                               | BindingFlags.IgnoreCase);
    //        var question = prop.Name;
    //        var Answer = SportsTelegram.Global.FetchInnerAnswerForGames(prop.PropertyType.FullName, telegramUserMessageHistory, Message, items.Answer);
    //        footballGoalQuestions.GetType().GetProperty(question).SetValue(footballGoalQuestions, Answer, null);
    //    }
    //    Sportsmandu.Api.Controllers.ProductController.FootballGoalQuestionEffects(footballGoalQuestions);
    //    //MessageToReturn = footballGoalQuestions.Message;       
    //    MessageToReturn = " sdfsdfdsf ";
    //    return MessageToReturn;
    //}
}
