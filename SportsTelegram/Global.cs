﻿using Mapster;
using Sportsmandu.Models.League;
using Sportsmandu.Models.Telegram;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using static SportsTelegram.Program;
namespace SportsTelegram
{
    public static class Global
    {
        public static string BaseUrl()
        {
            return "http://sportsmandu.ngrok.io";
        }
        public static ReplyKeyboardMarkup GetReplyKeyboardMarkup(IEnumerable<TelegramGenericViewModel> Categories)
        {
            Categories = Categories.Where(y => y.Name != null && y.Name != "");

            List<List<KeyboardButton>> KeyboardButtons = new List<List<KeyboardButton>>();
            List<KeyboardButton> KeyboardButton = new List<KeyboardButton>();

            foreach (var items in Categories)
            {
                KeyboardButton kb = new KeyboardButton();
                kb.Text = items.Name;
                KeyboardButton.Add(kb);
            }
            for (int x = 0; x <= Categories.Count() - 1; x += 3)
            {
                List<KeyboardButton> KeyboardButtona = new List<KeyboardButton>();
                KeyboardButtona.AddRange(KeyboardButton.Skip(x).Take(3));
                KeyboardButtons.Add(KeyboardButtona);
            }
            ReplyKeyboardMarkup ReplyKeyboardMarkup = new ReplyKeyboardMarkup(KeyboardButtons);
            return ReplyKeyboardMarkup;
        }
        public static InlineKeyboardMarkup GetInlineKeyboardMarkup(string Name, string callBackId)
        {
            List<InlineKeyboardButton> lst = new List<InlineKeyboardButton>();
            lst.Add(new InlineKeyboardButton
            {
                Text = Name,
                CallbackData = callBackId,
            });
            List<List<InlineKeyboardButton>> inlineKeyboardButtonList = new List<List<InlineKeyboardButton>>();
            try
            {
                List<InlineKeyboardButton> InlineKeyboardButton = new List<InlineKeyboardButton>();
                InlineKeyboardButton.Add(lst.SingleOrDefault());
                inlineKeyboardButtonList.Add(InlineKeyboardButton);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            var inlineKeyboard = new InlineKeyboardMarkup(inlineKeyboardButtonList);
            return inlineKeyboard;
        }
        public static InlineKeyboardMarkup GetInlineKeyboardMarkup(IEnumerable<TelegramGenericViewModel> TelegramGenericViewModel)
        {
            TelegramGenericViewModel = TelegramGenericViewModel.Where(y => y.Name != null && y.Name != "");
            List<InlineKeyboardButton> lst = new List<InlineKeyboardButton>();
            foreach (var items in TelegramGenericViewModel)
            {
                lst.Add(new InlineKeyboardButton
                {
                    Text = items.Name,
                    //CallbackData = items.Name + ":" + items.Id.ToString()
                    CallbackData = items.Id.ToString(),
                });
            }
            List<List<InlineKeyboardButton>> inlineKeyboardButtonList = new List<List<InlineKeyboardButton>>();
            int noOfLines = 3;
            for (int x = 0; x <= TelegramGenericViewModel.Count() - 1; x += noOfLines)
            {
                try
                {
                    List<InlineKeyboardButton> InlineKeyboardButton = new List<InlineKeyboardButton>();
                    InlineKeyboardButton.AddRange(lst.Skip(x).Take(3));
                    inlineKeyboardButtonList.Add(InlineKeyboardButton);
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            var inlineKeyboard = new InlineKeyboardMarkup(inlineKeyboardButtonList);
            return inlineKeyboard;
        }
        public static T ToEnum<T>(this string value)//, T defaultValue)
        {
            return (T)Enum.Parse(typeof(T), value, true);
            //if (string.IsNullOrEmpty(value))
            //{
            //    return defaultValue;
            //}
            //T result;
            //return Enum.TryParse<T>(value, true, out result) ? result : defaultValue;
        }


        public static Type GetEnumType(string enumName)
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                var type = assembly.GetType(enumName);
                if (type == null)
                    continue;
                if (type.IsEnum)
                    return type;
            }
            return null;
        }

        //var categories = (enumGameEventTypeFootball)Enum.Parse(typeof(enumGameEventTypeFootball), sportCategory.ToString());
        //enumSportCategory enumss;
        //var kk = (Enum.TryParse(sportCategory.ToString(), out enumss));
        public static List<TelegramGenericViewModel> GetListFromEnum()
        {
            // CategoryMaLoopHuna Paryooo            
            string enumSportCategoryFullName = string.Concat("Sportsmandu.Models.Telegram.enumSportCategory, Sportsmandu.Models, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");
            Type sportsCategoryType = Type.GetType(enumSportCategoryFullName);
            foreach (var Category in sportsCategoryType.GetFields())
            {
                if (Category.Name != "value__")
                {
                    enumSportCategory enumss;
                    var kk = (Enum.TryParse(Category.Name, out enumss));
                    Debug.WriteLine(Category.Name);

                    string SportsCategoryType = $"Sportsmandu.Models.Telegram.QuestionAnswer.enumGameEventType{enumss}, Sportsmandu.Models, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
                    //;// string.Concat("Sportsmandu.Models.Telegram.QuestionAnswer." + enumss + "Questions+enumGameEventType" + enumss); // enumGameEventTypeFootball
                    Type eventType = Type.GetType(SportsCategoryType);
                    if (eventType != null)
                        foreach (var Event in eventType.GetFields())
                        {
                            if (Event.Name != "value__")
                            {
                                GameCategoryEventDetail gameCategoryEventDetail = new GameCategoryEventDetail();
                                gameCategoryEventDetail.Category = enumss;
                                gameCategoryEventDetail.GameEventType = Event.Name;
                                string SportsCategorySubType = $"Sportsmandu.Models.Telegram.QuestionAnswer.enumGameEventType{enumss}SubType{Event.Name}, Sportsmandu.Models, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
                                //string SportsCategorySubType = string.Concat(SportsCategoryType + "SubType" + Event.Name);
                                Type subEventType = Type.GetType(SportsCategorySubType);
                                if (subEventType != null)
                                {
                                    foreach (var SubEvent in subEventType.GetFields())
                                    {
                                        if (SubEvent.Name != "value__")
                                        {
                                            gameCategoryEventDetail.GameEventSubType.Add(SubEvent.Name);
                                            Debug.WriteLine(SubEvent.Name);
                                        }
                                    }
                                }
                                _gameCategoryEventDetail.Add(gameCategoryEventDetail);
                            }
                        }
                }
            }
            return new List<TelegramGenericViewModel>();
        }
        public static dynamic FetchInnerAnswerForGames(string Name, TelegramUserMessageHistory telegramUserMessageHistory, Message Message, string Answer, int ParamId = 0)
        {            
            if (Name.Contains("SeasonFixtureTeamsViewModelPlayers"))
            {
                var returnPlayer = telegramUserMessageHistory.SeasonMatch.Teams.Select(x => x.Players);
                foreach (var items in returnPlayer)
                {
                    SeasonFixtureTeamsViewModelPlayers result = items.Where(x => x.Name == Answer).FirstOrDefault(); ;
                    if (result != null) return result;
                }
                return null;
            }
            if (Name.Contains("SeasonFixtureTeamsViewModel"))
            {
                SeasonFixtureTeamsViewModel neededClub = telegramUserMessageHistory.SeasonMatch.Teams.Where(x => x.Name == Answer).SingleOrDefault();
                return neededClub;
            }
            if (Name.Contains("SeasonMatchViewModelTelegram"))
            {
                SeasonMatchViewModelTelegram SeasonFixtureVMForTelegram  =  telegramUserMessageHistory.SeasonMatch.Adapt<SeasonMatchViewModelTelegram>();
                return SeasonFixtureVMForTelegram;
            }
            if (Name.Contains("Boolean"))
            {
                return Answer == "Yes" ? true : false;
            }
            if (Name.Contains("Int32"))
            {
                return Answer;
            }
            return null;
        }
    }
}