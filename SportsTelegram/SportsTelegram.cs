﻿using Sportsmandu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Mapster;
using System.Reflection;
using Telegram.Bot.Types;
using Sportsmandu.Models.League;
using Sportsmandu.Models.Telegram.QuestionAnswer;
using Sportsmandu.Models.Telegram;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using Telegram.Bot.Types.InlineQueryResults;
using System.Net.Http;
using Microsoft.AspNetCore.SignalR.Client;
using Sportsmandu.Models.Generic;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
//using static Sportsmandu.Models.Telegram.QuestionAnswer.FootballQuestions;
namespace SportsTelegram
{
    public class Program
    {
        private static string _baseUrl = "https://sportsmandu.com/";
        private static string _baseUrlSignalR = _baseUrl + "sportsmanduhub";
        private static HubConnection _connection;
        private static readonly HttpClient client = new HttpClient();
        private static ConnectionState _connectionState = ConnectionState.Disconnected;
        private static IMemoryCache cache;
        private enum ConnectionState
        {
            Connected,
            Disconnected,
            Faulted
        }
        public static List<GameCategoryEventDetail> _gameCategoryEventDetail = new List<GameCategoryEventDetail>();
        public static GenericNameToBeDefined _genericNameToBeDefined = null;
        private static List<TelegramUserMessageHistory> _userTelegramMessageHistory = new List<TelegramUserMessageHistory>();
        private static List<Sportsmandu.Models.League.SeasonMatchViewModelTelegram> _allSeason = new List<Sportsmandu.Models.League.SeasonMatchViewModelTelegram>();
        private static readonly TelegramBotClient Bot = new TelegramBotClient("728709019:AAEaE3cjmkMHSDjlU1b0u0kY2JivzRE7PMo");        
        static void Main(string[] args)
        {

            var provider = new ServiceCollection()
                       .AddMemoryCache()
                       .BuildServiceProvider();
            cache = provider.GetService<IMemoryCache>();
            StartConnectionAsync();
            _connection.On<UniversalDataContainer>("onUniversalGet", (incomingMessage) =>
            {
                Program p = new Program();
                p.InvokeAWESOME(incomingMessage.Command, incomingMessage);
            });
            FillGameCategoryEventDetails();
            GetAllMatchFixtures();
            var me = Bot.GetMeAsync().Result;
            Console.Title = me.Username;
            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.OnCallbackQuery += BotOnCallbackQueryReceived;
            Bot.OnInlineQuery += BotOnInlineQueryReceived;
            Bot.OnInlineResultChosen += BotOnChosenInlineResultReceived;
            Bot.OnReceiveError += BotOnReceiveError;
            Bot.StartReceiving(Array.Empty<UpdateType>());
            Console.WriteLine($"Start listening for @{me.Username}");
            Console.ReadLine();
            Bot.StopReceiving();
        }
        public static bool Authorize(Message Message)
        {
            return true;
            GenericResult genericResult = Sportsmandu.Api.Controllers.ProductController.TelegramCheckAuth(Message.Chat.Username);
            if(genericResult.Succeded == false)
            {
                Bot.SendTextMessageAsync(
                                Message.Chat.Id,
                                genericResult.Message,
                                replyMarkup: new ReplyKeyboardRemove());
                return false;
            }
            return true;
        }        
        private static async void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs inlineQueryEventArgs)
        {
            Console.WriteLine($"Received inline query from: {inlineQueryEventArgs.InlineQuery.From.Id}");

            InlineQueryResultBase[] results = {
                new InlineQueryResultLocation(
                    id: "1",
                    latitude: 40.7058316f,
                    longitude: -74.2581888f,
                    title: "New York")   // displayed result
                    {
                        InputMessageContent = new InputLocationMessageContent(
                            latitude: 40.7058316f,
                            longitude: -74.2581888f)    // message if result is selected
                    },

                new InlineQueryResultLocation(
                    id: "2",
                    latitude: 13.1449577f,
                    longitude: 52.507629f,
                    title: "Berlin") // displayed result
                    {
                        InputMessageContent = new InputLocationMessageContent(
                            latitude: 13.1449577f,
                            longitude: 52.507629f)   // message if result is selected
                    }
            };
            await Bot.AnswerInlineQueryAsync(
                inlineQueryEventArgs.InlineQuery.Id,
                results,
                isPersonal: true,
                cacheTime: 0);
        }
        private static List<TelegramGenericViewModel> GetListFromEnum(enumSportCategory sportCategory)
        {
            List<TelegramGenericViewModel> returnString = new List<TelegramGenericViewModel>();
            switch (sportCategory)
            {
                case enumSportCategory.Football:
                    var football = Enum.GetValues(typeof(enumGameEventTypeFootball)).Cast<enumGameEventTypeFootball>().ToList();
                    foreach (var items in football)
                    {
                        returnString.Add(new TelegramGenericViewModel(items.ToString(), items.ToString()));
                    }
                    break;
                case enumSportCategory.Cricket:
                    var cricket = Enum.GetValues(typeof(enumGameEventTypeCricket)).Cast<enumGameEventTypeCricket>().ToList();
                    foreach (var items in cricket)
                    {
                        returnString.Add(new TelegramGenericViewModel(items.ToString(), items.ToString()));
                    }
                    break;
                //case enumSportCategory.Golf:
                //    break;
                //case enumSportCategory.Swimming:
                //    break;
                //case enumSportCategory.Athletics:
                //    break;
                //case enumSportCategory.Archery:
                //    break;
                //case enumSportCategory.Dandi_Biyo:
                //    break;
                //case enumSportCategory.Ice_Hockey:
                //    break;
                //case enumSportCategory.Kabbadi:
                //    break;
                //case enumSportCategory.Volleyball:
                //    break;
                //case enumSportCategory.Table_Tennis:
                //    break;
                //case enumSportCategory.Elephant_Polo:
                //    break;
                //case enumSportCategory.Basketball:
                //    break;
                //case enumSportCategory.Lawn_Tennis:
                //    break;
                //case enumSportCategory.Taekwondo:
                //    break;
                //case enumSportCategory.Badminton:
                //    break;
                //case enumSportCategory.Boxing:
                //    break;
                //case enumSportCategory.Cycling:
                //    break;
                //case enumSportCategory.Field_hockey:
                //    break;
                //case enumSportCategory.Handball:
                //    break;
                //case enumSportCategory.Karate:
                //    break;
                //case enumSportCategory.Kho_kho:
                //    break;
                //case enumSportCategory.Rowing_:
                //    break;
                //case enumSportCategory.Shooting:
                //    break;
                //case enumSportCategory.Squash:
                //    break;
                //case enumSportCategory.Triathlon:
                //    break;
                //case enumSportCategory.Weightlifting:
                //    break;
                //case enumSportCategory.Wushu:
                //    break;
                //case enumSportCategory.Wrestling_:
                //    break;
                //case enumSportCategory.Judo:
                //    break;
                //case enumSportCategory.Paragliding:
                //    break;
                default:
                    break;
            }
            return returnString;
        }

        private static List<TelegramGenericViewModel> SeasonEventType(enumSportCategory Category)
        {
            List<TelegramGenericViewModel> returnString = new List<TelegramGenericViewModel>();
            returnString.AddRange(GetListFromEnum(Category));
            return returnString;
        }
        
        private static async void SendTyping(long ChatId)
        {
            //enumGameEventTypeFootball MyStatus = "Goal".ToEnum(enumGameEventTypeFootball.MisMatch);
            //enumGameEventTypeFootball MyStatus = "Goal".ToEnum<enumGameEventTypeFootball>();
            //enumGameEventTypeFootball gameEventsFootball = "Goal"(enumGameEventTypeFootball)Enum.Parse(typeof(enumGameEventTypeFootball), "Goal", true);
            //enumGameEventTypeCricket gameEventsCricket = (enumGameEventTypeCricket)Enum.Parse(typeof(enumGameEventTypeCricket),"Active");
            await Bot.SendChatActionAsync(ChatId, ChatAction.Typing);
            await Task.Delay(500);
        }

        private static Sportsmandu.Models.Generic.SearchIndexViewModel Search(string SearchTerm)
        {
            var searchResult = Sportsmandu.Api.Controllers.ProductController.Search(new Sportsmandu.Models.Generic.SearchViewModel
            {
                SearchTerm = SearchTerm,
                Culture = "En"
            });
            return searchResult;
        }
        private static List<TelegramGenericViewModel> GetAllMatchFixtures(string param= "SeasonMatch")
        {

            List<TelegramGenericViewModel> stuffsToSend = new List<TelegramGenericViewModel>();
            if (_allSeason.Count > 0)
            {
                _allSeason = new List<SeasonMatchViewModelTelegram>();
            }
            if (!cache.TryGetValue("matchfixtures", out stuffsToSend))
            {

            }else
            {
                return stuffsToSend;
            }
                //TODO PASS CACHE STUFFS 
                foreach (var SeasonMatch in Sportsmandu.Api.Controllers.ProductController.AllSeasonMatch("En",null))//.Where(x=>x.Category.Id == 1))
            {
                bool AddToAllSeason = true;
                foreach (var teams in SeasonMatch.Teams)
                {
                    if (teams.Players.Count == 0)
                    {
                        AddToAllSeason = false;
                        break;
                    }
                }
                if (AddToAllSeason || param == "assigplayertofixture")
                {
                    foreach(var items in SeasonMatch.Teams)
                    {
                        items.Name = items.Name.Trim().Replace("\t", "");
                    }
                    _allSeason.Add(SeasonMatch.Adapt<Sportsmandu.Models.League.SeasonMatchViewModelTelegram>());
                }
            }
            _allSeason = _allSeason.Take(10).ToList();
            //.Where(x=>x.SeasonId == 3041) // 2016 South Asian Games
            //.Where(x=>x.Id == "fa843419-810d-4705-ba25-7d62f2c469da")
            //.Where(x => x.Category.Name == enumSportCategory.Football.ToString() || x.Category.Name == enumSportCategory.Cricket.ToString())
            //.Take(10)
            ;
            List<TelegramGenericViewModel> telegramGenericViewModel = new List<TelegramGenericViewModel>();
            string TeamsInvolved = string.Empty;
            string moreinfo = string.Empty;
            foreach (var season in _allSeason)
            {
                TeamsInvolved = string.Empty;
                moreinfo = string.Empty;

                foreach (var team in season.Teams)
                {
                    TeamsInvolved += " " + team.Name;
                }
                const string text =
                @"
                <b>Season</b> :{3}
                <b>Teams</b> :{1}
                <b>Ground</b> :{0}
                <b>Match Type</b>:{2}";
                moreinfo = string.Format(text, season.Ground.Name, TeamsInvolved, season.Category.Name, season.Season.Name);
                telegramGenericViewModel.Add(new TelegramGenericViewModel
                {
                    Name = TeamsInvolved,
                    Id = param + ":" + season.Id,
                    MoreInfo = moreinfo
                });
            }            
            return telegramGenericViewModel;
        }

        public static async void FinalGenericSend(long chatId, string segment, string data)
        {            
            if (segment == "categories")
            {
                SendTyping(chatId);
                var allCategories = Sportsmandu.Api.Controllers.ProductController.AllCategories("En"); // Enum.GetValues(typeof(enumSportCategory)).Cast<enumSportCategory>().ToList();                
                await Bot.SendTextMessageAsync(
                           chatId,
                           "Choose Menu",
                           replyMarkup: SportsTelegram.Global.GetInlineKeyboardMarkup(allCategories.Adapt<List<TelegramGenericViewModel>>()));

            } else if (segment == "gameEvent") {
                if (_gameCategoryEventDetail.Where(x => x.GameEventType == data).SingleOrDefault() != null)
                {
                    // Yedi SubMenu Cha bhane
                    await Bot.SendTextMessageAsync(
                       chatId,
                       "Choose Menu",
                       replyMarkup: SportsTelegram.Global.GetInlineKeyboardMarkup(_gameCategoryEventDetail.Where(x => x.GameEventType == data).SingleOrDefault().GameEventSubType.Adapt<List<TelegramGenericViewModel>>()));
                }
            } else if (segment == "seasonmatch")
            {
                foreach (var matches in GetAllMatchFixtures())
                {
                    await Bot.SendTextMessageAsync(
                       chatId,
                       matches.MoreInfo,
                       ParseMode.Html,
                       replyMarkup: SportsTelegram.Global.GetInlineKeyboardMarkup(matches.Name, matches.Id)
                       );
                }
            }
            else if (segment == "assigplayertofixture")
            {
                var fixturesToDisplay = GetAllMatchFixtures("assigplayertofixture");
                foreach (var matches in fixturesToDisplay)
                {
                    await Bot.SendTextMessageAsync(
                       chatId,
                       matches.MoreInfo,
                       ParseMode.Html,
                       replyMarkup: SportsTelegram.Global.GetInlineKeyboardMarkup(matches.Name, matches.Id)
                       );
                }
            }
            else if (segment == "uploadphoto")
            {
                if(data == null)
                {
                    await Bot.SendTextMessageAsync(
                             chatId,
                             "Please Enter Search Term",
                             ParseMode.Html,
                                replyMarkup:new ForceReplyMarkup()
                             );
                }
                if( data != null)
                {                    
                    var searchResult = Search(data);
                foreach (var player in searchResult.Players)
                {
                    GenericNameToBeDefined g = new GenericNameToBeDefined
                    {
                        Id = player.Id,
                        RequestType = "Photo",
                        Segment = "Player"
                    };
                    _genericNameToBeDefined = g;
                    if (player.Thumbnail != null)
                    {
                        string file = $"C:\\Users\\MasterStill\\source\\repos\\Sportsmandu\\Sportsmandu\\wwwroot\\{player.Thumbnail}";
                        var fileName = file.Split(Path.DirectorySeparatorChar).Last();
                        using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            await Bot.SendPhotoAsync(
                                chatId,
                                fileStream,
                                null);
                        }
                        await Bot.SendTextMessageAsync(
                             chatId,
                             player.Name,
                             ParseMode.Html,
                                replyMarkup: SportsTelegram.Global.GetInlineKeyboardMarkup(player.Name, JsonConvert.SerializeObject(g))
                             );
                    }
                    else
                    {
                        await Bot.SendTextMessageAsync(
                        chatId,
                        player.Name,
                        ParseMode.Html,
                           replyMarkup: SportsTelegram.Global.GetInlineKeyboardMarkup(player.Name, JsonConvert.SerializeObject(g))
                        );
                    }                  
                }
                }
            }
        }
        public class GenericNameToBeDefined
        {
            public string Segment { get; set; }
            public string RequestType { get; set; } // Photo
            public string Id { get; set; }
        }
        private static void AddToUserHistory(Telegram.Bot.Types.Message Message)
        {
            long ChatId = Message.Chat.Id;
            string Username = Message.Chat.Username;
            var userHistory = _userTelegramMessageHistory.Where(x => x.ChatId == ChatId).Where(x => x.UserName == Username).SingleOrDefault();
            if (Message.Text == "Back") return;
            if (userHistory == null)
            {
                _userTelegramMessageHistory.Add(new TelegramUserMessageHistory
                {
                    ChatId = ChatId,
                    Message = new List<string>(new string[] { Message.Text }),// message.Text,
                    UserName = Username
                });
            }
            else
            {
                userHistory.Message.Add(Message.Text);
            }
        }
        private static TelegramUserMessageHistory MyHistory(Telegram.Bot.Types.Message Message)
        {
            TelegramUserMessageHistory t = new TelegramUserMessageHistory();
            long ChatId = Message.Chat.Id;
            string username = Message.Chat.Username;
            t = _userTelegramMessageHistory.Where(x => x.UserName == username).Where(x => x.ChatId == ChatId).SingleOrDefault();
            return t;
        }      
        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            Message message = messageEventArgs.Message;
            
            if (message.Type == MessageType.Photo) {
                Console.WriteLine("Photo Message Received !!");
                var test = Bot.GetFileAsync(message.Photo[message.Photo.Count() - 1].FileId).Result;                
                System.IO.FileStream fileStream;
                string fileName = _genericNameToBeDefined.Segment + "_" + _genericNameToBeDefined.Id + "_" + message.Chat.Username + "_" + Guid.NewGuid() + ".jpg";
                string fullpath = @"c:\telegramtest\" + fileName;
                using (fileStream = System.IO.File.OpenWrite(fullpath))
                {
                    Console.WriteLine("Processing Photo....");
                    var file = Bot.GetInfoAndDownloadFileAsync(
                        fileId: test.FileId,
                        destination: fileStream
                    ).Result;
                    Console.WriteLine("Photo Processed And Photo Saved....");
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(Global.BaseUrl());
                        var content = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("Segment", _genericNameToBeDefined.Segment),
                            new KeyValuePair<string, string>("RequestType", _genericNameToBeDefined.RequestType),
                            new KeyValuePair<string, string>("Guid ", _genericNameToBeDefined.Id),
                            new KeyValuePair<string, string>("TelegramUserName", message.Chat.Username),
                            new KeyValuePair<string, string>("FileName", fileName)
                        });
                        var result = client.PostAsync("/Api/AddPhotosForVerification", content).Result;
                        string resultContent = result.Content.ReadAsStringAsync().Result;
                        Console.WriteLine(resultContent);
                        }
                   
                    await Bot.SendTextMessageAsync(
                         message.Chat.Id,
                         "Great Photo Updated ! ",
                         replyMarkup: new ReplyKeyboardRemove());                   
                    List<TelegramGenericViewModel> returnString = new List<TelegramGenericViewModel>();
                    _genericNameToBeDefined = null;
                    // return;
                }
                var newPhoto = Sportsmandu.Global.FrontEndUseChangeBackground(fullpath, SportsmanduImage.enumImageSegment.club_profile,true);
                using (var fileStreama = new FileStream(newPhoto, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    await Bot.SendPhotoAsync(
                        message.Chat.Id,
                        fileStreama,
                        null);
                }
            }
            else
            {
                if (_genericNameToBeDefined != null)
                {
                    await Bot.SendTextMessageAsync(
                               message.Chat.Id,
                               "Cool, Send me the Photo",
                               replyMarkup: new ReplyKeyboardRemove());
                    List<TelegramGenericViewModel> returnString = new List<TelegramGenericViewModel>();
                    return; 
                }
                if (message.ReplyToMessage != null){
                    Console.WriteLine("Reply Came of : " + message.ReplyToMessage.Text);
                    if(message.ReplyToMessage.Text == "Please Enter Search Term")
                    {
                        FinalGenericSend(message.Chat.Id, "uploadphoto",message.Text);
                    }
                }
            }
            if (message == null || message.Type != MessageType.Text) return;
            if (Authorize(message) == false) return;
            long ChatId = message.Chat.Id;
            string username = message.Chat.Username;
            Console.WriteLine($"Received Message @{message.Text} From @{username}");
            AddToUserHistory(message);
            var allMatchFixtures = GetAllMatchFixtures();
            var MyHistory = Program.MyHistory(message);
            if (MyHistory.Awaiting_Answer && !message.Text.Contains("/start"))
            {
                //!!TODO WRITE TO ANOTHER PART OF THE CODE THE BELOW LINE DOESNT BELONGS HERE ## IMP
                //if(MyHistory.Settings == null)
                //{
                //    message.Text = "Settings";
                //}
                AskQuestions(message);
                return;
            }
            switch (message.Text.Split(' ').First())
            {
                //case "/categories":
                //    FinalGenericSend(message.Chat.Id, "categories", null);
                //    break;
                case "/seasonmatch":
                    FinalGenericSend(message.Chat.Id, "seasonmatch", null);
                    break;
                case "/uploadphoto":
                    FinalGenericSend(message.Chat.Id, "uploadphoto", null);
                    break;
                case "/assigplayertofixture":
                    FinalGenericSend(message.Chat.Id, "assigplayertofixture", null);
                    break;
                default:
                    //FillGameCategoryEventDetails(); // TODO fetch from DB
                    if (message.Text == "Back")
                    {
                        var backMenu = MyHistory.Message.Skip(MyHistory.Message.Count() - 1).Take(1).SingleOrDefault();
                        break;
                    } else if (message.Text != "Back" && MyHistory.SeasonMatch != null && _gameCategoryEventDetail.Where(x=>x.Category.ToString() == MyHistory.SeasonMatch.Category.Name.ToString()).Where(x => x.GameEventType == message.Text).SingleOrDefault() != null)
                    {
                        // Main Menu Ho Yo                        
                        MyHistory.Route = message.Text;
                        // Yedi Sub Menu cha bhan process this
                        var data = _gameCategoryEventDetail.Where(x => x.GameEventType == message.Text).FirstOrDefault().GameEventSubType;
                        if (data.Count > 0)
                        {
                            List<TelegramGenericViewModel> returnString = new List<TelegramGenericViewModel>();
                            foreach (var items in data)
                            {
                                returnString.Add(new TelegramGenericViewModel(items.ToString(), items.ToString()));
                            }
                            if (returnString.Count > 0)
                                await Bot.SendTextMessageAsync(
                                   ChatId,
                                   "Choose Menu",
                                   replyMarkup: SportsTelegram.Global.GetReplyKeyboardMarkup(returnString.Adapt<List<TelegramGenericViewModel>>()));
                        }
                        else
                        {
                            AskQuestions(message);
                        }
                    }
                    else if (message.Text != "Back" && _gameCategoryEventDetail.Where(x => x.GameEventSubType.Contains(message.Text)).SingleOrDefault() != null)
                    {
                        // Question Answer Part Suru Huncha Aba !!!                       
                        // SUB TYPE                        
                        MyHistory.SubRoute = message.Text;
                        AskQuestions(message);
                    }
                    else if (MyHistory.SeasonMatch != null && _gameCategoryEventDetail.Where(x=>x.Category.ToString() == MyHistory.SeasonMatch.Category.Name).Where(x => x.GameEventType.Contains(message.Text)).SingleOrDefault() != null)
                    {
                        // Question Answer Part Suru Huncha Aba !!!                       
                        // No SUB TYPE
                        AskQuestions(message);
                    }
                    else
                    {
                        MyHistory.Route = "";
                        MyHistory.SubRoute = "";
                        MyHistory.QuestionAnswers = new List<QuestionAnswers>();
                        MyHistory.Awaiting_Answer = false;
                        const string usage = @"
                        /seasonmatch - View Matches For Reporting
                        /uploadphoto - Upload Photos
                        /assigplayertofixture - Assign Players to Match
                        ";
                        await Bot.SendTextMessageAsync(
                            message.Chat.Id,
                            usage,
                            replyMarkup: new ReplyKeyboardRemove());
                        List<TelegramGenericViewModel> returnString = new List<TelegramGenericViewModel>();
                    }                    
                    break;
            }
        }
        private async static void AskQuestions(Message message)
        {            
            long ChatId = message.Chat.Id;
            string username = message.Chat.Username;
            var myHistory = MyHistory(message);
            var category = myHistory.Category; //"Cricket";
            var menu = myHistory.Route; ;// "Out";
            var SubMenu = myHistory.SubRoute;                        
            string TypeQuery = $"Sportsmandu.Models.Telegram.QuestionAnswer.{category}Questions+{SubMenu}, Sportsmandu.Models, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            //"Sportsmandu.Models.Telegram.QuestionAnswer.FootballQuestions+Goal, Sportsmandu.Models, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"
            //"Sportsmandu.Models.Telegram.QuestionAnswer.FootballQuestions, Sportsmandu.Models, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"
            //Type type = Type.GetType(FootballQuestions);
            Type myType = null;
            myType = Type.GetType(TypeQuery);
            if (myType == null)
            {
                TypeQuery = $"Sportsmandu.Models.Telegram.QuestionAnswer.{category}Questions+{menu}, Sportsmandu.Models, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";                
                myType = Type.GetType(TypeQuery);
            }
            if (myType == null)
                return;            
            if (myHistory.Current_Match_Id == null)
            {
                //No Season Selected !
                foreach (var matches in GetAllMatchFixtures())
                {
                    await Bot.SendTextMessageAsync(
                       ChatId,
                       matches.MoreInfo,
                       ParseMode.Html,
                       replyMarkup: SportsTelegram.Global.GetInlineKeyboardMarkup(matches.Name, matches.Id)
                       );
                }
                return;
            }
            if (myHistory.Awaiting_Answer)
            {                
                    myHistory.QuestionAnswers.LastOrDefault().Answer = message.Text;                
            }
            string lastQuestion = string.Empty;
            foreach (var property in myType.GetRuntimeProperties())
            {
                if (myHistory.Awaiting_Answer)
                {
                    if (!property.Name.Contains("Do_Not_Ask"))
                    {
                        myHistory.QuestionAnswers.LastOrDefault().Answer = message.Text;
                    }
                }
                if (!myHistory.QuestionAnswers.Select(x => x.Question).Contains(property.Name))
                {
                    if (property.Name != "Final_Question")
                        Console.WriteLine($"Asking Question To @{username}  -->> @{property.Name}");

                    if (property.PropertyType.Name == "SeasonFixtureTeamsViewModel") ////"Clubs"
                    {
                        List<SeasonFixtureTeamsViewModel> teamsToChoose = myHistory.SeasonMatch.Teams;
                        if (property.Name.Contains("BowlingTeam"))
                        {
                            //_userTelegramMessageHistory.Where(x=>x.ChatId == message.Chat.Id).SingleOrDefault().QuestionAnswers.Add(new QuestionAnswers
                            //{
                            //    Question = property.Name,
                            //    Answer = 
                            //});
                            myHistory.QuestionAnswers.Add(new QuestionAnswers
                            {
                                Question = property.Name,
                            });
                            message.Text = myHistory.SeasonMatch.Teams.Where(x => x.Name != message.Text).SingleOrDefault().Name;
                            await Bot.SendTextMessageAsync(
                               ChatId,
                               "Great We Recorded " + message.Text + " As Bowling Team",
                               replyMarkup: new ReplyKeyboardRemove());
                            AskQuestions(message);
                        }
                        else
                        {
                            await Bot.SendTextMessageAsync(
                               ChatId,
                               property.Name,
                               replyMarkup: SportsTelegram.Global.GetReplyKeyboardMarkup(teamsToChoose.Adapt<List<TelegramGenericViewModel>>()));
                        }
                    } else if (property.PropertyType.Name == "SeasonFixtureTeamsViewModelPlayers") //"Player"
                    {
                        string MySelectedTeam = string.Empty;
                        if(property.Name == "Baller" || property.Name == "Wicket_Keeper")
                        {
                            MySelectedTeam = myHistory.QuestionAnswers.Where(x => x.Question.Contains("Bowling")).SingleOrDefault().Answer;
                        }else if (property.Name == "Batter" || property.Name == "Runner")
                        {
                            MySelectedTeam = myHistory.QuestionAnswers.Where(x => x.Question.Contains("Batting")).SingleOrDefault().Answer;
                        }                        
                        else if(menu == enumGameEventTypeCricket.Out.ToString())
                        {
                            if (property.Name == "Do_Not_Ask_Who_Was_Out")
                            {
                                myHistory.QuestionAnswers.Add(new QuestionAnswers
                                {
                                    Question = property.Name,
                                    Answer = myHistory.Settings.Where(x => x.Question == "Batter").SingleOrDefault().Answer
                                });
                                message.Text = myHistory.Settings.Where(x => x.Question == "Batter").SingleOrDefault().Answer;
                                await Bot.SendTextMessageAsync(
                                   ChatId,
                                   "Great We Recorded " + myHistory.Settings.Where(x => x.Question == "Batter").SingleOrDefault().Answer + " Was Out ",
                                   replyMarkup: new ReplyKeyboardRemove());
                                AskQuestions(message);
                            }
                            MySelectedTeam = myHistory.Settings.Where(x => x.Question.Contains("Bowling")).Where(x=>x.Answer != null).SingleOrDefault().Answer;
                        }
                        else
                        {
                            try
                            {
                                MySelectedTeam = myHistory.QuestionAnswers.Where(x => x.Question.Contains("Which_Team")).SingleOrDefault().Answer;
                            }
                            catch (Exception)
                            {
                                MySelectedTeam = myHistory.Settings.Where(x => x.Question.Contains("BowlingTeam")).SingleOrDefault().Answer;
                            }

                        }
                        var PlayersToChoose =   myHistory.SeasonMatch
                                                .Teams.Where(x => x.Name == MySelectedTeam)
                                                .Select(x => x.Players).SingleOrDefault();
                        await Bot.SendTextMessageAsync(
                           ChatId,
                           property.Name,
                           replyMarkup: SportsTelegram.Global.GetReplyKeyboardMarkup(PlayersToChoose.Adapt<List<TelegramGenericViewModel>>()));
                    }
                    else if (property.PropertyType.Name == "Boolean")
                    {
                        if (property.Name == "Final_Question")
                        {
                            FillUpWithMenu(message, myHistory.QuestionAnswers);
                            myHistory.Awaiting_Answer = false;
                            myHistory.QuestionAnswers = new List<QuestionAnswers>();                            
                        }
                        else
                        {
                            List<TelegramGenericViewModel> buttons = new List<TelegramGenericViewModel>();
                            buttons.Add(new TelegramGenericViewModel
                            {
                                Name = "Yes",
                                Id = "true"
                            });
                            buttons.Add(new TelegramGenericViewModel
                            {
                                Name = "No",
                                Id = "false"
                            });
                            await Bot.SendTextMessageAsync(
                               ChatId,
                               property.Name,
                               replyMarkup: SportsTelegram.Global.GetReplyKeyboardMarkup(buttons));//.Adapt<List<TelegramGenericViewModel>>()));
                                                                             //if (property.Name == name && property.CanRead)
                        }
                    }
                    else if (property.PropertyType.Name == "int32") {
                        await Bot.SendTextMessageAsync(
                               ChatId,
                               "Please Input the Number Required",
                               replyMarkup: new ReplyKeyboardRemove());
                    }
                    else if (property.PropertyType.Name == "Double")
                    {
                        await Bot.SendTextMessageAsync(
                               ChatId,
                               property.Name,
                               replyMarkup: new ReplyKeyboardRemove());
                    }
                    if (property.Name != "Final_Question")
                    {
                        myHistory.Awaiting_Answer = true;
                        if (!property.Name.Contains("Do_Not_Ask"))
                        {
                            myHistory.QuestionAnswers.Add(new QuestionAnswers
                            {
                                Question = property.Name,
                            });
                        }
                    }                    
                    return;
                }
            }
        }
        private async static void FillUpWithMenu(Message Message, List<QuestionAnswers> QuestionAnswers)
        {                       
            long chatId = Message.Chat.Id;
            var myHistory = MyHistory(Message);
            if(myHistory.Route == "Settings")
            {                
                _userTelegramMessageHistory.Where(x => x.UserName == Message.Chat.Username).SingleOrDefault().Settings = QuestionAnswers;
                if (_allSeason.Where(x => x.Guid == _userTelegramMessageHistory.Where(y => y.UserName == Message.Chat.Username).SingleOrDefault().SeasonMatch.Guid).SingleOrDefault().Settings == QuestionAnswers) ;
            }
            enumSportCategory enumSportCategory;
            var sportCategory = _allSeason.Where(x => x.Id == myHistory.Current_Match_Id).SingleOrDefault().Category.Name;
            var kk = (Enum.TryParse(sportCategory, out enumSportCategory));
            QuestionAnswers.Add(new Sportsmandu.Models.Telegram.QuestionAnswers
            {
                Question = "Question_Which_SeasonFixture",
                Answer = myHistory.Current_Match_Id
            });
            QuestionAnswers.Add(new Sportsmandu.Models.Telegram.QuestionAnswers
            {
                Question = "ParamGUID",
                Answer = Guid.NewGuid().ToString()
            });
            ProcessEventDetails(Message, QuestionAnswers,myHistory);
            await Bot.SendTextMessageAsync(
                                chatId,
                                $"Great Event Recorded",
                            replyMarkup: SportsTelegram.Global.GetReplyKeyboardMarkup(SeasonEventType(enumSportCategory)));
        }
        private static void ProcessEventDetails(Message Message, List<QuestionAnswers> QuestionAnswers,TelegramUserMessageHistory telegramUserMessageHistory){                        
            var myHistory = telegramUserMessageHistory;
            var eventResponse = SportsTelegram.ProcessingQuestions.FootballEvents.ProcessForFootBall(Message, QuestionAnswers, telegramUserMessageHistory);
            //SET TO SETTINGS             
            //if (myHistory.Category == enumSportCategory.Football)
            //{
            //    Bot.SendTextMessageAsync(
            //                    Message.Chat.Id,
            //                    eventResponse);
            //}else if(myHistory.Category == enumSportCategory.Cricket)
            //{
            //    var eventResponse = SportsTelegram.ProcessingQuestions.CricketEvents.ProcessForCricket(Message, QuestionAnswers, telegramUserMessageHistory);
            //    Bot.SendTextMessageAsync(
            //                    Message.Chat.Id,
            //                    eventResponse);
            //}
        }             
        private static async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            //if (_allSeason.Count() == 0)
            //{
            //    _allSeason = Sportsmandu.Api.Controllers.ProductController.AllSeasonMatch("En").ToList().Adapt<List<Sportsmandu.Models.League.SeasonMatchViewModelTelegram>>();
            //}
            var callbackQuery = callbackQueryEventArgs.CallbackQuery;
            var Message = callbackQuery.Message;
            long chatId = Message.Chat.Id;
            string username = Message.Chat.Username;
            string data = callbackQuery.Data;
            Console.WriteLine($"Received Message @{Message.Text} From @{username}");
            AddToUserHistory(Message);
            SendTyping(chatId);
            if (data.Replace(" ", "_").Split(":")[0] == "SeasonMatch")
            {
                //int id = Int32.Parse(data.Replace(" ", "_").Split(":")[1]);
                string id = data.Replace(" ", "_").Split(":")[1];
                var currentSeason = _allSeason.Where(x => x.Id == id).SingleOrDefault();
                await Bot.AnswerCallbackQueryAsync(
                    callbackQuery.Id,
                    //$"Received {callbackQuery.Data.Replace(" ", "_").Split(":")[0]}");
                    $"Great now Reporting to ");//The Season {currentSeason.Teams} VS { currentSeason.TeamTwo.Name }");
                enumSportCategory enumSportCategory;
                var sportCategory = currentSeason.Category.Name;
                var kk = (Enum.TryParse(sportCategory, out enumSportCategory));
                _userTelegramMessageHistory.Where(x => x.UserName == username).Where(x => x.ChatId == chatId).SingleOrDefault().Category = enumSportCategory;                
                _userTelegramMessageHistory.Where(x => x.UserName == username).Where(x => x.ChatId == chatId).SingleOrDefault().SeasonMatch = currentSeason;                
                if (callbackQuery.Data.Contains(":")) {
                    await Bot.SendTextMessageAsync(
                                chatId,
                                $"Great now Reporting ",// to {currentSeason.TeamOne.Name} VS { currentSeason.TeamTwo.Name }",
                            replyMarkup: SportsTelegram.Global.GetReplyKeyboardMarkup(SeasonEventType(enumSportCategory)));
                }
                currentSeason.Reporting = true;
                currentSeason.ReportingByChatId = chatId;
                MyHistory(Message).Current_Match_Id = id;
                if (currentSeason.MatchStarted)
                {
                    //Start Bhai sakyeko pachi ko details
                }
                else
                {
                    // Plimiary Games Setup
                    if(_userTelegramMessageHistory.Where(x => x.UserName == username).Where(x => x.ChatId == chatId).SingleOrDefault().Settings.Count == 0)
                    {
                        _userTelegramMessageHistory.Where(x => x.UserName == username).Where(x => x.ChatId == chatId).SingleOrDefault().Route = "Settings";
                        AskQuestions(Message);
                    }

                }
            }
            else if (data.Replace(" ", "_").Split(":")[0] == "assigplayertofixture")
            {
                Bot.SendTextMessageAsync(
                                Message.Chat.Id,
                                "Great Select Team You Want To Assign Players For",
                                replyMarkup: new ReplyKeyboardRemove());

                // Send Team
                // Send Selected Players//
                UniversalDataContainer udc = new UniversalDataContainer
                {
                    Command = "List",
                    Param = "SeasonTeamPlayers",
                    Id = "1", // TeamId
                    SubParam = "Season",
                    SubParamId = "1", //Season Id                    
                };
            }
            else
            {
                GenericNameToBeDefined finalData = JsonConvert.DeserializeObject<GenericNameToBeDefined>(data);
                if (finalData.RequestType == "Photo")
                {
                    List<TelegramGenericViewModel> tgvm = new List<TelegramGenericViewModel>();
                    tgvm.Add(new TelegramGenericViewModel
                    {
                        Id = "",
                        Name = " Cover Photo "
                    });
                    tgvm.Add(new TelegramGenericViewModel
                    {
                        Id = "",
                        Name = " Image "
                    });
                    tgvm.Add(new TelegramGenericViewModel
                    {
                        Id = "",
                        Name = " Thumbnail "
                    });
                    await Bot.SendTextMessageAsync(
                             chatId,
                             $"Great What Type Of Photo You Want To Upload!?",// to {currentSeason.TeamOne.Name} VS { currentSeason.TeamTwo.Name }",
                           replyMarkup: SportsTelegram.Global.GetReplyKeyboardMarkup(tgvm));
                }
            }
        }
        private static void FillGameCategoryEventDetails()
        {
            Global.GetListFromEnum();            
        }       
        private static void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs chosenInlineResultEventArgs)
        {
            Console.WriteLine($"Received inline result: {chosenInlineResultEventArgs.ChosenInlineResult.ResultId}");
        }
        private static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            Console.WriteLine("Received error: {0} — {1}",
                receiveErrorEventArgs.ApiRequestException.ErrorCode,
                receiveErrorEventArgs.ApiRequestException.Message);
        }
        async static void Connect()
        {
            try
            {
                if (_connectionState != ConnectionState.Connected)
                {
                    Console.WriteLine("Trying To Connect...." + DateTime.Now.Minute + ":" + DateTime.Now.Second);
                    await _connection.StartAsync();
                    _connectionState = ConnectionState.Connected;
                    await _connection.InvokeAsync<UniversalDataContainer>("IAmTelegram");
                    Console.WriteLine("Connected !!!!" + DateTime.Now.Minute + ":" + DateTime.Now.Second);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Server Service Not Ready.... " + DateTime.Now.Minute + ":" + DateTime.Now.Second);
            }
        }
        public static async Task StartConnectionAsync()
        {
            Console.WriteLine("Console Init....");
            _connection = new HubConnectionBuilder()
            .WithUrl(_baseUrlSignalR, options =>
            {
                options.Headers["ClientMessage"] = "Console";
            })
                .Build();
            _connection.Closed += async (error) =>
            {
                aa:
                Console.WriteLine("Client Disconnected !");
                _connectionState = ConnectionState.Disconnected;
                await Task.Delay(new Random().Next(0, 5) * 1000);
                try
                {
                    Connect();
                }
                catch (Exception) { }
                if (_connectionState != ConnectionState.Connected)
                    goto aa;
            };
            bb:
            Connect();
            if (_connectionState != ConnectionState.Connected)
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                goto bb;
            }
        }
        private void InvokeAWESOME(string methodName, UniversalDataContainer UniversalDataContainer)
        {
            try
            {
                Program m = new Program();
                MethodInfo method = m.GetType().GetMethod(methodName);
                method.Invoke(m, new UniversalDataContainer[] { UniversalDataContainer });
            }
            catch (Exception ex)
            {
                Console.WriteLine("SOME COMMAND WRONG !");
            }
        }
    }
}