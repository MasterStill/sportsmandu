﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sportsmandu.Models.Entitiesss
{
    public static class Global
    {
        public static int CommissionPercentage = 2;
        public static int VatPercentage = 13;
        public static string BaseUrl()
        {            
            if (System.IO.Directory.GetCurrentDirectory().ToLower().Contains("repos"))
            {
                //return "http://7909bbf3.ngrok.io";
                return "http://localhost:5000";
            }
            return "http://sportsmandu.com";
        }
        public static string GetCleanUrl(string String)
        {
            try
            {
            String = ToTitleCase(String);
            String = String
                 .Trim()
                 .Replace(",", "")
                 .Replace("\"", "")
                 .Replace(" ", "-")
                 .Replace("|", "")
                 .Replace("\t", "")
                 .Replace("(", "")
                 .Replace(")", "")
                 .Replace("--", "-")
                 .Replace("--", "-")
                 .Replace("-I-", "-")
                 .Replace("'", "")
                 ;
                if (String.EndsWith("-"))
                    String = String.Substring(0, String.Length - 1);
                return String;
            }
            catch
            {
                return "47: Global.cs : Error";
            }
        }
        public static string ToTitleCase(this string str)
        {
            var tokens = str.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < tokens.Length; i++)
            {
                var token = tokens[i];
                tokens[i] = token.Substring(0, 1).ToUpper() + token.Substring(1);
            }

            return string.Join(" ", tokens);
        }
    }
}
