﻿using System.Collections.Generic;
namespace Sportsmandu.Models.Messaging.Notification
{
    public enum MessageType
    {
        All, Few
    }
    public class PushMessage
    {
        public string Message { get; set; }
        public string Title { get; set; }
        public MessageType MessageType { get; set; }
        public List<string> DeviceIds { get; set; }
        public List<PushMessageData> Data {get;set;}
    }
    public class PushMessageData{
        public string Key {get;set;}
        public string Value {get;set;}
    }
    public class UserNotification
    {
        public int UserId { get; set; }
        public string DeviceId { get; set; }
        public virtual ApplicationUser User {get;set;}
    }
    public class AllNotification
    {
        public string DeviceId { get; set; }
    }
    public class SignalUser{
        public int UserId {get;set;}
        public string ConnectionId {get;set;}
        public string DeviceId { get; set; }
    }
}