﻿using System.Collections.Generic;
namespace Sportsmandu.Models.Messaging.Notification
{
   public class Newsletter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public  string Email { get; set; }
    }
    public class ContactForm
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}