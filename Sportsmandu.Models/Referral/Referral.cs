﻿using System;
namespace Sportsmandu.Models.Referral
{
    public class Referral
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public enumReferral Status { get; set; }
        public DateTime ReferralDate { get; set; }
        public DateTime? AcceptedDate { get; set; }
        public Boolean Delflag { get; set; }
        public int ReferralById { get; set; }
        public string Segment { get; set; }
        public ApplicationUser ReferralBy { get; set; }
    }
}
