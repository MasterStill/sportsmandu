﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
namespace Sportsmandu.Models.Referral
{
    public class ReferralCreateViewModel
    {
        [Required]
        public string FullName { get; set; }
        [Required]
        public string Email { get; set; }        
        public string Segment { get; set; } // k ko lagi referral gayeko ho ? Club / players / season 
    }
}
