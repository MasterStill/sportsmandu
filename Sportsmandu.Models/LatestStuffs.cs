﻿using System;
using System.Collections.Generic;
namespace Sportsmandu.Models
{
    public class UserRequest
    {
        public enumChannel Channel { get; set; }
        public string ChannelMessageDetails { get; set; }
    }
    public enum enumChannel
    {
        Discord
    }
    public class SuperVisor
    {
        public SuperVisor()
        {
            this.RoomId = new List<RoomDetails>();
        }
        public string ConnectionId { get; set; }
        public enumSuperVisorRole Role { get; set; } // General matra ho ki ? room banaune matra ho ki ? user verification matra garne ho ki ?
        public bool isBusy { get; set; }
        public List<RoomDetails> RoomId { get; set; }
    }
    public enum enumSuperVisorRole
    {
        Roome_Creation, User_Verification
    }
    public class RoomDetails
    {        
        public string RoomId { get; set; }
        public string Password { get; set; }
    }
    public class MoreInfoAboutAutmationSupervisor
    {
        public string Guid { get; set; }
        public string WorkerGuid { get; set; }
        public string Version { get; set; }
        public enumSuperVisorRole Role { get; set; }
    }
}