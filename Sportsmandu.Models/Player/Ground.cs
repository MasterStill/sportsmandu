﻿using Sportsmandu.Model.Club.ViewModels;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Generic;
using Sportsmandu.Models;
using Sportsmandu.Models.Gallery;
using System;
using System.Collections.Generic;
namespace Sportsmandu.Model.Entities.Ground
{
    public class GroundVMFORES
    {
        public GroundVMFORES()
        {
            this.Social = new List<SocialVMFFORES>();
            this.Gallery = new List<GalleryVMFORES>();
        }
        public string Id { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public bool Complete { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public DateTime? FoundedDate { get; set; }
        public string CoverImage { get; set; }
        public string Description { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string Summary { get; set; }
        public string CultureCode { get; set; }
        public enumVenueType Type { get; set; }
        public string CountryName { get; set; }
        public string URL { get; set; }
        public List<SocialVMFFORES> Social { get; set; }
        public List<GalleryVMFORES> Gallery { get; set; }
    }
    public class GroundSubscription
    {
        public GroundSubscription() {
            this.Ground = new Ground();
            this.User = new ApplicationUser();
        }
        public int GroundId { get; set; }
        public int UserId { get; set; }
        public virtual Ground Ground { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
    public class GroundConvertedCulture
    {
        public GroundConvertedCulture()
        {
            this.Ground = new Ground();
            this.Culture = new Culture();
        }
        public int GroundId { get; set; }
        public int CultureId { get; set; }
        public virtual Culture Culture { get; set; }
        public virtual Ground Ground { get; set; }
    }
    public class GroundSocial
    {
        public string Link { get; set; }
        public enumSocial Social { get; set; }
        public int GroundId { get; set; }
        public virtual Ground Ground { get; set; }
    }
    public class Ground : SportsmanduMandatory, ISportsmanduBase
    {
        public Ground()
        {
            this.Country = new Country();
            this.Subscription = new HashSet<GroundSubscription>();
            this.ConvertedCulture = new HashSet<GroundConvertedCulture>();
            this.Social = new HashSet<GroundSocial>();
        }
        public ICollection<GroundConvertedCulture> ConvertedCulture { get; set; }
        public ICollection<GroundSubscription> Subscription { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        public string Name { get; set; }
        public string FoundedDate { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string FullStreetName { get; set; }
        public string EstablishedDate { get; set; }
        public string Source { get; set; }
        public string Image { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public bool BookingEnabled { get; set; }
        public virtual ICollection<GroundSocial> Social { get; set; }
        public int CountryId { get; set; }
        public virtual Country Country { get; set; }
        public enumVenueType Type { get; set; }
        public string URL
        {
            get
            {
                try
                {
                    return Sportsmandu.Models.Entitiesss.Global.GetCleanUrl($"{Culture.Code}/{Country.Name}/{Type}/{Guid}/{Name}");
                }
                catch
                {
                    return "ERROR";
                }
            }
        }
    }    
    public enum enumVenueType {
           Ground,Venue
    }
}