using Community.Model.Entities.ViewModels;
using Sportsmandu.Models;
using Sportsmandu.Models.League;
using System.Collections.Generic;
namespace Sportsmandu.Model.Entities.Player
{
    public class Category : SportsmanduMandatory//, ISportsmanduBase
    {
        public Category()
        {
            this.Categories = new HashSet<SeasonCategories>();
            this.SubCategories = new HashSet<SubCategory>();
        }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public ICollection<SeasonCategories> Categories { get; set; }
        public ICollection<SubCategory> SubCategories { get; set; }
    }
}
namespace Sportsmandu.Model
{
    public class SubCategory : SportsmanduMandatory
    {    
        public SubCategory()
        {
            this.Category = new Sportsmandu.Model.Entities.Player.Category();
        }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual Sportsmandu.Model.Entities.Player.Category Category { get; set; }
    }
    public class SubCategoryCreateViewModel :  CreateMandatory
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
public class CategoryCreateVIewModel
{    
    public string Name { get; set; }    
    public string Image { get; set; }
    public string Description { get; set; }
    public int CultureId { get; set; }
}