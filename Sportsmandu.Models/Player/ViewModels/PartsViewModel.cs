﻿namespace Sportsmandu.Model.Entities.ViewModels
{
     public class PartsViewModel : PartsViewModelElasticSearch
    {
            public string Id { get; set; }
            public string PartId { get; set; }
            public int ProductId { get; set; }
        // Samsung >> Galaxy Note 5 >> Screen >> Note 5 ko screen !
        //public string CultureName { get; set; }
        //public string PartsId { get; set; }
        //public string PartsDescription {get;set;}
        //public List<SubPartsViewModel> SubParts { get; set; }
    }
    public class PartsViewModelElasticSearch 
    {
        public string PartDescription { get; set; }
        public string PartName { get; set; }
        public string Image { get; set; } 
    }
} 