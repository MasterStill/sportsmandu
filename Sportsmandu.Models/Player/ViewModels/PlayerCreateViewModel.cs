﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Community.Model.Entities.ViewModels;

namespace Sportsmandu.Model.Entities.ViewModels
{
    public class PlayerCreateViewModel : CreateMandatory
    {
        [Required]
        public string Name { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        public string Description { get; set; }        
        public string Image { get; set; }
        public string Contact { get; set; }
        public string DOB { get; set; }
        public string Height { get; set; }
        public string Source { get; set; }
        public string Email { get; set; }        
        public int CategoryId { get; set; }        
        public int CountryId { get; set; }        
        public int Gender { get; set; }
    }
}