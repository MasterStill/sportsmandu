﻿using System.ComponentModel.DataAnnotations;
using Community.Model.Entities.ViewModels;
using Sportsmandu.Models;

namespace Sportsmandu.Model.Entities.ViewModels
{
    public class ClubCreateViewModel : CreateMandatory
    {
        
        public string Image { get; set; }
        
        public string Name { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        public string EstablishedDate { get; set; }        
        public enumClubType ClubType { get; set; }
        public string CurrentCoach { get; set; }// TODO Create a seperate ROLE Entity 
        public string FormorPresident { get; set; }// TODO
        public string ManagerName { get; set; }// TODO
        public string Source { get; set; }
        public string Description { get; set; }
        public string Alias { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string FullStreetName { get; set; }
        public int CountryId { get; set; }        
        public string Logo { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Website { get; set; }
        public string HomeGroundId { get; set; }
        //public ClubTypeMemberShip ClubMe { get; set; }
    }
}