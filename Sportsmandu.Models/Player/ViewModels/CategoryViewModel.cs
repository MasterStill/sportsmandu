﻿namespace Sportsmandu.Model.Entities.ViewModels
{
    public class CategoryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public int multilingualid { get; set; }
        public bool Converted { get; set; }        
        public string CultureCode { get; set; }
    }
    public class CategoryViewModelFORES
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }        
        public string CultureCode { get; set; }
    }
    public class ApiCategoryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string CultureCode { get; set; }
    }
}