using System.Collections.Generic;
namespace Sportsmandu.Model.Entities.ViewModels
{
    public class PlayerAdminViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public bool Converted { get; set; }
        public string URL { get; set; }
        public string Description { get; set; }
        //public string Category { get; set; }
        public string CategoryName { get; set; }
        public int multilingualid { get; set; }
    }
    public class AdminViewModel
    {
        public int Id { get; set; }
        //public string Guid { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public bool Converted { get; set; }
        public string URL { get; set; }
        public int multilingualid { get; set; }
    }
}