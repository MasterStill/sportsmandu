using Sportsmandu.Model.Club.ViewModels;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Generic;
using Sportsmandu.Models;
using Sportsmandu.Models.Gallery;
using Sportsmandu.Models.League;
using System;
using System.Collections.Generic;
namespace Sportsmandu.Model.Entities.ViewModels
{
    public class PlayerViewModel
    {
        public string Summary { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string DOB { get; set; }
        public string URL { get; set; }
        public string PlayerRole { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public bool Converted { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
        public string CultureCode { get; set; }
    }
    public class PlayerViewModelEs : ElasticMandatory
    {
        public string Id { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        public string DOB { get; set; }
        public string Image { get; set; }
        public string Role { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
        public string CultureCode { get; set; }
        public string CountryName { get; set; }
        public string Height { get; set; }
        public string Gender { get; set; }
        public string PlayerRole { get; set; }
        public List<SocialVMFFORES> Social { get; set; }
        public List<GalleryVMFORES> Gallery { get; set; }
        public virtual List<ClubPlayerViewModel> Club { get; set; }
        public virtual List<SeasonVMForPlayerViewModelEs> Seasons { get; set; }
    }
    public class ClubPlayerViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public DateTime JoinedDate { get; set; }
        public DateTime ResignedDate { get; set; }
        public string URL { get; set; }
    }
    public class SeasonVMForPlayerViewModelEs
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public string LeagueName { get; set; }
        public string LeagueThumbnail { get; set; }
        public string URL { get; set; }
    }
    public class ClubPlayerViewModelApi
    {
        public string PlayerId { get; set; }
        public string PlayerName { get; set; }
        public string PlayerImage { get; set; }
        public string PlayerRole { get; set; }
        public string PlayerCategoryName { get; set; }
        public int ShirtNumber { get; set; }
        public DateTime JoinedDate { get; set; }
        public DateTime ResignedDate { get; set; }                
    }
    //public class ClubEmployeeViewModelApi
    //{
    //    public string Id { get; set; }
    //    public string Name { get; set; }
    //    public string Image { get; set; }
    //    public string DesignationId { get; set; }
    //    public DateTime JoinedDate { get; set; }
    //    public DateTime ResignedDate { get; set; }
    //}
    public class EmployeeViewModelApi
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public string Image { get; set; }
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public DateTime? JoinedDate { get; set; }
        public DateTime? ResignedDate { get; set; }
    }
    public class ApiPlayerViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
        public string CultureTitle { get; set; }
    }    
    public class ApiClubViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
    }
    public class GroundViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public bool Converted { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
        public CountryViewModel Country { get; set; }
        public string CultureTitle { get; set; }        
        //public string PartsPartName { get; set; }
    }
    public class CountryViewModel    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
    }
    //   public class ProductViewModel
    // {
    //     public string Name { get; set; }
    //     public string Image { get; set; }

    //     public string Description { get; set; }
    //     //public ICollection<PartsViewModel> Parts {get;set;}        
    // }
    // public class PartsViewModel
    // {
    //     public string Id { get; set; }
    //     public string Name { get; set; }
    //     public string Image { get; set; }

    //     public string Description { get; set; }
    // }
}