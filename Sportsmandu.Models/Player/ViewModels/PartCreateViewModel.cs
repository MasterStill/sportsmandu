﻿using Community.Model.Entities.ViewModels;
namespace Sportsmandu.Model.Entities.ViewModels
{
    public class PartCreateViewModel : CreateMandatory
    {
      public string Image {get;set;}
      public  string Name { get; set; }      
      public string Description {get;set;}
    }
}
