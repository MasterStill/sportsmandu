﻿using System.ComponentModel.DataAnnotations;
using Community.Model.Entities.ViewModels;

namespace Sportsmandu.Model.Entities.ViewModels
{
    public class GroundCreateViewModel : CreateMandatory
    {
        // [Required]
        public string Image { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        // [Required]
        public string Name { get; set; }
        public string EstablishedDate { get; set; }
        public string Description { get; set; }
        public string Country { get; set; }
        public string Source { get; set; }
        public string Email { get; set; }
        public int CountryId { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string FullStreetName { get; set; }
    }
}