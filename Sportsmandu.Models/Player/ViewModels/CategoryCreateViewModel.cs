﻿using System.ComponentModel.DataAnnotations;
using Community.Model.Entities.ViewModels;
namespace Sportsmandu.Model.Entities.ViewModels
{
    public class CategoryCreateViewModel : CreateMandatory
    {      
      public string Image {get;set;}      
      public  string Name { get; set; }      
      public string Description {get;set;}
    }
}
