﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Sportsmandu.Model.Entities.ViewModels
{
    public class PartViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public int multilingualid { get; set; }
        public bool Converted { get; set; }
    }
}
