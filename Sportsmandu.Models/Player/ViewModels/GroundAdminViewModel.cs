﻿namespace Sportsmandu.Model.Entities.ViewModels
{
    public class GroundAdminViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public bool Converted { get; set; }
        public string Description { get; set; }
        public int multilingualid { get; set; }
    }
}