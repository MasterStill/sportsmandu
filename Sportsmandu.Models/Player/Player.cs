using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Generic;
using Sportsmandu.Models;
using Sportsmandu.Models.enums;
using System.Collections.Generic;
namespace Sportsmandu.Model.Entities.Player
{
    public enum enumSocial {
        Website,Facebook,Youtube, RSS, Instagram,Twitter
    }
    public class PlayerSocial
    {
        public string Link { get; set; }
        public enumSocial Social { get; set; }
        public int PlayerId { get; set; }
        public virtual Player Player { get; set; }        
    }
    public class PlayerConvertedCulture
    {
        public PlayerConvertedCulture()
        {
            this.Player = new Player();
            this.Culture = new Culture();
        }
        public int PlayerId { get; set; }
        public int CultureId { get; set; }
        public virtual Culture Culture { get; set; }
        public virtual Player Player { get; set; }
    }
   
    public class Player : SportsmanduMandatory, ISportsmanduBase
    {
        public Player()
        {
            this.Club = new HashSet<ClubPlayers>();
            this.ConvertedCulture = new HashSet<PlayerConvertedCulture>();
            this.Season = new HashSet<Models.League.SeasonClubPlayers>();
            this.Subscription = new HashSet<PlayerSubscription>();
            this.Social = new HashSet<PlayerSocial>(); 
        }
        public virtual ICollection<PlayerSubscription> Subscription { get; set; }
        public enumPlayerRole PlayerRole { get; set; } // Backey Goalkeeper
        public string CoverImage { get; set; }
        public int XId { get; set; }
        public string Thumbnail { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string Contact { get; set; }
        public string DOB { get; set; }
        public string StatsString { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string Source { get; set; }
        public string Email { get; set; }
        public int CategoryId { get; set; }
        public int? CountryId { get; set; }
        public virtual ICollection<PlayerSocial> Social { get; set; }
        public virtual ICollection<ClubPlayers> Club { get; set; }
        public virtual ICollection<PlayerConvertedCulture> ConvertedCulture { get; set; } 
        public virtual ICollection<Models.League.SeasonClubPlayers> Season { get; set; }
        public virtual Category Category { get; set; }
        public virtual Country Country { get; set; }
        public enumGender Gender { get; set; }
        public string URL
        {
            get
            {
                try
                {
                    var result =  Sportsmandu.Models.Entitiesss.Global.GetCleanUrl($"{Culture.Code}/{Country.Name}/{Category.Name}/Player/{Guid}/{Name}");
                    return result;
                }
                catch
                {
                    return "ERROR";
                }
            }
        }
    }
    public class PlayerSubscription
    {
        public PlayerSubscription()
        {
            this.Player = new Player();
            this.User = new ApplicationUser();
        }
        public string PlayerGuid { get; set; }
        public string UserEmail { get; set; }
        public virtual Player Player { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
    public enum enumGender
    {
        Male, Female, Trans_Gender
    }
}