﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Sportsmandu.Models
{    
    public class Advertisement
    {
        public int Id { get; set; }
        [Required]
        public enumFrontEndSegments Segment { get; set; }
        [Required]
        public int SegmentPos { get; set; }
        [Required]
        public bool refreshOnBreakpointChange { get; set; }
        [Required]
        public bool promobox { get; set; }
        [Required]
        public int refreshRate { get; set; }
        [Required]
        public bool Script { get; set; }
        public string URL { get; set; }
        public string Image { get; set; }
        public enumAdlotypes adlotypes { get; set; }
        public string AdTypeName { get; set; } // Nepal // Sydney // Australia
        public int TimesDisplayed { get; set; } //TODO WIll only run once a day for backup purpose // real data will be stored in elasticsearch
        public int ClickToAdvt { get; set; } //TODO WIll only run once a day for backup purpose // real data will be stored in elasticsearch        
    }
    public class IpInfo
    {

        [JsonProperty("ip")]
        public string Ip { get; set; }

        [JsonProperty("hostname")]
        public string Hostname { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("loc")]
        public string Loc { get; set; }

        [JsonProperty("org")]
        public string Org { get; set; }

        [JsonProperty("postal")]
        public string Postal { get; set; }
    }
    public enum enumAdlotypes
    {
        Country,Zone
    }
    public enum enumFrontEndSegments
    {
        HomePage, Player
    }
}