﻿using System;
using System.Collections.Generic;

namespace Sportsmandu.Models
{
    public class AnnapurnaPost
    {
        public int Id { get; set; }
        public string Highlights { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        public string IntroText { get; set; }
        public string VideoLink { get; set; }
        public string FeaturedImage { get; set; }
        public string PublishOn { get; set; }
        public string AuthorImageUrl { get; set; }
        public string AuthorName { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string SubHeading { get; set; }
        public string Highlight { get; set; }
        public string Caption { get; set; }
        public string ViewCount { get; set; }
        //public string AdditionalImages { get; set; }
        //public string RelatedNews { get; set; }
        public string PublishedOn { get; set; }
        public string PublishedOnEnglish { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescriptions { get; set; }
    }
    public class AnnapurnaPostScrapResult
    {
        public List<AnnapurnaPost> Data { get; set; }
    }
    public class AnnapurnaPostSingleScrapResult
    {
        public AnnapurnaPost News { get; set; }
        public AnnapurnaAuthor Author { get; set; }
    }
    public class AnnapurnaAuthor
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }
}