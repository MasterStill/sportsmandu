﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Community.Model.Entities.ViewModels;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Models;
namespace Sportsmandu.Model.Entities.Globalization
{
    public class Culture : ISportsmanduBase
    {
        public Culture()
        {
            this.Player = new HashSet<PlayerConvertedCulture>();
            this.Club = new HashSet<ClubConvertedCulture>();
            this.Blog = new HashSet<BlogConvertedCulture>();
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string Guid { get; set; }
        public int CreatedById { get; set; }
        public bool Delflag { get; set; }
        public bool Verified { get; set; }
        public int multilingualid { get; set; }
        public int CultureId { get; set; }
        public virtual ICollection<PlayerConvertedCulture> Player { get; set; }
        public virtual ICollection<ClubConvertedCulture> Club { get; set; }
        public virtual ICollection<BlogConvertedCulture> Blog { get; set; }
    }
    public class Globalization : SportsmanduMandatory//, ISportsmanduBase
    {
        public Globalization()
        {
            this.LanguageTitle = new LanguageTitle();
        }
        public int LanguageTitleId { get; set; }
        public string Value { get; set; }
        public virtual LanguageTitle LanguageTitle { get; set; }
    }   
    public class LanguageTitle
    {
        public LanguageTitle()
        {
            this.SiteSection = new HashSet<SiteSectionLanguageTitle>();
            this.Globalizations = new HashSet<Globalization>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<SiteSectionLanguageTitle> SiteSection { get; set; }
        public virtual ICollection<Globalization> Globalizations { get; set; }
    }
    // Forgot_Your_Password
    // Global
    // Add_Account
    public class SiteSection
    {
        public SiteSection()
        {
            this.LanguageTitles = new HashSet<SiteSectionLanguageTitle>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<SiteSectionLanguageTitle> LanguageTitles { get; set; }
    }
    public class SiteSectionLanguageTitle
    {
        public SiteSectionLanguageTitle()
        {
            this.SiteSection = new SiteSection();
            this.LanguageTitle = new LanguageTitle();
        }
        public int SiteSectionId { get; set; }
        public int LanguageTitleId { get; set; }
        public virtual SiteSection SiteSection { get; set; }
        public virtual LanguageTitle LanguageTitle { get; set; }
    }
    public class SiteSectionLanguageTitleForEs
    {
        public string Id { get; set; }
        public string SiteSectionName { get; set; }
        public string LanguageTitleName { get; set; }
        public List<GlobalizationVMforSiteSectionLanguagteTitleFORES> LanguageTitleGlobalizations { get; set; }
    }
    public class GlobalizationVMforSiteSectionLanguagteTitleFORES
    {
        public string Value { get; set; }
        public string CultureCode { get; set; }
    }
    public class GlobalizationAdminViewModel
    {
        public int Id { get; set; }
        public int LanguageTitleId { get; set; }
        public bool Converted { get; set; }
        public int multilingualid { get; set; }
        public virtual LanguageTitle LanguageTitle { get; set; }
        public string Value { get; set; }
    }
    public class GlobalizationCreateViewModel : CreateMandatory
    {
        [Required]
        public string Value { get; set; }
        [Required]
        public int LanguageTitleId { get; set; }
    }
}