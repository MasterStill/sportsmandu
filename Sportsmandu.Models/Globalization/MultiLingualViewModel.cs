﻿using Community.Model.Entities.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sportsmandu.Models.Globalization
{
    public class MultiLingualViewModel
    {
        public int Id { get; set; }
        public bool Converted { get; set; }
        public int CultureId { get; set; }
        public int multilingualid { get; set; }
    }
}