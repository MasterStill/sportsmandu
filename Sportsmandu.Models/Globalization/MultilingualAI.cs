﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sportsmandu.Models.Globalization
{
    public class MultilingualAI
    {
        public List<int> idToRemove { get; set; }
        public IEnumerable<MultiLingualViewModel> items { get; set; }
    }
}
