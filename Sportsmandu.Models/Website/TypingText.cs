﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sportsmandu.Models.Website
{
    public class TypingTextt
    {
        public TypingTextt(string Culture)
        {
            switch (Culture)
            {
                case "नेपाली":
                    this.TopMessage = "हामी हाम्रो सेवाग्राही लाई एअक्दम राम्रो सेवा गर्छौ";
                    this.HeadingMessage = "हामी लाई आफ्नो बिग्रिएको काम दिनुस ढुक्क हुनुस";
                    this.TypingText = new List<string>
                    {
                        "केइ फुट्यो ?", "घर को धारा बिग्रियो ?", "मोटर सेर्विसिंग गर्ने समय छैन ?"
                    };
                    this.SupportPhoneNumber = "९८५१०५०६७८";
                    this.CopyrightText = "कपीराइट २०१९ रीपैरमाण्डु. सबै अधिकार सुरक्षित ।";
                    this.FAQ = "प्रश्न हरु";
                    this.About = "हाम्रो बारेमा";
                    this.ContactUsText = "सम्पर्क गर्नुहोस";
                    this.ContactUsSubText = "सदैब तपाइको सहयोगी";
                    this.EmailAddressText = "इमेल";
                    this.SiteLocationText = "लोकेशन";
                    this.PhoneNumberText = "फोन नंबर";
                    this.MapLocationText = "म्याप लोकेशन";
                    this.LatestBlogText = "ब्लग";
                    this.LatestBlogSubText = "सदैब तपाइको सहयोगी";
                    this.HeresourPhoneNumberText = "हाम्रो फोन नम्बर तिप्नुहोस";
                    this.HeresourPhoneNumberSubText = "तपाइको समस्या <span>२४ घण्टा मा हल</span>";
                    this.CustomerCareText = "ग्राहक सेवा";
                    break;
                case "English":                    
                default:
                    this.TopMessage = "We provide our best services for our clients";
                    this.HeadingMessage = "Hire Us and Be Secure";
                    this.TypingText = new List<string>
                    {
                        "Stop Data Theft", "Recover Your Damage", "Get Fresh Android"
                    };
                    this.SupportPhoneNumber = "9851050678";
                    this.CopyrightText = "Copyright 2018 Sportsmandu. All right reserved.";
                    this.FAQ = "FAQ";
                    this.About = "About";
                    this.ContactUsText= "Contact Us";
                    this.ContactUsSubText = "Always Here To Help You";
                    this.EmailAddressText = "Email Adress";
                    this.SiteLocationText = "Site Location";
                    this.PhoneNumberText = "Phone Number";
                    this.MapLocationText = "Map Location";
                    this.LatestBlogText = "Latest Blog";
                    this.LatestBlogSubText = "Always Here To Help You";
                    this.HeresourPhoneNumberText = "Here's our Phone Number";
                    this.HeresourPhoneNumberSubText = "Fast Turn Around Service <span>Repairs in 24 hrs!</span>";
                    this.CustomerCareText = "Customer Care";
                    break;
            }
        }
        public string TopMessage { get; set; }
        public string HeadingMessage { get; set; }
        public List<string> TypingText { get; set; }
        public string SupportPhoneNumber { get; set; }
        public string CopyrightText { get; set; }
        public string FAQ { get; set; }
        public string About { get; set; }
        public string ContactUsText { get; set; }
        public string ContactUsSubText { get; set; }
        public string CustomerCareText { get; set; }
        public string EmailAddressText { get; set; }
        public string SiteLocationText { get; set; }
        public string PhoneNumberText { get; set; }
        public string MapLocationText { get; set; }
        public string LatestBlogText { get; set; }
        public string LatestBlogSubText { get; set; }
        public string HeresourPhoneNumberText { get; set; }
        public string HeresourPhoneNumberSubText { get; set; }
    }
}
