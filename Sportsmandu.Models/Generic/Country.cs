using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sportsmandu.Models;
namespace Sportsmandu.Model.Generic
{
    
    public class Country : SportsmanduMandatory, ISportsmanduBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
    public class City : SportsmanduMandatory, ISportsmanduBase
    {
        public string Name { get; set; }
        public int CountryId { get; set; }
    }
    public class District : SportsmanduMandatory, ISportsmanduBase
    {
        public string Name { get; set; }
        
    }
    public class Address //: SportsmanduMandatory, ISportsmanduBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Int { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
    public class ClassFieldTypeForView
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; } // TO DO FETCH FROM DB
    }
    public class ClassFieldTypeForViewFORES
    {
        public string Guid { get; set; }
        public int Id { get; set; }
        public List<NameAndValueForES> Stats { get; set; }
    }
    public class NameAndValueForES
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}