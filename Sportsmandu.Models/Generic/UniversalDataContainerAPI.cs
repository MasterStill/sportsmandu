﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
namespace Sportsmandu.Models.Generic
{
    public class ITest
    {
        //[Keyword] // For ElasticSearch
        //public string AccountId { get; set; }        
        public string Id { get; set; }
        //public dynamic Results {get;set;}
    }
    public class UniversalDataContainer : ITest
    {
        public UniversalDataContainer() { }
        //public string AccountId { get; set; }        
        public string UserId { get; set; }
        public string Command { get; set; }
        public string Param { get; set; }
        public string SubParam { get; set; }
        public string SubParamId { get; set; }
        public int Page { get; set; }
        public dynamic Data { get; set; }
        public dynamic Result { get; set; }
        public SupportedCultures Culture { get; set; }
        public string Message { get; set; }
    }
    public enum SupportedCultures
    {
      En,Np   
    }
    public class UniversalDataContainerAPI : ITest
    {
        public UniversalDataContainerAPI() { }
        //public string AccountId { get; set; }
        public string UserId { get; set; }
        public enumCommandApi CommandApi { get; set; }
        public string Param { get; set; }
        public string SubParam { get; set; }
        public int Page { get; set; }
        public dynamic Data { get; set; }
    }
    
    [JsonConverter(typeof(StringEnumConverter))]
    public enum enumCommandAutomationCommand
    {
        CREATE_ROOM,
        START_GAME,
        CLOSE_ROOM,
        ROOM_PASSWORD,
        VERIFY_USER
    }
    public enum enumCommandAutomationCommandResponseBack
    {
        ROOM_CREATED,REQUEST_NOT_FULFILLED,USER_VERIFIED,MESSAGE
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum enumCommand
    {
        Players,
        Clubs,
        Leagues,
        Seasons,
        Association,
        Categories,
        Partners,
        SeasonFixture,
        SeasonMatchEvent,
        SeasonMatchEventPlayers,
        BlogPost,
        Grounds,
        Prediction,
        AdminMessage,
        SeasonStandings,
        SeasonStats,
        ClubPlayers,
        Videos
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum enumCommandApi
    {
        Account
    }
    [JsonConverter(typeof(StringEnumConverter))]
    public enum enumParam
    {
        LIST,
        ADD,
        UPDATE,
        DELETE,
        EXECUTE
    }
}
