﻿using Sportsmandu.Model.Club.ViewModels;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Models.League;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sportsmandu.Model
{
    public class ElasticMandatory
    {
        public string Id { get; set; }
        public bool Complete { get; set; }
    }
}
namespace Sportsmandu.Models.Generic
{
    public class SearchViewModel
    {
        public string SearchTerm { get; set; }
        public string Culture { get; set; }
    }
    public class Search
    {
        public string SearchTerm { get; set; }
        public string Culture { get; set; }
        public string IPAddress { get; set; }
    }

    public class SearchIndexViewModel
    {
        public List<PlayerViewModel> Players { get; set; }
        public List<ClubPlayerViewModel> Clubs { get; set; }
        public List<SeasonVMFORES> Seasons { get; set; }
        public List<PartnerVMFORES> Partner { get; set; }
    }
}