﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Database.Seed;
namespace Sportsmandu.Data
{
    public abstract class ApplicationDbContext<TUser, TRole, TKey, TUserClaim, TUserRole, TUserLogin, TRoleClaim, TUserToken> : IdentityDbContext<TUser, TRole, TKey, TUserClaim, TUserRole, TUserLogin, TRoleClaim, TUserToken>
    where TUser : IdentityUser<TKey>
    where TRole : IdentityRole<TKey>
    where TKey : IEquatable<TKey>
    where TUserClaim : IdentityUserClaim<TKey>
    where TUserRole : IdentityUserRole<TKey>
    where TUserLogin : IdentityUserLogin<TKey>
    where TRoleClaim : IdentityRoleClaim<TKey>
    where TUserToken : IdentityUserToken<TKey>
    {
        public ApplicationDbContext(DbContextOptions options) : base(options) { }
        protected ApplicationDbContext() { }
        public new DbSet<TRoleClaim> RoleClaims { get; set; }
        public new DbSet<TRole> Roles { get; set; }
        public new DbSet<TUserClaim> UserClaims { get; set; }
        public new DbSet<TUserLogin> UserLogins { get; set; }
        public new DbSet<TUserRole> UserRoles { get; set; }
        public new DbSet<TUser> Users { get; set; }
        public new DbSet<TUserToken> UserTokens { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            base.OnModelCreating(builder);
            new DatabaseContextSeed().Seed(builder);
        }
    }
}