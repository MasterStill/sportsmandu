﻿using Sportsmandu.Model.Entities.Globalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Sportsmandu.Models.Gallery
{
    public class GalleryVMFORES
    {
        public GalleryVMFORES()
        {
            this.Photos = new List<GalleryPhotoVMFORES>();
        }
        public int Id { get; set; }
        public string Guid { get; set; }
        public string CultureCode { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public virtual List<GalleryPhotoVMFORES> Photos { get; set; }
    }
    public class GalleryPhotoVMFORES
    {
        public string Name { get; set; }
        public string CultureCode  { get; set; }
        public string Thumbnail { get; set; }
        public string Image { get; set; }
    }
    public class Gallery 
    {
        public Gallery()
        {
            this.Photos = new HashSet<GalleryPhoto>();
        }
        public int Id { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public int Multilingualid { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedById { get; set; }
        public bool Verified { get; set; }
        public bool DelFlag { get; set; }
        public string SegmentId { get; set; }
        public enumClaimTypeSegments Segment { get; set; }
        public int CultureId { get; set; }
        public string Thumbnail { get; set; }
        public Culture Culture { get; set; }
        public virtual ICollection<GalleryPhoto> Photos { get; set; }
    }
    public class GalleryPhoto
    {
        public GalleryPhoto()
        {
            this.Gallery = new Gallery();
        }
        public int Id { get; set; }
        public string Guid { get; set; }
        [Display(Name = "Title")]
        public string Name { get; set; }
        public bool DelFlag { get; set; }
        public int Multilingualid { get; set; }
        public int CreatedById { get; set; }
        public bool Verified { get; set; }
        public string Thumbnail { get; set; }
        public string Image { get; set; }
        public int CultureId { get; set; }
        public Culture Culture { get; set; }
        public int GalleryId { get; set; }
        public virtual Gallery Gallery {get;set;}
    }
}