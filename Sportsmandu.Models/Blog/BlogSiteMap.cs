﻿using System;
using System.Collections.Generic;
using System.Text;
namespace Sportsmandu.Models.Blog
{
    public class SiteMap
    {
        public string WebsiteName { get; set; }
        public string URL { get; set; }
    }
}