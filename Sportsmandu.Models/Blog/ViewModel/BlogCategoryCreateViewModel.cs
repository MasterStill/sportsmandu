﻿using System.ComponentModel.DataAnnotations;
using Sportsmandu.Model.Entities.Blog;
namespace Community.Model.Entities.ViewModels
{
    public class BlogCategoryCreateViewModel : CreateMandatory
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public BlogType Type {get;set;}
    }
}
