﻿using Sportsmandu.Models.Entities.Blog;
namespace Sportsmandu.Model.Entities.Blog
{
        public class BlogPostBlogCategory
        {
            public  int PostId { get; set; }
            public  int CategoryId { get; set; }
            public virtual BlogPost Post { get; set; }
            public virtual BlogCategory Category { get; set; }
        }
    }
