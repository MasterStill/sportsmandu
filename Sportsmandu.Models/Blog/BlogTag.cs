﻿using Sportsmandu.Model;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Model.Entities.Globalization;
using System.Collections.Generic;
namespace Sportsmandu.Models.Entities.Blog
{
    public class BlogTag: SportsmanduMandatory, ISportsmanduBase
    {
        public BlogTag(){
            this.Tags = new List<BlogPostBlogTag>(); 
        }
        public string Name { get; set; }
        public string URL{
            get{
                return "Come_Here_1.1.1.1.sa";
                //return Culture.Title + "/"+ Tags[0].Post.BlogType.ToString()  +"/Tag/" +  Global.GetCleanUrl(Name) ;//+ "/" ;
            }
        }
        public virtual List<BlogPostBlogTag> Tags { get; set; }
    }
}