﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Sportsmandu.Models;
using Sportsmandu;
namespace Sportsmandu.Model.Entities.Blog
{
    public class BlogAdminViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public bool Converted { get; set; }
        public string URL { get; set; }
        public string Verified { get; set; }
        public int multilingualid { get; set; }
    }
    public class BlogPost : SportsmanduMandatory, ISportsmanduBase
    {
        public BlogPost()
        {
            this.ConvertedCulture = new HashSet<BlogConvertedCulture>();
            Tags = new List<BlogPostBlogTag>();
        }
        public string URL
        {
            get
            {
                try
                {
                    return Sportsmandu.Models.Entitiesss.Global.GetCleanUrl($"{Culture.Code}/Home/NewsDetails/{Guid}/{Title}");
                }
                catch
                {
                    return "ERROR";
                }
                //if(!Website.Construction)
                //return "http://" + "blog.Sportsmandu.com" + "/" + Culture.Title + "/" + BlogType   +"/" + Sportsmandu.glo Global.GetCleanUrl(Category.Title) + "/" +  MultiLingualId + "/" +  Global.GetCleanUrl(Title);
                //return "http://" + Globals.Global.OnConstructionWebsiteName(Website.Alias) + "/" + Culture.Title + "/" + BlogType + "/" + Globals.Global.GetCleanUrl(Category.Title) + "/" + MultiLingualId + "/" + Global.GetCleanUrl(Title);
            }
        }
        [Required(ErrorMessage = "ThumbmailRequired")]
        public string Thumbnail { get; set; }
        [Required]
        public string Image { get; set; }
        [Required]
        [MinLength(4)]
        [MaxLength(250)]
        public string Title { get; set; }                
        //[Required]
        //[MinLength(1)]
        public string Summary { get; set; }
        public bool CommentAllowed { get; set; }
        public string Source { get; set; }
        public string Api { get; set; }
        public string password { get; set; }
        [Required(ErrorMessage = "CategoryRequired")]
        public int CategoryId { get; set; }
        public virtual PublishStatus PublishStatus { get; set; }
        [Required]
        public DateTime PublishDate { get; set; }
        public string PublishDateNp { get; set; }
        public string AuthorImageUrl { get; set; }
        public string AuthorName { get; set; }
        public virtual Visibility Visibility { get; set; }
        public virtual BlogCategory Category { get; set; }
        public virtual List<BlogPostBlogTag> Tags { get; set; }
        public BlogType BlogType { get; set; }
        [Required]
        [MinLength(1)]
        public string Description { get; set; }
        public string CoverImage { get; set; }
        public int MiscId { get; set; }
        public virtual ICollection<BlogConvertedCulture> ConvertedCulture { get; set; }
    }
    public class BlogPostVMFORES
    {
        public string Guid { get; set; }
        public string Id { get; set; }
        public string URL { get; set; }
        public string Thumbnail { get; set; }        
        public string Image { get; set; }
        public string AuthorImageUrl { get; set; }
        public string AuthorName { get; set; }
        public string CoverImage { get; set; }
        public string Title { get; set; }
        public string Source { get; set; }
        public string Summary { get; set; }
        public string PublishDateNp { get; set; }
        public bool CommentAllowed { get; set; }
        public string CategoryTitle { get; set; }
        public string PublishStatus { get; set; }
        public DateTime PublishDate { get; set; }

        public string BlogType { get; set; }        
        public string Description { get; set; }
        public string CultureCode { get; set; }
        public string Type { get; set; }
    }
    public class BlogConvertedCulture
    {
        public BlogConvertedCulture()
        {
            this.Blog = new BlogPost();
            this.Culture = new Globalization.Culture();
        }
        public int BlogId { get; set; }
        public int CultureId { get; set; }
        public virtual Globalization.Culture Culture { get; set; }
        public virtual BlogPost Blog { get; set; }
    }
    public enum BlogType
    {
        Blog, News, Inspiration
    }
}