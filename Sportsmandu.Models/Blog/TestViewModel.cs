﻿using System.ComponentModel.DataAnnotations;
namespace Sportsmandu.Model.Entities.Blog
{
    public class TestViewModel
    {
        public int Id {get;set;}
        public string Guid { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }
        public string Name { get; set; }
        public string Thumbnail {get;set;}          
        [MaxLength(10)]
        public string Summary {get;set;}
        public string CreatedDate {get;set;}
        public string PublishDate {get;set;}
        public string Image {get;set;}
    }
}
