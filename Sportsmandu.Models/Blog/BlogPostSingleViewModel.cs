﻿using System;
using Sportsmandu.Models.Entities.Blog;

namespace Community.Model.Entities.ViewModels
{
    public class BlogPostSingleViewModel
    {        
        public string Title { get; set; }
        public string Content {get;set;}
        public string Summary {get;set;}
        public bool CommentAllowed { get; set; }
        public string password { get; set; }
        public DateTime PublishDate { get; set; }
        public string ThumbnailUrl{get;set;}
        public string ImageUrl {get;set;}
        public string CategoryId { get; set; }
        public bool Translated{get;set;}
        public BlogTag Tag { get; set; } 
    }
} 