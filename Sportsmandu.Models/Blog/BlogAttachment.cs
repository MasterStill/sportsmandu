﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Sportsmandu.Models.Entities.Blog
{
    public class BlogAttachment: SportsmanduMandatory//;//, ICommunityBase
    {
        public string Name { get; set; }
        public bool Image  { get; set; }
        public string LinkUrl  { get; set; }

    }
}