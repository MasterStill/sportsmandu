﻿using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Models;
using System.Collections.Generic;
namespace Sportsmandu.Model.Entities.Blog
{
    public class BlogCategory: SportsmanduMandatory, ISportsmanduBase
    {
        public virtual int? ParentCategoryId { get; set; }
        public string Title {get;set;}
        public virtual BlogCategory ParentCategory { get; set; }
        public string URL {
            get{
                return "Return here 1.sd..5.";
                //if(!Website.Construction)
                //return "http://" + "blog.Sportsmandu.com" + "/" + Culture.Title + "/" + Type  +  "/Category/" +  Global.GetCleanUrl(Title);
                //return "http://" + Globals.Global.OnConstructionWebsiteName(Website.Alias) + "/" + Culture.Title + "/" + Type  +  "/Category/" +  Globals.Global.GetCleanUrl(Title);
            }
        }
        public virtual ICollection<BlogCategory> ChildCategories { get; set; }
        public virtual ICollection<BlogPost> Posts { get; set; }
        public string password { get; set; }        
        public BlogType Type {get;set;}
    }
}