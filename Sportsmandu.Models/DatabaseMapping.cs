using Microsoft.EntityFrameworkCore;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Entities.Ground;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Models;
using Sportsmandu.Models.Association;
using Sportsmandu.Models.League;
using Sportsmandu.Models.Messaging.Notification;
using Sportsmandu.Models.Stats;

namespace Sportsmandu.Database.Seed
{
    public class SportsmanduMappingEF
    {
        public static void SportsmanduMapping(ModelBuilder builder)
        {
            //builder.Entity<SeasonStatsDb>().HasKey(x => new { x.SeasonId });
            builder.Entity<SeasonFixtureSubscription>().HasKey(x => new { x.SeasonFixtureGuid, x.UserId });
            builder.Entity<GroundSubscription>().HasKey(x => new { x.GroundId, x.UserId });
            builder.Entity<LeagueSubscription>().HasKey(x => new { x.LeagueId, x.UserId });
            builder.Entity<ClubSubscription>().HasKey(x => new { x.ClubId, x.UserId });
            builder.Entity<PlayerSubscription>().HasKey(x => new { x.PlayerGuid, x.UserEmail });
            builder.Entity<SeasonSubscription>().HasKey(x => new { x.SeasonId, x.UserId });
            builder.Entity<PlayerSocial>().HasKey(x => new { x.PlayerId, x.Social });
            builder.Entity<GroundSocial>().HasKey(x => new { x.GroundId, x.Social });
            builder.Entity<PartnersSocial>().HasKey(x => new { x.PartnerId, x.Social });
            builder.Entity<AssociationsSocial>().HasKey(x => new { x.AssociationId, x.Social });
            builder.Entity<LeagueSocial>().HasKey(x => new { x.LeagueId, x.Social });
            builder.Entity<ClubsSocial>().HasKey(x => new { x.ClubId, x.Social });
            builder.Entity<Predict>().HasKey(x => new { x.UserId, x.FixtureGuid });
            builder.Entity<SeasonCategoryClubGroup>().HasKey(x => new { x.SeasonId, x.ClubId,x.CategoryId});
            builder.Entity<ApplicationUserRole>().HasKey(x => new { x.RoleId, x.UserId, x.Segment, x.SegmentValue });

            builder.Entity<ApplicationUserGames>().HasKey(x => new { x.GameId, x.UserId});

            builder.Entity<ApplicationUserRole>()
            .HasOne(x => x.User)
            .WithMany(x => x.Roles)
            .HasForeignKey(x => x.UserId);
            //builder.Entity<SeasonMatch>().HasKey(x => new { x.Id, x.TeamOneId, x.TeamTwoId, x.GroundId, x.CategoryId, x.MatchType, x.SeasonId, x.Date });
            builder.Entity<UserNotification>().HasKey(x => new { x.UserId, x.DeviceId });
            builder.Entity<AllNotification>().HasKey(x => new { x.DeviceId });
            builder.Entity<SignalUser>().HasKey(x => new { x.UserId, x.ConnectionId, x.DeviceId });
            builder.Entity<BlogPostBlogTag>()
              .HasOne(x => x.Post)
              .WithMany(x => x.Tags)
              .HasForeignKey(x => x.PostId);
            builder.Entity<BlogPostBlogTag>().HasKey(x => new { x.PostId, x.TagId });
            builder.Entity<ClubPlayers>().HasKey(x => new { x.ClubId, x.PlayerId });
            builder.Entity<ClubCategory>().HasKey(x => new { x.ClubId, x.CategoryId });
            
            builder.Entity<SiteSectionLanguageTitle>().HasKey(x => new { x.LanguageTitleId, x.SiteSectionId });
            builder.Entity<SiteSection>().HasKey(x => new { x.Id });
            builder.Entity<SiteSectionLanguageTitle>()
              .HasOne(x => x.SiteSection)
              .WithMany(x => x.LanguageTitles)              
              .HasForeignKey(x => x.SiteSectionId)
              .IsRequired();                        
               
            
            builder.Entity<LanguageTitle>().HasKey(x => new { x.Id });


            builder.Entity<ClubPlayers>()
              .HasOne(x => x.Club)
              .WithMany(x => x.Players)
              .HasForeignKey(x => x.ClubId);
            builder.Entity<ClubEmployee>()
            .HasOne(x => x.Club)
            .WithMany(x => x.Employee);
            builder.Entity<ClubEmployee>().HasKey(x => new { x.ClubId, x.EmployeeId, x.DesignationId });

            builder.Entity<SeasonEmployee>()
            .HasOne(x => x.Season)
            .WithMany(x => x.Employee);
            builder.Entity<SeasonEmployee>().HasKey(x => new { x.SeasonId, x.EmployeeId, x.DesignationId });

            builder.Entity<AssociationEmployee>()
            .HasOne(x => x.Association)
            .WithMany(x => x.Employee);
            builder.Entity<AssociationEmployee>().HasKey(x => new { x.AssociationId, x.EmployeeId, x.DesignationId });

            builder.Entity<LeagueEmployee>()
                        .HasOne(x => x.League)
                        .WithMany(x => x.Employee);

            builder.Entity<LeagueEmployee>().HasKey(x => new { x.LeagueId, x.EmployeeId, x.DesignationId });

                     

            builder.Entity<SeasonPartners>().HasKey(x => new { x.SeasonId, x.PartnerId });
            //builder.Entity<SeasonMatch>()
            //    .Property(b => b.Id)
            //    .ValueGeneratedOnAdd();

            //builder.Entity<SeasonMatchEvent>()
            //                .Property(b => b.Id)
            //                .Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;

            builder.Entity<SeasonGround>().HasKey(x => new { x.SeasonId, x.GroundId });
            builder.Entity<FixturePlayerStatDb>().HasKey(x => new { x.FixtureGuid, x.PlayerId,x.Type,x.Sub_Type});

            builder.Entity<SeasonGround>()
             .HasOne(x => x.Season)
             .WithMany(x => x.Grounds)
             .HasForeignKey(x => x.SeasonId);

            //builder.Entity<SeasonMatch>().HasKey(x => new { x.Id });
            //builder.Entity<SeasonMatch>().Property(t => t.Id)
            //    .ValueGeneratedOnAdd();
            //builder.Entity<SeasonMatch>()
            //    .Property(i => i.Id);
            //    builder.Entity<SeasonMatch>()

            //    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            builder.Entity<SeasonMatchEvent>().HasKey(x => new { x.ClubId, x.PlayerId, x.SeasonMatchGuid, x.DateTime });



            builder.Entity<SeasonPartners>()
                          .HasOne(x => x.Season)
                          .WithMany(x => x.Partners)
                          .HasForeignKey(x => x.SeasonId);

            builder.Entity<SeasonCategories>().HasKey(x => new { x.SeasonId, x.CategoryId });
            builder.Entity<SeasonCategories>()
                          .HasOne(x => x.Season)
                          .WithMany(x => x.Categories)
                          .HasForeignKey(x => x.SeasonId);



            builder.Entity<SeasonFixtureTeams>().HasKey(x => new { x.SeasonFixtureGuid, x.ClubId });
            builder.Entity<SeasonFixtureTeamPlayers>().HasKey(x => new { x.SeasonFixtureGuid, x.ClubId, x.PlayerId }); // TODO GAREKO CHAINA YO

            builder.Entity<SeasonClubs>().HasKey(x => new { x.SeasonId, x.ClubId });
            builder.Entity<SeasonClubs>()
                          .HasOne(x => x.Season)
                          .WithMany(x => x.Clubs)
                          .HasForeignKey(x => x.SeasonId);
            builder.Entity<SeasonClubPlayers>().HasKey(x => new { x.SeasonId, x.ClubId, x.PlayerId });

            builder.Entity<PlayerConvertedCulture>().HasKey(x => new { x.PlayerId, x.CultureId });
            builder.Entity<PlayerConvertedCulture>()
                                      .HasOne(x => x.Player)
                                      .WithMany(x => x.ConvertedCulture)
                                      .HasForeignKey(x => x.PlayerId);


            builder.Entity<GroundConvertedCulture>().HasKey(x => new { x.GroundId, x.CultureId });
            builder.Entity<GroundConvertedCulture>()
                                      .HasOne(x => x.Ground)
                                      .WithMany(x => x.ConvertedCulture)
                                      .HasForeignKey(x => x.GroundId);

            builder.Entity<LeagueConvertedCulture>().HasKey(x => new { x.LeagueId, x.CultureId });
            builder.Entity<LeagueConvertedCulture>()
                                      .HasOne(x => x.League)
                                      .WithMany(x => x.ConvertedCulture)
                                      .HasForeignKey(x => x.LeagueId);
            builder.Entity<SeasonConvertedCulture>().HasKey(x => new { x.SeasonId, x.CultureId });
            builder.Entity<SeasonConvertedCulture>()
                                      .HasOne(x => x.Season)
                                      .WithMany(x => x.ConvertedCulture)
                                      .HasForeignKey(x => x.SeasonId);
            builder.Entity<PartnerConvertedCulture>().HasKey(x => new { x.PartnerId, x.CultureId });
            builder.Entity<PartnerConvertedCulture>()
                                      .HasOne(x => x.Partner)
                                      .WithMany(x => x.ConvertedCulture)
                                      .HasForeignKey(x => x.PartnerId);
            builder.Entity<AssociationConvertedCulture>().HasKey(x => new { x.AssociationId, x.CultureId });
            builder.Entity<AssociationConvertedCulture>()
                                      .HasOne(x => x.Association)
                                      .WithMany(x => x.ConvertedCulture)
                                      .HasForeignKey(x => x.AssociationId);

            builder.Entity<ClubConvertedCulture>().HasKey(x => new { x.ClubId, x.CultureId });
            builder.Entity<ClubConvertedCulture>()
                                      .HasOne(x => x.Club)
                                      .WithMany(x => x.ConvertedCulture)
                                      .HasForeignKey(x => x.ClubId);

            builder.Entity<BlogConvertedCulture>().HasKey(x => new { x.BlogId, x.CultureId });
            builder.Entity<BlogConvertedCulture>()
                                      .HasOne(x => x.Blog)
                                      .WithMany(x => x.ConvertedCulture)
                                      .HasForeignKey(x => x.BlogId);

        }
    }
}