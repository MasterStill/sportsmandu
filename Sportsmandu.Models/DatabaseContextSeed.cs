﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Sportsmandu.Model.Entities;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Model.Entities.Global;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Generic;
using Sportsmandu.Models;
using Sportsmandu.Models.Messaging.Notification;
using System;
namespace Sportsmandu.Database.Seed
{
    public sealed class DatabaseContextSeed
    {
        public void Seed(ModelBuilder builder)
        {
            SportsmanduMappingEF.SportsmanduMapping(builder);
            var hasher = new PasswordHasher<ApplicationUser>();
            builder.Entity<ApplicationRole>().HasData(new ApplicationRole { Id = 1, Name = "Super_Admin", NormalizedName = "Super_Admin".ToUpper() });
            builder.Entity<ApplicationRole>().HasData(new ApplicationRole { Id = 2, Name = "Admin", NormalizedName = "Admin".ToUpper() });
            builder.Entity<ApplicationRole>().HasData(new ApplicationRole { Id = 3, Name = "ClubAdmin", NormalizedName = "ClubAdmin".ToUpper() });
            builder.Entity<ApplicationRole>().HasData(new ApplicationRole { Id = 4, Name = "ClubEditor", NormalizedName = "ClubEditor".ToUpper() });
            builder.Entity<ApplicationUser>().HasData(new ApplicationUser
            {
                Id = 1,
                UserName = "masterstill@gmail.com",
                NormalizedUserName = "masterstill@gmail.com",
                Email = "masterstill@gmail.com",
                NormalizedEmail = "materstill@gmail.com",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "123!@#Subrat!!"),
                SecurityStamp = string.Empty,
                multilingualid = 1//TODO This fucking is done by error ,w ill be removed in newer version after launch
            });
            AddStore(builder);
            AddCategory(builder);
            AddCulture(builder);
            AddAdminMenu(builder);
            AddCountry(builder);
        }        
        public void AddStore(ModelBuilder builder)
        {
            builder.Entity<Club>().HasData(new Sportsmandu.Models.Club
            {
                Id = 1,
                Name = "Sportsmandu Base Club",
                CreatedById = 1,
                multilingualid = 1,
                CultureId = 1
            });            
        }
        public void AddCulture(ModelBuilder builder)
        {
            builder.Entity<Culture>().HasData(new Culture
            {
                Id = 1,
                CultureId = 1,
                multilingualid = 1,
                Title = "English",
                Code = "En",
                Delflag = false,
                CreatedById = 1,
                Verified = true
            }
            , new Culture
            {
                Id = 2,
                CultureId = 2,
                multilingualid = 2,
                Title = "नेपाली",
                Code = "Np",
                Delflag = false,
                CreatedById = 1,
                Verified = true
            },
            new Culture
            {
                Id = 3,
                CultureId = 3,
                multilingualid = 3,
                Title = "मधिसे",
                Code = "Ma",
                Delflag = false,
                CreatedById = 1,
                Verified = true
            }, new Culture
            {
                Id = 4,
                CultureId = 4,
                multilingualid = 4,
                Title = "जनजाती",
                Code = "Jana",
                Delflag = false,
                CreatedById = 1,
                Verified = true
            });
        }
        public void AddAdminMenu(ModelBuilder builder)
        {

            builder.Entity<AdminMenu>().HasData(
             new AdminMenu
             {
                 Id = 3,
                 multilingualid = 3,
                 CanCreate = true,
                 CreatedById = 1,
                 CultureId = 1,
                 Summary = "Deals With Category",
                 ImageUrl = "",
                 Url = "Admin/1/Category",
                 Title = "Category",
                 Converted = true,
                 Meta = "Small Category Description",
                 Verified = true
             },
            new AdminMenu
            {
                Id = 5,
                multilingualid = 5,
                CanCreate = true,
                CreatedById = 1,
                CultureId = 1,
                Summary = "Deals With Players",
                ImageUrl = "",
                Url = "Admin/1/Players",
                Title = "Players",
                Converted = true,
                Meta = "Players Description",
                Verified = true
            },
             new AdminMenu
             {
                 Id = 1,
                 multilingualid = 1,
                 CanCreate = true,
                 CreatedById = 1,
                 CultureId = 1,
                 Summary = "Deals With Clubs",
                 ImageUrl = "",
                 Url = "Admin/1/Clubs",
                 Title = "Clubs",
                 Converted = true,
                 Meta = "Clubs Description",
                 Verified = true
             },
             new AdminMenu
             {
                 Id = 2,
                 multilingualid = 2,
                 CanCreate = true,
                 CreatedById = 1, 
                 CultureId = 1,
                 Summary = "Deals With Grounds",
                 ImageUrl = "",
                 Url = "Admin/1/Grounds",
                 Title = "Grounds",
                 Converted = true,
                 Meta = "Grounds Description",
                 Verified = true
             },
             new AdminMenu
             {
                 Id = 4,
                 multilingualid = 4,
                 CanCreate = true,
                 CreatedById = 1,
                 CultureId = 1,
                 Summary = "Deals With Leagues",
                 ImageUrl = "",
                 Url = "Admin/1/Leagues",
                 Title = "Leagues",
                 Converted = true,
                 Meta = "Leagues Description",
                 Verified = true
             },
              new AdminMenu
              {
                  Id = 6,
                  multilingualid = 6,
                  CanCreate = true,
                  CreatedById = 1,
                  CultureId = 1,
                  Summary = "Deals With Associations",
                  ImageUrl = "",
                  Url = "Admin/1/Associations",
                  Title = "Associations",
                  Converted = true,
                  Meta = "Associations Description",
                  Verified = true
              },
              new AdminMenu
              {
                  Id = 7,
                  multilingualid = 7,
                  CanCreate = true,
                  CreatedById = 1,
                  CultureId = 1,
                  Summary = "Deals With Seasons / Cup / Tournments",
                  ImageUrl = "",
                  Url = "Admin/1/Seasons",
                  Title = "Seasons / Cup / Tournments",
                  Converted = true,
                  Meta = "Seasons / Cup / Tournments Description",
                  Verified = true
              },
              new AdminMenu
              {
                  Id = 8,
                  multilingualid = 8,
                  CanCreate = true,
                  CreatedById = 1,
                  CultureId = 1,
                  Summary = "Deals With Partners of League",
                  ImageUrl = "",
                  Url = "Admin/1/Partners",
                  Title = "Partners of League",
                  Converted = true,
                  Meta = "Partners of League Description",
                  Verified = true
              },
              new AdminMenu
              {
                  Id = 9,
                  multilingualid = 9,
                  CanCreate = true,
                  CreatedById = 1,
                  CultureId = 1,
                  Summary = "Deals With Website Globalization",
                  ImageUrl = "",
                  Url = "Admin/1/Globalizations",
                  Title = "Website Globalization",
                  Converted = true,
                  Meta = "Website Globalization Description",
                  Verified = true
              },
              new AdminMenu
              {
                  Id = 10,
                  multilingualid = 10,
                  CanCreate = true,
                  CreatedById = 1,
                  CultureId = 1,
                  Summary = "Deals With News / Interview",
                  ImageUrl = "",
                  Url = "Admin/1/Blogs/",
                  Title = "Website News",
                  Converted = true,
                  Meta = "Website News Description",
                  Verified = true
              },
              new AdminMenu
              {
                  Id = 11,
                  multilingualid = 11,
                  CanCreate = true,
                  CreatedById = 1,
                  CultureId = 1,
                  Summary = "Deals With Videos Segment",
                  ImageUrl = "",
                  Url = "Admin/1/Videos/",
                  Title = "Website Videos",
                  Converted = true,
                  Meta = "Website Videos Description",
                  Verified = true
              }
            );
        }
        public void AddCountry(ModelBuilder builder)
        {
            builder.Entity<Country>().HasData(
                new Country
                {
                    Id = 1,
                    CultureId = 1,
                    multilingualid = 1,
                    Name = "Nepal",
                    Verified = true,
                    Description = "Nepal Awesome Country Ho",
                    CreatedById = 1
                },
                new Country
                {
                    Id = 2,
                    CultureId = 1,
                    multilingualid = 2,
                    Name = "India",
                    Verified = true,
                    Description = "India pani sanai Awesome Country Ho",
                    CreatedById = 1
                }, new Country
                {
                    Id = 3,
                    CultureId = 2,
                    multilingualid = 1,
                    Name = "Nepal in Nepali",
                    Verified = true,
                    Description = "Nepal Awesome Country Ho in Nepali",
                    CreatedById = 1
                }, new Country
                {
                    Id = 4,
                    CultureId = 2,
                    multilingualid = 2,
                    Name = "India in Nepali",
                    Verified = true,
                    Description = "India ko bare ma nepali ma sano description",
                    CreatedById = 1
                }


                );
        }
        // public void AddAssociation(ModelBuilder builder)
        // {
        //     builder.Entity<Association>().HasData(
        //         new Association
        //         {
        //             Id = 1,
        //             CultureId = 1,
        //             multilingualid = 1,
        //             Name = "Nepal",
        //             Verified = true,
        //             Description = "Nepal Awesome Association Ho",
        //             CreatedById = 1
        //         },
        //         new Association
        //         {
        //             Id = 2,
        //             CultureId = 2,
        //             multilingualid = 1,
        //             Name = "India",
        //             Verified = true,
        //             Description = "India pani sanai Awesome Association Ho",
        //             CreatedById = 1
        //         });
        // }
        public void AddCategory(ModelBuilder builder)
        {
            builder.Entity<Category>().HasData(
                new Category
                {
                    Id = 1,
                    CultureId = 1,
                    multilingualid = 1,
                    Name = "Football",
                    Verified = true,
                    Description = "Football ko starting in Nepal ko description",
                    CreatedById = 1
                },
                new Category
                {
                    Id = 2,
                    CultureId = 1,
                    multilingualid = 2,
                    Name = "Cricket",
                    Verified = true,
                    Description = "Cricket ko starting in Nepal ko description",
                    CreatedById = 1
                }
            );
        }
    }
}