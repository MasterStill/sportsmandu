﻿using Sportsmandu.Models.Generic;
using Sportsmandu.Models.League;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Resources;
using Sportsmandu.Model;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Generic;

namespace Sportsmandu.Models
{
    public class Game : SportsmanduMandatory
    {
        public Game()
        {
            this.ApplicationUserGames = new HashSet<ApplicationUserGames>();
        }
        public string Name { get; set; }
        public string Image { get; set; }
        public enumGameType Type { get; set; }
        public string Thumbnail { get; set; }
        public string Description { get; set; }
        public string FolderUrl { get; set; }
        public ICollection<ApplicationUserGames> ApplicationUserGames { get; set; }
        public string URL
        {
            get
            {
                try
                {
                    var result =
                        Sportsmandu.Models.Entitiesss.Global.GetCleanUrl(
                            $"{Culture.Code}/Game/PlayGame/{Guid}/{Name}");
                    return result;
                }
                catch (Exception)
                {
                    return "ERROR";
                }
            }
        }
    }

    public enum enumGameType
    {
        Level, High_Score , Points
    }

    public class ApplicationUserGames
    {
        public ApplicationUserGames()
        {
            this.User = new ApplicationUser();
            this.Game = new Game();
        }
        public int UserId { get; set; }
        public int GameId { get; set; }
        public int Level { get; set; }
        public int HighScore { get; set; }
        public int Points { get; set; }
        public ApplicationUser User { get; set; }
        public Game Game { get; set; }
    }

    public class GameScoreCreateVM
    {
        public string Guid { get; set; } // Game Guid
        public string Points { get; set; } // Can have score and high score too or may be point achieved.
        public string Level { get; set; } // Can have score and high score too 
        public string HighScore { get; set; } // Can have score and high score too
        public string Salt { get; set; }  // Unique Identifier to check if the data is tampred !
    }

    public class ScoreViewModel
    {
        public int Level { get; set; }
        public int HighScore { get; set; }
        public int Points { get; set; }
        public string UserImage { get; set; }
        public string UserFullName { get; set; }
    }
    public class GameViewModel
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string Thumbnail { get; set; }
        public enumGameType Type { get; set; }
        public string FolderUrl { get; set; }

        public string URL { get; set; }

        public List<ScoreViewModel> HighScore { get; set; }
    }
}