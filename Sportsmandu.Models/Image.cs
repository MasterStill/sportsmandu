﻿using System;
using System.Collections.Generic;
using System.Text;
namespace Sportsmandu.Models
{
    public class SportsmanduImage
    {
        public class ImageSize
        {
            public enumImageSegment enumImageSegment { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public string DefaultNoImagePath { get; set; }
            public string DefaultSavePath { get; set; }
        }
        public enum enumImageSegment
        {
            player_profile, player_coverImage, club_profile, club_coverImage, season_profile, season_coverImage, ground_profile, ground_coverImage, league_profile, league_coverImage,user_avatar
        }
    }    
}
