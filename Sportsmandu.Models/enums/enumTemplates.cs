﻿namespace Sportsmandu.Models.enums
{
    public enum enumEmailTemplate
    {
        WelcomeEmail, ConfirmationEmail, ForgotyourPassword, RefferalInvitation, InvitationForRoleNewUser, AssignedToRole
        // To Be Added for Sportsmandu
        // Invitation_to_customer from vendors
        // Customer_Receipt
        // Has_New_Email ( for customer from vendors about any new messages in the account )
    }
}
