﻿namespace Sportsmandu.Models.enums
{
    public enum enumUserType
    {
        User, SupportStaff, AccountStaff, CommunityAdmin
    }
    public enum enumPlayerRole
    {
        Unknown, Goalkeeper, Defender, Midfielder, Forward, Striker,Right_Back,Centre_Back,Defensive_Midfielder,Centre_Midfielder,// Football
        Bowler, Batsman, Wicketkeeper, Fielder, Allrounder, // Cricket
        Swingman, Point_Guard, Guard, // Basketball
        Setter, Outside_Hitter, Middle_Blocker, Libero, Opposite, //Volleyball
                                                                  // Field Hockey
        Centre, Left_and_right_backs, Circlerunner, Left_and_right_Wingers, Substitutes // Handball
    }
    public enum enumGameRolesFootball
    {
        Unknown, Goalkeeper, Defender, Midfielder, Forward, Striker, Right_Back, Center_Back
    }
    public enum enumGameRolesCricket
    {
        Unknown,Bowler, Batsman, Wicketkeeper, Fielder, Allrounder,
    }
    public enum enumGameRolesBasketball
    {
        Unknown,Swingman, Forward, Point_Guard, Guard
    }
    public enum enumGameRolesVolleyball
    {
        Unknown,Setter, Outside_Hitter, Middle_Blocker, Libero, Opposite
    }
    public enum enumGameRolesFieldHockey
    {
        Unknown,Goalkeeper, Defender, Midfielder, Forward
    }
    public enum enumGameRolesFieldHandball
    {
        Unknown,Goalkeeper, Centre, Left_and_right_backs, Circlerunner, Left_and_right_Wingers, Substitutes
    }
}