﻿using System;
namespace Sportsmandu.Models
{
    public class ClubRating
    {                
        public int Id { get; set; }
        public int StoreId { get; set; }    
        public int UserId { get; set; }
        public string Comments { get; set; }
        public DateTime? ThisDateTime { get; set; }
        public int? Rating { get; set; }        
    }
}