﻿using Sportsmandu.Models.Generic;
using Sportsmandu.Models.League;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Sportsmandu.Model;

namespace Sportsmandu.Models
{
    public class Predict
    {
        public Predict()
        {
            this.Fixture = new SeasonFixture();
            this.User = new ApplicationUser();
        }
        public virtual SeasonFixture Fixture {get;set;}
        public string FixtureGuid { get; set; }
        public enumFixturePredict Prediction { get; set; }
        public int? TeamId {get;set;}
        public virtual Club Team { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        public bool Delflag { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool PredictionSucceded { get; set; }
        public PredictionResult Result { get; set; }
    }
    public enum PredictionResult
    {
        Not_Finalized, Correct , Prediction_Failed
    }
    //public class FixturePredictionVMForEs
    //{
    //    public string UserFullName { get; set; }
    //    public string CultureCode { get; set; }
    //    public string UserName { get; set; }
    //    public DateTime CreatedDate { get; set; }
    //    public string UserImage { get; set; }
    //    public string Result { get; set; }
    //    public string UserEmail { get; set; }
    //    public string TeamName { get; set; }
    //    public string ResultText
    //    {
    //        get
    //        {
    //            if (CultureCode == "Np")
    //            {
    //                if (Result == "Not_Finalized")
    //                {
    //                    return "नतिजा आएको छैन";
    //                }
    //                else if (Result == "Prediction_Failed")
    //                {
    //                    return "अनुमान मिलेन";
    //                }
    //                else if (Result == "Correct")
    //                {
    //                    return "अनुमान मिल्यो";
    //                }
    //            }
    //            else
    //            {
    //                return Result.Replace("_", " ");
    //            }
    //            return null;
    //        }
    //    }
    //    public NameIdImageViewModel Team { get; set; }
    //    public int? TeamId { get; set; }
    //    public string PredictionText
    //    {
    //        get
    //        {
    //            if (TeamId != null)
    //            {
    //                return Team.Name + (CultureCode == "En" ? " Will Win" : " ले जित्नेछ");
    //            }
    //            else
    //            {
    //                return CultureCode == "En" ? "The Game Will Be Draw" : "प्रतिस्पर्धा बराबरी हुनेछ";
    //            }
    //        }
    //    }
    //    public enumFixturePredict Prediction { get; set; }
    //    public int Points { get; set; }
    //}
        public class PredictionVMFORES : ElasticMandatory
        {
        public string CultureCode { get; set; }
        public int UserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string FullName { get; set; }
        public string UserFullName { get; set; }
        public string UserImage { get; set; }
        public string UserEmail { get; set; }        
        public string Prediction { get; set; }
        public string FixtureGuid { get; set; }
        public string Result { get; set; }
        public string ResultText { get
            {
                if(CultureCode == "Np")
                {
                    if (Result == "Not_Finalized")
                    {
                        return "नतिजा आएको छैन";
                    }else if (Result == "Prediction_Failed")
                    {
                        return "अनुमान मिलेन";
                    }
                    else if (Result == "Correct")
                    {
                        return "अनुमान मिल्यो";
                    }
                }
                else
                {
                    return Result.Replace("_", " ");
                }
                return null;
            }
        }
        public string PredictionText { get
            {
               if(TeamId != null)
               {
                    return Team.Name + (CultureCode == "En" ?  " Will Win" : " ले जित्नेछ");
               }else
               {
                    return CultureCode == "En" ? "The Game Will Be Draw" : "प्रतिस्पर्धा बराबरी हुनेछ";
               }
            }
        }
        public int? TeamId { get; set; }
        //public virtual SeasonFixture Fixture { get; set; }
        public List<NameIdImageViewModel> Teams { get; set; }
        public NameIdImageViewModel Team { get; set; }
        public NameIdImageViewModel FixtureSeason { get; set; }
        public NameIdImageViewModel FixtureCategory { get; set; }
        public DateTime FixtureDate { get; set; }
        public string Id
        {
            get
            {
                return UserId + "_" + FixtureGuid + "_" + (CultureCode == null ? "En" : CultureCode);
            }
        }
        public int Points {
            get {
                try
                {
                    return Result.Contains("Correct") ? 3 : 0;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
        public bool PredictionSucceded { get {
                try
                {
                    return Result.Contains("Correct") ? true : false;
                }
                catch (Exception ex)
                {
                    return false;
                }
            } }
    }
    public enum enumFixturePredict
    {
        Win , Loose , Draw
    }
}