﻿using Sportsmandu.Models.Generic;
using Sportsmandu.Models.League;
using System.Collections.Generic;
using Sportsmandu.Model;

namespace Sportsmandu.Models.Stats
{
    public class SeasonStatsGooglePlayers
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public int TypeData { get; set; }
    }
    public class SeasonStatsFromGoogle
    {        
        public string Title { get; set; }
        public List<SeasonStatsGooglePlayers> Players { get; set; }
        //public string Segment { get; set; }
    }
    public class StatPlayer
    {        
        public int TypeData { get; set; }
        public int Id { get; set; }
        public int ClubId { get; set; }
    }
    public class SeasonStatsDb
    {
        public int Id { get; set; }
        public int SeasonId { get; set; }
        public string SeasonStatsData { get; set; }
    }

    public class FixtureTeamStatsDb
    {
        public int SeasonId { get; set; }
    }
    public class FixturePlayerStatDb // This is new i dont know where i wrote for older version and no time for search lol !
    {
        public string FixtureGuid { get; set; }
        public int PlayerId { get; set; }
        public string Type { get; set; }
        public string Sub_Type { get; set; }
        public int Value { get; set; }
    }
    public class SeasonStats
    {
        public int SeasonId { get; set; }
        public enumCricketStats Stats_Title { get;set; }
        public List<StatPlayer> Players { get; set; }
    }
    public class SeasonStatsVMFORES : ElasticMandatory
    {
        public SeasonStatsVMFORES()
        {
            this.Players = new List<PlayersForSeasonStatsVMFORES>();
        }
        public string Id { get
            {
                return SeasonGuid + Stats_Title;
            }
        }
        public int SeasonId { get; set; }
        public string SeasonGuid { get; set; }
        public string Stats_Title { get; set; }
        public List<PlayersForSeasonStatsVMFORES> Players { get; set; }
        public string CultureCode { get; set; }
        //public string Club{ get; set; }
        //public string ClubImage { get; set; }
    }
    public class PlayersForSeasonStatsVMFORES
    {        
        public string Name { get; set; }             
        public string Thumbnail { get; set; }
        public string URL { get; set; }   
        public int Data { get; set; }
        public string Club { get; set; }
        public string ClubImage { get; set; }
    }
    public enum enumCricketStats
    {
        Runs,Wickets,Best_Bowling_Figures,Batting_Average,Bowling_Average,Most_Hundreds,Most_Fifties,Most_Economical_Bowlers,Five_Wicket_Hauls,Sixes,Fours,Boundaries, Highest_Scores
    }
}