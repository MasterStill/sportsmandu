﻿namespace Sportsmandu.Models.Stats
{
    public class PlayerStatsSquash
    {
        public int Won { get; set; }
        public int Played { get; set; }
        public string BestGame { get; set; } // BestGameURL
    }    
    public class ClubPlayerStatsSquash
    {
        public int Won { get; set; }
        public int Played { get; set; }
    }
    public class SeasonMatchStatsSquash : PointSystem
    {
        
    }
    public class SeasonStandingSquash
    {
        public int Gold_Medal_Won { get; set; }
        public int Silver_Medal_Won { get; set; }
        public int Bronze_Medal_Won { get; set; }
    }
    public class ClubStatsSquash
    {
        public int Played { get; set; }
        public int Won { get; set; }
    }

    public class PointSystem
    {
        public int Points_Round_One { get; set; }
        public int Points_Round_Two { get; set; }
        public int Points_Round_Three { get; set; }
        public int Points_Round_Four { get; set; }
        public int Points_Round_Five { get; set; }
        public int Points_Round_Six { get; set; }        
    }
    public class SeasonMatchStatsVolleyBall : PointSystem
    {

    }
}