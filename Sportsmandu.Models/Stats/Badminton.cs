﻿namespace Sportsmandu.Models.Stats
{
    public class PlayerStatsCricket
    {
        public string Id { get; set; } // PlayerId
        public int ODIMatches { get; set; }
        public int T20IMatches { get; set; }
        public int FCMatches { get; set; }
        public int LAMatches { get; set; }
    }
}