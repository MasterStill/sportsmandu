﻿namespace Sportsmandu.Models.Stats
{
    public class PlayerStatsFootball
    {        
        public int Fouls { get; set; }
        public int YellowCard { get; set; }
        public int RedCard { get; set; }        
        public int Goal { get; set; }
    }
    public class ClubPlayerStatsFootball
    {                
        public int Fouls { get; set; }
        public int YellowCard { get; set; }
        public int RedCard { get; set; }
        public int Goal { get; set; }
    }
    public class SeasonMatchStatsFootball
    {        
        public int Shots { get; set; }
        public int Goals { get; set; }
        public int ShotsOnGoal { get; set; }
        public int Possession { get; set; }
        public int Offsides { get; set; }
        public int CornerKick { get; set; }
        public int Save { get; set; }
    }
    class SeasonStatsFootBall : ClubStatsFootball
    {        
        public int PlayedGames { get; set; }
        public int Won { get; set; }
        public int Lost { get; set; }
    }
    class ClubStatsFootball
    {        
        public int GamesPlayed { get; set; }
        public int YellowCard { get; set; }
        public int RedCard { get; set; }
        public int GoalFor { get; set; }
        public int GoalAgainst { get; set; }
        public int GamesWon { get; set; }
        public int GamesLost { get; set; }
    }
}