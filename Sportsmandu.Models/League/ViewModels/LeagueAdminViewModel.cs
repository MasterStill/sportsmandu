using System.ComponentModel.DataAnnotations;
using Community.Model.Entities.ViewModels;

namespace Sportsmandu.Models.League
{
    public class LeagueAdminViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Founded { get; set; }
        public string Website { get; set; }
        public string Source { get; set; }
        public bool Converted { get; set; }
        public string CountryName { get; set; }
        public string Email { get; set; }
        public string CategoryName { get; set; }
        public string Category { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public int multilingualid { get; set; }
    }
    public class LeagueCreateViewModel : CreateMandatory
    {        
        public string Name { get; set; }
        public string Description { get; set; }        
        public string Image { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        public string Contact { get; set; }
        public string Source { get; set; }
        public string Email { get; set; }        
        public int CountryId { get; set; }        
        public int CategoryId { get; set; }
        //public int CountryId { get; set; }
    }
    public class LeagueViewModel
    {
        public string Name { get; set; }
        public string Founded { get; set; }
        public string Website { get; set; }
        public string Source { get; set; }
    }
    public class AssociationAdminViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Founded { get; set; }
        public string Website { get; set; }
        public string Source { get; set; }
        public bool Converted { get; set; }
        public string CountryName { get; set; }
        public string Email { get; set; }
        public string CategoryName { get; set; }
        public string Category { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public int multilingualid { get; set; }
    }
    public class AssociationCreateViewModel : CreateMandatory
    {        
        public string Name { get; set; }
        public string Description { get; set; }        
        public string Image { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        public string Contact { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string FullStreetName { get; set; }
        public string Source { get; set; }
        public string Email { get; set; }        
        public int CountryId { get; set; }        
        public int CategoryId { get; set; }
        //public int CountryId { get; set; }
    }
    public class AssociationViewModel
    {
        public string Name { get; set; }
        public string Founded { get; set; }
        public string Website { get; set; }
        public string Source { get; set; }
    }
}