﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Sportsmandu.Model;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Entities.Player;

namespace Sportsmandu.Models.League
{
    public class PartnersSocial
    {
        public string Link { get; set; }
        public enumSocial Social { get; set; }
        public int PartnerId { get; set; }
        public virtual Partner Club { get; set; }        
    }
    public class PartnerConvertedCulture
    {
        public PartnerConvertedCulture()
        {
            this.Partner = new Partner();
            this.Culture = new Culture();
        }
        public int PartnerId { get; set; }
        public int CultureId { get; set; }
        public virtual Culture Culture { get; set; }
        public virtual Partner Partner { get; set; }
    }
    public class Partner : SportsmanduMandatory, ISportsmanduBase
    {
        public Partner()
        {
            this.Seasons = new HashSet<SeasonPartners>();
            this.PartnerSocial = new HashSet<PartnersSocial>();
            this.ConvertedCulture = new HashSet<PartnerConvertedCulture>();
        }
        public string Name { get; set; }
        public string Image { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        public string Email { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string FullStreetName { get; set; }
        // public int Rating { get; set; }
        public virtual ICollection<SeasonPartners> Seasons { get; set; }       
        public virtual ICollection<PartnersSocial> PartnerSocial { get; set; }
        public virtual ICollection<PartnerConvertedCulture> ConvertedCulture { get; set; } 
        public string URL
        {
            get
            {
                try
                {
                    return Sportsmandu.Models.Entitiesss.Global.GetCleanUrl($"{Culture.Code}/Partners/{Guid}/{Name}");
                }
                catch
                {
                    return "ERROR";
                }
            }
        }
    }
    public class PartnerTitle : SportsmanduMandatory, ISportsmanduBase
    {
        public string Name { get; set; }
    }
    public class PartnerCreateViewModel
    {
        [Required]
        public int CultureId { get; set; }
        public int multilingualid { get; set; }
        [Required]
        public string Name { get; set; }
        public string Email { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string FullStreetName { get; set; }
        public string Image { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
    }
    public class PartnerVMFORES
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Thumbnail{ get; set; }
        public string CoverImage { get; set; }
        public string Email { get; set; }
        public string URL { get; set; }
        public string Description { get; set; }
        public string CultureCode { get; set; }
        // public ICollection<SeasonsVMForPartnerVM> Seasons { get; set; }
    }

    //public class GroundVMFORES
    //{
    //    public string Id { get; set; }
    //    public string Name { get; set; }
    //    public string Image { get; set; }
    //    public string Thumbnail { get; set; }
    //    public string CoverImage { get; set; }
    //    public string Description { get; set; }
    //    public string Summary { get; set; }
    //    public string CultureCode { get; set; }        
    //}

    public class PartnerAdminViewModel
    {
        public int Id { get; set; }
        public bool Converted { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public int multilingualid { get; set; }
    }
}