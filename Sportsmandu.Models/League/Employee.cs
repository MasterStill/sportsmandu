﻿using System.Collections.Generic;
using Sportsmandu.Model;
using Sportsmandu.Model.Entities.Player;

namespace Sportsmandu.Models.League
{
    public class Employee : SportsmanduMandatory, ISportsmanduBase
    {
        public Employee()
        {
            this.ClubEmployee = new HashSet<ClubEmployee>();
            this.LeagueEmployee = new HashSet<LeagueEmployee>();
            this.SeasonEmployee = new HashSet<SeasonEmployee>();
            //this.Employee = new HashSet<SeasonEmployee>();
        }
        public string Name { get; set; }
        //public int DesignationId { get; set; }
        public string Extension { get; set; }
        public string MobileNumber { get; set; }
        public string PhoneNumber { get; set; }
        public int XId { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public virtual Designation Designation { get; set; }
        public ICollection<ClubEmployee> ClubEmployee { get; set; }
        public ICollection<LeagueEmployee> LeagueEmployee { get; set; }
        public ICollection<SeasonEmployee> SeasonEmployee { get; set; }
        public string Image { get; set; }
        public int CountryId { get; set; }
        public enumGender Gender { get; set; }
        public string Thumbnail { get; set; }
        public string CoverImage { get; set; }
    }
    public class Designation : SportsmanduMandatory, ISportsmanduBase
    {
        public string Name { get; set; }
        public int Order { get; set; }
    }
}