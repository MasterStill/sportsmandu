﻿using Sportsmandu.Model;
using Sportsmandu.Model.Club.ViewModels;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Generic;
using Sportsmandu.Models.Gallery;
using Sportsmandu.Models.League;
using System;
using System.Collections.Generic;

namespace Sportsmandu.Models.Association
{
    public class AssociationsSocial
    {
        public string Link { get; set; }
        public enumSocial Social { get; set; }
        public int AssociationId { get; set; }
        public virtual Association Association { get; set; }        
    }
    public class AssociationConvertedCulture
    {
        public AssociationConvertedCulture()
        {
            this.Association = new Association();
            this.Culture = new Culture();
        }
        public int AssociationId { get; set; }
        public int CultureId { get; set; }
        public virtual Culture Culture { get; set; }
        public virtual Association Association { get; set; }
    }
    public class AssociationEmployee
    {
        public AssociationEmployee()
        {
            this.Association = new Association();
            this.Employee = new Employee();
            this.Designation = new Designation();
        }
        public int EmployeeId { get; set; }
        public int AssociationId { get; set; }
        public int DesignationId { get; set; }
        public DateTime? JoinedDate { get; set; }
        public DateTime? ResignedDate { get; set; }
        public virtual Designation Designation { get; set; }
        public Employee Employee { get; set; }
        public virtual Association Association { get; set; }
    }
    public class Association : SportsmanduMandatory, ISportsmanduBase // Governing Body
    {
        public Association(){
            this.AssociationSocial = new HashSet<AssociationsSocial>();
            this.ConvertedCulture = new HashSet<AssociationConvertedCulture>();
            this.Employee = new HashSet<AssociationEmployee>();
        }
        public string Name { get; set; }
        public string Image { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        public int Founded { get; set; }
        public string President { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string FullStreetName { get; set; }
        public string Website { get; set; }
        public string Description { get; set; }
        public int CountryId { get; set; }
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public string Email { get; set; }
        public virtual Country Country { get; set; }
        public virtual ICollection<AssociationEmployee> Employee { get; set; }
        public virtual ICollection<AssociationsSocial> AssociationSocial { get; set; }
        public virtual ICollection<AssociationConvertedCulture> ConvertedCulture { get; set; } 

        public string URL
        {
            get
            {
                try
                {
                    return Sportsmandu.Models.Entitiesss.Global.GetCleanUrl($"{Culture.Code}/{Country.Name}/Association/{Guid}/{Name.Trim()}");
                }
                catch
                {
                    return "ERROR";
                }
            }
        }
    }
    public class AssociationVMFORES
    {
        public AssociationVMFORES()
        {
            this.Social = new List<SocialVMFFORES>();
            this.Gallery = new List<GalleryVMFORES>();
        }
        public string Id { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public string CoverImage { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
        public string CultureCode { get; set; }
        public string CountryName { get; set; }
        public string URL { get; set; }
        public List<SocialVMFFORES> Social { get; set; }
        public List<GalleryVMFORES> Gallery { get; set; }
    }
}