﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sportsmandu.Model;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Entities.Ground;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Generic;
using Sportsmandu.Models;
using Sportsmandu.Models.enums;
using Sportsmandu.Models.Gallery;
using Sportsmandu.Models.Generic;
using Sportsmandu.Models.Stats;
using Sportsmandu.Models.Telegram;

namespace Sportsmandu.Models.League
{
    // League to season huncha jasto sahid smarika league cup 2001 
   
    public class Season : SportsmanduMandatory, ISportsmanduBase
    {
        public Season()
        {
            this.Partners = new HashSet<SeasonPartners>();
            this.Clubs = new HashSet<SeasonClubs>();
            this.Categories = new HashSet<SeasonCategories>();
            this.Grounds = new HashSet<SeasonGround>();
            this.Employee = new HashSet<SeasonEmployee>();
            this.Subscription = new HashSet<SeasonSubscription>();
            this.ConvertedCulture = new HashSet<SeasonConvertedCulture>();
        }
        public virtual ICollection<SeasonSubscription> Subscription { get; set; }
        public virtual ICollection<SeasonEmployee> Employee { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        //public string StatsString { get; set; }
        public string Description { get; set; }
        public DateTime StartedDate { get; set; }
        public DateTime EndDate { get; set; }
        public int LeagueId { get; set; }
        public int? HostCityId { get; set; }
        public int? TopPlacedTeamOneId { get; set; }
        public int? TopPlacedTeamTwoId { get; set; }
        public int? TopPlacedTeamThreeId { get; set; }
        public TournamentType TournamentType { get; set; }
        public virtual League League { get; set; }
        public virtual City HostCity { get; set; }
        public ICollection<SeasonPartners> Partners { get; set; }
        public ICollection<SeasonClubs> Clubs { get; set; }
        public ICollection<SeasonCategories> Categories { get; set; }
        public ICollection<SeasonGround> Grounds { get; set; }
        public virtual ICollection<SeasonConvertedCulture> ConvertedCulture { get; set; }
        public virtual Club TopPlacedTeamOne { get; set; }
        public virtual Club TopPlacedTeamTwo { get; set; }
        public virtual Club TopPlacedTeamThree { get; set; }
        public enumSeasonFixtureRound CurrentRound { get; set; }
        public string URL
        {
            get
            {
                try
                {
                    return Sportsmandu.Models.Entitiesss.Global.GetCleanUrl($"{Culture.Code}/Season/{Guid}/{Name}");
                }
                catch
                {
                    return "ERROR";
                }
            }
        }
    }
    public class SeasonConvertedCulture
    {
        public SeasonConvertedCulture()
        {
            this.Season = new Season();
            this.Culture = new Culture();
        }
        public int SeasonId { get; set; }
        public int CultureId { get; set; }
        public virtual Culture Culture { get; set; }
        public virtual Season Season { get; set; }
    }
    public class SeasonSubscription {     
        public SeasonSubscription(){
            this.Season = new Season();
            this.User =  new ApplicationUser();
    }
    public int SeasonId { get; set; }
    public int UserId { get; set; }
    public virtual Season Season { get; set; }
    public virtual ApplicationUser User { get; set; }
    }
public class SeasonEmployee
    {
        public SeasonEmployee()
        {
            this.Season = new Season();
            this.Employee = new Employee();
            this.Designation = new Designation();
        }
        public int EmployeeId { get; set; }
        public int SeasonId { get; set; }
        public int DesignationId { get; set; }
        public DateTime? JoinedDate { get; set; }
        public DateTime? ResignedDate { get; set; }
        public virtual Designation Designation { get; set; }
        public Employee Employee { get; set; }
        public virtual Season Season { get; set; }
    }

    public class SeasonCategories
    { // Tyo Season Ma k K Games Huncha !??
        public SeasonCategories()
        {
            this.Season = new Season();
            this.Category = new Category();
        }
        public int CategoryId { get; set; }
        public int SeasonId { get; set; }
        public string StatsString { get; set; } // TODO BreakDown
        public virtual Category Category { get; set; }
        public virtual Season Season { get; set; }
    }
    //public class SeasonMatchPlayers 
    //{
    //    //Tyo season ko match ma koko players Cha ? ( might be the case if there are more players than required in a team)
    //    // Wa ko ko substitue ma cha 
    //    // Wa kun position ma cha ? for Our Sports Journalist.
    //    [Key]
    //    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    //    public int Id { get; set; }
    //    public string SeasonFixtureGuid { get; set; }
    //    public int ClubId { get; set; }
    //    public int PlayerId { get; set; }
    //    public enumSeasonMatchPlayersType PlayersType { get; set; }
    //    public virtual Club Club { get; set; }
    //    public virtual Player Player { get; set; }
    //    public int PlayerX { get; set; }
    //    public int PlayerY { get; set; }
    //    public string PlayerRole { get; set; }
    //    public virtual SeasonFixture SeasonFixture { get; set; }
    //}
    public enum enumSeasonMatchPlayersType
    {
        First, Substitute
    }
    public class SeasonFixtureSubscription
    {
        public SeasonFixtureSubscription()
        {
            this.SeasonFixture = new SeasonFixture();
            this.User = new ApplicationUser();
        }
        public string SeasonFixtureGuid { get; set; }
        public int UserId { get; set; }
        public virtual SeasonFixture SeasonFixture { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
    //public class SeasonFixtureVMForTelegram
    //{
    //    public string Guid { get; set; }
    //    public string StatsString { get; set; }
    //    public enumFixtureStatus Status { get; set; }
    //    public string Summary { get; set; } // Team A won by 1 Goal // Team A Won

    //}
    public class SeasonFixture //: SportsmanduMandatory
    {
        public SeasonFixture()
        {
            this.Subscription = new HashSet<SeasonFixtureSubscription>();
            this.Teams = new HashSet<SeasonFixtureTeams>();
            this.Predictions = new HashSet<Predict>();
        }
        public ICollection<SeasonFixtureSubscription> Subscription { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public string Guid { get; set; }
        public int SeasonId { get; set; }
        public int GroundId { get; set; }
        public int CategoryId { get; set; }
        public int? SubCategoryId { get; set; }
        public enumSeasonMatchType MatchType { get; set; }
        public DateTime Date { get; set; }
        [ForeignKey("SeasonId")]
        public virtual Season Season { get; set; }
        [ForeignKey("GroundId")]
        public virtual Ground Ground { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }
        [ForeignKey("SubCategoryId")]
        public virtual SubCategory SubCategory { get; set; }
        public ICollection<SeasonFixtureTeams> Teams { get; set; }
        public string StatsString { get; set; }
        public enumFixtureStatus Status { get; set; }
        public string Summary { get; set; } // Team A won by 1 Goal // Team A Won
        public enumSeasonFixtureRound CurrentRound { get; set; }
        public virtual ICollection<Predict> Predictions { get; set; }
    }
    public enum enumSeasonFixtureRound
    {
        General,Quater_Final,Semi_Final,Final,KnockOut,For_Third_Place
    }
    public enum enumTeamFixtureStatus
    {
        Not_Started,Playing, Won , Lost , Draw
    }
    public enum enumFixtureStatus
    {
        Not_Started, Started, Completed
    }
    public class SeasonFixtureTeams
    {
        public SeasonFixtureTeams()
        {
            this.Club = new Club();
            this.SeasonFixture = new SeasonFixture();
        }
        public string SeasonFixtureGuid { get; set; }
        public int ClubId { get; set; }
        public enumTeamFixtureStatus Status { get; set; }
        public string StatsString { get; set; }
        public virtual SeasonFixture SeasonFixture { get; set; }
        public virtual Club Club { get; set; }
    }
    public class SeasonFixtureTeamPlayers
    {
        public string SeasonFixtureGuid { get; set; }
        public int ClubId { get; set; }
        public int PlayerId { get; set; }
        public virtual Club Club { get; set; }
        public virtual Player Player { get; set; }
        public virtual SeasonFixture SeasonFixture { get; set; }
    }
    public class SeasonFixtureTeamsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public string Image { get; set; }
        public string CoverImage { get; set; }
        public string URL { get; set; }
        public List<ClassFieldTypeForView> Stats { get; set; }
        public string Status { get; set; }
        public virtual List<SeasonFixtureTeamsViewModelPlayers> Players { get; set; }
    }
    public class SeasonFixtureTeamsViewModelPlayers
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PlayerRole { get; set; }
        public string Image { get; set; }
        public string URL { get; set; }
    }
    public class SeasonMatch
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        //public string Guid { get; set; }
        public int SeasonId { get; set; }
        public int TeamOneId { get; set; }
        public int TeamTwoId { get; set; }
        public int GroundId { get; set; }
        public int CategoryId { get; set; }
        public enumSeasonMatchType MatchType { get; set; }
        public DateTime Date { get; set; }
        public virtual Season Season { get; set; }
        public virtual Ground Ground { get; set; }
        public virtual Category Category { get; set; }
        //public virtual Club TeamOne { get; set; }
        //public virtual Club TeamTwo { get; set; }
    }
    public enum enumSeasonMatchType
    {
        Default, Male, Female
    }
    public class SeasonGround
    {
        public SeasonGround()
        {
            this.Season = new Season();
            this.Ground = new Ground();
        }
        public int SeasonId { get; set; }
        public int GroundId { get; set; }
        public virtual Season Season { get; set; }
        public virtual Ground Ground { get; set; }
    }
    public class SeasonMatchTeamCreateViewModel
    {
        public int ClubId { get; set; }
        public int SeasonGuid { get; set; }
    }
    public class SeasonMatchCreateViewModel
    {
        public string Guid { get; set; }
        public int SeasonId { get; set; }
        public int GroundId { get; set; }
        [Required]
        public int CategoryId { get; set; }
        public string CurrentRound { get; set; }
        public string Status { get; set; }
        public string MatchType { get; set; }
        public DateTime Date { get; set; }
        public List<TeamsViewModelForSeasonMatchCreateViewModel> Teams { get; set; }
    }
    public class TeamsViewModelForSeasonMatchCreateViewModel
    {
        public int Id { get; set; } // Club Id
    }
    public class SeasonMatchEvent
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string SeasonMatchGuid { get; set; }
        public DateTime DateTime { get; set; }
        public int PlayerId { get; set; }
        public int ClubId { get; set; }
        //public enumGameEventType Type { get; set; }
        public string Detail { get; set; } // Run >> 2 ; // Card >> Red
        public string SubDetail { get; set; } // Any Sub details like if ( playerId catches a balll of subdetail ) // MightBeLinked as Players only ! ineeda brainstrom this stupid thingi
        public string SubDetailPlayerId { get; set; } // Any Sub details like if ( playerId catches a balll of subdetail )
        public virtual SeasonFixture SeasonMatch { get; set; }
        public virtual Club Club { get; set; }
        public virtual Player Player { get; set; }
    }
    public class SeasonClubsViewModel
    {
        public int SeasonId { get; set; }
        public int ClubId { get; set; }
        public string ClubName { get; set; }
        public string SeasonName { get; set; }
        public string ClubImage { get; set; }
    }
    public class SeasonCategoriesViewModel
    {
        public int SeasonId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryImage { get; set; }
        public List<SeasonSubCategoryViewModel> CategorySubCategories { get;set;}
    }
    public class SeasonSubCategoryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class SeasonGroundViewModel
    {
        public int SeasonId { get; set; }
        public NameIdImageViewModel Ground { get; set; }
    }
    public class NameIdImageViewModel
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public string URL { get; set; }
        public string CoverImage { get; set; }
    }
    public class SeasonMatchViewModel
    {
        public string Id { get; set; }
        public string Guid { get; set; }
        public int SeasonId { get; set; }        
        //public string StatsString { get; set; }
        public DateTime Date { get; set; }
        public NameIdImageViewModel Ground { get; set; }
        public NameIdImageViewModel Season { get; set; }
        public NameIdImageViewModel Category { get; set; }
        public string Status { get; set; }  //Not_Started, Started, Completed
        public string CurrentRound { get; set; } // General,Quater_Final,Semi_Final,Final,KnockOut,For_Third_Place
        public string MatchType { get; set; } // Default , Male , Female
        public List<SeasonFixtureTeamsViewModel> Teams { get; set; }
        public List<GalleryVMFORES> Gallery { get; set; }
        public string CultureCode { get; set; }
        public string MyPrediction { get; set; } // Just to use in view
    }
    public class SeasonMatchViewModelTelegram : SeasonMatchViewModel
    {
        public bool MatchStarted { get; set; }
        public bool Reporting { get; set; }
        public long ReportingByChatId { get; set; }
        public List<QuestionAnswers> Settings { get; set; }
    }
    //public class SeasonMatchViewModelTelegramTeams
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public string Image { get; set; }
    //    public List<ClubPlayersViewModel> Players { get; set; }
    //}
    //public class PlayersFor
    public class SeasonMatchViewModelForJhole
    {
        public int Id { get; set; }
        public int SeasonId { get; set; }
        public DateTime Date { get; set; }
        public NameIdImageViewModel Ground { get; set; }
        public NameIdImageViewModel Category { get; set; }
        public TeamViewModelForJHOLE TeamOne { get; set; }
        public TeamViewModelForJHOLE TeamTwo { get; set; }
    }
    public class TeamViewModelForJHOLE
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public List<PlayersViewModelForTeamViewModelForJHOLE> Players { get; set; }
    }
    public class PlayersViewModelForTeamViewModelForJHOLE
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public int PlayerX { get; set; }
        public int PlayerY { get; set; }
        public enumSeasonMatchPlayersType PlayersType { get; set; }
        public string PlayerRole { get; set; }
    }
    public class SeasonPartnerViewModel
    {
        public int Id { get; set; }
        public int SeasonId { get; set; }
        public NameIdImageViewModel Partner { get; set; }
        public PartnerTitleViewModelForSeasonPartnerViewModel PartnerTitle { get; set; }
    }
    public class PartnerTitleViewModelForSeasonPartnerViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class SeasonEditViewModel
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Alias { get; set; }
        public string Logo { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
        public int CultureId { get; set; }
        public int multilingualid { get; set; }

    }
    public class SeasonViewModel
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        //public string StatsString { get; set; }
        public string Name { get; set; }
        public string LeagueName { get; set; }
        public string LeagueCategory { get; set; }
    }
    public class SeasonsVMForPartnerVM
    {
        public string Name { get; set; }
        public string Image { get; set; }
    }
    public class SeasonPartners // Dhangadi premier leage 2017 and 2018 ma different cha so Season with Partner is good
    {
        public SeasonPartners()
        {
            this.Partner = new Partner();
            this.Season = new Season();
        }
        public int PartnerTitleId { get; set; }
        public int SeasonId { get; set; }
        public int PartnerId { get; set; }
        public virtual Season Season { get; set; }
        public virtual Partner Partner { get; set; }
        public virtual PartnerTitle PartnerTitle { get; set; }
    }
    public class SeasonClubs
    {
        public SeasonClubs()
        {
            this.Season = new Season();
            this.Club = new Club();
        }
        public int SeasonId { get; set; }
        public int ClubId { get; set; }        
        public Club Club { get; set; }
        public Season Season { get; set; }
    }
    public class SeasonCategoryClubGroup
    {
        public int SeasonId { get; set; }
        public int CategoryId { get; set; }
        public int ClubId { get; set; }
        public string GroupName { get; set; }
        public virtual Club Club { get; set; }
        public virtual Season Season { get; set; }
        public virtual Category Category { get; set; }
    }
    public class SeasonClubPlayers
    {
        public SeasonClubPlayers()
        {
            this.Season = new Season();
        }
        public int SeasonId { get; set; }
        public int ClubId { get; set; }
        public int PlayerId { get; set; }
        public enumPlayerRole PlayerRole { get; set; } // Backey Goalkeeper
        public virtual Club Club { get; set; }
        public virtual Player Player { get; set; }
        public virtual Season Season { get; set; }
    }
    public class SeasonCreateViewModel
    {

        public int CultureId { get; set; }

        public string Name { get; set; }
        public string Image { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        public DateTime StartedDate { get; set; }
        public DateTime EndDate { get; set; }

        public int LeagueId { get; set; }
        public string Description { get; set; }
        public int multilingualid { get; set; }
    }
    public class SeasonAdminViewModel
    {
        public int Id { get; set; }
        public bool Converted { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public string Name { get; set; }
        public string LeagueName { get; set; }
        public string LeagueCategory { get; set; }
        public int multilingualid { get; set; }
    }
    public class SeasonVMFORES : ElasticMandatory
    {
        public SeasonVMFORES()
        {
            this.Standings = new List<SeasonStandingVMFORES>();
        }
        //public int Id { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string CultureCode { get; set;}
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public string URL { get; set; }
        public string CoverImage { get; set; }
        public string Description { get; set; }
        public DateTime? StartedDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string TournamentType { get; set; }
        public virtual LeagueVMforSeasonVMFORES League { get; set; }
        // public ICollection<Clubs> Clubs { get; set; }
        public List<GalleryVMFORES> Gallery { get; set; }
        public List<ClubVMForSeasonVMFORES> Clubs { get; set; }
        public List<PartnerVMforSeasonVMFORES> Partners { get; set; }
        public List<SeasonStandingVMFORES> Standings { get; set; }
        public List<SeasonStatsVMFORES> Stats { get; set; }
    }

    public class PartnerVMforSeasonVMFORES
    {
        public string PartnerTitleName { get; set; }
        public IDNAMEIMAGEVMFORES Partner { get; set; }
    }
    public class ClubVMForSeasonVMFORES
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public string Thumbnail { get; set; }
        // public List<PlayerViewModelEs> Players { get;set;}
        public List<ClubPlayersViewModelFORES> Players { get; set; }
    }
    public class SeasonStanding
    {
        public Season Season { get; set; }
        public Club Club { get; set; }
        public int Played { get; set; }
        public int Won { get; set; }
        public int Draw { get; set; }
        public int Lost { get; set; }
        public int Points
        {
            get
            {
                return (Won * 3) + (Draw * 1);
            }
        }
    }
    public class SeasonStandingVMFORES : ElasticMandatory
    {
        public NameIdImageViewModel Club { get; set; }
        public NameIdImageViewModel Season { get; set; }
        public int Played { get; set; }
        public int Won { get; set; }
        public int Draw { get; set; }
        public int Lost { get; set; }
        public int Points { get; set; }
    }
}