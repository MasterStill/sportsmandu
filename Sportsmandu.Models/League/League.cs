using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Sportsmandu.Model;
using Sportsmandu.Model.Club.ViewModels;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Model.Generic;
using Sportsmandu.Models.Gallery;

namespace Sportsmandu.Models.League
{
        public class LeagueSocial
        {
            public string Link { get; set; }
            public enumSocial Social { get; set; }
            public int LeagueId { get; set; }
            public virtual League League { get; set; }
        }
        public class LeagueConvertedCulture
    {
        public LeagueConvertedCulture()
        {
            this.League = new League();
            this.Culture = new Culture();
        }
        public int LeagueId { get; set; }
        public int CultureId { get; set; }
        public virtual Culture Culture { get; set; }
        public virtual League League { get; set; }
    }
    public class LeagueSubscription
    {
        public LeagueSubscription()
        {
            this.League = new League();
            this.User = new ApplicationUser();
        }
        public int LeagueId { get; set; }
        public int UserId { get; set; }
        public virtual League League { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
    public class League : SportsmanduMandatory, ISportsmanduBase // League = Competitions
    {
        public League()
        {
            this.Subscription = new HashSet<LeagueSubscription>();
            this.Employee = new HashSet<LeagueEmployee>();
            this.LeagueSocial = new HashSet<LeagueSocial>();
            this.ConvertedCulture = new HashSet<LeagueConvertedCulture>();
        }
        public virtual ICollection<LeagueSubscription> Subscription { get; set; }
        public virtual ICollection<LeagueEmployee> Employee { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        public string Founded { get; set; }
        // public string Promotion { get; set; } // Martyr's Memorial C-Division League >> promoted to >> Martyr's Memorial B-Division League // TODO
        // public string Relegation { get; set; }
        public int CategoryId { get; set; }
        public int CountryId { get; set; }
        public string CurrentChampions { get; set; }
        public string Website { get; set; }
        public string Source { get; set; }
        public int XId { get; set; }
        public string Description { get; set; }
        [EnumDataType(typeof(enumLeagueType))]
        public enumLeagueType LeagueType { get; set; }
        public TournamentType TournamentType { get; set; }
        public virtual ICollection<LeagueSocial> LeagueSocial { get; set; }
        public virtual ICollection<LeagueConvertedCulture> ConvertedCulture { get; set; } 
        public virtual Country Country { get; set; } // Originated Country
        public virtual Category Category { get; set; }  // Football // Cricket // Multiple Games // So category ma multiple Categories bhanne chaiyooo
        public string URL
        {
            get
            {
                try
                {
                    return Sportsmandu.Models.Entitiesss.Global.GetCleanUrl($"{Culture.Code}/{Country.Name}/League/{Guid}/{Name}");
                }
                catch
                {
                    return "ERROR";
                }
            }
        }
    }





    public class LeagueEmployee
    {
        public LeagueEmployee()
        {
            this.League = new League();
            this.Employee = new Employee();
            this.Designation = new Designation();
        }
        public int EmployeeId { get; set; }
        public int LeagueId { get; set; }
        public int DesignationId { get; set; }
        public DateTime? JoinedDate { get; set; }
        public DateTime? ResignedDate { get; set; }
        public virtual Designation Designation { get; set; }
        public Employee Employee { get; set; }
        public virtual League League { get; set; }
    }
    public enum enumLeagueType
    {
        Domestic, International, DomesticClubs, InternationalClubs
    }
    public class Score
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Description { get; set; }
        public string Image { get; set; } // The event might have Image 
        public DateTime Date { get; set; }

    }

    public class LeagueVMforSeasonVMFORES
    {
        public string Name { get; set; }
        public string URL { get; set; }
        public string Image { get; set; }
        public string Founded { get; set; }
        public string Website { get; set; }
        public string Source { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
        public string TournamentType { get; set; }
    }
    public class LeagueVMFORES
    {
        public string Id { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public string CoverImage { get; set; }
        public string Description { get; set; }
        public string CultureCode { get; set; }
        public string URL { get; set; }
        public SeasonVMFORES Season { get; set; }
        public List<GalleryVMFORES> Gallery { get; set; }
        public List<SocialVMFFORES> Social { get; set; }
    }
    public class IDNAMEIMAGEVMFORES
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string URL { get; set; }
    }
    public enum TournamentType
    {
        KnockOut, RoundRobin
    }
}