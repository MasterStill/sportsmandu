﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sportsmandu.Data;
using Sportsmandu.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
namespace Sportsmandu.Models
{
    // The below implementation dont work out ! either i have to Reference Identity User in Models ( different project ) 
    // Which i dont want
    //public class ApplicationUser : Sportsmandu.Models.ApplicaitonUseraa.ApplicationUser  IdentityUser<int>
    public enum UserType
    {
        User, SupportStaff, AccountStaff, SportsmanduAdmin, StoreOwner, StoreStaff
    }
    public class ApplicationUserVMFORES
    {
        public ApplicationUserVMFORES()
        {
            this.Predictions = new List<PredictionVMFORESForApplicationUserVMFORES>();
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Guid { get; set; }
        public string Avatar { get; set; }
        public string Email { get; set; }
        public string HideEmail
        {
            get
            {
                return "";
            }
        }
        public List<PredictionVMFORESForApplicationUserVMFORES> Predictions { get; set; }
        public int Total_Prediction_Count
        {
            get
            {
                return Predictions.Count();
            }
        }
        public int Successed_Predictions_Count
        {
            get
            {
                return Predictions.Where(x => x.PredictionSucceded).Count();
            }
        }
    }

    public class PredictionVMFORESForApplicationUserVMFORES
    {
        public string FixtureGuid { get; set; }
        public enumFixturePredict Prediction { get; set; }
        public int? TeamId { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool PredictionSucceded { get; set; }
        public string Result { get; set; }
    }
    public class ApplicationUser :  IdentityUser<int> , ISportsmanduBase
    {
        public ApplicationUser()
        {
            this.RegisteredDate = DateTime.Now;
            this.SignalrGuid = System.Guid.NewGuid();
            this.Predictions = new HashSet<Predict>();
        }
        public List<ApplicationUserRole> Roles { get; set; }
        public virtual ICollection<Predict> Predictions { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public bool Online { get; set; }
        public string Guid { get; set; }
        public string AdminGuid { get; set; }
        public string UserIdGuid { get; set; }
        public Guid SignalrGuid { get; set; }
        public string Image { get; set; }
        public string TelegramUserName { get; set; }
        public DateTime RegisteredDate { get; set; }
        public UserType UserType { get; set; }
        public string IP { get; set; }
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        public string Avatar
        {
            get
            {
                return Image;
            }
        }



        // Testing ko lagi matra ho yo haru        
        public int multilingualid { get; set; }
        public int CreatedById { get; set; }
        public bool Delflag { get; set; }
    }    
        
    public class EmailInvitation
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public enumClaimTypeSegments Segment { get; set; }
        public string SegmentValue { get; set; }
        public Boolean Accepted { get; set; }
        public DateTime? AcceptedDate { get; set; }
        public DateTime InvitedDate { get; set; }
        public int RoleId { get; set; }
        public ApplicationRole Role { get; set; }
        public int AddedById { get; set; }
        public ApplicationUser AddeddBy { get; set; }
    }

    public class GenericResult
    {
        public bool Succeded { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public string[] ArrayData {get;set;}
    }
    public class LoadMoreClientViewModel
    {
        public string groupName { get; set; }
        public string fullName { get; set; }
        public string lastMessage { get; set; }
        public string avatar { get; set; }
        public bool online { get; set; }
        public string webSite { get; set; }
    }        
    public class ApplicationUserClaim : IdentityUserClaim<int> {
    }

    public class ApplicationUserLogin : IdentityUserLogin<int> { }
    public class ApplicationUserToken : IdentityUserToken<int> { }
    
    public class ApplicationUserRole : IdentityUserRole<int>
    {
        //public int ClubId { get; set; }
        //public virtual Club Club { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ApplicationRole Role { get; set; }
        public enumClaimTypeSegments Segment { get; set; }
        public string SegmentValue { get; set; }
        public int CreatedById { get; set; }
        public  bool Delflag { get; set; }
    }    
    public enum enumSportsmanduCoreApplicationSegment
    {
        AdminPanel,Website,Telegram
    }
    public enum enumClaimTypeSegments
    {
        BlogPosts, Players, Clubs, Grounds, Leagues, Association, Seasons, Partners,Globalizations, Associations , Fixture,Videos
    }
    
    public class ApplicationRole : IdentityRole<int>//, ISportsmanduBase//, ApplicationUserRole, ApplicationRoleClaim>
    {
       // public int Id { get; set; }
        //public int? multilingualid { get; set; }
        //public int CreatedById { get; set; }
        //public bool Delflag { get; set; }
    }
    public class ApplicationRoleClaim : IdentityRoleClaim<int> { }
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<ApplicationUser> passwordHasher, IEnumerable<IUserValidator<ApplicationUser>> userValidators, IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<ApplicationUser>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
        }
    }
}