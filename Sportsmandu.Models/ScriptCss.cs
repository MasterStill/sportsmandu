﻿using System;
using System.Collections.Generic;
using System.Text;
namespace Sportsmandu.Models
{
    public class ScriptCss
    {
        public string Script { get; set; }
        public string HTML { get; set; }
        public string Css { get; set; }
        public string IFrame { get; set; }
    }
}
 