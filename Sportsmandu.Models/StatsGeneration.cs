﻿namespace Sportsmandu.Models
{
    public class StatsDB
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public int Statfield { get; set; }
        public string StatfieldType { get; set; }
        public string Type { get; set; }  // Stats / Standings 
    }
}