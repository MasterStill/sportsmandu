﻿using System;
using System.Collections.Generic;
using System.Text;
namespace Sportsmandu.Models
{
    public class Alexa
    {
        public Alexa()
        {
            this.Date = DateTime.Now;
        }
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string WebsiteName { get; set; }
        public long GlobalRank { get; set; }
        public long CountryRank { get; set; }
    }
}
