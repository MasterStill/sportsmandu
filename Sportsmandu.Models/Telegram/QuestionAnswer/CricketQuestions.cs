﻿using Sportsmandu.Models.League;
using System.ComponentModel.DataAnnotations;
namespace Sportsmandu.Models.Telegram.QuestionAnswer
{
    //"Sportsmandu.Models.Telegram.QuestionAnswer.CricketQuestions+Caught_OutQuestions
    public class CricketQuestions
    {
        public class c
        {
            [Display(Name = "Final_Question")]
            public bool Final_Question { get; set; }
            public SeasonMatchViewModelTelegram Question_Which_SeasonFixture { get; set; }

            public string ParamGUID { get; set; }

        }
        public class Caught_Out  : c
        {
            public SeasonFixtureTeamsViewModelPlayers Who_Caught_Him { get; set; }
            public SeasonFixtureTeamsViewModelPlayers Do_Not_Ask_Who_Was_Out { get; set; }
            [Display(Name = "Final_Question")]            
            public string Message
            {
                get
                {
                    //TODOOOOO ##TODO Sending Message According to subscriber fan club garna baki
                    string FinalMessageToSend = string.Empty;
                    FinalMessageToSend = $"Nice Catch By {Who_Caught_Him.Name} !";
                    //else
                    //    FinalMessageToSend = $"Great , {Question_Who_Scored_A_Goal.Name} Scored a Goal For His Own Team !";
                    return FinalMessageToSend;
                }
            }            
        }
        public class Run_Out
        {
            public SeasonFixtureTeamsViewModelPlayers Who_Ran_Him_Out { get; set; }
            public SeasonFixtureTeamsViewModelPlayers Who_Was_Out { get; set; }
            public SeasonFixtureTeamsViewModelPlayers Do_Not_Ask_Who_Was_Out { get; set; }
            [Display(Name = "Final_Question")]
            public bool Final_Question { get; set; }
            public string Message
            {
                get
                {
                    //TODOOOOO ##TODO Sending Message According to subscriber fan club garna baki
                    string FinalMessageToSend = string.Empty;
                    FinalMessageToSend = $"Great feilding By {Who_Ran_Him_Out.Name} !";
                    //else
                    //    FinalMessageToSend = $"Great , {Question_Who_Scored_A_Goal.Name} Scored a Goal For His Own Team !";
                    return FinalMessageToSend;
                }
            }
            public SeasonMatchViewModelTelegram Question_Which_SeasonFixture { get; set; }
            public string ParamGUID { get; set; }
        }
        public class Hit_Wicket : c
        {
            public SeasonFixtureTeamsViewModelPlayers Do_Not_Ask_Who_Was_Out { get; set; }
        }
        public class Bowled : Hit_Wicket
        {            
        }
        public class Lbw : Hit_Wicket
        {

        }
        public class Stumped : Hit_Wicket
        {
        }

        public class Mankad : Hit_Wicket
        {

        }
        public class Obstructing_The_Field : Run_Out
        {

        }
        public class Handling : Run_Out
        {

        }

        public class Run
        {
            public int No_Of_Run { get; set; }
            [Display(Name = "Final_Question")]
            public bool Final_Question { get; set; }
            public SeasonMatchViewModelTelegram Question_Which_SeasonFixture { get; set; }
            public string ParamGUID { get; set; }
        }
        public class Settings
        {
            public SeasonFixtureTeamsViewModel Who_Won_The_Toss { get; set; }
            public SeasonFixtureTeamsViewModel BattingTeam { get; set; }
            public SeasonFixtureTeamsViewModel Do_Not_Ask_BowlingTeam { get; set; }
            public SeasonFixtureTeamsViewModelPlayers Baller { get; set; }
            public SeasonFixtureTeamsViewModelPlayers Wicket_Keeper { get; set; }
            public SeasonFixtureTeamsViewModelPlayers Batter { get; set; }
            public SeasonFixtureTeamsViewModelPlayers Runner { get; set; }
            [Display(Name = "Final_Question")]
            public bool Final_Question { get; set; }
            public SeasonMatchViewModelTelegram Question_Which_SeasonFixture { get; set; }
            public string ParamGUID { get; set; }
        }
        public class No_Ball
        {
            public bool Was_there_a_run_scored { get; set; }
            
            [Display(Name = "Final_Question")]
            public bool Final_Question { get; set; }
            public SeasonMatchViewModelTelegram Question_Which_SeasonFixture { get; set; }
            public string ParamGUID { get; set; }
        }
        public class Wide_Ball
        {

        }
        public class Dead_Ball
        {

        }
        public class Bye
        {

        }
        public class Leg_Bye
        {

        }

        public class Injury
        {

        }
        public class Wide
        {

        }
        public class Dot
        {

        }
    }

    //To include: NO ball pachi ko ball free hit(a legitimate delivery but same rules as a no ball)
    //Bye- run added to extra batsman and bowler both not credited with the run (same for leg bye)
    //Over automatic? after 6 deliveries
    //Stats automatic
    //How to handle injury cases? Who was injured? Was there a change in batsman or bowler?
    //Super over cases? When runs are equal. 

    public enum enumGameEventTypeCricketSubTypeOut
    {
        Caught_Out, Hit_Wicket, Run_Out, Bowled, Stumped, Lbw, Mankad, Obstructing_The_Field, Handling
    }
    public enum enumGameEventTypeCricketSubTypeOther
    {
        Bye, Leg_Bye, Injury
    }
    public enum enumGameEventTypeCricketSubTypeIllegal_Delivery
    {
        No_Ball, Wide_Ball, Dead_Ball

    }
    public enum enumGameEventTypeCricket
    {
        Run, Out, Dot, Illegal_Delivery, Other, Settings
    }
}