﻿using Sportsmandu.Models.League; 
using System.ComponentModel.DataAnnotations;
namespace Sportsmandu.Models.Telegram.QuestionAnswer
{
    public class FootballQuestions
    {
        public class Goal
        {
            [Display(Name = "Question : Which Team Scored A Goal")]
            public SeasonFixtureTeamsViewModel Question_Which_Team_Scored_A_Goal { get; set; }
            [Display(Name = "Question : Who Scored A Goal")]
            public SeasonFixtureTeamsViewModelPlayers Question_Who_Scored_A_Goal { get; set; }
            [Display(Name = "Question : Who Passed For A Goal ")]
            public SeasonFixtureTeamsViewModelPlayers Question_Who_Passed_For_A_Goal { get; set; }
            [Display(Name = "Question : Was Own Goal")]
            public bool Question_OwnGoal { get; set; }
            [Display(Name = "Final_Question")]
            public bool Final_Question { get; set; }
            public SeasonMatchViewModelTelegram Question_Which_SeasonFixture { get; set; }
            public string ParamGUID { get; set; }
            public string Message
            {
                get
                {
                    string FinalMessageToSend = string.Empty;
                    if (Question_OwnGoal)
                    {
                        FinalMessageToSend = $"Thats Sad, {Question_Who_Scored_A_Goal.Name} Scored a Goal Against Own Team ! Bitch !";
                    }
                    else
                    {
                        FinalMessageToSend = $"Great , {Question_Who_Scored_A_Goal.Name} Scored a Goal For His Own Team !";
                    }
                    return FinalMessageToSend;
                }
            }
        }
        public class Substitute
        {
            [Display(Name = "Question : Which Team Substutited a Player")]
            public SeasonFixtureTeamsViewModel Question_Which_Team_Substutited { get; set; }
            [Display(Name = "Question : Who Was Sent Off")]
            public SeasonFixtureTeamsViewModelPlayers Question_Who_Was_Sent_Off { get; set; }
            [Display(Name = "Question : Who was in")]
            public SeasonFixtureTeamsViewModelPlayers Question_Who_Was_In { get; set; }
            [Display(Name = "Final_Question")]
            public bool Final_Question { get; set; }
            public SeasonMatchViewModelTelegram Question_Which_SeasonFixture { get; set; }
            public string ParamGUID { get; set; }

            public string Message
            {
                get
                {
                    return "Great!";
                }
            }
        }
        public class Corner_Kick
        {
            [Display(Name = "Question : Which Team Got a Corner")]
            public SeasonFixtureTeamsViewModel Question_Which_Team_Got_A_Corner { get; set; }            
            [Display(Name = "Final_Question")]
            public bool Final_Question { get; set; }
            public SeasonMatchViewModelTelegram Question_Which_SeasonFixture { get; set; }
            public string ParamGUID { get; set; }

            public string Message
            {
                get
                {
                    return "Great!";
                }
            }
        }
        public class Free_Kick
        {
            [Display(Name = "Question : Which Team Got a FreeKick")]
            public SeasonFixtureTeamsViewModel Question_Which_Team_Got_A_FreeKick { get; set; }
            [Display(Name = "Final_Question")]
            public bool Final_Question { get; set; }
            public SeasonMatchViewModelTelegram Question_Which_SeasonFixture { get; set; }
            public string ParamGUID { get; set; }

            public string Message
            {
                get
                {
                    return "Great!";
                }
            }
        }
        public class Penalty
        {
            [Display(Name = "Question : Which Team Got a Penalty")]
            public SeasonFixtureTeamsViewModel Question_Which_Team_Got_Penalty { get; set; }
            [Display(Name = "Final_Question")]
            public bool Final_Question { get; set; }
            public SeasonMatchViewModelTelegram Question_Which_SeasonFixture { get; set; }
            public string ParamGUID { get; set; }

            public string Message
            {
                get
                {
                    return "Great!";
                }
            }
        }
        public class Offside
        {
            [Display(Name = "Question : Which Team was Offside")]
            public SeasonFixtureTeamsViewModel Question_Which_Team_Was_Offside { get; set; }
            [Display(Name = "Final_Question")]
            public bool Final_Question { get; set; }
            public SeasonMatchViewModelTelegram Question_Which_SeasonFixture { get; set; }
            public string ParamGUID { get; set; }

            public string Message
            {
                get
                {
                    return "Great!";
                }
            }
        }
        public class Throw_In
        {
            [Display(Name = "Question : Which Team Got A Throw_In")]
            public SeasonFixtureTeamsViewModel Question_Which_Team_Got_A_Throw_In { get; set; }
            [Display(Name = "Final_Question")]
            public bool Final_Question { get; set; }
            public SeasonMatchViewModelTelegram Question_Which_SeasonFixture { get; set; }
            public string ParamGUID { get; set; }

            public string Message
            {
                get
                {
                    return "Great!";
                }
            }
        }
        public class Extra_Time
        {
            [Display(Name = "Question : How Long Game Was Extended")]
            public double Question_How_Long_Game_Was_Extended { get; set; }
            [Display(Name = "Final_Question")]
            public bool Final_Question { get; set; }
            public SeasonMatchViewModelTelegram Question_Which_SeasonFixture { get; set; }
            public string ParamGUID { get; set; }
            public string Message
            {
                get
                {
                    return "Great!";
                }
            }
        }
    }
    public enum enumGameEventTypeFootballSubTypeFoul
    {
        Handball, Tackle
    }
    public enum enumGameEventTypeFootball
    {
        Goal, Corner_Kick, Foul, Free_Kick, Penalty, Offside, Card, Throw_In, Substitute, Extra_Time,Settings
    }
} 