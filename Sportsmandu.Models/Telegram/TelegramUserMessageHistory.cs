﻿using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Models.Generic;
using Sportsmandu.Models.League;
using System.Collections.Generic;
namespace Sportsmandu.Models.Telegram
{
    public class TelegramUserMessageHistory
    {
        public TelegramUserMessageHistory()
        {
            this.QuestionAnswers = new List<QuestionAnswers>();
            this.Settings = new List<QuestionAnswers>();
        }
        public string UserName { get; set; }
        public long ChatId { get; set; }
        public string Route { get; set; }
        public enumSportCategory Category { get; set; }
        public string SubRoute { get; set; }
        //public string Last_Question { get; set; }
        //public string Current_Question { get; set; }
        public bool Awaiting_Answer { get; set; }
        public string Current_Match_Id { get; set; }
        public List<string> Message { get; set; }
        public SeasonMatchViewModelTelegram SeasonMatch { get; set; }  // Kun Match Cover Gariraheko ho ?
        public List<QuestionAnswers> QuestionAnswers { get; set; }
        public List<QuestionAnswers> Settings { get; set; }
    }
    public class QuestionAnswers
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }

    public class TelegramGenericViewModel
    {
        public TelegramGenericViewModel() { }
        public TelegramGenericViewModel(string Name, string Id, string MoreInfo = null)
        {
            this.Name = Name;
            this.Id = Id;
            this.MoreInfo = MoreInfo;
        }
        public string Name { get; set; }
        public string Id { get; set; }
        public string MoreInfo { get; set; }
    }
    public class GameCategoryEventDetail
    {
        public GameCategoryEventDetail()
        {
            this.GameEventSubType = new List<string>();
        }
        public enumSportCategory Category { get; set; }
        public string GameEventType { get; set; }
        public List<string> GameEventSubType { get; set; }
    }

    public class GameCategoryEventSettings
    {
        public enumSportCategory Category { get; set; }
        public int No_Of_TeamOnePlayers_In_Ground { get; set; }
        public int No_Of_TeamOnePlayers_In_Bench { get; set; }
        public int No_Of_TeamTwoPlayers_In_Ground { get; set; }
        public int No_Of_TeamTwoPlayers_In_Bench { get; set; }
    }
    public class GameCategoryRoles
    {   // To Identify who is balling/batting / stricker // Goalkeeper
        public enumSportCategory Category { get; set; }
        public string Name { get; set; }
        public List<PlayerViewModel> TeamOnePlayers { get; set; }
        public List<PlayerViewModel> TeamTwoPlayers { get; set; }
    }
    public enum enumSportCategory
    {
        Football, Cricket, Golf, Swimming, Athletics, Archery, Dandi_Biyo, Ice_Hockey, Kabbadi, Volleyball, Table_Tennis, Elephant_Polo, Basketball, Lawn_Tennis, Taekwondo, Badminton, Boxing, Cycling, Field_hockey, Handball, Karate, Kho_kho, Rowing_, Shooting, Squash, Triathlon, Weightlifting, Wushu, Wrestling_, Judo, Paragliding,
    }
    public class UniversalTelegramViewModel
    {
        public UniversalTelegramViewModel() { }
        public string AccountId { get; set; }
        public string UserId { get; set; }
        public enumCommandApi Command { get; set; }
        public string Param { get; set; }
        public string SubParam { get; set; }
        public int Page { get; set; }
        public dynamic Data { get; set; }
    }
}