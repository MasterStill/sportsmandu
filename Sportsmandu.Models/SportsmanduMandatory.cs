﻿using Sportsmandu.Model.Entities.Globalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Sportsmandu.Models
{
    public class SportsmanduMandatory
    {
        public SportsmanduMandatory()
        {            
            Delflag = false;
            Verified = false;
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Guid { get; set; }
        public int multilingualid { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public int? ModifiedById { get; set; }
        public int CreatedById { get; set; }
        public bool Verified { get; set; }
        public bool Delflag { get; set; }
        public int? CultureId { get; set; }
        public Culture Culture { get; set; }
        //public virtual User CreatedBy { get; set; }
        //public virtual User CreatedBy { get; set; }
    }
}