﻿namespace Sportsmandu.Models
{
    public class PhotoVerification
    {
        public string Id { get; set; }
        public string Segment { get; set; } // Player
        public string RequestType { get; set; } // Photo // CoverPhoto // Thumbnail
        public string Guid { get; set; } // Segment GUID
        public string TelegramUserName { get; set; } // The One Who Posted The Photo
        public string FileName { get; set; } // The One Who Posted The Photo
    }
    public class PhotoVerifications
    {
        public int Id { get; set; }
        public string Segment { get; set; } // Player
        public string RequestType { get; set; } // Photo // CoverPhoto // Thumbnail
        public string Guid { get; set; } // Segment GUID
        public string TelegramUserName { get; set; } // The One Who Posted The Photo
        public string FileName { get; set; } // The One Who Posted The Photo
    }
}