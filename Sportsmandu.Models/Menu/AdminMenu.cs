﻿using Sportsmandu.Models;
namespace Sportsmandu.Model.Entities.Global
{
    public class AdminMenu : SportsmanduMandatory,ISportsmanduBase
    {
        public string Title { get; set; }
        public string Meta { get; set; }
        public string Summary { get; set; }
        public string Url { get; set; }
        public bool CanCreate { get; set; }
        public string ImageUrl { get; set; }
        public bool Converted { get; set; }
    }
}