﻿using Sportsmandu.Model;
using Sportsmandu.Models;

namespace Community.Model.Entities.Global
{
    public class Menu : SportsmanduMandatory,ISportsmanduBase
    {
        //This menu will be used for Website Menu Items
        public string  Name { get; set; }
        public string  MenuUrl { get; set; }
        public string Url { 
            get{
                return Culture.Title + "/" + MenuUrl;
            }
        }
        public int Order {get;set;}
        public int? ParentMenu {get;set;}        
        public bool NewWindow {get;set;}                
    }
}