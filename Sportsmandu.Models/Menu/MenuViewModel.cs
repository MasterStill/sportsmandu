﻿namespace Community.Model.Entities.ViewModels
{
    public class MenuViewModel    
    {
        public int Id {get;set;}
        public string Name { get; set; }
        public string MenuUrl {get;set;}
        public bool NewWindow {get;set;}
        public int Order {get;set;}
    }
} 