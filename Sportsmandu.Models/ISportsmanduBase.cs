﻿namespace Sportsmandu.Model
{
    public interface ISportsmanduBase
    {
        int Id { get; set; }
        int multilingualid {get;set;}
        int CreatedById { get; set; }
        bool Delflag { get; set; }
        string Guid { get; set; }
    }
}