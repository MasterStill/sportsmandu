﻿using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Models.Gallery;
using Sportsmandu.Models.Generic;
using Sportsmandu.Models.League;
using System;
using System.Collections.Generic;

namespace Sportsmandu.Model.Club.ViewModels
{
    public class ClubViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class ClubViewModelFORES : ElasticMandatory //: ITest
    {        
        public string Name { get; set; }
        public string URL { get; set; }
        public string Guid { get; set; }
        public string CoverImage { get; set; }
        public string Thumbnail { get; set; }
        public string Image { get; set; }
        public string Founded { get; set; }
        public string Description { get; set; }
        public string HeaderImage { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public string CultureCode { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string FullStreetName { get; set; }
        public List<SocialVMFFORES> Social { get; set; }
        public List<GalleryVMFORES> Gallery { get; set; }
        public List<ElasticClubPlayersViewModel> Players { get; set; }
        public List<ElasticEmployeeViewModel> Employee { get; set; }
        public List<ElasticClubPlayersViewModel> ExPlayers { get; set; }
        public List<SeasonVMForClubSeasonVM> Seasons { get; set; }        
    }
    public class SocialVMFFORES
    {
        public string Id { get {
                return Link + Social;
            }
        }
        public string Link { get; set; }
        public string Social { get; set; }
    }

    //public class ClubSeasonVMForClubVMFORES
    //{
    //    public SeasonVMForClubSeasonVM Season { get; set; }
    //}

    public class SeasonVMForClubSeasonVM
    {
        public string Id { get; set; }
        public string URL { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public string Description { get; set; }
        public virtual LeagueVMFORSeasonVM League { get; set; }
    }
    public class LeagueVMFORSeasonVM
    {
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public string URL { get; set; }
    }

    public class ElasticEmployeeViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }        
        public string Image { get; set; }
        public string Thumbnail { get; set; }
        public DateTime? JoinedDate { get; set; }
        public DateTime? ResignedDate { get; set; }
        public string CountryName { get; set; }
        public string Designation { get; set; }
    }
    public class ElasticClubPlayersViewModel
    {
        public string PlayerId { get; set; }
        public string PlayerName { get; set; }
        public string PlayerURL { get; set; }
        public string PlayerImage { get; set; }
        public string PlayerThumbnail { get; set; }
        public string PlayerDob { get; set; }
        public string PlayerCountryName { get; set; }
        public string PlayerRole { get; set; }
        public string PlayerHeight { get; set; }
        public string PlayerCategoryName { get; set; }
        public string ShirtNumber { get; set; }
        public string JoinedDate { get; set; }
        public string ResignedDate { get; set; }
    }
}
