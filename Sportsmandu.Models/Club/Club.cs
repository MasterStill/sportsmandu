﻿using Sportsmandu.Model;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Entities.Ground;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Generic;
using Sportsmandu.Models.enums;
using Sportsmandu.Models.League;
using System;
using System.Collections.Generic;
namespace Sportsmandu.Models
{
    public enum enumClubType
    {
        Club, Country
    }
    public class ClubCategory
    {
        public int ClubId { get; set; }
        public int CategoryId { get; set; }
        public virtual Club Club { get; set; }
        public virtual Category Category { get; set; }
    }
    public class ClubsSocial
    {
        public string Link { get; set; }
        public enumSocial Social { get; set; }
        public int ClubId { get; set; }
        public virtual Club Club { get; set; }        
    }
    public class ClubConvertedCulture
    {
        public ClubConvertedCulture()
        {
            this.Club = new Club();
            this.Culture = new Culture();
        }
        public int ClubId { get; set; }
        public int CultureId { get; set; }
        public virtual Culture Culture { get; set; }
        public virtual Club Club { get; set; }
    }
    public class ClubSubscription
    {
        public ClubSubscription()
        {
            this.Club = new Club();
            this.User = new ApplicationUser();
        }
        public int ClubId { get; set; }
        public int UserId { get; set; }
        public virtual Club Club { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
    public class Club : SportsmanduMandatory, ISportsmanduBase
    {
        // Nepal Police CLub
        // Nepali National Football Team
        public string StringGuid { get; set; }
        //public string Guid
        //{
        //    get
        //    {
        //        return Sportsmandu.Models.Entitiesss.Global.GetCleanUrl(Name);
        //    }
        //}        
        public Club()
        {
            this.Seasons = new HashSet<SeasonClubs>();

            this.Subscription = new HashSet<ClubSubscription>();
            this.Players = new HashSet<ClubPlayers>();
            this.ExPlayers = new HashSet<ClubPlayers>();
            this.ConvertedCulture = new HashSet<ClubConvertedCulture>();
            this.Employee = new HashSet<ClubEmployee>();
            this.Fixtures = new HashSet<SeasonFixtureTeams>();
            this.ClubSocial = new HashSet<ClubsSocial>();
        }
        public virtual ICollection<ClubSubscription> Subscription { get; set; }
        public int? CountryId { get; set; }
        public string CoverImage { get; set; }
        public int XId { get; set; }
        public string Thumbnail { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string FullStreetName { get; set; }
        public string Alias { get; set; }
        public int Founded { get; set; }
        //public new int CultureId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Test {get;set;}
        public int? Test1 {get;set;}
        public string NickName { get; set; }
        public string Image { get; set; }
        public bool PaidClub { get; set; }
        public ClubTypeMemberShip ClubTypeMembership { get; set; }
        public string Description { get; set; }
        public string EstablishedDate { get; set; }
        // public string CurrentCoach { get; set; }// TODO Create a seperate ROLE Entity 
        // public string FormorPresident { get; set; }// TODO
        // public string ManagerName { get; set; }// TODO
        public string Email { get; set; }
        public int? HomeGroundId { get; set; }
        public virtual Ground HomeGround { get; set; }
        public string PhoneNumber { get; set; }
        public string Website { get; set; }
        public enumClubType ClubType { get; set; }
        public List<ClubRating> ClubRating { get; set; }
        public ICollection<ClubsSocial> ClubSocial { get; set; }
        public List<Address> Address { get; set; }
        public ICollection<ClubPlayers> Players { get; set; }
        public ICollection<ClubPlayers> ExPlayers { get; set; }
        public virtual ApplicationUser CreatedBy { get; set; }
        public ICollection<SeasonClubs> Seasons { get; set; }
        public ICollection<SeasonFixtureTeams> Fixtures { get; set; }
        public virtual Country Country { get; set; }
        public virtual ICollection<ClubConvertedCulture> ConvertedCulture { get; set; }
        public virtual ICollection<ClubEmployee> Employee { get; set; }
        public string URL
        {
            get
            {
                try
                {
                    return Sportsmandu.Models.Entitiesss.Global.GetCleanUrl($"{Culture.Code}/{Country.Name}/Team/{Guid}/{Name}");
                }
                catch
                {
                    return "ERROR";
                }
            }
        }
    }
    public class ClubPlayersViewModelFORES
    {
        public IDNAMEIMAGEVMFORES Player { get; set; }
        public DateTime JoinedDate { get; set; }
        public DateTime ResignedDate { get; set; }
        public int ShirtNumber { get; set; }
    }
    public class ClubplayersCreateViewModel
    {
        public string ClubId { get; set; }
        public int PlayerId { get; set; }
        public string ShirtNumber { get; set; }
        public string PlayerRole { get; set; }
        public string JoinedDate { get; set; }
        public string ResignedDate { get; set; }
        public string Role { get; set; }
    }
    public class ClubPlayers
    {
        public ClubPlayers()
        {
            this.Club = new Club();
            this.Player = new Player();
            //this.ResignedDate = DateTime.Now;
            //this.JoinedDate = DateTime.Now;
        }
        public int ClubId { get; set; }
        public int PlayerId { get; set; }
        public virtual Club Club { get; set; }
        public Player Player { get; set; }
        public DateTime? JoinedDate { get; set; }
        public DateTime? ResignedDate { get; set; }
        public int ShirtNumber { get; set; }
        public string SignedAt { get; set; } // Signed At Position
        public enumPlayerRole PlayerRole { get; set; }
    }
    public class ClubPlayersViewModel
    {
        public int PlayerId { get; set; }
        public int ClubId { get; set; }
        public string PlayerCategoryName { get; set; }
        public string PlayerName { get; set; }
        public string PlayerImage { get; set; }
        public DateTime JoinedDate { get; set; }
        public DateTime ResignedDate { get; set; }
        public int ShirtNumber { get; set; }
        public string PlayerRole { get; set; }
    }
    //public class TieSheet : SportsmanduMandatory//, ISportsmanduBase // TODO
    //{
    //    public DateTime Date { get; set; }
    //    public Ground GroundId { get; set; }
    //    public Ground Ground { get; set; }
    //    //team / team
    //}
    public class GameType : SportsmanduMandatory, ISportsmanduBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        // Cricket ko >> One Day International // Twenty20 International // FirstClass Cricket // List A Cricket
        //FootBall ko >> ??????
    }
    public class FootBallLineUp
    {
        public virtual Club Club { get; set; }
        public string Coach { get; set; }
        public string Formation { get; set; } // Football formations
        public List<Player> StartingPlayers { get; set; }
        public List<Player> Substitute { get; set; }
        public virtual SeasonFixture SeasonMatch { get; set; }
    }    
    public class SeasonMatchEventViewModel
    {
        public string DateTime { get; set; }
        public IDNAMEIMAGEVMFORES Club { get; set; }
        public IDNAMEIMAGEVMFORES Player { get; set; }
        public string Type { get; set; }
        public IDNAMEIMAGEVMFORES SeasonMatch { get; set; }
        public string Detail { get; set; }
    }
    public class GameEventShitsFootballToDecideTheName
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
    public class CricketCareerStatistics
    {
        public int Matches { get; set; }
        public int RunsScored { get; set; }
        public double BattingAverage { get; set; }
        public int Hundreds { get; set; }
        public int Fiftys { get; set; }
        public int TopScore { get; set; }
        public int BallsBowled { get; set; }
        public int Wickets { get; set; }
        public double BoulingAverage { get; set; }
        public string BestBowling { get; set; }
        public int Catches { get; set; }
        public int Stumpings { get; set; }
        public int GameTypeId { get; set; }
        public GameType GameType { get; set; }
    }
    public class ClubEmployee
    {
        public ClubEmployee()
        {
            this.Club = new Club();
            this.Employee = new Employee();
            this.Designation = new Designation();
        }
        public int EmployeeId { get; set; }
        public int ClubId { get; set; }
        public int DesignationId { get; set; }
        public DateTime? JoinedDate { get; set; }
        public DateTime? ResignedDate { get; set; }
        public virtual Designation Designation { get; set; }
        public Employee Employee { get; set; }
        public virtual Club Club { get; set; }
    }
    public enum ClubTypeMemberShip
    {
        Free, Standard, Premium, Enterprise
    }
}