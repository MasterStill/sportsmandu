﻿using System;
using System.Collections.Generic;
namespace Sportsmandu.Models
{
    public class Video
    {
        public Video()
        {
            this.Culture = new Model.Entities.Globalization.Culture();
        }
        public string YouTubeURL { get; set; }

        public string URL
        {
            get
            {
                return Sportsmandu.Models.Entitiesss.Global.GetCleanUrl(
                    $"{Culture.Code}/Home/Video/{Guid}/{Title}");
            }
        }
        public string YouTubeId { get; set; }
        public int Id { get; set; }
        public string Guid { get; set; }
        public string Author { get; set; }
        public DateTimeOffset UploadDate { get; set; }
        public string Name
        {
            get
            {
                return Title;
            }
        }
        public string CultureCode
        {
            get
            {
                return Culture.Code;
            }
        }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Thumbnail { get; set; }
        public bool Verified { get; set; }
        public bool Delflag { get; set; }
        public string Summary { get; set; }
        public TimeSpan Duration { get; set; }
        public string Keywords { get; set; }
        public int? CreatedById { get; set; }
        public VideoType VideoType { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Image { get; set; }
        public int MultiLingualId { get; set; }
        public int CultureId { get; set; }
        public virtual Model.Entities.Globalization.Culture Culture { get; set; }
        public string CoverImage { get; set; }
    }
    public class VideoVMForEs
    {
        public string URL { get; set; }
        public string YouTubeId { get; set; }
        public int Id { get; set; }
        public string Guid { get; set; }        
        public DateTimeOffset UploadDate { get; set; }
        public string Name
        {
            get
            {
                return Title;
            }
        }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Thumbnail { get; set; }
        public string Summary { get; set; }
        public TimeSpan Duration { get; set; }
        public string Keywords { get; set; }
        public VideoType VideoType { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Image { get; set; }
        public string CoverImage { get; set; }
        public string CultureCode { get; set; }
}
    public enum VideoType
    {
        Match,Player_Interview,Entertainment
    }
}