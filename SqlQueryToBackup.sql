﻿DECLARE 
      @SQL NVARCHAR(1000)
    , @DB_NAME NVARCHAR(100) = 'aspnet-Sportsmandu-C3DBF990-87FB-4D91-91E1-2AB440D03BDC'

SELECT TOP 1 @SQL = '
    BACKUP DATABASE [' + @DB_NAME + '] 
    TO DISK = ''' + REPLACE(mf.physical_name, '.mdf', '.bak') + ''''
FROM sys.master_files mf
WHERE mf.[type] = 0
    AND mf.database_id = DB_ID(@DB_NAME)

PRINT @SQL
EXEC sys.sp_executesql @SQL

BACKUP DATABASE [aspnet-Sportsmandu-C3DBF990-87FB-4D91-91E1-2AB440D03BDC] 
TO DISK = 'D:\DBBACKUP\aspnet-Sportsmandu-C3DBF990-87FB-4D91-91E1-2AB440D03BDC.bak'