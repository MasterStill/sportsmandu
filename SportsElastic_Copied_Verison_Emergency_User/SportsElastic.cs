﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Caching.Memory;
using Nest;
using Newtonsoft.Json;
using Sportsmandu.Model.Club.ViewModels;
using Sportsmandu.Model.Entities.Blog;
using Sportsmandu.Model.Entities.Globalization;
using Sportsmandu.Model.Entities.Ground;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Models;
using Sportsmandu.Models.Association;
using Sportsmandu.Models.Generic;
using Sportsmandu.Models.League;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
namespace SportsElastic
{
    class Program
    {
        private static HubConnection _connection;
        private static readonly HttpClient client = new HttpClient();
        private static ConnectionState _connectionState = ConnectionState.Disconnected;
        //private static string signalRbaseUrl = "https://sportsmandu.ngrok.io/";
        //private static string signalRbaseUrl = "http://localhost:5000/";
        private static string signalRbaseUrl = "https://sportsmandu.com/";
        private static string myApiUrl = "https://sportsmandu.com";
        //private static string myApiUrl = "https://localhost:44373"; 
        private enum ConnectionState
        { 
            Connected, 
            Disconnected,  
            Faulted 
        }
        async static void Connect() 
        {
            try
            {
                if(_connectionState != ConnectionState.Connected)
                {
                    Console.WriteLine("Trying To Connect...." + DateTime.Now.Minute + ":" + DateTime.Now.Second + " : " + signalRbaseUrl);
                    await _connection.StartAsync();
                    _connectionState = ConnectionState.Connected;
                    await _connection.InvokeAsync<UniversalDataContainer>("IAmConsole");
                    Console.WriteLine("Connected !!!!" + DateTime.Now.Minute + ":" + DateTime.Now.Second);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Server Service Not Ready.... " + DateTime.Now.Minute + ":" + DateTime.Now.Second);
            }
        }
        public static async Task StartConnectionAsync()
        {
            Console.WriteLine("Console Init....");
            _connection = new HubConnectionBuilder()
            .WithUrl(signalRbaseUrl + "sportsmanduhub", options =>            
            {
                options.Headers["ClientMessage"] = "Console";
            })
                .Build();
            _connection.Closed += async (error) =>
            {
                aa:
                Console.WriteLine("Client Disconnected !");
                _connectionState = ConnectionState.Disconnected;
                await Task.Delay(new Random().Next(0, 5) * 1000);
                try
                {
                    Connect();
                }
                catch (Exception){}
                if (_connectionState != ConnectionState.Connected)
                    goto aa;
            };
            bb:
            Connect();
            if(_connectionState != ConnectionState.Connected)
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                goto bb;
            }
            //UniversalDataContainer UniversalDataContainer = new UniversalDataContainer
            //{
            //    AccountId = "ALL",
            //    Command = enumCommand.SeasonFixture.ToString(),
            //    Param = enumParam.UPDATE.ToString(),
            //    SubParam = "1"
            //};
            //UniversalDataContainer UniversalDataContainer1 = new UniversalDataContainer
            //{
            //    AccountId = "ALL",
            //    Command = enumCommand.SeasonMatch.ToString(),
            //    Param = enumParam.LIST.ToString()
            //};            
            //await _connection.InvokeAsync<UniversalDataContainer>("UniversalGet", UniversalDataContainer);
        }
        private void InvokeAWESOME(string methodName, UniversalDataContainer UniversalDataContainer)
        {
            try
            {
                Program m = new Program();
                MethodInfo method = m.GetType().GetMethod(methodName);
                method.Invoke(m, new UniversalDataContainer[] { UniversalDataContainer });
            }
            catch (Exception ex)
            {
                Console.WriteLine("SOME COMMAND WRONG !");
            }
        }
        public static void Players(UniversalDataContainer UniversalDataContainer)
        {
            //TODO Ajha Dynamic Huna paryo Add and Update;            
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                case enumParam.UPDATE:
                default:
                    var result = JsonConvert.DeserializeObject<PlayerViewModelEs>(JsonConvert.SerializeObject(UniversalDataContainer.Result));
                    WriteToConsole(ElasticGenericAdd(enumElasticIndexes.sportsmanduplayers, result), UniversalDataContainer.Command);
                    break;
            }
        }
        public static void Clubs(UniversalDataContainer UniversalDataContainer)
        {
            //TODO Ajha Dynamic Huna paryo Add and Update;            
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                case enumParam.UPDATE:
                default:
                    var result = JsonConvert.DeserializeObject<ClubViewModelFORES>(JsonConvert.SerializeObject(UniversalDataContainer.Result));
                    WriteToConsole(ElasticGenericAdd(enumElasticIndexes.sportsmanduclubs, result), UniversalDataContainer.Command);
                    break;
            }
        }
        public static void Grounds(UniversalDataContainer UniversalDataContainer)
        {
            //TODO Ajha Dynamic Huna paryo Add and Update;            
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                case enumParam.UPDATE:
                default:
                    var result = JsonConvert.DeserializeObject<GroundVMFORES>(JsonConvert.SerializeObject(UniversalDataContainer.Result));
                    WriteToConsole(ElasticGenericAdd(enumElasticIndexes.sportsmandugrounds, result), UniversalDataContainer.Command);
                    break;
            }
        }
        public static void Seasons(UniversalDataContainer UniversalDataContainer)
        {
            //TODO Ajha Dynamic Huna paryo Add and Update;            
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                case enumParam.UPDATE:
                default:
                    var result = JsonConvert.DeserializeObject<SeasonVMFORES>(JsonConvert.SerializeObject(UniversalDataContainer.Result));
                    WriteToConsole(ElasticGenericAdd(enumElasticIndexes.sportsmanduseasons, result), UniversalDataContainer.Command);
                    break;
            }
        }
        public static void League(UniversalDataContainer UniversalDataContainer)
        {
            //TODO Ajha Dynamic Huna paryo Add and Update;            
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                case enumParam.UPDATE:
                default:
                    var result = JsonConvert.DeserializeObject<LeagueVMFORES>(JsonConvert.SerializeObject(UniversalDataContainer.Result));
                    WriteToConsole(ElasticGenericAdd(enumElasticIndexes.sportsmanduleagues, result), UniversalDataContainer.Command);
                    break;
            }
        }
        public static void Partners(UniversalDataContainer UniversalDataContainer)
        {
            //TODO Ajha Dynamic Huna paryo Add and Update;            
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                case enumParam.UPDATE:
                default:
                    var result = JsonConvert.DeserializeObject<PartnerVMFORES>(JsonConvert.SerializeObject(UniversalDataContainer.Result));
                    WriteToConsole(ElasticGenericAdd(enumElasticIndexes.sportsmandupartners, result), UniversalDataContainer.Command);
                    break;
            }
        }
        public static void Association(UniversalDataContainer UniversalDataContainer)
        {
            //TODO Ajha Dynamic Huna paryo Add and Update;            
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                case enumParam.UPDATE:
                default:
                    var result = JsonConvert.DeserializeObject<AssociationVMFORES>(JsonConvert.SerializeObject(UniversalDataContainer.Result));
                    WriteToConsole(ElasticGenericAdd(enumElasticIndexes.sportsmanduassociation, result), UniversalDataContainer.Command);
                    break;
            }
        }
        public static void SeasonFixture(UniversalDataContainer UniversalDataContainer)
            {
                //TODO ##TODO yo function ma lera janu bhanda pani tala ko function dynamic huna paryo!
                // _MatchEventRepository.Execute(UniversalDataContainer));
                switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
                {
                   case enumParam.ADD:
                   case enumParam.UPDATE:
                   default:
                    UniversalDataContainer.Command = enumElasticIndexes.sportsmanduseasonmatch.ToString();
                    SeasonMatchViewModel kk = UniversalDataContainer.Result.ToObject<SeasonMatchViewModel>();
                    var result = JsonConvert.DeserializeObject<SeasonMatchViewModel>(JsonConvert.SerializeObject(UniversalDataContainer.Result));
                    WriteToConsole(ElasticGenericAdd(enumElasticIndexes.sportsmanduseasonmatch, result), UniversalDataContainer.Command);
                    //var kk = JsonConvert.DeserializeObject<List<SeasonMatchViewModel>>(UniversalDataContainer.Data);
                    break;                        
                }
            }
        public static void Prediction(UniversalDataContainer UniversalDataContainer)
        {
            //TODO ##TODO yo function ma lera janu bhanda pani tala ko function dynamic huna paryo!
            // _MatchEventRepository.Execute(UniversalDataContainer));
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                case enumParam.UPDATE:
                default:
                    UniversalDataContainer.Command = enumElasticIndexes.sportsmanduprediction.ToString();                    
                    var result = JsonConvert.DeserializeObject<PredictionVMFORES>(JsonConvert.SerializeObject(UniversalDataContainer.Result));
                    WriteToConsole(ElasticGenericAdd(enumElasticIndexes.sportsmanduprediction, result), UniversalDataContainer.Command);                    
                    break;
            }
        }
        public static void BlogPost(UniversalDataContainer UniversalDataContainer)
        {
            switch (Enum.Parse<enumParam>(UniversalDataContainer.Param))
            {
                case enumParam.ADD:
                case enumParam.UPDATE:
                default:                    
                    UniversalDataContainer.Command = enumElasticIndexes.sportsmandublogposts.ToString();
                    BlogPostVMFORES kk = UniversalDataContainer.Result.ToObject<BlogPostVMFORES>();
                    var result = JsonConvert.DeserializeObject<BlogPostVMFORES>(JsonConvert.SerializeObject(UniversalDataContainer.Result));
                    WriteToConsole(ElasticGenericAdd(enumElasticIndexes.sportsmandublogposts, result), UniversalDataContainer.Command);
                    break;
            }
        }
        static void Main(string[] args)
        {
            client.Timeout = TimeSpan.FromMinutes(5);
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            StartConnectionAsync();
            _connection.On<UniversalDataContainer>("onUniversalGet", (incomingMessage) =>
            {
                Program p = new Program();
                p.InvokeAWESOME(incomingMessage.Command, incomingMessage);
            });
            //  elastic().DeleteIndex(enumElasticIndexes.sportsmandusitesectionlanguagetitle.ToString());
            //elastic().DeleteIndex(enumElasticIndexes.sportsmanduprediction.ToString());
            //  elastic().DeleteIndex(enumElasticIndexes.sportsmanduprediction.ToString());
            //foreach (enumElasticIndexes indexes in (enumElasticIndexes[])Enum.GetValues(typeof(enumElasticIndexes)))
            //{
            //elastic().DeleteIndex(indexes);
            //}
            //AssociationGetAndSaveToElasticsearch(); 
            //LeaguesGetAndSaveToElasticsearch();
            // BlogPostGetAndSaveToElasticsearch();
            //ClubsGetAndSaveToElasticsearch();
            //VideosGetAndSaveToElasticsearch(); 
            // GetAllSeasonMatchForEs();
            // GetAllSeasonForEs();
            //GroundsGetAndSaveToElasticsearch();
            //PlayersGetAndSaveToElasticsearch();
            //SiteSectionLanguageTitleAndSaveToElasticsearch();
            //PartnersGetAndSaveToElasticsearch(); 
            while (true)
            {
            }
        }
        private static void GroundsGetAndSaveToElasticsearch()
        {
            Console.WriteLine("Fetching Grounds To Save...");
            int intCount = 0;
            List<GroundVMFORES> p = new List<GroundVMFORES>();
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(myApiUrl + "/api/GetAllGroundsFORES");
            try {
                var msg = stringTask.Result;                        
            }catch(Exception ex) { }
        }
        private static List<VideoVMForEs> VideosGetAndSaveToElasticsearch()
        {
            Console.WriteLine("Fetching Videos To Save...");
            int intCount = 0;
            List<VideoVMForEs> p = new List<VideoVMForEs>();
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(myApiUrl + "/api/GetAllVideosFORES");
            var msg = stringTask.Result;
            p = JsonConvert.DeserializeObject<List<VideoVMForEs>>(msg);
            foreach (var items in p)
            {
                intCount += 1;
                var response = elastic().Index(items, i => i
                              .Index(enumElasticIndexes.sportsmanduvideos.ToString())
                              .Type(enumElasticIndexes.sportsmanduvideos.ToString()));
                WriteToConsole(response, "Videos >> " + intCount + ". " + items.Name + " Saved");
            }
            return p;
        }

        private static List<AssociationVMFORES> AssociationGetAndSaveToElasticsearch()
        {
            Console.WriteLine("Fetching Associations To Save...");
            int intCount = 0;
            List<AssociationVMFORES> p = new List<AssociationVMFORES>();
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(myApiUrl + "/api/GetAllAssociationFORES");
            var msg = stringTask.Result;
            p = JsonConvert.DeserializeObject<List<AssociationVMFORES>>(msg);
            foreach (var items in p)
            {
                intCount += 1;
                var response = elastic().Index(items, i => i
                              .Index(enumElasticIndexes.sportsmanduassociation.ToString())
                              .Type(enumElasticIndexes.sportsmanduassociation.ToString()));
                WriteToConsole(response, "Associations >> " + intCount + ". " + items.Name + " Saved");
            }
            return p;
        }
        private static void BlogPostGetAndSaveToElasticsearch(List<BlogPostVMFORES> BlogsToSave = null)
        {
            int intCount = 0;
            if (BlogsToSave != null)
            {
                foreach (var items in BlogsToSave)
                {
                    intCount += 1;
                    var response = elastic().Index(items, i => i
                                  .Index(enumElasticIndexes.sportsmandublogposts.ToString())
                                  .Type(enumElasticIndexes.sportsmandublogposts.ToString()));
                    WriteToConsole(response, "BlogPosts >> " + intCount + ". " + items.Title + " Saved");
                }
            }
            else
            {
                Console.WriteLine("Fetching BlogPosts To Save...");
                List<BlogPostVMFORES> p = new List<BlogPostVMFORES>();
                client.DefaultRequestHeaders.Accept.Clear();
                var stringTask = client.GetStringAsync(myApiUrl + "/api/GetAllBlogPostFORES");
                try
                {
                    var msg = stringTask.Result;
                }
                catch (Exception){ }
                //p = JsonConvert.DeserializeObject<List<BlogPostVMFORES>>(msg);
                //foreach (var items in p)
                //{
                //    intCount+=1;
                //    var response = elastic().Index(items, i => i
                //                  .Index(enumElasticIndexes.sportsmandublogposts.ToString())
                //                  .Type(enumElasticIndexes.sportsmandublogposts.ToString()));
                //    WriteToConsole(response, "BlogPosts >> " + intCount + ". " + items.Title + " Saved");
                //}
            }
        }

        private static void GetAllSeasonMatchForEs()
        {
            Console.WriteLine("Fetching Fixtures To Save...");
            var stringTaska = client.GetStringAsync(myApiUrl + "/api/GetAllSeasonMatchForEs");
            var msga = stringTaska.Result;
            var clubs = JsonConvert.DeserializeObject<List<SeasonViewModel>>(msga);
            GetAllSeasonMatchForEs(clubs);
        }
        private static void GetAllSeasonMatchForEs(List<SeasonViewModel> datas,List<SeasonMatchViewModel>seasonMatchViewModel = null)
        {
            int intCount = 0;
            if(seasonMatchViewModel != null)
            {
                foreach (var items in seasonMatchViewModel)
                {
                    //if (items.Teams.Count > 0)
                    {
                        intCount += 1;
                        var response = elastic().Index(items, i => i
                                      .Index(enumElasticIndexes.sportsmanduseasonmatch.ToString())
                                      .Type(enumElasticIndexes.sportsmanduseasonmatch.ToString()));
                        if (response.IsValid)
                            WriteToConsole(response, "SeasonMatch >> " + intCount + ". " + items.Teams.FirstOrDefault().Name + " VS " + items.Teams.LastOrDefault().Name + " Saved");
                        else
                            WriteToConsole(response, "SeasonMatch ERROR >> " + intCount + ". " + items.Season.Name);
                    }
                }
            }
            else
            {
                foreach (var seasons in datas)
                {
                    List<SeasonMatchViewModel> p = new List<SeasonMatchViewModel>();
                    client.DefaultRequestHeaders.Accept.Clear();
                    var stringTask = client.GetStringAsync(myApiUrl + "/api/GetAllSeasonMatchForEs/" + seasons.Id);
                    try
                    {
                        var msg = stringTask.Result;
                        p = JsonConvert.DeserializeObject<List<SeasonMatchViewModel>>(msg);
                        foreach (var items in p)
                        {
                            //if (items.Teams.Count > 0)
                            {
                                intCount += 1;
                                var response = elastic().Index(items, i => i
                                              .Index(enumElasticIndexes.sportsmanduseasonmatch.ToString())
                                              .Type(enumElasticIndexes.sportsmanduseasonmatch.ToString()));
                                if (response.IsValid)
                                    WriteToConsole(response, "SeasonMatch >> " + intCount + ". " + items.Teams.FirstOrDefault().Name + " VS " + items.Teams.LastOrDefault().Name + " Saved");
                                else
                                    WriteToConsole(response, "SeasonMatch ERROR >> " + intCount + ". " + items.Season.Name);
                            }
                        }
                    }
                    catch (Exception here)
                    {
                        Console.WriteLine("Error 167 se !! " + seasons.Name + ":" + seasons.Id);
                    }
                }
            }            
        }

        private static void GetAllSeasonForEs()
        {
            Console.WriteLine("Fetching Seasons To Save...");
            int intCount = 0;
            List<SeasonViewModel> p = new List<SeasonViewModel>();
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(myApiUrl + "/api/GetAllSeasonForEs");
            var msg = stringTask.Result;
            p = JsonConvert.DeserializeObject<List<SeasonViewModel>>(msg);
            foreach (var seasonMatch in p)
            {
                client.DefaultRequestHeaders.Accept.Clear();
                var stringTaska = client.GetStringAsync(myApiUrl + "/api/GetAllSeasonForEs" + "/" + seasonMatch.Id);
                var msga = stringTaska.Result;
                var pp = JsonConvert.DeserializeObject<SeasonVMFORES>(msga);               
                    intCount += 1;
                    var response = elastic().Index(pp, i => i
                                  .Index(enumElasticIndexes.sportsmanduseasons.ToString())
                                  .Type(enumElasticIndexes.sportsmanduseasons.ToString()));                    
                        WriteToConsole(response, "Seasons >> " + intCount + ". " + pp.Name + " Saved");                    
            }                            
        }
        private static void WriteToConsole(IIndexResponse Response,string SuccessText)
        {
            if (!Response.IsValid)
            {
                Console.WriteLine("Saving Error ElasticSearch");
            }
            else
            {
                Console.WriteLine(intGenericCount + " : " + SuccessText);
            }
        }      
        private static void PlayersGetAndSaveToElasticsearch()
        {
            Console.WriteLine("Fetching Players To Save...");            
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(myApiUrl + "/api/GetAllPlayersForES");
            try
            {
                var msg = stringTask.Result;
            }
            catch (Exception ex)
            {                
            }                     
        }

        private static void LeaguesGetAndSaveToElasticsearch()
        {
            Console.WriteLine("Fetching Leagues To Save...");
            int intCount = 0;
            List<LeagueVMFORES> p = new List<LeagueVMFORES>();
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(myApiUrl + "/api/LeagueVMFORES");
            var msg = stringTask.Result;
            p = JsonConvert.DeserializeObject<List<LeagueVMFORES>>(msg);
            foreach (var aa in p)
            {
                if (aa.Image != null)
                    aa.Image = aa.Image.Replace("http://sportsmandu.com", "");
            }
            foreach (var items in p)
            {

                intCount += 1;
                var response = elastic().Index(items, i => i
                              .Index(enumElasticIndexes.sportmanduleagues.ToString())
                              .Type(enumElasticIndexes.sportmanduleagues.ToString()));
                WriteToConsole(response, "Leagues >> " + intCount + ". " + items.Name + " Added");
            }
        }

        private static void PartnersGetAndSaveToElasticsearch()
        {
            Console.WriteLine("Fetching Partners To Save...");
            int intCount = 0;
            List<PartnerVMFORES> p = new List<PartnerVMFORES>();
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(myApiUrl + "/api/partnersFORES");
            var msg = stringTask.Result;
            p = JsonConvert.DeserializeObject<List<PartnerVMFORES>>(msg);
            foreach (var items in p)
            {
                var response = elastic().Index(items, i => i
                              .Index(enumElasticIndexes.sportsmandupartners.ToString())
                              .Type(enumElasticIndexes.sportsmandupartners.ToString()));
                WriteToConsole(response, "Partners >> " + intCount + ". " + items.Name + " Added");

            }
        }

        private static List<CategoryViewModelFORES> CategoriesGetAndSaveToElasticsearch()
        { 
            List<CategoryViewModelFORES> p = new List<CategoryViewModelFORES>();
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(myApiUrl + "/api/GetAllCategoriesFORES");
            var msg = stringTask.Result;
            p = JsonConvert.DeserializeObject<List<CategoryViewModelFORES>>(msg);
            return p;
        }
        
        private static void ClubsGetAndSaveToElasticsearch()
        {            
            Console.WriteLine("Fetching Teams To Save...");
            var stringTaska = client.GetStringAsync(myApiUrl + "/api/GetAllClubsFORES");
            var msga = stringTaska.Result;
            var clubs = JsonConvert.DeserializeObject<List<ClubViewModel>>(msga);
            int intCount = 0;
            foreach (var items in clubs)
            {
                //if(items.Id > 10080)
                {
                    intCount += 1;
                    client.DefaultRequestHeaders.Accept.Clear();                    
                    var stringTask = client.GetStringAsync(myApiUrl + "/api/GetAllClubsFORES/" + items.Id);
                    var msg = stringTask.Result;
                    var result = JsonConvert.DeserializeObject<List<ClubViewModelFORES>>(msg);
                    var response = elastic().Index(result.FirstOrDefault(), i => i
                                      .Index(enumElasticIndexes.sportsmanduclubs.ToString())
                                      .Type(enumElasticIndexes.sportsmanduclubs.ToString()));

                    WriteToConsole(response,"Teams >> " + intCount + ". " + items.Name + " Saved");
                }
            }
            //return p;
        }

        private static void SiteSectionLanguageTitleAndSaveToElasticsearch()
        {
            int intCount = 0;
            Console.WriteLine("Fetching Globalizations To Save...");
            List<SiteSectionLanguageTitleForEs> p = new List<SiteSectionLanguageTitleForEs>();
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(myApiUrl + "/api/GetAllSiteSectionLanguageTitleToSaveInEs");
            var msg = stringTask.Result;
            p = JsonConvert.DeserializeObject<List<SiteSectionLanguageTitleForEs>>(msg);
            foreach (var items in p)
            {
                items.Id = items.LanguageTitleName + "__" + items.SiteSectionName ;
                intCount += 1;
                var response = elastic().Index(items, i => i
                              .Index(enumElasticIndexes.sportsmandusitesectionlanguagetitle.ToString())
                              .Type(enumElasticIndexes.sportsmandusitesectionlanguagetitle.ToString()));
                foreach(var lang in items.LanguageTitleGlobalizations)
                {
                    WriteToConsole(response, "Globalizations >> " + intCount + ". " + items.LanguageTitleName + " > " + lang.Value + " ( "+ items.SiteSectionName +" ) " + " Saved");
                }                
            }
        }      
        public static ApplicationUser GetApplicationUserbyUserId(string userid, IMemoryCache _memoryCache = null)
        {
            ApplicationUser neededUser = new ApplicationUser();
            if (_memoryCache == null)
            {
                //##// Need to make this perfect on next Push !
                var userFromDb = elastic().Search<ApplicationUser>(s => s
                   .Index("users")
                   .Type("users")
                   .Query(q =>
                      q.MatchPhrase(m => m.Field(x => x.Id).Query(userid))
                   )
                );
                return userFromDb.Documents.FirstOrDefault();
            }
            if (!_memoryCache.TryGetValue(userid, out neededUser))
            {
                var userFromDb = elastic().Search<ApplicationUser>(s => s
                   .Index("users")
                   .Type("users")
                   .Query(q =>
                      q.MatchPhrase(m => m.Field(x => x.Id).Query(userid))
                   )
                );
                var returnUser = userFromDb.Documents;
                if (returnUser.Count > 0)
                {
                    neededUser = returnUser.SingleOrDefault();
                    _memoryCache.Set(userid, neededUser, new MemoryCacheEntryOptions()
                        .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
                    return neededUser;
                }
            }
            else
            {
                return neededUser;
            }
            return null;
        }      
        private static ElasticClient elastic()
        {
            //var node = new Uri("http://localhost:9200");
            var server = new Uri("https://7bb57ca9071244d3995c29781ee5a9a8.europe-west1.gcp.cloud.es.io:9243");
            var conn = new ConnectionSettings(
                 server
            );
            conn.BasicAuthentication("elastic", "DjngpWIIRWzrMczGPU1w0LVL");
            conn.EnableHttpCompression();
            var _elasticClient = new ElasticClient(conn);            
            return _elasticClient;
        }        
        static int intGenericCount = 0;
        public static IIndexResponse ElasticGenericAdd(enumElasticIndexes index,object stuff)
        {
            intGenericCount += 1;
            var response = elastic().Index(stuff, i => i
              .Index(index.ToString().ToLower())
              .Type(index.ToString().ToLower())); 
            return response;
        }
        public static T GenericGetById<T>(UniversalDataContainer UniversalDataContainer) where T : ITest
        {
            var searchResponse = elastic().Search<T>(s => s
                           .Index(UniversalDataContainer.Command.ToLower().ToString())
                           .From(0)
                           .Size(10000)
                           .Type(UniversalDataContainer.Command.ToLower().ToString())
                           .Query(q =>
                              q.MatchPhrase(k => k.Field(y => y.Id).Query(UniversalDataContainer.Id))
                           ));
            var list = searchResponse.Hits.Select(h => {
                return h.Source;
            }).ToList();
            return list.FirstOrDefault();
        }
        public static List<T> ElasticGenericGet<T>(UniversalDataContainer UniversalDataContainer) where T : ITest
        {
            //UniversalDataContainer.Command = UniversalDataContainer.Command.ToLower();// Needed it to be LowerCaase due to ElastsicSearch
            SearchDescriptor<T> debugQuery = new SearchDescriptor<T>();

            //if (UniversalDataContainer.AccountId == "ALL")
            {
                debugQuery.Index(UniversalDataContainer.Command.ToLower())
                    .From(0)
                    .Size(2000)
                    .Type(UniversalDataContainer.Command.ToLower());
            }
            //else
            //{
            //    debugQuery.Index(UniversalDataContainer.Command.ToLower())
            //        .Type(UniversalDataContainer.Command.ToLower())
            //        .Query(q =>
            //           q.Match(
            //               k => k.Field(y => y.AccountId).Query(UniversalDataContainer.AccountId)))
            //            .From(0)
            //            .Size(2000)
            //        ;
            //}
            string rawQueryText = string.Empty;
            //AFter migrating this aint working
            // using (MemoryStream mStream = new MemoryStream ()) {
            //     _elasticClient.Serializer.Serialize (debugQuery, mStream);
            //     rawQueryText = Encoding.ASCII.GetString (mStream.ToArray ());
            // }
            //WriteToFile (rawQueryText, "ElasticGenericGet" + UniversalDataContainer.Command, UniversalDataContainer.Command);
            var searchResponse = elastic().Search<T>(debugQuery);
            if (searchResponse.DebugInformation.ToUpper().Contains("INVALID"))
            {
                AddLog(new Log
                {
                    //AccountId = null,//getApplicationUsers().FirstOrDefault().Accounts.FirstOrDefault().Id,
                    Type = LOG_TYPES.CRITICAL_ERROR.ToString()
                });
            }
            var list = searchResponse.Hits.Select(h => {
                return h.Source;
            }).ToList();
            if (searchResponse.Documents != null && searchResponse.Documents.Count > 0)
            {
                return (List<T>)searchResponse.Documents.ToList();
            }
            return new List<T>();
        }
        public enum LOG_TYPES
        {
            CRITICAL_ERROR,To_Be_Verified, 

        }
        public static void AddLog(Log u)
        {
            try
            {
                var response = elastic().Index(u, i => i
                   .Index("logs")
                   .Type("logs"));
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            List<Log> l = new List<Log>();
            l.Add(u);
        }
        public class IpInfoDb
        {
            public string ipaddress { get; set; }
            public string countrycode { get; set; }
            public string countryname { get; set; }
            public string cityname { get; set; }
            public string regionname { get; set; }
            public string zipcode { get; set; }
            public string timezone { get; set; }
            public string latitude { get; set; }
            public string longitude { get; set; }
        }
        public class ServerEvent
        {
            public bool restart { get; set; }
            public string Id
            {
                get
                {
                    return "asdf";
                }
            }
        }
        public static void AddServerEvent(ServerEvent u)
        {
            try
            {
                var response = elastic().Index(u, i => i
                   .Index("serverevent")
                   .Type("serverevent"));
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                //Global.Live_Crash_Report(ex);
            }
        }
        public static bool GetServerEvent()
        {
            var searchResponse = elastic().Search<ServerEvent>(s => s
               .Index("serverevent")
               .Type("serverevent")
               .Query(q =>
                  q.MatchPhrase(k => k.Field(y => y.Id).Query("asdf"))
               )
            );
            var list = searchResponse.Hits.Select(h => {
                return h.Source;
            }).ToList();
            return list.SingleOrDefault().restart;
        }
        public static List<Log> GetLogs(string userId)
        {
            var searchResponse = elastic().Search<Log>(s => s
               .Index("logs")
               .Sort(k => k.Descending(x => x.Date))
               .From(0)
               .Size(8000)
               .Type("logs")
               .Query(q =>
                  q.MatchPhrase(k => k.Field(y => y.UserId).Query(userId))
               )
            );
            var list = searchResponse.Hits.Select(h => {
                return h.Source;
            }).ToList();
            return list;
        }        
        public static List<Log> GetLogs(string searchTerm, string onlyForType, string onlyType, string olyType)
        {
            var searchResponse = elastic().Search<Log>(s => s
               .Index("logs")
               .Sort(k => k.Descending(x => x.Date))
               .From(0)
               .Size(8000)
               .Type("logs")
               .Query(q =>
                  q.MatchPhrase(k => k.Field(y => y.Type).Query(searchTerm))
               )
            );
            var list = searchResponse.Hits.Select(h => {
                return h.Source;
            }).ToList();
            return list;
        }        
        public class Log : ITest
        {
            public Log()
            {
                this.Id = Guid.NewGuid().ToString();
                this.Date = DateTime.Now;
            }
            public string Id { get; set; }
            public dynamic Data { get; set; }
            public string Type { get; set; }
            public string UserId { get; set; }
            public DateTime Date { get; set; }
        }
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //_raygunClient.Send(e.ExceptionObject as Exception);
        }
    }
}