﻿using Discord;
using Discord.WebSocket;
using Microsoft.AspNetCore.SignalR.Client;
using Mindscape.Raygun4Net.AspNetCore;
using Sportsmandu.Models.Generic;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
namespace SportsDiscord
{
    public class DiscordProgram
    {
        private static DiscordSocketClient _client;
        private static readonly string _token = "NzkzMDQ4MTQxMDMzMjQyNjM0.X-mlog.DKqyi-JBQe8-bG85e7H8F93kJ1E";
        private static string currentRoomIdForVerificationPurpose = string.Empty;
        private static string WorkerGuid { get; set; }
        private static string Version = "0.001";
        static RaygunClient _raygunClient = new RaygunClient("vABPZc4KddKSDv1YE4NrQ");
        private static bool _disconnectedFromServer = false;
        private static string signalRbaseUrl = "";
        //private static string signalRbaseUrlInternal = "http://localhost:44393/";
        private static string signalRbaseUrlInternal = "http://40.88.142.249/";
        private static readonly HttpClient client = new HttpClient();
        private static HubConnection _connection;
        private static ConnectionState _connectionState = ConnectionState.Disconnected;
        private static HubConnection _Internalconnection;
        private static ConnectionState _internalConnectionState = ConnectionState.Disconnected;
        public static string[] Topics;
        private static string MyName;
        private static string _accessId;
        private enum ConnectionState
        {
            Connected,
            Disconnected,
            Faulted
        }
        static void Main(string[] args)
        {
            new DiscordProgram().MainAsync().GetAwaiter().GetResult();
            MyName = Environment.MachineName;
            Console.Write("My Name : " + MyName);
            signalRbaseUrlInternal = "https://localhost:44373/";//MichelCommon.MichelCommon.GetWebUrl();
            WriteLogs("Connecting To URL : " + signalRbaseUrlInternal);
            WriteLogs("Starting TaskForce!!!!" + DateTime.Now.Minute + ":" + DateTime.Now.Second);
            WorkerGuid = MyName;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Maina(args);
        }
        static void Maina(string[] args)
        {
            StartConnectionAsyncInternal();
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            while (true)
            {
            }
            //await Task.Delay(Timeout.Infinite);
        }
        public static async Task StartConnectionAsyncInternal()
        {
            Console.WriteLine("Console Init....");
            _Internalconnection = new HubConnectionBuilder()
            .WithUrl(signalRbaseUrlInternal + "sportsmanduhub", options =>
            {
                options.Headers["ClientMessage"] = "Console";
                options.Headers["GUID_TO_CONNECT"] = "GUID_TO_CONNECT";
            })
                .Build();
            //_Internalconnection.On<dynamic>("Reload", (ReceivedMessagea) =>
            //{
            //    Reload();
            //});
            _Internalconnection.On<UniversalDataContainer>("onUniversalGet", (UniversalDataContainer) =>
            {
                UniversalGet(UniversalDataContainer);
            });
            //_connection.On<dynamic, dynamic>("Reload", (ASD, dynamic) =>
            //{
            //    string AccessId = GetAccessId();
            //    win(AccessId);
            //});
            _Internalconnection.Closed += async (error) =>
            {
            aa:
                Console.WriteLine("Internal Client Disconnected !");
                _internalConnectionState = ConnectionState.Disconnected;
                await Task.Delay(new Random().Next(5, 10) * 1000);
                try
                {
                    ConnectInternal();
                }
                catch (Exception) { }
                if (_internalConnectionState != ConnectionState.Connected)
                    goto aa;
            };
        bb:
            ConnectInternal();
            if (_internalConnectionState != ConnectionState.Connected)
            {
                await Task.Delay(new Random().Next(10, 15) * 1000);
                goto bb;
            }
        }

        async static void ConnectInternal()
        {
            try
            {
                if (_internalConnectionState != ConnectionState.Connected)
                {
                    Console.WriteLine("Trying To Connect...." + DateTime.Now.Minute + ":" + DateTime.Now.Second + " : " + signalRbaseUrlInternal);
                    await _Internalconnection.StartAsync();
                    _internalConnectionState = ConnectionState.Connected;
                    await _Internalconnection.InvokeAsync<UniversalDataContainer>("IAmDiscord", $"GUID_TO_CONNECT||{WorkerGuid}||{Version}");
                    INITInternal();
                    Console.WriteLine("Connected Internal " + DateTime.Now.Minute + ":" + DateTime.Now.Second);
                }
            }
            catch (Exception ex)
            {
                _disconnectedFromServer = true;
                Console.WriteLine("Server Service Not Ready.... " + DateTime.Now.Minute + ":" + DateTime.Now.Second + " " + ex.Message);
            }
        }
        private static void INITInternal()
        {
            if (_disconnectedFromServer == true)
            {

            }
            _disconnectedFromServer = false;
        }
        public static void WriteLogs(string Log)
        {
            Console.WriteLine(Log);
            {
                var environment = AppDomain.CurrentDomain.BaseDirectory;
                string fileName = $"{environment}{"logs.txt"}";
                using (StreamWriter writer = File.AppendText(fileName))
                {
                    writer.WriteLine(Log);
                    if (!Log.Contains("*******") && Log.Trim() != "")
                    {
                        writer.WriteLine(Log + " : " + DateTime.Now);
                    }
                    else
                    {
                    }
                }
            }
        }
        //public static async Task ProcessReceivedMessage(string ReceivedMessagea)
        //{
        //    OnlyMessageType MessageType = JsonConvert.DeserializeObject<OnlyMessageType>((ReceivedMessagea));
        //    UniversalDataContainer UniversalDataContainer = new UniversalDataContainer
        //    {
        //        //AccountId = "ALL",
        //        Command = MessageType.MessageType,
        //        Param = "List",
        //        //Result = JsonConvert.SerializeObject(ReceivedMessage, Formatting.None)
        //        Result = ReceivedMessagea
        //    };
        //    _Internalconnection.InvokeAsync<UniversalDataContainer>("UniversalGet", UniversalDataContainer);                        
        //}       
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            _raygunClient.Send(e.ExceptionObject as Exception);
        }
        static async Task<string> PostURI(Uri u, HttpContent c)
        {
            var response = string.Empty;
            using (var client = new HttpClient())
            {
                HttpResponseMessage result = await client.PostAsync(u, c);
                if (result.IsSuccessStatusCode)
                {
                    response = result.Content.ReadAsStringAsync().Result;
                }
            }
            return response;
        }
        public async static Task UniversalGet(UniversalDataContainer UniversalDataContainer)
        {
            Console.WriteLine(UniversalDataContainer.Command + " Received !!");
            switch (Enum.Parse<enumCommandAutomationCommandResponseBack>(UniversalDataContainer.Command))
            {
                case enumCommandAutomationCommandResponseBack.ROOM_CREATED:                    ;
                    var chnl = _client.GetChannel(ulong.Parse(UniversalDataContainer.Param)) as IMessageChannel; // 4
                    await chnl.SendMessageAsync("The room starts when SLOT1 1st row and SLOT2 4th row are occupied");
                    await chnl.SendMessageAsync("The password is 1");
                    await chnl.SendMessageAsync("Here is the Room ID...");
                    await chnl.SendMessageAsync(UniversalDataContainer.SubParam);
                    break;
                case enumCommandAutomationCommandResponseBack.REQUEST_NOT_FULFILLED:
                    var chnl1 = _client.GetChannel(ulong.Parse(UniversalDataContainer.Param)) as IMessageChannel; // 4
                    await chnl1.SendMessageAsync(UniversalDataContainer.Command + ": " + UniversalDataContainer.Message);
                    break;
                case enumCommandAutomationCommandResponseBack.USER_VERIFIED:
                    var chnl2 = _client.GetChannel(ulong.Parse(UniversalDataContainer.Param)) as IMessageChannel; // 4
                    await chnl2.SendMessageAsync(UniversalDataContainer.Message);
                    break;
                default:
                    break;
            }
        }
        public DiscordProgram()
        {
            // It is recommended to Dispose of a client when you are finished
            // using it, at the end of your app's lifetime.
            _client = new DiscordSocketClient();
            _client.Log += LogAsync;
            _client.Ready += ReadyAsync;
            _client.MessageReceived += MessageReceivedAsync;            
        }
        private Task LogAsync(LogMessage log)
        {
            Console.WriteLine(log.ToString());
            return Task.CompletedTask;
        }
        public async Task MainAsync()
        {
            // Tokens should be considered secret data, and never hard-coded.
            await _client.LoginAsync(TokenType.Bot, _token);
            await _client.StartAsync();

            // Block the program until it is closed.
            //await Task.Delay(Timeout.Infinite);
        }
        // It is recommended to Dispose of a client when you are finished
        // using it, at the end of your app's lifetime.
        private Task ReadyAsync()
        {
            Console.WriteLine($"{_client.CurrentUser} is connedis" +
                $"cted!");
            return Task.CompletedTask;
        }
        public enum enumMessageContent
        {
            PING,
            ROOM,
            VERIFY_USER,
        }
        private async Task MessageReceivedAsync(SocketMessage message)
        {
            // The bot should never respond to itself.            
            if (message.Author.Id == _client.CurrentUser.Id)
                return;


            if (_Internalconnection.State != HubConnectionState.Connected)
            {
                await message.Channel.SendMessageAsync("Opps No Supervisor Connected to handle this request...PLease Try Again Later");
                return;
            }
            if (message.Content == $"!{enumMessageContent.PING.ToString()}")
                await message.Channel.SendMessageAsync("pong!");
            else if (message.Content == $"!{enumMessageContent.ROOM.ToString()}")
            {
                //await message.Channel.SendMessageAsync("Cool will let you know details in a while...The bot might be busy...let me check that for you..please wait...BRB.");
                UniversalDataContainer UniversalDataContainer = new UniversalDataContainer
                {
                    Command = enumCommandAutomationCommand.CREATE_ROOM.ToString(),
                    Param = message.Channel.Id.ToString(),
                    Result = message.Channel.Id,
                };
                _Internalconnection.InvokeAsync<UniversalDataContainer>("UniversalGet", UniversalDataContainer);
            }
            else if (message.Content.Contains($"!VERIFICATION_ROOM_ID"))
            {
                currentRoomIdForVerificationPurpose = message.Content.Trim().Replace($"!VERIFICATION_ROOM_ID", "");
            }
            else if(message.Content.Contains($"!{enumMessageContent.VERIFY_USER.ToString()}"))
            {
                if(currentRoomIdForVerificationPurpose == string.Empty)
                {
                    await message.Channel.SendMessageAsync("Oppss there are no Verification Workers at the moment, Please try again later");
                }
                else
                {                    
                    if(message.Content.Replace($"!{enumMessageContent.VERIFY_USER.ToString()}", "").Trim().Split(" ").Length > 1)
                    { 
                        UniversalDataContainer UniversalDataContainera = new UniversalDataContainer
                        {
                            Command = enumCommandAutomationCommand.VERIFY_USER.ToString(),
                            Param = message.Channel.Id.ToString(),
                            SubParam = message.Content.Replace($"!{enumMessageContent.VERIFY_USER.ToString()}", "")
                        };
                        _Internalconnection.InvokeAsync<UniversalDataContainer>("UniversalGet", UniversalDataContainera);
                    }
                    else
                    {
                        await message.Channel.SendMessageAsync("Great ! Happy that you want to see this stuff grow :)\n\nPLease Join RoomID that will be sent at end of the message. The password is 1");
                        string currentRoomId = currentRoomIdForVerificationPurpose;
                        await message.Channel.SendMessageAsync(currentRoomId);
                        await message.Channel.SendMessageAsync("When you get inside the room there are few things to be noted \n\n1. You can only be in Slot 1 , Slot 2, Slot 3,Slot 4 , Slot5, Slot 6\n 2. Response back your slot placement in the format \n\n !VERIFY_USER SLOT_SEATED_IN PUBG_USER_ID \n\n Where SLOT_SEATED = The Slot you are in i.e slot1 slot2\nPUBG_USER_ID = Your Ingame ID");
                    }
                }
            }
            else 
            {
                await message.Channel.SendMessageAsync($"Command Not Reconized !! the only available commands are \n1. !{enumMessageContent.PING} \n2. !{enumMessageContent.ROOM} \n3. !{enumMessageContent.VERIFY_USER}");
            }                        
        }
    }
}