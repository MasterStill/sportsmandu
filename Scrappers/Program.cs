﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Sportsmandu.Model.Entities.Player;
using Sportsmandu.Model.Entities.ViewModels;
using Sportsmandu.Models;
using Sportsmandu.Models.enums;
using Sportsmandu.Models.Generic;
using Sportsmandu.Models.League;
namespace Scrappers
{
    class Program
    {
        private static string signalRbaseUrl = "https://localhost:44373/";
        private static readonly HttpClient client = new HttpClient();
        private static HubConnection _connection;
        private static ConnectionState _connectionState = ConnectionState.Disconnected;
        private enum ConnectionState
        {
            Connected,
            Disconnected,
            Faulted
        }
        public static void INIT()
        {
            //GetLeague(4328);
            GetTeam(4328);
        }
        static void Main(string[] args)
        {
            client.Timeout = TimeSpan.FromMinutes(5);
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            StartConnectionAsync();
            _connection.On<UniversalDataContainer>("onUniversalGet", (incomingMessage) =>
            {
                Program p = new Program();
                Console.WriteLine(incomingMessage.Command  +  " : " +  incomingMessage.Data);
            });
            while (true)
            {
            }
        }
        public static async Task StartConnectionAsync()
        {
            Console.WriteLine("Console Init....");
            _connection = new HubConnectionBuilder()
                .WithUrl(signalRbaseUrl + "sportsmanduhub", options =>
                {
                    options.Headers["ClientMessage"] = "Scrapper";
                })
                .Build();
            _connection.Closed += async (error) =>
            {
                aa:
                Console.WriteLine("Client Disconnected !");
                _connectionState = ConnectionState.Disconnected;
                await Task.Delay(new Random().Next(30, 60) * 1000);
                try
                {
                    Connect();
                }
                catch (Exception) { }
                if (_connectionState != ConnectionState.Connected)
                    goto aa;
            };
            bb:
            Connect();
            if (_connectionState != ConnectionState.Connected)
            {
                await Task.Delay(new Random().Next(30, 60) * 1000);
                goto bb;
            }
            //UniversalDataContainer UniversalDataContainer = new UniversalDataContainer
            //{
            //    AccountId = "ALL",
            //    Command = enumCommand.SeasonFixture.ToString(),
            //    Param = enumParam.UPDATE.ToString(),
            //    SubParam = "1"
            //};
            //UniversalDataContainer UniversalDataContainer1 = new UniversalDataContainer
            //{
            //    AccountId = "ALL",
            //    Command = enumCommand.SeasonMatch.ToString(),
            //    Param = enumParam.LIST.ToString()
            //};            
            //await _connection.InvokeAsync<UniversalDataContainer>("UniversalGet", UniversalDataContainer);
        }
        async static void Connect()
        {
            try
            {
                if (_connectionState != ConnectionState.Connected)
                {
                    Console.WriteLine("Trying To Connect...." + DateTime.Now.Minute + ":" + DateTime.Now.Second + " : " + signalRbaseUrl);
                    await _connection.StartAsync();
                    _connectionState = ConnectionState.Connected;
                    await _connection.InvokeAsync<UniversalDataContainer>("IAmScrapper");
                    INIT();
                    Console.WriteLine("Connected !!!!" + DateTime.Now.Minute + ":" + DateTime.Now.Second);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Server Service Not Ready.... " + DateTime.Now.Minute + ":" + DateTime.Now.Second);
            }
        }
        private static int GetIdFromXID(enumClaimTypeSegments segments,int XID)
        {
            string ApiUrl = "http://localhost:5000/Api/GetDbIdFromXId/" + segments.ToString() + "/" + XID;
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(ApiUrl);
            var msg = stringTask.Result;
            return Int32.Parse(msg);
        }
        private static int GetCountryId(string Name)
        {
            //return 0;
            string ApiUrl = "http://localhost:5000/Api/GetCountryId/" + Name;
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(ApiUrl);
            var msg = stringTask.Result;
            return Int32.Parse(msg);
        }
        private static int GetCategoryId(string Name)
        {
            string ApiUrl = "http://localhost:5000/Api/GetCategoryId/" + Name;
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(ApiUrl);
            var msg = stringTask.Result;
            return Int32.Parse(msg);
        }
        private static async void GetLeague(int leagueid)
        {
            string ApiUrl = "https://www.thesportsdb.com/api/v1/json/1/lookupleague.php?id=" + leagueid;
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(ApiUrl);
            try
            {
                var msg = stringTask.Result;
                dynamic dyn = JsonConvert.DeserializeObject<ExpandoObject>(msg);
                dynamic Datas = dyn.leagues[0];
                int idLeague = Int32.Parse(Datas.idLeague);
                string idSoccerXML = Datas.idSoccerXML;
                string strSport = Datas.strSport;
                string strLeague = Datas.strLeague;
                string strLeagueAlternate = Datas.strLeagueAlternate;
                string strDivision = Datas.strDivision;
                string idCup = Datas.idCup;
                string intFormedYear = Datas.intFormedYear;
                string dateFirstEvent = Datas.dateFirstEvent;
                string strGender = Datas.strGender;
                string strCountry = Datas.strCountry;
                string strWebsite = Datas.strWebsite;
                string strFacebook = Datas.strFacebook;
                string strTwitter = Datas.strTwitter;
                string strYoutube = Datas.strYoutube;
                string strRSS = Datas.strRSS;
                string strDescriptionEN = Datas.strDescriptionEN;
                string strDescriptionDE = Datas.strDescriptionDE;
                string strDescriptionFR = Datas.strDescriptionFR;
                string strDescriptionIT = Datas.strDescriptionIT;
                string strDescriptionCN = Datas.strDescriptionCN;
                string strDescriptionJP = Datas.strDescriptionJP;
                string strDescriptionRU = Datas.strDescriptionRU;
                string strDescriptionES = Datas.strDescriptionES;
                string strDescriptionPT = Datas.strDescriptionPT;
                string strDescriptionSE = Datas.strDescriptionSE;
                string strDescriptionNL = Datas.strDescriptionNL;
                string strDescriptionHU = Datas.strDescriptionHU;
                string strDescriptionNO = Datas.strDescriptionNO;
                string strDescriptionPL = Datas.strDescriptionPL;
                string strDescriptionIL = Datas.strDescriptionIL;
                string strFanart1 = Datas.strFanart1;
                string strFanart2 = Datas.strFanart2;
                string strFanart3 = Datas.strFanart3;
                string strFanart4 = Datas.strFanart4;
                string strBanner = Datas.strBanner;
                string strBadge = Datas.strBadge;
                string strLogo = Datas.strLogo;
                string strPoster = Datas.strPoster;
                string strTrophy = Datas.strTrophy;
                string strNaming = Datas.strNaming;
                string strComplete = Datas.strComplete;
                string strLocked = Datas.strLocked;
                League L = new League
                {
                    //TODO Category
                    CountryId = GetCountryId(strCountry),
                    CategoryId = GetCategoryId(strSport),
                    Name = strLeague,
                    CreatedById = 1,
                    CultureId = 1,
                    CreatedDate = DateTime.Now,
                    XId = idLeague,
                    Description = strDescriptionEN,
                    Thumbnail = strBadge,
                    CoverImage = strFanart1,
                    Image = strBadge,
                    Website = strWebsite,
                    LeagueSocial = new List<LeagueSocial>
                    {
                        new LeagueSocial
                        {
                            Social = enumSocial.Facebook,
                            Link = strFacebook
                        },
                        new LeagueSocial
                        {
                            Social = enumSocial.Youtube,
                            Link = strYoutube
                        },
                        new LeagueSocial
                        {
                            Social = enumSocial.Website,
                            Link = strWebsite
                        },
                        new LeagueSocial
                        {
                            Social = enumSocial.RSS,
                            Link = strRSS
                        }
                    },
                };
                UniversalDataContainer UniversalDataContainer1 = new UniversalDataContainer
                {
                    Command = enumCommand.Leagues.ToString(),
                    Param = enumParam.ADD.ToString(),
                    Data = L
                };
                await _connection.InvokeAsync<UniversalDataContainer>("UniversalGet", UniversalDataContainer1);
            }
            catch (Exception ex)
            {
            }
        }
        private static async void GetTeam(int leagueid)
        {
            string ApiUrl = "https://www.thesportsdb.com/api/v1/json/1/lookup_all_teams.php?id=" + leagueid;
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(ApiUrl);
            try
            {
                var msg = stringTask.Result;
                dynamic dyn = JsonConvert.DeserializeObject<ExpandoObject>(msg);
                dynamic Datas = dyn.teams;
                foreach (var items in Datas)
                {
                    int idTeam = Int32.Parse(items.idTeam);
                    string idSoccerXML = items.idSoccerXML;
                    string intLoved = items.intLoved;
                    string strTeam = items.strTeam;
                    string strTeamShort = items.strTeamShort;
                    string strAlternate = items.strAlternate;
                    string intFormedYear = items.intFormedYear;
                    string strSport = items.strSport;
                    string strLeague = items.strLeague;
                    string idLeague = items.idLeague;
                    string strDivision = items.strDivision;
                    string strManager = items.strManager;
                    string strStadium = items.strStadium;
                    string strKeywords = items.strKeywords;
                    string strRSS = items.strRSS;
                    string strStadiumThumb = items.strStadiumThumb;
                    string strStadiumDescription = items.strStadiumDescription;
                    string strStadiumLocation = items.strStadiumLocation;
                    string intStadiumCapacity = items.intStadiumCapacity;
                    string strWebsite = items.strWebsite;
                    string strFacebook = items.strFacebook;
                    string strTwitter = items.strTwitter;
                    string strInstagram = items.strInstagram;
                    string strDescriptionEN = items.strDescriptionEN;
                    string strDescriptionDE = items.strDescriptionDE;
                    string strDescriptionFR = items.strDescriptionFR;
                    string strDescriptionCN = items.strDescriptionCN;
                    string strDescriptionIT = items.strDescriptionIT;
                    string strDescriptionJP = items.strDescriptionJP;
                    string strDescriptionRU = items.strDescriptionRU;
                    string strDescriptionES = items.strDescriptionES;
                    string strDescriptionPT = items.strDescriptionPT;
                    string strDescriptionSE = items.strDescriptionSE;
                    string strDescriptionNL = items.strDescriptionNL;
                    string strDescriptionHU = items.strDescriptionHU;
                    string strDescriptionNO = items.strDescriptionNO;
                    string strDescriptionIL = items.strDescriptionIL;
                    string strDescriptionPL = items.strDescriptionPL;
                    string strGender = items.strGender;
                    string strCountry = items.strCountry;
                    string strTeamBadge = items.strTeamBadge;
                    string strTeamJersey = items.strTeamJersey;
                    string strTeamLogo = items.strTeamLogo;
                    string strTeamFanart1 = items.strTeamFanart1;
                    string strTeamFanart2 = items.strTeamFanart2;
                    string strTeamFanart3 = items.strTeamFanart3;
                    string strTeamFanart4 = items.strTeamFanart4;
                    string strTeamBanner = items.strTeamBanner;
                    string strYoutube = items.strYoutube;
                    string strLocked = items.strLocked;
                    Club c = new Club
                    {
                        XId = idTeam,
                        Name = strTeam,
                        ShortName = strTeamShort,
                        Alias = strAlternate,
                        CountryId = GetCountryId(strCountry),
                        CreatedById = 1,
                        CreatedDate = DateTime.Now,
                        CultureId = 1,
                        Website = strWebsite,
                        EstablishedDate = intFormedYear,
                        Founded = Int32.Parse(intFormedYear),
                        Thumbnail = strTeamBadge,
                        CoverImage = strTeamFanart1,
                        Image = strTeamBadge,
                        NickName = strAlternate,
                        Description =  strDescriptionEN,
                        ClubSocial = new List<ClubsSocial>
                        {
                            new ClubsSocial
                            {
                                Social = enumSocial.Facebook,
                                Link = strFacebook
                            },
                            new ClubsSocial
                            {
                                Social = enumSocial.Youtube,
                                Link = strYoutube
                            },
                            new ClubsSocial
                            {
                                Social = enumSocial.Website,
                                Link = strWebsite
                            },
                            new ClubsSocial
                            {
                                Social = enumSocial.RSS,
                                Link = strRSS
                            },
                            new ClubsSocial
                            {
                                Social =   enumSocial.Twitter,
                                Link = strTwitter
                            },
                            new ClubsSocial
                            {
                                Social =  enumSocial.Instagram,
                                Link = strInstagram
                            }
                        }
                    };
                    UniversalDataContainer UniversalDataContainer1 = new UniversalDataContainer
                    {
                        Command = enumCommand.Clubs.ToString(),
                        Param = enumParam.ADD.ToString(),
                        Data = c
                    };
                    _connection.InvokeAsync<UniversalDataContainer>("UniversalGet", UniversalDataContainer1);
                    GetPlayer(idTeam);
                }
            }
            catch (Exception ex)
            {
            }
        }
        private static async void GetPlayer(int TeamId)
        {
            string ApiUrl = "https://www.thesportsdb.com/api/v1/json/1/lookup_all_players.php?id=" + TeamId;
            client.DefaultRequestHeaders.Accept.Clear();
            var stringTask = client.GetStringAsync(ApiUrl);
            try
            {
                var msg = stringTask.Result;
                dynamic dyn = JsonConvert.DeserializeObject<ExpandoObject>(msg);
                dynamic Datas = dyn.player;
                int currentClubDbId = GetIdFromXID(enumClaimTypeSegments.Clubs, TeamId);
                foreach (var items in Datas)
                {
                    try
                    {
                        int idPlayer = Int32.Parse(items.idPlayer);
                        int idTeam = Int32.Parse(items.idTeam);
                        string idTeamNational = items.idTeamNational;
                        string idSoccerXML = items.idSoccerXML;
                        string idPlayerManager = items.idPlayerManager;
                        string strNationality = items.strNationality;
                        string strPlayer = items.strPlayer;
                        string strTeam = items.strTeam;
                        string strTeamNational = items.strTeamNational;
                        string strSport = items.strSport;
                        string intSoccerXMLTeamID = items.intSoccerXMLTeamID;
                        string dateBorn = items.dateBorn;
                        string strNumber = items.strNumber;
                        string dateSigned = items.dateSigned;
                        string strSigning = items.strSigning;
                        string strWage = items.strWage;
                        string strOutfitter = items.strOutfitter;
                        string strKit = items.strKit;
                        string strAgent = items.strAgent;
                        string strBirthLocation = items.strBirthLocation;
                        string strDescriptionEN = items.strDescriptionEN;
                        string strDescriptionDE = items.strDescriptionDE;
                        string strDescriptionFR = items.strDescriptionFR;
                        string strDescriptionCN = items.strDescriptionCN;
                        string strDescriptionIT = items.strDescriptionIT;
                        string strDescriptionJP = items.strDescriptionJP;
                        string strDescriptionRU = items.strDescriptionRU;
                        string strDescriptionES = items.strDescriptionES;
                        string strDescriptionPT = items.strDescriptionPT;
                        string strDescriptionSE = items.strDescriptionSE;
                        string strDescriptionNL = items.strDescriptionNL;
                        string strDescriptionHU = items.strDescriptionHU;
                        string strDescriptionNO = items.strDescriptionNO;
                        string strDescriptionIL = items.strDescriptionIL;
                        string strDescriptionPL = items.strDescriptionPL;
                        string strGender = items.strGender;
                        string strSide = items.strSide;
                        string strPosition = items.strPosition;
                        string strCollege = items.strCollege;
                        string strFacebook = items.strFacebook;
                        string strWebsite = items.strWebsite;
                        string strTwitter = items.strTwitter;
                        string strInstagram = items.strInstagram;
                        string strYoutube = items.strYoutube;
                        string strHeight = items.strHeight;
                        string strWeight = items.strWeight;
                        string intLoved = items.intLoved;
                        string strThumb = items.strThumb;
                        string strCutout = items.strCutout;
                        string strRender = items.strRender;
                        string strBanner = items.strBanner;
                        string strFanart1 = items.strFanart1;
                        string strFanart2 = items.strFanart2;
                        string strFanart3 = items.strFanart3;
                        string strFanart4 = items.strFanart4;
                        string strCreativeCommons = items.strCreativeCommons;
                        string strLocked = items.strLocked;
                        try
                        {
                            var role = Enum.Parse<enumPlayerRole>(strPosition.Replace(" ", "_"));
                            Player p = new Player
                            {
                                Name = strPlayer,
                                DOB = dateBorn,
                                CultureId = 1,
                                CountryId = GetCountryId(strNationality),
                                CategoryId = GetCategoryId(strSport),
                                CreatedById = 1,
                                Gender = Enum.Parse<enumGender>(strGender),
                                Height = strHeight,
                                Weight = strWeight,
                                Thumbnail = strCutout,
                                Image = strRender,
                                CoverImage = strFanart1,
                                PlayerRole = role,
                                XId = idPlayer,
                                Description = strDescriptionEN,
                                Social = new List<PlayerSocial>
                                {
                                    new PlayerSocial
                                    {
                                        Social = enumSocial.Facebook,
                                        Link = strFacebook
                                    },
                                    new PlayerSocial
                                    {
                                        Social = enumSocial.Twitter,
                                        Link = strTwitter
                                    },
                                    new PlayerSocial
                                    {
                                        Social = enumSocial.Youtube,
                                        Link = strYoutube
                                    }
                                }
                            };
                            UniversalDataContainer UniversalDataContainer1 = new UniversalDataContainer
                            {
                                Command = enumCommand.Players.ToString(),
                                Param = enumParam.ADD.ToString(),
                                Data = p
                            };
                            _connection.InvokeAsync<UniversalDataContainer>("UniversalGet", UniversalDataContainer1);
                            Task.Delay(500).Wait();
                            ClubPlayers cp = new ClubPlayers
                            {
                                ClubId = currentClubDbId,
                                PlayerId = GetIdFromXID(enumClaimTypeSegments.Players, p.XId),
                                PlayerRole = p.PlayerRole

                            };
                            if (cp.ClubId != 0 && cp.PlayerId != 0)
                            {
                                UniversalDataContainer UniversalDataContainer = new UniversalDataContainer
                                {
                                    Command = enumCommand.ClubPlayers.ToString(),
                                    Param = enumParam.ADD.ToString(),
                                    Data = cp
                                };
                                _connection.InvokeAsync<UniversalDataContainer>("UniversalGet", UniversalDataContainer);
                            }
                        }
                        catch (Exception ex)
                        {
                            var role = Enum.Parse<enumPlayerRole>(strPosition.Replace(" ", "_"));
                            Employee p = new Employee
                            {
                                Name = strPlayer,
                                CultureId = 1,
                                CountryId = GetCountryId(strNationality),
                                CreatedById = 1,
                                Gender = Enum.Parse<enumGender>(strGender),
                                Thumbnail = strCutout,
                                Image = strRender,
                                CoverImage = strFanart1,
                                XId = idPlayer,
                                Description = strDescriptionEN,
                            };
                            //ClubEmployee cp = new ClubEmployee
                            //{
                            //    ClubId = currentClubDbId,
                            //    Employee = GetIdFromXID(enumClaimTypeSegments.Players, p.XId),
                            //    PlayerRole = p.PlayerRole
                            //};
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex){
            }
        }
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //_raygunClient.Send(e.ExceptionObject as Exception);
        }
    }
}